CREATE DATABASE  IF NOT EXISTS `bookingsystem` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bookingsystem`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bookingtour1
-- ------------------------------------------------------
-- Server version	8.0.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `blogID` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int NOT NULL,
  PRIMARY KEY (`blogID`),
  KEY `userID` (`userID`),
  CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogcomment`
--

DROP TABLE IF EXISTS `blogcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogcomment` (
  `commentID` int NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parentCommentID` int DEFAULT NULL,
  `blogID` int NOT NULL,
  `userID` int NOT NULL,
  PRIMARY KEY (`commentID`),
  KEY `blogID` (`blogID`),
  KEY `userID` (`userID`),
  KEY `parentCommentID` (`parentCommentID`),
  CONSTRAINT `blogComment_ibfk_1` FOREIGN KEY (`blogID`) REFERENCES `blog` (`blogID`),
  CONSTRAINT `blogComment_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  CONSTRAINT `blogComment_ibfk_3` FOREIGN KEY (`parentCommentID`) REFERENCES `blogcomment` (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogcomment`
--

LOCK TABLES `blogcomment` WRITE;
/*!40000 ALTER TABLE `blogcomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogfavorite`
--

DROP TABLE IF EXISTS `blogfavorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogfavorite` (
  `favoriteID` int NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `blogID` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  PRIMARY KEY (`favoriteID`),
  KEY `blogID` (`blogID`),
  KEY `userID` (`userID`),
  CONSTRAINT `blogfavorite_ibfk_1` FOREIGN KEY (`blogID`) REFERENCES `blog` (`blogID`),
  CONSTRAINT `blogfavorite_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogfavorite`
--

LOCK TABLES `blogfavorite` WRITE;
/*!40000 ALTER TABLE `blogfavorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogfavorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bloglike`
--

DROP TABLE IF EXISTS `bloglike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bloglike` (
  `likeID` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blogID` int NOT NULL,
  `userID` int NOT NULL,
  PRIMARY KEY (`likeID`),
  KEY `blogID` (`blogID`),
  KEY `userID` (`userID`),
  CONSTRAINT `blogLike_ibfk_1` FOREIGN KEY (`blogID`) REFERENCES `blog` (`blogID`),
  CONSTRAINT `blogLike_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bloglike`
--

LOCK TABLES `bloglike` WRITE;
/*!40000 ALTER TABLE `bloglike` DISABLE KEYS */;
/*!40000 ALTER TABLE `bloglike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogpicture`
--

DROP TABLE IF EXISTS `blogpicture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogpicture` (
  `pictureID` int NOT NULL AUTO_INCREMENT,
  `pictureURL` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blogID` int NOT NULL,
  PRIMARY KEY (`pictureID`),
  KEY `blogID` (`blogID`),
  CONSTRAINT `blogPicture_ibfk_1` FOREIGN KEY (`blogID`) REFERENCES `blog` (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogpicture`
--

LOCK TABLES `blogpicture` WRITE;
/*!40000 ALTER TABLE `blogpicture` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogpicture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_codes`
--

DROP TABLE IF EXISTS `booking_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booking_codes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bookingID` int NOT NULL,
  `tourCode` char(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bookingID` (`bookingID`),
  CONSTRAINT `booking_codes_ibfk_1` FOREIGN KEY (`bookingID`) REFERENCES `bookingdetails` (`bookingID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_codes`
--

LOCK TABLES `booking_codes` WRITE;
/*!40000 ALTER TABLE `booking_codes` DISABLE KEYS */;
INSERT INTO `booking_codes` VALUES (17,123,'0RPEXUAUAO'),(18,124,'KK3P9XUXH3'),(19,124,'XKR98O6ZTZ'),(20,124,'KYQN92VQ3M'),(21,124,'C36SAHAYB3'),(22,142,'5LYY7SBU37'),(23,144,'FK1ETGKZPM');
/*!40000 ALTER TABLE `booking_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookingdetails`
--

DROP TABLE IF EXISTS `bookingdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookingdetails` (
  `bookingID` int NOT NULL AUTO_INCREMENT,
  `finalPrice` double NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` int NOT NULL,
  `customerAddress` varchar(255) NOT NULL,
  `discountID` int DEFAULT NULL,
  `tourID` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  `ticketID` int DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `bookingDate` date DEFAULT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `paymentMethod` varchar(255) DEFAULT NULL,
  `totalPeople` int DEFAULT NULL,
  PRIMARY KEY (`bookingID`),
  KEY `discountID` (`discountID`),
  KEY `tourID` (`tourID`),
  KEY `userID` (`userID`),
  KEY `ticketID` (`ticketID`),
  CONSTRAINT `bookingdetails_ibfk_1` FOREIGN KEY (`discountID`) REFERENCES `discount` (`discountID`),
  CONSTRAINT `bookingdetails_ibfk_2` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`),
  CONSTRAINT `bookingdetails_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  CONSTRAINT `bookingdetails_ibfk_4` FOREIGN KEY (`ticketID`) REFERENCES `ticket` (`ticketID`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookingdetails`
--

LOCK TABLES `bookingdetails` WRITE;
/*!40000 ALTER TABLE `bookingdetails` DISABLE KEYS */;
INSERT INTO `bookingdetails` VALUES (123,3000000,'oke','tientvhs176262@fpt.edu.vn',339258608,'to 5 phuong tan thinh',NULL,14,NULL,NULL,0,'2024-06-27','tran viet tien',NULL,40),(124,2000000,'ok','trantientn03@gmail.com',339258608,'to 5 phuong tan thinh',NULL,14,NULL,NULL,1,'2024-01-05','tran viet tien',NULL,100),(125,6000000,'','tientran2003@gmail.com',339258608,'to 5 phuong tan thinh',NULL,14,NULL,NULL,0,'2024-02-01','tran viet tien',NULL,200),(126,10000000,'','tientran2003@gmail.com',339258608,'to 5 phuong tan thinh',NULL,14,NULL,NULL,0,'2024-03-21','tran viet tien',NULL,1),(127,3000000,'','tientran2003@gmail.com',339258608,'to 5 phuong tan thinh',NULL,14,NULL,NULL,0,'2024-04-28','tran viet tien',NULL,1),(128,700000,'nice','user1@example.com',123456789,'123 Main St',NULL,14,31,NULL,0,'2024-05-29','John Doe',NULL,2),(131,6000000,'good','user4@example.com',123456792,'321 Pine St',NULL,14,31,NULL,0,'2022-07-03','Lucy Brown',NULL,4),(133,2000000,'excellent','user6@example.com',123456794,'987 Birch St',NULL,14,33,NULL,0,'2021-07-05','Emily Davis',NULL,2),(134,1000000,'superb','user7@example.com',123456795,'543 Maple St',NULL,17,31,NULL,0,'2023-07-06','Chris Lee',NULL,1),(136,900000,'marvelous','user9@example.com',123456797,'219 Walnut St',NULL,15,33,NULL,0,'2024-07-08','David Green',NULL,20),(137,250000,'pleasant','user10@example.com',123456798,'432 Chestnut St',NULL,15,31,NULL,0,'2024-07-09','Laura White',NULL,100),(138,1000000,'pleasant','user11@example.com',123456798,'432 Chestnut St',NULL,21,NULL,NULL,0,'2024-07-09','minh',NULL,20),(139,10000000,'pleasant','user12@example.com',123456798,'432 Chestnut St',NULL,22,NULL,NULL,0,'2024-07-09','David Green',NULL,50),(140,50000000,'pleasant','user13@example.com',123456798,'432 Chestnut St',NULL,23,NULL,NULL,0,'2024-07-09','David Green',NULL,10),(141,25000000,'pleasant','user14@example.com',123456798,'432 Chestnut St',NULL,24,NULL,NULL,0,'2024-07-09','David Green',NULL,5),(142,4000000,'Cứu','minhpdhe140049@fpt.edu.com',393544314,'Hòa lạc',NULL,14,31,NULL,1,'2024-07-15','minh',NULL,2),(143,4000000,NULL,'minhpdhe140049@fpt.edu.com',393544314,'Hòa lạc',NULL,14,31,NULL,1,'2024-07-15','minh','vnpay',2),(144,10000000,'','minhpdhe140049@fpt.edu.vn',393544314,'ha noi',NULL,14,31,NULL,1,'2024-07-15','minh',NULL,5),(145,2000000,'','minhpdhe140049@fpt.edu.com',393544314,'Hòa lạc',NULL,14,31,NULL,0,'2024-07-15','minh',NULL,1),(146,2000000,NULL,'minhpdhe140049@fpt.edu.com',393544314,'Hòa lạc',NULL,14,31,NULL,1,'2024-07-15','minh','vnpay',1),(147,-1,'','minhbkqn@gmail.com',393544314,'dafad',NULL,16,NULL,NULL,0,'2024-07-18','minh test giai quyet',NULL,1),(148,10000000,'','minhbkqn@gmail.com',393544314,'sn 179 thôn trại mới a ',NULL,28,NULL,NULL,0,'2024-07-18','minh',NULL,1);
/*!40000 ALTER TABLE `bookingdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cancelledbookings`
--

DROP TABLE IF EXISTS `cancelledbookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cancelledbookings` (
  `cancelID` int NOT NULL AUTO_INCREMENT,
  `bookingID` int NOT NULL,
  `cancelDate` date NOT NULL,
  `cancelReason` varchar(255) DEFAULT NULL,
  `penaltyPercentage` double NOT NULL,
  `refundAmount` double NOT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`cancelID`),
  KEY `bookingID` (`bookingID`),
  CONSTRAINT `cancelledbookings_ibfk_1` FOREIGN KEY (`bookingID`) REFERENCES `bookingdetails` (`bookingID`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cancelledbookings`
--

LOCK TABLES `cancelledbookings` WRITE;
/*!40000 ALTER TABLE `cancelledbookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `cancelledbookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactrequests`
--

DROP TABLE IF EXISTS `contactrequests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contactrequests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `tourID` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  `isNew` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactrequests`
--

LOCK TABLES `contactrequests` WRITE;
/*!40000 ALTER TABLE `contactrequests` DISABLE KEYS */;
INSERT INTO `contactrequests` VALUES (1,'Tran Viet Tien','trantientn03@gmail.com','3403040343','okk','okk',1,'2024-06-08 17:34:44',0,1),(2,'Tran Viet Tien','trantientn03@gmail.com','0339258608','dep tuyet voi','dep tuyet voi',2,'2024-06-10 06:19:38',0,1),(3,'tien','tientran2003@gmail.com','0339258608','dep','dep',14,'2024-06-11 15:22:36',1,0),(4,'tien','tientran2003@gmail.com','0339258608','dep','dep',14,'2024-06-11 15:27:18',1,0),(5,'tien','tientran2003@gmail.com','0339258608','rat dep','rat dep',14,'2024-06-11 16:23:14',1,0),(6,'tien','trantien03@gmail.com','0339258608','tot lam tour du lich rat dep','trantien03@gmail.com',14,'2024-06-11 18:14:47',1,0),(7,'tien','trantientn03@gmail.com','0339258608','tot lam tour du lich rat dep','trantientn03@gmail.com',14,'2024-06-11 19:08:22',1,0),(8,'tran viet tien','user9@gmail.com','0339258608','ok','user9@gmail.com',14,'2024-06-12 04:04:10',1,0),(9,'tran viet tien','trantientn03@gmail.com','033925860','dep','trantientn03@gmail.com',14,'2024-06-12 07:19:45',1,0),(10,'tran viet tien','testcompany1@example.com','0339258608','dep','Đoạn trường tân thanh, thường được biết đến với cái tên đơn giản là Truyện Kiều, là một truyện thơ của đại thi hào Nguyễn Du. Đây được xem là truyện thơ nổi tiếng nhất và xét vào hàng kinh điển trong văn học Việt Nam, tác phẩm được viết bằng chữ Nôm, theo thể lục bát, gồm 3.254 câu. Wikipedia',14,'2024-06-12 07:48:27',1,0),(11,'tran viet tien','testcompany1@example.com','0339258608','tot lam tour du lich rat dep','tot lam tour du lich rat dep',14,'2024-06-14 13:50:14',1,0),(12,'tran viet tien','testcompany1@example.com','0339258608','tot lam tour du lich rat dep','tot lam tour du lich rat dep',14,'2024-06-14 14:06:48',0,1);
/*!40000 ALTER TABLE `contactrequests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `discount` (
  `discountID` int NOT NULL AUTO_INCREMENT,
  `discountCode` varchar(256) NOT NULL,
  `discountName` varchar(255) NOT NULL,
  `discountFrom` date NOT NULL,
  `discountTo` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `discountType` int DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountReduce` int DEFAULT NULL,
  `tourID` int DEFAULT NULL,
  PRIMARY KEY (`discountID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
INSERT INTO discount (discountID, discountCode, discountName, discountFrom, discountTo, status, discountType, discountPercentage, discountReduce, tourID) VALUES
(1, 'SUMMER20', 'Summer Special', '2024-07-01', '2024-07-31', 1, 'percentage', 20, NULL, NULL),
(2, 'NEWYEAR2024', 'New Year Celebration', '2024-12-20', '2025-01-05', 1, 'percentage', 15, NULL, NULL),
(3, 'SPRING2024', 'Spring Adventure', '2024-03-01', '2024-03-31', 1, 'percentage', 10, NULL, NULL),
(4, 'HOLIDAY50', 'Holiday Bonanza', '2024-12-01', '2024-12-25', 1, 'reduce', NULL, 50, NULL),
(5, 'BLACKFRIDAY', 'Black Friday Deal', '2024-11-25', '2024-11-30', 1, 'reduce', NULL, 100, NULL);

/*!40000 ALTER TABLE `discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedbackID` int NOT NULL AUTO_INCREMENT,
  `feedbackContent` varchar(255) NOT NULL,
  `userID` int DEFAULT NULL,
  `tourID` int DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  PRIMARY KEY (`feedbackID`),
  KEY `userID` (`userID`),
  KEY `tourID` (`tourID`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'Tour rất tuyệt vời, hướng dẫn viên nhiệt tình và chu đáo.',31,14,'2024-06-24'),(3,'Dịch vụ tốt, đồ ăn ngon, cảnh đẹp.',33,14,'2024-08-06'),(4,'Chuyến đi tuyệt vời, tôi rất hài lòng.',34,14,'2024-09-06'),(5,'Hướng dẫn viên rất nhiệt tình và kiến thức sâu rộng.',31,14,'2024-10-06'),(7,'Chuyến đi rất vui, tôi sẽ quay lại.',33,20,'2024-12-06'),(8,'Tour không tốt như mong đợi, cần cải thiện dịch vụ.',34,21,'2024-01-06'),(9,'Hướng dẫn viên không nhiệt tình, nhưng cảnh đẹp bù lại.',31,22,'2024-02-06'),(11,'Chuyến đi thú vị, dịch vụ tốt.',33,24,'2024-04-06'),(12,'Tôi rất hài lòng với chuyến đi này.',34,25,'2024-05-06'),(13,'Dịch vụ chưa tốt, nhưng cảnh đẹp.',31,26,'2024-06-06'),(15,'Thời gian tham quan hợp lý, hướng dẫn viên vui tính.',33,15,'2024-07-19'),(16,'Cảnh đẹp, lịch trình hợp lý.',34,16,'2024-08-07'),(17,'Chuyến đi đáng nhớ, dịch vụ tốt.',31,17,'2024-09-07'),(19,'Tour tốt, giá cả hợp lý.',33,19,'2024-11-07'),(20,'Chuyến đi vui vẻ, sẽ tham gia lần sau.',34,20,'2024-12-07');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guide`
--

DROP TABLE IF EXISTS `guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guide` (
  `guideID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(20) DEFAULT NULL,
  `experience` text,
  `imageURL` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  PRIMARY KEY (`guideID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guide`
--

LOCK TABLES `guide` WRITE;
/*!40000 ALTER TABLE `guide` DISABLE KEYS */;
INSERT INTO `guide` VALUES (49,'Trần Việt Tiến','tientvhs176262@fpt.edu.vn','339258608','Có 20 năm kinh nghiệm trong lĩnh vực hướng dẫn viên du lịch trong nước .','https://tuyensinhcanuoc.com/project/uploads/2019/11/huong-dan-vien-du-lich-quoc-te.jpg','Tổ 5 phường Tân Thịnh Thành Phố Thái Nguyên',1),(50,'Nguyễn Thị W','user26@gmail.com','123456799','có 20 năm kinh nghiệm trong nghê hướng dẫn viên du lịch','http://image.com','Tổ 5 phường Tân Thịnh Thành Phố Thái Nguyên',1),(51,'Nguyễn Thị W','user26@gmail.com','123456799','10 nam kinh nghiem','http://image.com','Tổ 5 phường Tân Thịnh Thành Phố Thái Nguyên',1);
/*!40000 ALTER TABLE `guide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotels` (
  `hotel_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=445 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location` (
  `locationID` int NOT NULL AUTO_INCREMENT,
  `locationName` varchar(255) NOT NULL,
  `locationDescription` varchar(255) DEFAULT NULL,
  `locationImage` varchar(255) DEFAULT NULL,
  `tourID` int DEFAULT NULL,
  PRIMARY KEY (`locationID`),
  KEY `tourID` (`tourID`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Hà Nội','Thủ đô nghìn năm văn hiến',NULL,NULL),(2,'TP Hồ Chí Minh','Thành phố mang tên Bác',NULL,NULL),(3,'An Giang','Vùng đất sông nước',NULL,NULL),(4,'Bà Rịa – Vũng Tàu','Biển xanh cát trắng',NULL,NULL),(5,'Bắc Giang','Xứ sở vải thiều',NULL,NULL),(6,'Bắc Kạn','Miền núi non hùng vĩ',NULL,NULL),(7,'Bạc Liêu','Quê hương công tử Bạc Liêu',NULL,NULL),(8,'Bắc Ninh','Đất kinh Bắc cổ kính',NULL,NULL),(9,'Bến Tre','Xứ dừa miền Tây',NULL,NULL),(10,'Bình Định','Miền đất võ cổ truyền',NULL,NULL),(11,'Bình Dương','Vùng đất công nghiệp',NULL,NULL),(12,'Bình Phước','Quê hương hạt điều',NULL,NULL),(13,'Bình Thuận','Biển xanh nắng vàng',NULL,NULL),(14,'Cà Mau','Đất mũi cực Nam',NULL,NULL),(15,'Cần Thơ','Thủ phủ miền Tây',NULL,NULL),(16,'Cao Bằng','Vùng núi non xanh',NULL,NULL),(17,'Đà Nẵng','Thành phố đáng sống',NULL,NULL),(18,'Đắk Lắk','Thủ phủ cà phê',NULL,NULL),(19,'Đắk Nông','Đất đỏ bazan',NULL,NULL),(20,'Điện Biên','Chiến thắng lịch sử',NULL,NULL),(21,'Đồng Nai','Vùng đất công nghiệp',NULL,NULL),(22,'Đồng Tháp','Sen hồng miền Tây',NULL,NULL),(23,'Gia Lai','Đất đỏ bazan',NULL,NULL),(24,'Hà Giang','Cao nguyên đá Đồng Văn',NULL,NULL),(25,'Hà Nam','Vùng đất văn hóa',NULL,NULL),(26,'Hà Tĩnh','Quê hương Nguyễn Du',NULL,NULL),(27,'Hải Dương','Quê hương bánh đậu xanh',NULL,NULL),(28,'Hải Phòng','Thành phố hoa phượng đỏ',NULL,NULL),(29,'Hậu Giang','Miền sông nước',NULL,NULL),(30,'Hòa Bình','Miền núi non tươi đẹp',NULL,NULL),(31,'Hưng Yên','Đất nhãn lồng',NULL,NULL),(32,'Khánh Hòa','Biển xanh cát trắng',NULL,NULL),(33,'Kiên Giang','Đảo ngọc Phú Quốc',NULL,NULL),(34,'Kon Tum','Núi rừng Tây Nguyên',NULL,NULL),(35,'Lai Châu','Vùng núi cao',NULL,NULL),(36,'Lâm Đồng','Xứ sở ngàn hoa',NULL,NULL),(37,'Lạng Sơn','Miền biên giới',NULL,NULL),(38,'Lào Cai','Sapa mù sương',NULL,NULL),(39,'Long An','Vùng đất miền Tây',NULL,NULL),(40,'Nam Định','Thành phố biển',NULL,NULL),(41,'Nghệ An','Quê hương Bác Hồ',NULL,NULL),(42,'Ninh Bình','Cố đô Hoa Lư',NULL,NULL),(43,'Ninh Thuận','Vùng đất nắng',NULL,NULL),(44,'Phú Thọ','Đất tổ Hùng Vương',NULL,NULL),(45,'Phú Yên','Gành Đá Đĩa',NULL,NULL),(46,'Quảng Bình','Hang Sơn Đoòng',NULL,NULL),(47,'Quảng Nam','Phố cổ Hội An',NULL,NULL),(48,'Quảng Ngãi','Biển Mỹ Khê',NULL,NULL),(49,'Quảng Ninh','Vịnh Hạ Long',NULL,NULL),(50,'Quảng Trị','Địa đạo Vịnh Mốc',NULL,NULL),(51,'Sóc Trăng','Chùa Dơi',NULL,NULL),(52,'Sơn La','Mộc Châu',NULL,NULL),(53,'Tây Ninh','Núi Bà Đen',NULL,NULL),(54,'Thái Bình','Đồng lúa bát ngát',NULL,NULL),(55,'Thái Nguyên','Đồi chè xanh mướt',NULL,NULL),(56,'Thanh Hóa','Biển Sầm Sơn',NULL,NULL),(57,'Thừa Thiên Huế','Cố đô Huế',NULL,NULL),(58,'Tiền Giang','Chợ nổi Cái Bè',NULL,NULL),(59,'Trà Vinh','Cù lao Long Hòa',NULL,NULL),(60,'Tuyên Quang','Thủ đô kháng chiến',NULL,NULL),(61,'Vĩnh Long','Miệt vườn cây trái',NULL,NULL),(62,'Vĩnh Phúc','Tam Đảo',NULL,NULL),(63,'Yên Bái','Mù Cang Chải',NULL,NULL);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lucky_discount`
--

DROP TABLE IF EXISTS `lucky_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lucky_discount` (
  `discountID` int NOT NULL AUTO_INCREMENT,
  `discountCode` varchar(256) NOT NULL,
  `discountName` varchar(255) NOT NULL,
  `discountFrom` date NOT NULL,
  `discountTo` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountReduce` int DEFAULT NULL,
  PRIMARY KEY (`discountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lucky_discount`
--

LOCK TABLES `lucky_discount` WRITE;
/*!40000 ALTER TABLE `lucky_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `lucky_discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `luckyspins`
--

DROP TABLE IF EXISTS `luckyspins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `luckyspins` (
  `userID` int NOT NULL,
  `numberSpin` int DEFAULT '1',
  `TimeLastSpins` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`),
  CONSTRAINT `LuckySpins_fk_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `luckyspins`
--

LOCK TABLES `luckyspins` WRITE;
/*!40000 ALTER TABLE `luckyspins` DISABLE KEYS */;
INSERT INTO `luckyspins` VALUES (16,1,NULL),(26,1,NULL),(30,1,NULL),(31,1,NULL),(33,1,NULL),(34,1,NULL),(36,1,NULL),(44,1,NULL),(45,1,NULL);
/*!40000 ALTER TABLE `luckyspins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `idNew` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `shortDescription` text NOT NULL,
  `longDescription` text NOT NULL,
  `publishDate` datetime NOT NULL,
  `imageTheme` varchar(255) DEFAULT NULL,
  `partnerID` int DEFAULT NULL,
  PRIMARY KEY (`idNew`),
  KEY `fk_news_partner` (`partnerID`),
  CONSTRAINT `fk_news_partner` FOREIGN KEY (`partnerID`) REFERENCES `partner` (`partnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Trải nghiệm du lịch tại Phượng Hoàng','Phượng Hoàng cổ trấn không phải là cái tên xa lạ với những ai đam mê du lịch Trung Quốc. Nơi đây từ lâu đã trở thành điểm đến say đắm lòng người bởi vẻ đẹp cổ kính, trầm mặc như tranh vẽ. Nằm dọc theo dòng sông Đà Giang thơ mộng, Phượng Hoàng cổ trấn như một bức tranh thủy mặc sống động, níu chân du khách bởi những ngôi nhà gỗ cổ kính, những con đường lát đá rêu phong và bầu không khí thanh bình, yên tĩnh.','Phượng Hoàng cổ trấn tọa lạc tại phía tây tỉnh Hồ Nam, bên dòng Đà Giang thơ mộng, Phượng Hoàng cổ trấn (FengHuang zhen) sừng sững như một viên ngọc quý giữa lòng Trung Hoa. Nơi đây mang trong mình bề dày lịch sử hơn 1.300 năm, ẩn chứa những giá trị văn hóa và kiến trúc truyền thống độc đáo. May mắn thay, Phượng Hoàng ít chịu ảnh hưởng bởi chiến tranh, nên những nét đẹp tĩnh lặng của một đô thị cổ Phương Đông vẫn được bảo tồn gần như nguyên vẹn.','2024-05-05 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_image`
--

DROP TABLE IF EXISTS `news_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news_image` (
  `newImageID` int NOT NULL AUTO_INCREMENT,
  `idNew` int NOT NULL,
  `newsImageURL` varchar(255) NOT NULL,
  PRIMARY KEY (`newImageID`),
  KEY `fk_idNew` (`idNew`),
  CONSTRAINT `fk_news_image_new` FOREIGN KEY (`idNew`) REFERENCES `news` (`idNew`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_image`
--

LOCK TABLES `news_image` WRITE;
/*!40000 ALTER TABLE `news_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partner` (
  `partnerID` int NOT NULL AUTO_INCREMENT,
  `partnerName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phoneNumber` int NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `roleID` int DEFAULT NULL,
  PRIMARY KEY (`partnerID`),
  KEY `roleID` (`roleID`),
  CONSTRAINT `partner_ibfk_2` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES (1,'Trần Việt Tiến','tiencsgo123@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f',123456789,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(2,'Nguyễn Văn Hoàng','testcompany2@example.com','0ad01d09c3ee727bdfaad9a59980de1f',123456789,'Tổ 4 Phường Tân Thịnh Thái Nguyên','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(3,'Nguyễn Văn Huy','testcompany3@example.com','0ad01d09c3ee727bdfaad9a59980de1f',123456789,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(4,'Nguyễn Văn Duy','testcompany4@example.com','0ad01d09c3ee727bdfaad9a59980de1f',123456789,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(5,'Nguyễn Văn Đức','testcompany5@example.com','0ad01d09c3ee727bdfaad9a59980de1f',123456789,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(6,'Nguyễn Văn An','nguyenvanan@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654321,'Số 10, Đường 3/2, Quận 10, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(7,'Nguyễn Văn Bình','nguyenvanbinh@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654322,'Số 20, Đường Nguyễn Huệ, Quận 1, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(8,'Nguyễn Văn Cường','nguyenvancuong@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654323,'Số 30, Đường Lê Lợi, Quận 1, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(9,'Nguyễn Văn Dũng','nguyenvandung@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654324,'Số 40, Đường Hai Bà Trưng, Quận 1, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(10,'Nguyễn Văn Em','nguyenvanem@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654325,'Số 50, Đường Trần Hưng Đạo, Quận 1, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(11,'Nguyễn Văn Phú','nguyenvanphu@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654326,'Số 60, Đường Lý Thái Tổ, Quận 10, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(12,'Nguyễn Văn Hưng','nguyenvanhung@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654327,'Số 70, Đường Điện Biên Phủ, Quận 10, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(13,'Nguyễn Văn Khoa','nguyenvankhoa@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654328,'Số 80, Đường Võ Văn Tần, Quận 10, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(14,'Nguyễn Văn Lam','nguyenvanlam@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654329,'Số 90, Đường Nguyễn Trãi, Quận 5, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3),(15,'Nguyễn Văn Mạnh','nguyenvanmanh@example.com','0ad01d09c3ee727bdfaad9a59980de1f',987654330,'Số 100, Đường Hùng Vương, Quận 5, TP.HCM','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',1,3);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `postID` int NOT NULL AUTO_INCREMENT,
  `postTitle` varchar(255) NOT NULL,
  `postContent` varchar(255) DEFAULT NULL,
  `createAt` date NOT NULL,
  `partnerID` int DEFAULT NULL,
  `image` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`postID`),
  KEY `partnerID` (`partnerID`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`partnerID`) REFERENCES `partner` (`partnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `reviewID` int NOT NULL AUTO_INCREMENT,
  `tourID` int DEFAULT NULL,
  `guideID` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `comment` text,
  `reviewDate` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reviewID`),
  KEY `tourID` (`tourID`),
  KEY `guideID` (`guideID`),
  KEY `userID` (`userID`),
  CONSTRAINT `review_ibfk_1` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`),
  CONSTRAINT `review_ibfk_2` FOREIGN KEY (`guideID`) REFERENCES `guide` (`guideID`),
  CONSTRAINT `review_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (32,14,49,44,3,'huong dan vien rat co tam huyet','2024-07-08','Tot');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `roleID` int NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'customer'),(3,'partner'),(4,'ADMIN'),(5,'Hướng dẫn viên');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_type`
--

DROP TABLE IF EXISTS `room_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_type` (
  `room_type` int NOT NULL,
  `roomTypeName` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`room_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_type`
--

LOCK TABLES `room_type` WRITE;
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `room_id` int NOT NULL AUTO_INCREMENT,
  `hotel_id` int NOT NULL,
  `room_type` int NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `price` double NOT NULL,
  `imageIdRoom` int DEFAULT NULL,
  `max_guests` int NOT NULL,
  `description` text,
  `number_beds` int DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `hotel_id` (`hotel_id`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomsbooking`
--

DROP TABLE IF EXISTS `roomsbooking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roomsbooking` (
  `userId` int DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  `room_type` int NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `price` double NOT NULL,
  `imageIdRoom` int DEFAULT NULL,
  `max_guests` int NOT NULL,
  `description` text,
  `number_beds` int DEFAULT NULL,
  `date_book` date DEFAULT NULL,
  KEY `fk_roomcancel_user` (`userId`),
  KEY `fk_roomcancel_room` (`room_id`),
  CONSTRAINT `fk_roomcancel_room` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_roomcancel_user` FOREIGN KEY (`userId`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomsbooking`
--

LOCK TABLES `roomsbooking` WRITE;
/*!40000 ALTER TABLE `roomsbooking` DISABLE KEYS */;
/*!40000 ALTER TABLE `roomsbooking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule` (
  `scheduleID` int NOT NULL AUTO_INCREMENT,
  `scheduleTitle` varchar(255) NOT NULL,
  `scheduleContent` varchar(255) DEFAULT NULL,
  `tourID` int DEFAULT NULL,
  `scheduleDate` date DEFAULT NULL,
  PRIMARY KEY (`scheduleID`),
  KEY `fk_schedule_tour` (`tourID`),
  CONSTRAINT `fk_schedule_tour` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (88,'TP HỒ CHÍ MINH - PHÚ QUỐC - HÒN THƠM - THỊ TRẤN HOÀNG HÔN - GRAND WORLD (Ăn trưa, chiều)',NULL,16,'2024-09-21'),(89,'ABC',NULL,14,'2024-07-16');
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduledetails`
--

DROP TABLE IF EXISTS `scheduledetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scheduledetails` (
  `detailID` int NOT NULL AUTO_INCREMENT,
  `scheduleID` int DEFAULT NULL,
  `detailTitle` varchar(255) DEFAULT NULL,
  `detailContent` text,
  `note` text,
  PRIMARY KEY (`detailID`),
  KEY `scheduleID` (`scheduleID`),
  CONSTRAINT `scheduledetails_ibfk_1` FOREIGN KEY (`scheduleID`) REFERENCES `schedule` (`scheduleID`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduledetails`
--

LOCK TABLES `scheduledetails` WRITE;
/*!40000 ALTER TABLE `scheduledetails` DISABLE KEYS */;
INSERT INTO `scheduledetails` VALUES (122,88,'TP HỒ CHÍ MINH - PHÚ QUỐC - HÒN THƠM - THỊ TRẤN HOÀNG HÔN - GRAND WORLD (Ăn trưa, chiều)','TP HỒ CHÍ MINH - PHÚ QUỐC - HÒN THƠM - THỊ TRẤN HOÀNG HÔN - GRAND WORLD (Ăn trưa, chiều)','TP HỒ CHÍ MINH - PHÚ QUỐC - HÒN THƠM - THỊ TRẤN HOÀNG HÔN - GRAND WORLD (Ăn trưa, chiều)'),(123,89,'day1','cdsfsdf','sdfsdfsdf'),(124,89,'day1','cdsfsdf','sdfsdfsdf'),(125,89,'day1','cdsfsdf','sdfsdfsdf');
/*!40000 ALTER TABLE `scheduledetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `ticketID` int NOT NULL AUTO_INCREMENT,
  `bookingID` int DEFAULT NULL,
  `typeID` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `gender` tinyint DEFAULT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`ticketID`),
  KEY `typeID_idx` (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (60,0,1,'hai',1,'2000-02-02'),(61,118,1,'Nga',0,'1997-02-02'),(62,120,1,'Nga',0,'1997-02-02'),(63,120,2,'Minh',1,'2001-01-01'),(64,121,1,'Nga',0,'1997-02-02'),(65,122,1,'Nga',0,'1999-01-01'),(66,125,1,'tran van a',1,'1937-05-15'),(67,126,1,'tran van a',1,'1937-05-15'),(68,127,1,'tran van a',1,'1937-05-15'),(69,142,1,'minh',1,'1934-06-14'),(70,142,1,'tien',1,'1934-04-10'),(71,144,1,'duy',1,'1939-06-15'),(72,144,1,'tien',1,'1939-06-12'),(73,144,1,'minh',1,'1938-07-16'),(74,144,1,'minhnh',1,'1937-10-18'),(75,144,1,'minhnh',1,'1936-06-12'),(76,145,1,'duy',1,'1937-05-14'),(77,147,1,'duy',1,'1938-09-15'),(78,148,1,'minh',1,'1935-06-15');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickettype`
--

DROP TABLE IF EXISTS `tickettype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tickettype` (
  `typeID` int NOT NULL AUTO_INCREMENT,
  `typeName` varchar(255) NOT NULL,
  `typeCode` varchar(255) NOT NULL,
  `ageMin` int DEFAULT NULL,
  `ageMax` int DEFAULT NULL,
  `isDefault` tinyint DEFAULT '1',
  `tourID` int DEFAULT NULL,
  PRIMARY KEY (`typeID`),
  KEY `fk_tourID_idx` (`tourID`),
  CONSTRAINT `fk_tourID` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickettype`
--

LOCK TABLES `tickettype` WRITE;
/*!40000 ALTER TABLE `tickettype` DISABLE KEYS */;
INSERT INTO `tickettype` VALUES (1,'Người lớn (≥ 12 tuổi)','ADULT',12,NULL,1,NULL),(2,'Trẻ em (7-12 tuổi)','CHILD1',7,12,1,NULL),(3,'Trẻ nhỏ (3-7 tuổi)','CHILD2',3,7,1,NULL),(4,'Em bé (< 3 tuổi)','CHILD3',0,3,1,NULL),(15,'Vé Vip','VIP',18,100,0,20);
/*!40000 ALTER TABLE `tickettype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tour`
--

DROP TABLE IF EXISTS `tour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tour` (
  `tourID` int NOT NULL AUTO_INCREMENT,
  `tourName` varchar(255) NOT NULL,
  `tourDescription` varchar(255) DEFAULT NULL,
  `startLocation` varchar(255) DEFAULT NULL,
  `endLocation` varchar(255) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date DEFAULT NULL,
  `price` double NOT NULL,
  `numberOfPeople` int NOT NULL,
  `partnerID` int DEFAULT NULL,
  `thumbnails` varchar(255) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `hotel_id` int DEFAULT NULL,
  PRIMARY KEY (`tourID`),
  KEY `partnerID` (`partnerID`),
  KEY `fk_hotel` (`hotel_id`),
  CONSTRAINT `fk_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`),
  CONSTRAINT `tour_ibfk_2` FOREIGN KEY (`partnerID`) REFERENCES `partner` (`partnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tour`
--

LOCK TABLES `tour` WRITE;
/*!40000 ALTER TABLE `tour` DISABLE KEYS */;
INSERT INTO `tour` VALUES (14,'Trung Quốc: Thượng Hải - Hàng Châu - Vô Tích - Tô Châu','oke','Hà Nội','Hà Nội','2024-07-16','2024-07-20',200000,500,3,'images/trungquoc.png',1,NULL),(15,'Đông Bắc: Hà Nội - Hà Giang - Lũng Cú - Đồng Văn - Mã Pí Lèng - Mèo Vạc - Cao Bằng','tháp rùa','Hải Dương','Quảng Ninh','2024-07-15','2024-07-17',200000,9,3,'images/campuchiatheme.jpg',1,NULL),(16,'TP.HCM - BANGKOK - PATTAYA – CHỢ NỔI BỐN MIỀN (Ăn sáng, trưa, tối)','VietNam TPHCM','Hà Nội','Hồ Chí Minh','2024-08-01','2024-08-05',200000,100,3,'images/hongkong.jpg',1,NULL),(17,'Miền Trung: Đà Nẵng - Huế - Quảng Bình','Tour miền Trung hấp dẫn','Đà Nẵng','Quảng Bình','2024-09-01','2024-09-05',300000,50,3,'images/mientrung1.jpg',1,NULL),(18,'Miền Tây: Cần Thơ - Bến Tre - Mỹ Tho','Tour miền Tây sông nước','Cần Thơ','Mỹ Tho','2024-10-01','2024-10-05',250000,40,3,'images/mientay1.jpg',1,NULL),(19,'Tây Nguyên: Đà Lạt - Buôn Ma Thuột','Tour Tây Nguyên hoang dã','Đà Lạt','Buôn Ma Thuột','2024-11-01','2024-11-05',350000,30,3,'images/taynguyen1.jpg',1,NULL),(20,'Miền Bắc: Hà Nội - Sapa - Hạ Long','Tour miền Bắc kỳ thú','Hà Nội','Hạ Long','2024-12-01','2024-12-05',400000,60,3,'images/mienbac1.jpg',1,NULL),(21,'Miền Trung: Hội An - Đà Nẵng - Huế','Tour miền Trung di sản','Hội An','Huế','2024-01-01','2024-01-05',300000,70,10,'images/mientrung2.jpg',1,NULL),(22,'Miền Tây: Cần Thơ - Sóc Trăng - Bạc Liêu','Tour miền Tây đặc sắc','Cần Thơ','Bạc Liêu','2024-02-01','2024-02-05',250000,80,11,'images/mientay2.jpg',1,NULL),(23,'Tây Nguyên: Pleiku - Kon Tum - Đà Lạt','Tour Tây Nguyên trải nghiệm','Pleiku','Đà Lạt','2024-03-01','2024-03-05',350000,90,12,'images/taynguyen2.jpg',1,NULL),(24,'Miền Bắc: Hà Nội - Ninh Bình - Hạ Long','Tour miền Bắc khám phá','Hà Nội','Hạ Long','2024-04-01','2024-04-05',400000,100,13,'images/mienbac2.jpg',1,NULL),(25,'Miền Trung: Quảng Nam - Quảng Ngãi - Bình Định','Tour miền Trung đặc biệt','Quảng Nam','Bình Định','2024-05-01','2024-05-05',300000,110,14,'images/mientrung3.jpg',1,NULL),(26,'Miền Tây: An Giang - Đồng Tháp - Cà Mau','Tour miền Tây sông nước','An Giang','Cà Mau','2024-06-01','2024-06-05',250000,120,15,'images/mientay3.jpg',1,NULL),(27,'Thành phố hồ chí mình hà nội','Thành phố hồ chí mình hà nội','Hà Nội','Quảng Ninh','2024-07-03','2024-07-27',20000,200,3,'images/1.jpg',1,NULL),(28,'Du lịch Hà Nội - TP Hồ Chí Minh','Tour của minh','Hà Nội','TP Hồ Chí Minh','2024-07-20','2024-07-25',10000000,100,3,'images/malai3.jpg',1,NULL);
/*!40000 ALTER TABLE `tour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tour_guide`
--

DROP TABLE IF EXISTS `tour_guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tour_guide` (
  `tourID` int NOT NULL,
  `guideID` int NOT NULL,
  `tourType` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tourID`,`guideID`),
  KEY `guideID` (`guideID`),
  CONSTRAINT `tour_guide_ibfk_1` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`),
  CONSTRAINT `tour_guide_ibfk_2` FOREIGN KEY (`guideID`) REFERENCES `guide` (`guideID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tour_guide`
--

LOCK TABLES `tour_guide` WRITE;
/*!40000 ALTER TABLE `tour_guide` DISABLE KEYS */;
INSERT INTO `tour_guide` VALUES (14,49,'Hướng dẫn viên tiễn'),(14,50,'HDV dẫn đoàn'),(14,51,'Hướng dẫn viên tiễn');
/*!40000 ALTER TABLE `tour_guide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tour_images`
--

DROP TABLE IF EXISTS `tour_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tour_images` (
  `imageID` int NOT NULL AUTO_INCREMENT,
  `tourID` int NOT NULL,
  `imageURL` varchar(255) NOT NULL,
  PRIMARY KEY (`imageID`),
  KEY `tourID` (`tourID`),
  CONSTRAINT `tour_images_ibfk_1` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tour_images`
--

LOCK TABLES `tour_images` WRITE;
/*!40000 ALTER TABLE `tour_images` DISABLE KEYS */;
INSERT INTO `tour_images` VALUES (33,15,'images/pexels-vladalex94-1402787 - Copy - Copy.jpg'),(34,15,'images/pexels-vladalex94-1402787 - Copy (2).jpg'),(35,15,'images/pexels-vladalex94-1402787.jpg'),(36,15,'images/pexels-wangming-photo-115695-354939 - Copy - Copy.jpg'),(37,16,'images/pexels-chevanon-1108099.jpg'),(38,16,'images/pexels-pixabay-531880.jpg'),(39,16,'images/pexels-stywo-1054218.jpg'),(40,16,'images/pexels-therato-1933239.jpg'),(41,27,'images/hinh-anh-gau-bac-cuc-30.jpg'),(42,27,'images/gau-bac-cuc-chup-anh-tu-suong-2 - Copy (2) - Copy.webp'),(43,27,'images/gau-bac-cuc-chup-anh-tu-suong-2 - Copy - Copy - Copy - Copy.webp'),(44,27,'images/gau-bac-cuc-chup-anh-tu-suong-2 - Copy - Copy (2) - Copy.webp'),(45,14,'images/campuchia1.jpg'),(46,14,'images/campuchia2.jpg'),(47,14,'images/campuchiatheme.jpg'),(48,14,'images/hongkong.jpg'),(49,28,'images/hongkong1.jpg'),(50,28,'images/malai1.jpg'),(51,28,'images/malai4.jpg'),(52,28,'images/malay2.jpg');
/*!40000 ALTER TABLE `tour_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tourticket`
--

DROP TABLE IF EXISTS `tourticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tourticket` (
  `tourID` int NOT NULL,
  `typeID` int NOT NULL,
  `priceTicket` double DEFAULT NULL,
  PRIMARY KEY (`tourID`,`typeID`),
  KEY `typeID` (`typeID`),
  CONSTRAINT `tourticket_ibfk_1` FOREIGN KEY (`tourID`) REFERENCES `tour` (`tourID`),
  CONSTRAINT `tourticket_ibfk_2` FOREIGN KEY (`typeID`) REFERENCES `tickettype` (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tourticket`
--

LOCK TABLES `tourticket` WRITE;
/*!40000 ALTER TABLE `tourticket` DISABLE KEYS */;
INSERT INTO `tourticket` VALUES (14,1,2000000),(14,2,500000),(14,3,100000),(14,4,50000),(14,15,NULL),(15,1,1000000),(15,2,500000),(15,3,100000),(15,4,50000),(21,1,10000000),(21,2,5000000),(21,3,2000000),(21,4,1000000),(28,1,10000000),(28,2,5000000),(28,3,2000000),(28,4,1000000),(28,15,NULL);
/*!40000 ALTER TABLE `tourticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userID` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `userDOB` date NOT NULL,
  `phoneNumber` int NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `userPicture` varchar(255) DEFAULT NULL,
  `roleID` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `roleID` (`roleID`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (16,'Trần Việt Tiến','user9@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',1234567891,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',4,1),(26,'Trần Việt Tiến','tientvhs176262@fpt.edu.vn','0ad01d09c3ee727bdfaad9a59980de1f','2003-08-11',339258608,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',5,1),(30,'Nguyễn Hà Thành','user13@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',1234567895,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(31,'Trần Văn Thắng','user14@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',1234567896,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(33,'Ngô Thị Vân','user16@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',1234567897,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(34,'Trần Việt Tiến','tientvhs176262@fpt.edu.vn','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',1234567897,'136 Xuân Thủy, Dịch Vọng Hậu, Cầu Giấy, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(35,'Nguyễn Văn N','user17@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456790,'123 Đường A, Quận B, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(36,'Nguyễn Thị O','user18@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456791,'456 Đường B, Quận C, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(37,'Nguyễn Văn P','user19@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456792,'789 Đường C, Quận D, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(38,'Nguyễn Thị Q','user20@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456793,'101 Đường D, Quận E, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(39,'Nguyễn Văn R','user21@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456794,'112 Đường E, Quận F, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(40,'Nguyễn Thị S','user22@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456795,'123 Đường F, Quận G, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(41,'Nguyễn Văn T','user23@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456796,'134 Đường G, Quận H, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(42,'Nguyễn Thị U','user24@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456797,'145 Đường H, Quận I, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(43,'Nguyễn Văn V','user25@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456798,'156 Đường I, Quận J, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',2,1),(44,'Nguyễn Thị W','user26@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-01-01',123456799,'167 Đường J, Quận K, Hà Nội','https://images.pexels.com/photos/10295513/pexels-photo-10295513.jpeg?auto=compress&cs=tinysrgb&w=600',5,1),(45,'Tran','tientvhs176262@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2003-08-11',339258608,'to 5',NULL,1,1),(46,'pham duc minh','minh@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2002-06-05',393544314,'Cạnh quán trà chanh bụi phố tân xã',NULL,1,1),(47,'mrc','minhbkqn@gmail.com','0ad01d09c3ee727bdfaad9a59980de1f','2000-06-06',393544314,'Cạnh quán trà chanh bụi phố tân xã',NULL,1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_discount`
--

DROP TABLE IF EXISTS `user_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_discount` (
  `userID` int NOT NULL,
  `discountID` int NOT NULL,
  PRIMARY KEY (`userID`,`discountID`),
  KEY `userID` (`userID`),
  KEY `discountID` (`discountID`),
  CONSTRAINT `user_discount_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE,
  CONSTRAINT `user_discount_ibfk_2` FOREIGN KEY (`discountID`) REFERENCES `discount` (`discountID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_discount`
--

LOCK TABLES `user_discount` WRITE;
/*!40000 ALTER TABLE `user_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_guides`
--

DROP TABLE IF EXISTS `user_guides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_guides` (
  `userID` int NOT NULL,
  `guideID` int NOT NULL,
  PRIMARY KEY (`userID`,`guideID`),
  KEY `guideID` (`guideID`),
  CONSTRAINT `user_guides_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  CONSTRAINT `user_guides_ibfk_2` FOREIGN KEY (`guideID`) REFERENCES `guide` (`guideID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_guides`
--

LOCK TABLES `user_guides` WRITE;
/*!40000 ALTER TABLE `user_guides` DISABLE KEYS */;
INSERT INTO `user_guides` VALUES (44,51);
/*!40000 ALTER TABLE `user_guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_lucky_discount`
--

DROP TABLE IF EXISTS `user_lucky_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_lucky_discount` (
  `userID` int NOT NULL,
  `discountID` int NOT NULL,
  PRIMARY KEY (`userID`,`discountID`),
  KEY `user_lucky_discount_fk_discount` (`discountID`),
  CONSTRAINT `user_lucky_discount_fk_discount` FOREIGN KEY (`discountID`) REFERENCES `lucky_discount` (`discountID`) ON DELETE CASCADE,
  CONSTRAINT `user_lucky_discount_fk_user` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_lucky_discount`
--

LOCK TABLES `user_lucky_discount` WRITE;
/*!40000 ALTER TABLE `user_lucky_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_lucky_discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bookingtour1'
--

--
-- Dumping routines for database 'bookingtour1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-21 22:14:49
INSERT INTO hotels (hotel_id, name, address, city, phone, email, website, image) VALUES 
(19, 'InterContinental Hanoi Landmark72', 'Keangnam Hanoi Landmark Tower, Plot E6, Pham Hung Road, Nam Tu Liem District', 'Hà Nội', '02439344367', 'info@hanoilandmark72.com', 'https://landmark72.intercontinental.com/', 'https://landmark72.intercontinental.com/upload/images/landmark.jpg'),
(20, 'Sofitel Legend Metropole Hanoi', '15 Ngo Quyen Street, Hoan Kiem District', 'Hà Nội', '02438266919', 'contact@sofitel.com', 'https://sofitel-saigon-plaza.com/vi/', 'https://manoirdesartshtotel.com/images/slide/74.jpg'),
(21, 'Park Hyatt Saigon', '2 Lam Son Square, District 1', 'Hồ Chí Minh', '02838241234', 'reservations.sapih@hyatt.com', 'https://www.hyatt.com', 'https://assets.hyatt.com/content/dam/hyatt/hotel/external.jpg'),
(22, 'Fusion Resort Phu Quoc', 'Vung Bau Bay, Cua Can Village, Phu Quoc', 'Phú Quốc', '02973988999', 'info@fusionresorts.com', 'https://camranh.fusionresorts.com/vi/trang-chu/', 'https://camranh.fusionresorts.com/wp-content/uploads/image.jpg'),
(23, 'Azerai La Residence Hue', '5 Le Loi Street', 'Huế', '02343823884', 'info@azerai.com', 'https://azerai.com', 'https://cdn.shortpixel.ai/client/to_webp_w_600.jpg'),
(24, 'Banyan Tree Lang Co', 'Cu Du Village, Loc Vinh Commune, Phu Loc District', 'Lăng Cô', '02343958888', 'langco@banyantree.com', 'https://www.banyantree.com', 'https://www.banyantree.com/_next/image?url=photo.jpg'),
(25, 'duy', 'Hà Nội', 'NULL', '0243944336', 'user@gmail.com', 'https://manoirdesartshtotel.com', 'https://manoirdesartshtotel.com/images/slide/35.jpg'),
(26, 'duy', 'Hà Nội', 'NULL', '0243944336', 'duy@gmail.com', 'https://manoirdesartshtotel.com', 'https://manoirdesartshtotel.com/images/slide/35.jpg');


INSERT INTO rooms (room_id, hotel_id, room_type, name, price, imageIdRoom, max_guests, description, number_beds) VALUES 
(1, 19, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1),
(2, 20, 2, 'Luxury Suite', 250, 102, 3, 'Luxury suite with all amenities', 2),
(3, 21, 1, 'Cozy Single Room', 100, 103, 1, 'Cozy single room with a queen bed', 1),
(4, 22, 3, 'Ocean View Room', 180, 104, 2, 'Comfortable double room with ocean view', 1),
(5, 23, 2, 'Large Suite', 220, 105, 4, 'Large suite with a balcony', 2),
(6, 24, 2, 'Garden View Room', 170, 106, 2, 'Modern double room with a garden view', 1);



INSERT INTO roomsbooking (userId, room_id, room_type, name, price, imageIdRoom, max_guests, description, number_beds, date_book) VALUES 
(16, 1, 1, 'duy', 10, 1, 2, 'hello', 1, '2024-07-14'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18'),
(33, 2, 2, 'Luxury Suite', 250, 102, 3, 'Luxury suite with all amenities', 2, '2024-07-18'),
(33, 1, 1, 'City View Room', 150, 101, 2, 'Spacious room with a city view', 1, '2024-07-18');


INSERT INTO room_type (room_type, roomTypeName) VALUES 
(1, 'Vip'),
(2, 'Medium'),
(3, 'Normal');

INSERT INTO post (postID, postTitle, postContent, createAt, partnerID, image) VALUES 
(1, 'Cẩm nang du lịch Hà Nội', 'Khám phá Hà Nội - Thủ đô ngàn năm văn hiến với những di tích lịch sử và văn hóa đặc sắc.', '2024-07-26', 1, 'https://dulichkhampha24.com/wp-content/uploads/2019/09/kinh-nghiem-du-lich-Ha-Noi-1.jpg'),
(2, 'Cẩm nang du lịch Sapa', 'Trải nghiệm vẻ đẹp thiên nhiên và văn hóa độc đáo của Sapa, nơi có những thửa ruộng bậc thang và dân tộc thiểu số.', '2024-07-25', 1, 'https://cits.asia/wp-content/uploads/2022/07/du-lich-sapa.jpg'),
(3, 'Cẩm nang du lịch Đà Nẵng', 'Thành phố biển Đà Nẵng nổi tiếng với bãi biển Mỹ Khê tuyệt đẹp và cây cầu Rồng huyền thoại.', '2024-07-24', 1, 'https://img.cand.com.vn/resize/800x800/NewFiles/Images/2021/10/16/7_du-1634353732500.jpg'),
(4, 'Cẩm nang du lịch Phú Quốc', 'Hòn đảo ngọc Phú Quốc với bãi biển trong xanh và những khu nghỉ dưỡng sang trọng.', '2024-07-23', 1, 'https://puolotrip.com/wp-content/uploads/2013/01/Kinh-nghiem-di-du-lich-phu-quoc-jpg.webp'),
(5, 'Cẩm nang du lịch Huế', 'Khám phá cố đô Huế với những di sản văn hóa thế giới và ẩm thực đặc sắc.', '2024-07-22', 1, 'https://cdn.eva.vn/upload/1-2023/images/2023-03-18/du-lich-hue-co-gi-choi-10-diem-den-dep-nin-tho-ai-toi-cung-chup-anh-chay-may-73181-lich-su-hinh-thanh-dai-noi-hue-700x524-1679111651-586-width700height524.jpg');

ALTER TABLE post MODIFY COLUMN postContent TEXT;
