/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import model.Blog;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;
import model.BlogComment;
import model.BlogLike;
import model.BlogPicture;
import java.sql.*;
import model.User;

/**
 *
 * @author MSI
 */
public class BlogDAO {

    private Connection con;
    private ArrayList<Blog> blog = new ArrayList<>();

    public String status = "";

    public BlogDAO() {
        con = new DBContext().connection;
    }

    public BlogDAO(Connection con) {
        this.con = con;
    }

    // Method to create a new blog
    public void createBlog(Blog blog) throws SQLException {
        String sql = "INSERT INTO blog (title, content, created_at, userID) VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getCreatedAt());
            statement.setInt(4, blog.getUser().getUserID());
            statement.executeUpdate();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    blog.setBlogID(generatedKeys.getInt(1));
                }
            }
        }
    }

    // Method to like a blog
    public void likeBlog(BlogLike blogLike) throws SQLException {
        String sql = "INSERT INTO bloglike (created_at, blogID, userID) VALUES (?, ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setDate(1, blogLike.getCreatedAt());
            statement.setInt(2, blogLike.getBlog().getBlogID());
            statement.setInt(3, blogLike.getUser().getUserID());
            statement.executeUpdate();
        }
    }

    // Method to add a comment to a blog
    public void addComment(BlogComment comment) throws SQLException {
        String sql = "INSERT INTO blogcomment (comment, created_at, parentCommentID, blogID, userID) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, comment.getComment());
            statement.setDate(2, comment.getCreatedAt());
            if (comment.getParentCommentID() > 0) {
                statement.setInt(3, comment.getParentCommentID());
            } else {
                statement.setNull(3, Types.INTEGER);
            }
            statement.setInt(4, comment.getBlog().getBlogID());
            statement.setInt(5, comment.getUser().getUserID());
            statement.executeUpdate();
        }
    }

    // Method to add a picture to a blog
    public void addBlogPicture(BlogPicture blogPicture) throws SQLException {
        String sql = "INSERT INTO blogpicture (pictureURL, created_at, blogID) VALUES (?, ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, blogPicture.getPictureURL());
            statement.setTimestamp(2, blogPicture.getCreatedAt());
            statement.setInt(3, blogPicture.getBlog().getBlogID());
            statement.executeUpdate();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    blogPicture.setPictureID(generatedKeys.getInt(1));
                }
            }
        }
    }

    // Method to check if the user has liked the blog
    public boolean isUserLikedBlog(int blogID, int userID) throws SQLException {
        String sql = "SELECT COUNT(*) FROM bloglike WHERE blogID = ? AND userID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
            }
        }
        return false;
    }

    // Method to mark a blog as favorite
    public void addFavorite(int blogID, int userID) throws SQLException {
        String sql = "INSERT INTO blogfavorite (created_at, blogID, userID) VALUES (NOW(), ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    // Method to unmark a blog as favorite
    public void removeFavorite(int blogID, int userID) throws SQLException {
        String sql = "DELETE FROM blogfavorite WHERE blogID = ? AND userID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    // Method to check if a blog is favorited by the user
    public boolean isFavorite(int blogID, int userID) throws SQLException {
        String sql = "SELECT COUNT(*) FROM blogfavorite WHERE blogID = ? AND userID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
            }
        }
        return false;
    }

    // Method to retrieve blogs with comments, likes, pictures, and favorites
    public List<Blog> getAllBlogs(int userID, int offset, int limit) throws SQLException {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM blog ORDER BY created_at DESC LIMIT ? OFFSET ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("blogID"));
                    blog.setTitle(rs.getString("title"));
                    blog.setContent(rs.getString("content"));
                    blog.setCreatedAt(rs.getTimestamp("created_at"));
                    blog.setUser(getUserByID(rs.getInt("userID")));
                    blog.setLikes(getLikesByBlogID(blog.getBlogID()));
                    blog.setComments(getCommentsByBlogID(blog.getBlogID()));
                    blog.setPictures(getPicturesByBlogID(blog.getBlogID()));
                    blog.setFavorite(isFavorite(blog.getBlogID(), userID));  // Set favorite status
                    blogs.add(blog);
                }
            }
        }
        return blogs;
    }

    public List<BlogComment> getCommentsByBlogID(int blogID) throws SQLException {
        List<BlogComment> comments = new ArrayList<>();
        String sql = "SELECT bc.*, u.userName FROM blogcomment bc INNER JOIN user u ON bc.userID = u.userID WHERE bc.blogID = ? AND bc.parentCommentID IS NULL";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    BlogComment comment = new BlogComment();
                    comment.setCommentID(rs.getInt("commentID"));
                    comment.setComment(rs.getString("comment"));
                    comment.setCreatedAt(rs.getDate("created_at"));
                    comment.setParentCommentID(rs.getInt("parentCommentID"));
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    comment.setUser(user);
                    comment.setReplies(getRepliesByParentCommentID(comment.getCommentID())); // Fetch replies
                    comments.add(comment);
                }
            }
        }
        return comments;
    }

    private List<BlogComment> getRepliesByParentCommentID(int parentCommentID) throws SQLException {
        List<BlogComment> replies = new ArrayList<>();
        String sql = "SELECT bc.*, u.userName FROM blogcomment bc INNER JOIN user u ON bc.userID = u.userID WHERE bc.parentCommentID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, parentCommentID);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    BlogComment reply = new BlogComment();
                    reply.setCommentID(rs.getInt("commentID"));
                    reply.setComment(rs.getString("comment"));
                    reply.setCreatedAt(rs.getDate("created_at"));
                    reply.setParentCommentID(rs.getInt("parentCommentID"));
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    reply.setUser(user);
                    replies.add(reply);
                }
            }
        }
        return replies;
    }

    // Method to retrieve likes for a specific blog
    public List<BlogLike> getLikesByBlogID(int blogID) throws SQLException {
        List<BlogLike> likes = new ArrayList<>();
        String sql = "SELECT * FROM bloglike WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    BlogLike like = new BlogLike();
                    like.setLikeID(rs.getInt("likeID"));
                    like.setCreatedAt(rs.getDate("created_at"));
                    // Add logic to retrieve Blog and User associated with the like
                    likes.add(like);
                }
            }
        }
        return likes;
    }

    // Method to retrieve pictures for a specific blog
    public List<BlogPicture> getPicturesByBlogID(int blogID) throws SQLException {
        List<BlogPicture> pictures = new ArrayList<>();
        String sql = "SELECT * FROM blogpicture WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    BlogPicture picture = new BlogPicture();
                    picture.setPictureID(rs.getInt("pictureID"));
                    picture.setPictureURL(rs.getString("pictureURL"));
                    picture.setCreatedAt(rs.getTimestamp("created_at"));
                    // Add logic to retrieve Blog associated with the picture
                    pictures.add(picture);
                }
            }
        }
        return pictures;
    }

    // Method to retrieve a blog by its ID
    public Blog getBlogByID(int blogID) throws SQLException {
        String sql = "SELECT b.*, u.userName FROM blog b INNER JOIN user u ON b.userID = u.userID WHERE b.blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("blogID"));
                    blog.setTitle(rs.getString("title"));
                    blog.setContent(rs.getString("content"));
                    blog.setCreatedAt(rs.getTimestamp("created_at"));
                    // Set the user
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    blog.setUser(user);
                    // Retrieve likes, comments, and pictures
                    blog.setLikes(getLikesByBlogID(blogID));
                    blog.setComments(getCommentsByBlogID(blogID));
                    blog.setPictures(getPicturesByBlogID(blogID));
                    return blog;
                }
            }
        }
        return null;
    }

    public void addLike(int blogID, int userID) throws SQLException {
        String sql = "INSERT INTO bloglike (created_at, blogID, userID) VALUES (NOW(), ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    public void removeLike(int blogID, int userID) throws SQLException {
        String sql = "DELETE FROM bloglike WHERE blogID = ? AND userID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    public int getTotalLikes(int blogID) throws SQLException {
        String sql = "SELECT COUNT(*) FROM bloglike WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, blogID);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        }
        return 0;
    }

    public User getUserByID(int userId) throws SQLException {
        User user = null;
        String query = "SELECT * FROM user WHERE userID = ?";
        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setUserID(resultSet.getInt("userID"));
                user.setUserName(resultSet.getString("userName"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setUserDOB(resultSet.getDate("userDOB"));
                user.setPhoneNumber(resultSet.getString("phoneNumber"));
                user.setAddress(resultSet.getString("address"));
                user.setUserPicture(resultSet.getString("userPicture"));
                user.setRoleID(resultSet.getInt("roleID"));
                user.setStatus(resultSet.getBoolean("status"));
            }
        }
        return user;
    }

    public int getTotalBlogs() throws SQLException {
        String sql = "SELECT COUNT(*) FROM blog";
        try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    public List<Blog> getBlogs(int offset, int limit) throws SQLException {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM blog ORDER BY created_at DESC LIMIT ? OFFSET ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("blogID"));
                    blog.setTitle(rs.getString("title"));
                    blog.setContent(rs.getString("content"));
                    blog.setCreatedAt(rs.getTimestamp("created_at"));
                    blog.setUser(getUserByID(rs.getInt("userID")));
                    blog.setLikes(getLikesByBlogID(blog.getBlogID()));
                    blog.setComments(getCommentsByBlogID(blog.getBlogID()));
                    blog.setPictures(getPicturesByBlogID(blog.getBlogID()));
                    blogs.add(blog);
                }
            }
        }
        return blogs;
    }

    public void updateBlogWithPictures(Blog blog) throws SQLException {
        String sqlUpdate = "UPDATE blog SET title = ?, content = ?, created_at = ? WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlUpdate)) {
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getCreatedAt());
            statement.setInt(4, blog.getBlogID());
            statement.executeUpdate();
        }

        String sqlDeletePictures = "DELETE FROM blogpicture WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeletePictures)) {
            statement.setInt(1, blog.getBlogID());
            statement.executeUpdate();
        }

        String sqlInsertPicture = "INSERT INTO blogpicture (pictureURL, created_at, blogID) VALUES (?, ?, ?)";
        try (PreparedStatement statement = con.prepareStatement(sqlInsertPicture)) {
            for (BlogPicture picture : blog.getPictures()) {
                statement.setString(1, picture.getPictureURL());
                statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                statement.setInt(3, blog.getBlogID());
                statement.addBatch();
            }
            statement.executeBatch();
        }
    }

    public void updateBlogWithoutPictures(Blog blog) throws SQLException {
        String sqlUpdate = "UPDATE blog SET title = ?, content = ?, created_at = ? WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlUpdate)) {
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getCreatedAt());
            statement.setInt(4, blog.getBlogID());
            statement.executeUpdate();
        }
    }

    public boolean deleteBlog(int blogID) throws SQLException {
        String sqlDeleteFavorites = "DELETE FROM blogfavorite WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteFavorites)) {
            statement.setInt(1, blogID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException("Error deleting blog favorites: " + e.getMessage(), e);
        }

        String sqlDeletePictures = "DELETE FROM blogpicture WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeletePictures)) {
            statement.setInt(1, blogID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException("Error deleting blog pictures: " + e.getMessage(), e);
        }

        String sqlDeleteComments = "DELETE FROM blogcomment WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteComments)) {
            statement.setInt(1, blogID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException("Error deleting blog comments: " + e.getMessage(), e);
        }

        String sqlDeleteLikes = "DELETE FROM bloglike WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteLikes)) {
            statement.setInt(1, blogID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException("Error deleting blog likes: " + e.getMessage(), e);
        }

        String sqlDeleteBlog = "DELETE FROM blog WHERE blogID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteBlog)) {
            statement.setInt(1, blogID);
            int rowsAffected = statement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            throw new SQLException("Error deleting blog: " + e.getMessage(), e);
        }
    }

    public List<Blog> getBlogsByUser(int userID, int offset, int limit) throws SQLException {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM blog WHERE userID = ? ORDER BY created_at DESC LIMIT ? OFFSET ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, userID);
            statement.setInt(2, limit);
            statement.setInt(3, offset);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("blogID"));
                    blog.setTitle(rs.getString("title"));
                    blog.setContent(rs.getString("content"));
                    blog.setCreatedAt(rs.getTimestamp("created_at"));
                    blog.setUser(getUserByID(rs.getInt("userID")));
                    blog.setLikes(getLikesByBlogID(blog.getBlogID()));
                    blog.setComments(getCommentsByBlogID(blog.getBlogID()));
                    blog.setPictures(getPicturesByBlogID(blog.getBlogID()));
                    blog.setLiked(isUserLikedBlog(blog.getBlogID(), userID));
                    blogs.add(blog);
                }
            }
        }
        return blogs;
    }

    public List<Blog> getFavoriteBlogsByUser(int userID, int offset, int limit) throws SQLException {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT b.* FROM blog b INNER JOIN blogfavorite bf ON b.blogID = bf.blogID WHERE bf.userID = ? ORDER BY bf.created_at DESC LIMIT ? OFFSET ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, userID);
            statement.setInt(2, limit);
            statement.setInt(3, offset);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("blogID"));
                    blog.setTitle(rs.getString("title"));
                    blog.setContent(rs.getString("content"));
                    blog.setCreatedAt(rs.getTimestamp("created_at"));
                    blog.setUser(getUserByID(rs.getInt("userID")));
                    blog.setLikes(getLikesByBlogID(blog.getBlogID()));
                    blog.setComments(getCommentsByBlogID(blog.getBlogID()));
                    blog.setPictures(getPicturesByBlogID(blog.getBlogID()));
                    blog.setLiked(isUserLikedBlog(blog.getBlogID(), userID));
                    blogs.add(blog);
                }
            }
        }
        return blogs;
    }

    public void updateComment(BlogComment comment) throws SQLException {
        String sql = "UPDATE blogcomment SET comment = ?, created_at = ? WHERE commentID = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, comment.getComment());
            statement.setDate(2, comment.getCreatedAt());
            statement.setInt(3, comment.getCommentID());
            statement.executeUpdate();
        }
    }

    public void deleteComment(int commentID) throws SQLException {
        String sqlDeleteReplies = "DELETE FROM blogcomment WHERE parentCommentID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteReplies)) {
            statement.setInt(1, commentID);
            statement.executeUpdate();
        }

        String sqlDeleteComment = "DELETE FROM blogcomment WHERE commentID = ?";
        try (PreparedStatement statement = con.prepareStatement(sqlDeleteComment)) {
            statement.setInt(1, commentID);
            statement.executeUpdate();
        }
    }
    
    public BlogComment getCommentByID(int commentID) throws SQLException {
    String sql = "SELECT bc.*, u.userName FROM blogcomment bc INNER JOIN user u ON bc.userID = u.userID WHERE bc.commentID = ?";
    try (PreparedStatement statement = con.prepareStatement(sql)) {
        statement.setInt(1, commentID);
        try (ResultSet rs = statement.executeQuery()) {
            if (rs.next()) {
                BlogComment comment = new BlogComment();
                comment.setCommentID(rs.getInt("commentID"));
                comment.setComment(rs.getString("comment"));
                comment.setCreatedAt(rs.getDate("created_at"));
                comment.setParentCommentID(rs.getInt("parentCommentID"));
                User user = new User();
                user.setUserID(rs.getInt("userID"));
                user.setUserName(rs.getString("userName"));
                comment.setUser(user);
                comment.setBlog(getBlogByID(rs.getInt("blogID"))); // Set the associated blog
                comment.setReplies(getRepliesByParentCommentID(comment.getCommentID())); // Fetch replies
                return comment;
            }
        }
    }
    return null;
}
    


}
