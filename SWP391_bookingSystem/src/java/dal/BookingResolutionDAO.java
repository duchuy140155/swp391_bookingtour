/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.BookingResolution;

/**
 *
 * @author Admin
 */
public class BookingResolutionDAO extends DBContext{
public List<BookingResolution> getUnresolvedBookings() {
        List<BookingResolution> unresolvedBookings = new ArrayList<>();
        String sql = "SELECT * FROM BookingResolution WHERE resolutionStatus = 'Chưa giải quyết'";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                BookingResolution bookingResolution = new BookingResolution();
                bookingResolution.setBookingID(rs.getInt("bookingID"));
                bookingResolution.setResolutionStatus(rs.getString("resolutionStatus"));
                unresolvedBookings.add(bookingResolution);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return unresolvedBookings;
    }

    public int countUnresolvedBookings() {
        String sql = "SELECT COUNT(*) FROM BookingResolution WHERE resolutionStatus = 'Chưa giải quyết'";
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void updateResolutionStatus(int bookingID, String resolutionStatus) {
        String sql = "UPDATE BookingResolution SET resolutionStatus = ? WHERE bookingID = ?";
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, resolutionStatus);
            ps.setInt(2, bookingID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addResolutionStatus(int bookingID, String resolutionStatus) {
        String sql = "INSERT INTO BookingResolution (bookingID, resolutionStatus) VALUES (?, ?)";
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, bookingID);
            ps.setString(2, resolutionStatus);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
