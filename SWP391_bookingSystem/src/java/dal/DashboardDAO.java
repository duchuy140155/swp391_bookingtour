package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.TourStatistic;

public class DashboardDAO extends DBContext {

    // Thống kê doanh thu theo tháng
    public List<TourStatistic> getMonthlyRevenue(int partnerId, int year) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT MONTH(b.bookingDate) AS month, YEAR(b.bookingDate) AS year, SUM(b.finalPrice) AS totalRevenue " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? AND YEAR(b.bookingDate) = ? " +
                     "GROUP BY YEAR(b.bookingDate), MONTH(b.bookingDate) " +
                     "ORDER BY YEAR(b.bookingDate), MONTH(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ps.setInt(2, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setMonth(rs.getInt("month"));
                stat.setYear(rs.getInt("year"));
                stat.setTotalRevenue(rs.getDouble("totalRevenue"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Thống kê doanh thu theo năm
    public List<TourStatistic> getYearlyRevenue(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT YEAR(b.bookingDate) AS year, SUM(b.finalPrice) AS totalRevenue " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY YEAR(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setYear(rs.getInt("year"));
                stat.setTotalRevenue(rs.getDouble("totalRevenue"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Thống kê số lượng tour đã bán theo tháng
    public List<TourStatistic> getMonthlyTourCount(int partnerId, int year) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT MONTH(b.bookingDate) AS month, YEAR(b.bookingDate) AS year, COUNT(b.tourID) AS totalTours " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? AND YEAR(b.bookingDate) = ? " +
                     "GROUP BY YEAR(b.bookingDate), MONTH(b.bookingDate) " +
                     "ORDER BY YEAR(b.bookingDate), MONTH(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ps.setInt(2, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setMonth(rs.getInt("month"));
                stat.setYear(rs.getInt("year"));
                stat.setTotalTours(rs.getInt("totalTours"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Thống kê số lượng tour đã bán theo năm
    public List<TourStatistic> getYearlyTourCount(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT YEAR(b.bookingDate) AS year, COUNT(b.tourID) AS totalTours " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY YEAR(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setYear(rs.getInt("year"));
                stat.setTotalTours(rs.getInt("totalTours"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Thống kê số lượng khách hàng tham gia tour theo tháng
    public List<TourStatistic> getMonthlyCustomerCount(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT MONTH(b.bookingDate) AS month, YEAR(b.bookingDate) AS year, SUM(b.totalPeople) AS totalCustomers " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY YEAR(b.bookingDate), MONTH(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setMonth(rs.getInt("month"));
                stat.setYear(rs.getInt("year"));
                stat.setTotalCustomers(rs.getInt("totalCustomers"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Thống kê số lượng khách hàng tham gia tour theo năm
    public List<TourStatistic> getYearlyCustomerCount(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT YEAR(b.bookingDate) AS year, SUM(b.totalPeople) AS totalCustomers " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY YEAR(b.bookingDate)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setYear(rs.getInt("year"));
                stat.setTotalCustomers(rs.getInt("totalCustomers"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    // Lấy tổng số tour đã bán
    public int getTotalToursSold(int partnerId) {
        int totalTours = 0;
        String sql = "SELECT COUNT(b.tourID) AS totalTours " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalTours = rs.getInt("totalTours");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalTours;
    }

    // Lấy tổng doanh thu
    public double getTotalRevenue(int partnerId) {
        double totalRevenue = 0.0;
        String sql = "SELECT SUM(b.finalPrice) AS totalRevenue " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalRevenue = rs.getDouble("totalRevenue");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalRevenue;
    }

    // Lấy tổng số khách hàng tham gia tour
    public int getTotalCustomers(int partnerId) {
        int totalCustomers = 0;
        String sql = "SELECT SUM(b.totalPeople) AS totalCustomers " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalCustomers = rs.getInt("totalCustomers");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalCustomers;
    }

    public String getMostPopularTour(int partnerId) {
        String mostPopularTour = "";
        String sql = "SELECT t.tourName, COUNT(b.tourID) AS totalBookings, SUM(b.finalPrice) AS totalRevenue " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? " +
                     "GROUP BY t.tourName " +
                     "ORDER BY totalBookings DESC, totalRevenue DESC " +
                     "LIMIT 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                mostPopularTour = rs.getString("tourName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mostPopularTour;
    }

    public List<TourStatistic> getCustomerCountByTour(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT t.tourName, SUM(b.totalPeople) AS totalCustomers " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY t.tourName";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setTourName(rs.getString("tourName"));
                stat.setTotalCustomers(rs.getInt("totalCustomers"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }

    public List<TourStatistic> getHighestRevenueTours(int partnerId) {
        List<TourStatistic> stats = new ArrayList<>();
        String sql = "SELECT t.tourName, SUM(b.finalPrice) AS totalRevenue " +
                     "FROM bookingdetails b JOIN tour t ON b.tourID = t.tourID " +
                     "WHERE t.partnerID = ? GROUP BY t.tourName " +
                     "ORDER BY totalRevenue DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, partnerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TourStatistic stat = new TourStatistic();
                stat.setTourName(rs.getString("tourName"));
                stat.setTotalRevenue(rs.getDouble("totalRevenue"));
                stats.add(stat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stats;
    }
}
