/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.RevenueByPartner;

/**
 *
 * @author Admin
 */
public class RevenueByPartnerDAO extends DBContext {

    public RevenueByPartnerDAO() {
        super(); // Gọi constructor của DBContext để kết nối cơ sở dữ liệu
    }

    public List<RevenueByPartner> getRevenueByPartner() {
        List<RevenueByPartner> revenues = new ArrayList<>();
        String query = "SELECT p.partnerName, SUM(b.finalPrice) AS totalRevenue "
                + "FROM bookingsystem.bookingdetails b "
                + "JOIN tour t ON b.tourID = t.tourID "
                + "JOIN partner p ON t.partnerID = p.partnerID "
                + "GROUP BY p.partnerName";

        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                String partnerName = rs.getString("partnerName");
                double totalRevenue = rs.getDouble("totalRevenue");
                revenues.add(new RevenueByPartner(partnerName, totalRevenue));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return revenues;
    }

    // Ví dụ cập nhật phương thức getToursSoldByMonth
    public List<RevenueByPartner> getToursSoldByMonth(int month, int year) {
        List<RevenueByPartner> list = new ArrayList<>();
        String query = "SELECT COUNT(bookingID) AS tourCount, MONTH(bookingDate) AS month "
                + "FROM bookingsystem.bookingdetails "
                + "WHERE MONTH(bookingDate) = ? AND YEAR(bookingDate) = ? "
                + "GROUP BY MONTH(bookingDate)";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, month);
            ps.setInt(2, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int tourCount = rs.getInt("tourCount");
                int resultMonth = rs.getInt("month");
                list.add(new RevenueByPartner(null, 0, tourCount, resultMonth, null, 0, 0));  // Sử dụng constructor mới
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<RevenueByPartner> getTotalCustomersByMonth(int month, int year) {
        List<RevenueByPartner> list = new ArrayList<>();
        String query = "SELECT SUM(totalPeople) AS totalCustomers, MONTH(bookingDate) AS month "
                + "FROM bookingsystem.bookingdetails "
                + "WHERE MONTH(bookingDate) = ? AND YEAR(bookingDate) = ? "
                + "GROUP BY MONTH(bookingDate)";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, month);
            ps.setInt(2, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int totalCustomers = rs.getInt("totalCustomers");
                int resultMonth = rs.getInt("month");
                // Sử dụng constructor đầy đủ, truyền giá trị null hoặc 0 cho các tham số không có
                list.add(new RevenueByPartner(null, 0, 0, resultMonth, null, 0, totalCustomers));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<RevenueByPartner> getToursSoldByYear(int year) {
        List<RevenueByPartner> list = new ArrayList<>();
        String query = "SELECT MONTH(bookingDate) AS month, COUNT(bookingID) AS tourCount "
                + "FROM bookingsystem.bookingdetails "
                + "WHERE YEAR(bookingDate) = ? "
                + "GROUP BY MONTH(bookingDate) "
                + "ORDER BY MONTH(bookingDate)";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int tourCount = rs.getInt("tourCount");
                int resultMonth = rs.getInt("month");
                list.add(new RevenueByPartner(null, 0, tourCount, resultMonth, null, 0, 0));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<RevenueByPartner> getTotalCustomersByYear(int year) {
        List<RevenueByPartner> list = new ArrayList<>();
        String query = "SELECT MONTH(bookingDate) AS month, SUM(totalPeople) AS totalCustomers "
                + "FROM bookingsystem.bookingdetails "
                + "WHERE YEAR(bookingDate) = ? "
                + "GROUP BY MONTH(bookingDate) "
                + "ORDER BY MONTH(bookingDate)";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int totalCustomers = rs.getInt("totalCustomers");
                int resultMonth = rs.getInt("month");
                list.add(new RevenueByPartner(null, 0, 0, resultMonth, null, 0, totalCustomers));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    // Phương thức lấy doanh thu của từng partner theo tháng
    public List<RevenueByPartner> getRevenueByPartnerByMonth() {
        List<RevenueByPartner> list = new ArrayList<>();
        String query = "SELECT p.partnerName, MONTH(b.bookingDate) AS month, SUM(b.finalPrice) AS totalRevenue "
                + "FROM bookingsystem.bookingdetails b "
                + "JOIN bookingsystem.tour t ON b.tourID = t.tourID "
                + "JOIN bookingsystem.partner p ON t.partnerID = p.partnerID "
                + "GROUP BY p.partnerName, MONTH(b.bookingDate) "
                + "ORDER BY MONTH(b.bookingDate), p.partnerName";

        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                String partnerName = rs.getString("partnerName");
                int month = rs.getInt("month");
                double totalRevenue = rs.getDouble("totalRevenue");
                list.add(new RevenueByPartner(partnerName, totalRevenue, 0, month, null, 0, 0));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public void close() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueByPartnerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//    
//    public static void main(String[] args) {
//    RevenueByPartnerDAO revenueDAO = new RevenueByPartnerDAO();
//
//    // Kiểm tra phương thức getRevenueByPartner
//    List<RevenueByPartner> revenueByPartnerList = revenueDAO.getRevenueByPartner();
//    System.out.println("Revenue By Partner:");
//    for (RevenueByPartner revenue : revenueByPartnerList) {
//        System.out.println("Partner: " + revenue.getPartnerName() + ", Total Revenue: " + revenue.getTotalRevenue());
//    }
//
//    // Kiểm tra phương thức getToursSoldByMonth cho tháng 7 năm 2024
//    List<RevenueByPartner> toursSoldByMonth = revenueDAO.getToursSoldByMonth(7, 2024);
//    System.out.println("\nTours Sold By Month:");
//    for (RevenueByPartner tour : toursSoldByMonth) {
//        System.out.println("Month: " + tour.getMonth() + ", Tours Sold: " + tour.getTourCount());
//    }
//
//    // Kiểm tra phương thức getTotalCustomersByMonth cho tháng 7 năm 2024
//    List<RevenueByPartner> totalCustomersByMonth = revenueDAO.getTotalCustomersByMonth(7, 2024);
//    System.out.println("\nTotal Customers By Month:");
//    for (RevenueByPartner customer : totalCustomersByMonth) {
//        System.out.println("Month: " + customer.getMonth() + ", Total Customers: " + customer.getTotalCustomers());
//    }
//
//    revenueDAO.close();
//}

//      public static void main(String[] args) {
//        RevenueByPartnerDAO revenueDAO = new RevenueByPartnerDAO();
//        int year = 2024;
//
//        // Kiểm tra phương thức getToursSoldByYear
//        List<RevenueByPartner> toursSoldByYear = revenueDAO.getToursSoldByYear(year);
//        for (RevenueByPartner tour : toursSoldByYear) {
//            System.out.println("Tháng: " + tour.getMonth() + ", Số tour: " + tour.getTourCount());
//        }
//
//        // Kiểm tra phương thức getTotalCustomersByYear
//        List<RevenueByPartner> totalCustomersByYear = revenueDAO.getTotalCustomersByYear(year);
//        for (RevenueByPartner customer : totalCustomersByYear) {
//            System.out.println("Tháng: " + customer.getMonth() + ", Tổng số khách hàng: " + customer.getTotalCustomers());
//        }
//
//        revenueDAO.close();
//    }
    public static void main(String[] args) {
        RevenueByPartnerDAO revenueDAO = new RevenueByPartnerDAO();
        List<RevenueByPartner> revenuesByMonth = revenueDAO.getRevenueByPartnerByMonth();

        for (RevenueByPartner revenue : revenuesByMonth) {
            System.out.println("Partner: " + revenue.getPartnerName() + ", Month: " + revenue.getMonth() + ", Revenue: " + revenue.getTotalRevenue());
        }

        revenueDAO.close();
    }

}
