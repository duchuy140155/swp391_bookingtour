/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.Tour;
import model.BookingDetails;
import model.Discount;
import model.Ticket;
import model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;

/**
 *
 * @author Admin
 */
public class BookingDAO extends DBContext {

    public Tour getTourByID(int tourID) {
        Tour tour = null;
        String query = "SELECT * FROM Tour WHERE tourID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    tour = new Tour(
                            rs.getInt("tourID"),
                            rs.getString("tourName"),
                            rs.getString("tourDescription"),
                            rs.getString("startLocation"),
                            rs.getString("endLocation"),
                            rs.getDate("startDate"),
                            rs.getDate("endDate"),
                            rs.getDouble("price"),
                            rs.getInt("numberOfPeople"),
                            rs.getString("thumbnails"),
                            rs.getBoolean("status")
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tour;
    }

    public int saveBooking(BookingDetails bookingDetails) {
        String query = "INSERT INTO bookingdetails (finalPrice, note, email, phoneNumber, customerAddress, tourID, userID, status, bookingDate, customerName, paymentMethod,totalPeople,discountID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
        try (PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setDouble(1, bookingDetails.getFinalPrice());
            ps.setString(2, bookingDetails.getNote());
            ps.setString(3, bookingDetails.getEmail());
            ps.setInt(4, bookingDetails.getPhoneNumber());
            ps.setString(5, bookingDetails.getCustomerAddress());
            ps.setInt(6, bookingDetails.getTour().getTourID());
            if (bookingDetails.getUserID() != null) {
                ps.setInt(7, bookingDetails.getUserID().getUserID());
            } else {
                ps.setNull(7, java.sql.Types.INTEGER);
            }
            ps.setInt(8, bookingDetails.getStatus());
            ps.setDate(9, bookingDetails.getBookingDate());
            ps.setString(10, bookingDetails.getCustomerName());
            ps.setString(11, bookingDetails.getPaymentMethod());
            ps.setInt(12, bookingDetails.getTotalPeople());
            if (bookingDetails.getDiscountID() != null) {
                ps.setInt(13, bookingDetails.getDiscountID().getDiscountID());
            } else {
                ps.setNull(13, java.sql.Types.INTEGER);
            }
            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating booking failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
   
    public List<BookingDetails> getBookingsByTourID(int tourID) {
        List<BookingDetails> bookings = new ArrayList<>();
        String query = "SELECT * FROM bookingdetails WHERE tourID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    BookingDetails booking = new BookingDetails();
                    booking.setBookingID(rs.getInt("bookingID"));
                    booking.setFinalPrice(rs.getDouble("finalPrice"));
                    booking.setNote(rs.getString("note"));
                    booking.setEmail(rs.getString("email"));
                    booking.setPhoneNumber(rs.getInt("phoneNumber"));
                    booking.setCustomerAddress(rs.getString("address"));
                    booking.setStatus(rs.getInt("status"));
                    booking.setBookingDate(rs.getDate("bookingDate"));

                    // Retrieve Tour
                    int tourId = rs.getInt("tourID");
                    Tour tour = getTourByID(tourId);
                    booking.setTour(tour);

                    // Các phần khác
                    bookings.add(booking);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookings;
    }

    public void saveTourCode(int bookingID, String tourCode) throws SQLException {
        String insertCodeSQL = "INSERT INTO booking_codes (bookingID, tourCode) VALUES (?, ?)";
        try (PreparedStatement insertCodeStmt = connection.prepareStatement(insertCodeSQL)) {
            insertCodeStmt.setInt(1, bookingID);
            insertCodeStmt.setString(2, tourCode);
            insertCodeStmt.executeUpdate();
        }
    }

    public boolean checkPin(String tourCode, int tourID) {
        String query = "SELECT 1 FROM booking_codes "
                + "JOIN bookingdetails ON booking_codes.bookingID = bookingdetails.bookingID "
                + "WHERE tourCode = ? AND bookingdetails.tourID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, tourCode);
            preparedStatement.setInt(2, tourID);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();  // If a row is returned, it means the pin and tourID are valid.
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;  // In case of an exception, return false.
    }

//
//    public List<BookingDetails> getBillHistoriesByUserID(int userID) {
//        List<BookingDetails> billHistories = new ArrayList<>();
//        String query = "SELECT bd.finalPrice, bd.bookingDate, bd.totalPeople, t.tourName, bd.paymentMethod "
//                + "FROM BookingDetails bd "
//                + "JOIN Tour t ON bd.tourID = t.tourID "
//                + "WHERE bd.userID = ? AND bd.status = true"; // Assuming status true indicates completed bookings
//
//        try (PreparedStatement ps = connection.prepareStatement(query)) {
//            ps.setInt(1, userID);
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()) {
//                BookingDetails booking = new BookingDetails();
//                booking.setFinalPrice(rs.getDouble("finalPrice"));
//                booking.setBookingDate(rs.getDate("bookingDate"));
//                booking.setTotalPeople(rs.getInt("totalPeople"));
//                booking.setTourName(rs.getString("tourName"));
//                booking.setPaymentMethod(rs.getString("paymentMethod"));
//
//                billHistories.add(booking);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return billHistories;
//    }
    public List<BookingDetails> getBillHistoriesByUserID(int userID, int start, int total) {
        List<BookingDetails> billHistories = new ArrayList<>();
        String query = "SELECT bd.finalPrice, bd.bookingDate, bd.totalPeople, t.tourID, t.tourName, bd.paymentMethod FROM BookingDetails bd JOIN Tour t ON bd.tourID = t.tourID WHERE bd.userID = ? AND bd.status = true LIMIT ?, ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            ps.setInt(2, start);
            ps.setInt(3, total);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BookingDetails booking = new BookingDetails();
                booking.setFinalPrice(rs.getDouble("finalPrice"));
                booking.setBookingDate(rs.getDate("bookingDate"));
                booking.setTotalPeople(rs.getInt("totalPeople"));
                booking.setPaymentMethod(rs.getString("paymentMethod"));

                Tour tour = new Tour();
                tour.setTourID(rs.getInt("tourID"));
                tour.setTourName(rs.getString("tourName"));
                booking.setTour(tour);

                billHistories.add(booking);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return billHistories;
    }

    public int countBillHistoriesByUserID(int userID) {
        int count = 0;
        String query = "SELECT COUNT(*) FROM BookingDetails WHERE userID = ? AND status = true";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public User getUserById(int userId) throws SQLException {
        User user = null;
        String query = "SELECT * FROM user WHERE userID = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setUserID(resultSet.getInt("userID"));
                user.setUserName(resultSet.getString("userName"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setUserDOB(resultSet.getDate("userDOB"));
                user.setPhoneNumber(resultSet.getString("phoneNumber"));
                user.setAddress(resultSet.getString("address"));
                user.setUserPicture(resultSet.getString("userPicture"));
                user.setRoleID(resultSet.getInt("roleID"));
                user.setStatus(resultSet.getBoolean("status"));
            }
        }
        return user;
    }
     public boolean checkPin(String pin) {
        String query = "SELECT COUNT(*) FROM booking_codes WHERE tourCode = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, pin);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
