package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Schedule;
import model.ScheduleDetails;

public class ScheduleDAO extends DBContext {

    private Connection con;

    public ScheduleDAO() {
        con = new DBContext().connection;
    }

    // create
    public int addSchedule(Schedule schedule) throws SQLException {
        String sql = "INSERT INTO Schedule (scheduleTitle, scheduleContent, scheduleDate, tourID) VALUES (?, ?, ?, ?)";
        int scheduleID = -1;
        try (PreparedStatement ps = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, schedule.getScheduleTitle());
            ps.setString(2, schedule.getScheduleContent());
            ps.setDate(3, new Date(schedule.getScheduleDate().getTime()));
            ps.setInt(4, schedule.getTourID());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                scheduleID = rs.getInt(1);
            }
        }
        return scheduleID;
    }

    // Read 
    public List<Schedule> getTourSchedules(int tourID) throws SQLException {
        List<Schedule> schedules = new ArrayList<>();
        String sql = "SELECT s.scheduleID, s.scheduleTitle, s.scheduleContent, s.scheduleDate, s.tourID, "
                + "sd.detailID, sd.detailTitle, sd.detailContent, sd.note "
                + "FROM Schedule s "
                + "LEFT JOIN ScheduleDetails sd ON s.scheduleID = sd.scheduleID "
                + "WHERE s.tourID = ? "
                + "ORDER BY s.scheduleDate";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                Schedule currentSchedule = null;
                while (rs.next()) {
                    int scheduleID = rs.getInt("scheduleID");
                    if (currentSchedule == null || currentSchedule.getScheduleID() != scheduleID) {
                        if (currentSchedule != null) {
                            schedules.add(currentSchedule);
                        }
                        currentSchedule = new Schedule(
                                rs.getInt("scheduleID"),
                                rs.getString("scheduleTitle"),
                                rs.getString("scheduleContent"),
                                rs.getInt("tourID"),
                                rs.getDate("scheduleDate")
                        );
                        currentSchedule.setDetails(new ArrayList<>());
                    }
                    if (rs.getInt("detailID") != 0) {
                        ScheduleDetails detail = new ScheduleDetails(
                                rs.getInt("detailID"),
                                rs.getInt("scheduleID"),
                                rs.getString("detailTitle"),
                                rs.getString("detailContent"),
                                rs.getString("note")
                        );
                        currentSchedule.getDetails().add(detail);
                    }
                }
                if (currentSchedule != null) {
                    schedules.add(currentSchedule);
                }
            }
        }
        return schedules;
    }

    public void updateSchedule(Schedule schedule) throws SQLException {
        String updateScheduleQuery = "UPDATE schedule SET scheduleTitle = ?, scheduleContent = ?, scheduleDate = ? WHERE scheduleID = ?";
        try (PreparedStatement stmt = con.prepareStatement(updateScheduleQuery)) {
            stmt.setString(1, schedule.getScheduleTitle());
            stmt.setString(2, schedule.getScheduleContent());
            stmt.setDate(3, new java.sql.Date(schedule.getScheduleDate().getTime()));
            stmt.setInt(4, schedule.getScheduleID());
            stmt.executeUpdate();
        }

        String updateDetailQuery = "UPDATE scheduledetails SET detailTitle = ?, detailContent = ?, note = ? WHERE scheduleID = ?";
        try (PreparedStatement stmt = con.prepareStatement(updateDetailQuery)) {
            for (ScheduleDetails detail : schedule.getDetails()) {
                stmt.setString(1, detail.getDetailTitle());
                stmt.setString(2, detail.getDetailContent());
                stmt.setString(3, detail.getNote());
                stmt.setInt(4, schedule.getScheduleID());
                stmt.executeUpdate();
            }
        }
    }

    public void deleteSchedule(int scheduleID) throws SQLException {
        String deleteDetailsSql = "DELETE FROM ScheduleDetails WHERE scheduleID = ?";
        String deleteScheduleSql = "DELETE FROM Schedule WHERE scheduleID = ?";

        try (PreparedStatement ps1 = con.prepareStatement(deleteDetailsSql); PreparedStatement ps2 = con.prepareStatement(deleteScheduleSql)) {
            ps1.setInt(1, scheduleID);
            ps1.executeUpdate();

            ps2.setInt(1, scheduleID);
            ps2.executeUpdate();
        }
    }

    public int addScheduleDetails(ScheduleDetails details) throws SQLException {
        String sql = "INSERT INTO ScheduleDetails (scheduleID, detailTitle, detailContent, note) VALUES (?, ?, ?, ?)";
        int detailID = -1;
        try (PreparedStatement ps = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, details.getScheduleID());
            ps.setString(2, details.getDetailTitle());
            ps.setString(3, details.getDetailContent());
            ps.setString(4, details.getNote());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                detailID = rs.getInt(1);
            }
        }
        return detailID;
    }

    public List<ScheduleDetails> getDetailsByScheduleID(int scheduleID) throws SQLException {
        List<ScheduleDetails> details = new ArrayList<>();
        String sql = "SELECT detailID, scheduleID, detailTitle, detailContent, note FROM ScheduleDetails WHERE scheduleID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, scheduleID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    ScheduleDetails detail = new ScheduleDetails(
                            rs.getInt("detailID"),
                            rs.getInt("scheduleID"),
                            rs.getString("detailTitle"),
                            rs.getString("detailContent"),
                            rs.getString("note")
                    );
                    details.add(detail);
                }
            }
        }
        return details;
    }

    public Schedule getScheduleByID(int scheduleID) throws SQLException {
        String query = "SELECT * FROM schedule WHERE scheduleID = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, scheduleID);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    Schedule schedule = new Schedule();
                    schedule.setScheduleID(rs.getInt("scheduleID"));
                    schedule.setScheduleTitle(rs.getString("scheduleTitle"));
                    schedule.setScheduleContent(rs.getString("scheduleContent"));
                    schedule.setScheduleDate(rs.getDate("scheduleDate"));
                    schedule.setTourID(rs.getInt("tourID"));
                    return schedule;
                }
            }
        }
        return null;
    }

    public void updateScheduleDetails(ScheduleDetails details) throws SQLException {
        String sql = "UPDATE ScheduleDetails SET detailTitle = ?, detailContent = ?, note = ? WHERE detailID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, details.getDetailTitle());
            ps.setString(2, details.getDetailContent());
            ps.setString(3, details.getNote());
            ps.setInt(4, details.getDetailID());
            ps.executeUpdate();
        }
    }

    // Add this method to get tour start date
    public Date getTourStartDate(int tourID) throws SQLException {
        String sql = "SELECT startDate FROM Tour WHERE tourID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getDate("startDate");
                }
            }
        }
        return null;
    }

public Date getStartDateByTourID(int tourID) throws SQLException {
    String query = "SELECT startDate FROM tour WHERE tourID = ?";
    try (PreparedStatement ps = connection.prepareStatement(query)) {
        ps.setInt(1, tourID);
        try (ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getDate("startDate");
            } else {
                return null;
            }
        }
    }
}

}
