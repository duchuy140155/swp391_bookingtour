/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.News;
import model.NewsImage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class NewsDAO extends DBContext {

    public List<News> getAllNews() {
        List<News> newsList = new ArrayList<>();
        String query = "SELECT * FROM news";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setIdNew(rs.getInt("idNew"));
                news.setTitle(rs.getString("title"));
                news.setShortDescription(rs.getString("shortDescription"));
                news.setLongDescription(rs.getString("longDescription"));
                news.setPublishDate(rs.getTimestamp("publishDate"));
                news.setImageTheme(rs.getString("imageTheme"));
                news.setPartnerID(rs.getInt("partnerID"));
                newsList.add(news);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newsList;
    }

    public News getNewsById(int idNew) {
        News news = null;
        String query = "SELECT * FROM news WHERE idNew = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idNew);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                news = new News();
                news.setIdNew(rs.getInt("idNew"));
                news.setTitle(rs.getString("title"));
                news.setShortDescription(rs.getString("shortDescription"));
                news.setLongDescription(rs.getString("longDescription"));
                news.setPublishDate(rs.getTimestamp("publishDate"));
                news.setImageTheme(rs.getString("imageTheme"));
                news.setPartnerID(rs.getInt("partnerID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return news;
    }

    public int addNews(News news) {
        String query = "INSERT INTO news (title, shortDescription, longDescription, publishDate, imageTheme, partnerID) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortDescription());
            ps.setString(3, news.getLongDescription());
            ps.setTimestamp(4, new java.sql.Timestamp(news.getPublishDate().getTime()));
            ps.setString(5, news.getImageTheme());
            ps.setInt(6, news.getPartnerID());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void updateNews(News news) {
        String query = "UPDATE news SET title = ?, shortDescription = ?, longDescription = ?, publishDate = ?, imageTheme = ?, partnerID = ? WHERE idNew = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortDescription());
            ps.setString(3, news.getLongDescription());
            ps.setTimestamp(4, new java.sql.Timestamp(news.getPublishDate().getTime()));
            ps.setString(5, news.getImageTheme());
            ps.setInt(6, news.getPartnerID());
            ps.setInt(7, news.getIdNew());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteNews(int idNew) {
        String query = "DELETE FROM news WHERE idNew = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idNew);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<News> getNewsByPage(int page, int pageSize) {
        List<News> newsList = new ArrayList<>();
        String query = "SELECT * FROM news LIMIT ? OFFSET ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, pageSize);
            ps.setInt(2, (page - 1) * pageSize);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setIdNew(rs.getInt("idNew"));
                news.setTitle(rs.getString("title"));
                news.setShortDescription(rs.getString("shortDescription"));
                news.setLongDescription(rs.getString("longDescription"));
                news.setPublishDate(rs.getTimestamp("publishDate"));
                news.setImageTheme(rs.getString("imageTheme"));
                news.setPartnerID(rs.getInt("partnerID"));
                newsList.add(news);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newsList;
    }

    public int getNewsCount() {
        int count = 0;
        String query = "SELECT COUNT(*) FROM news";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<News> searchNewsByTitle(String query, int page, int pageSize) {
        List<News> newsList = new ArrayList<>();
        String sql = "SELECT * FROM news WHERE title LIKE ? LIMIT ? OFFSET ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "%" + query + "%");
            ps.setInt(2, pageSize);
            ps.setInt(3, (page - 1) * pageSize);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setIdNew(rs.getInt("idNew"));
                news.setTitle(rs.getString("title"));
                news.setShortDescription(rs.getString("shortDescription"));
                news.setLongDescription(rs.getString("longDescription"));
                news.setPublishDate(rs.getTimestamp("publishDate"));
                news.setImageTheme(rs.getString("imageTheme"));
                news.setPartnerID(rs.getInt("partnerID"));
                newsList.add(news);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newsList;
    }

    public int getSearchNewsCount(String query) {
        int count = 0;
        String sql = "SELECT COUNT(*) FROM news WHERE title LIKE ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "%" + query + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public void addNewsImage(NewsImage newsImage) {
        String query = "INSERT INTO newsImage (idNew, newsImageURL) VALUES (?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, newsImage.getIdNew());
            ps.setString(2, newsImage.getNewsImageURL());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
