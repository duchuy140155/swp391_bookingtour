/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.hotels;

/**
 *
 * @author Admin
 */
public class HotelDAO extends DBContext {

    public List<hotels> loadAllHotel() {
        List<hotels> list = new ArrayList<>();
        String sql = "select * from hotels";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new hotels(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                rs.getString(8)
                ));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public hotels getHotelById(int id) {
        hotels hotel = new hotels();
        String sql = "select * from hotels where hotel_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                hotel = new hotels(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                );
                break;
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return hotel;
    }

    public List<hotels> getHotelByName(String name) {
        List<hotels> list = new ArrayList<>();
    String sql = "SELECT * FROM hotels WHERE name LIKE ? OR name = ?";
    try (PreparedStatement st = connection.prepareStatement(sql)) {
        st.setString(1, "%" + name + "%");
        st.setString(2, name);
        try (ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                list.add(new hotels(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                rs.getString(8)));
            }
        }
    } catch (Exception e) {
        System.out.println("Error: " + e.getMessage());
        e.printStackTrace();
    }
    return list;
    }

    public List<hotels> getHotelByLocation(String name) {
    List<hotels> list = new ArrayList<>();
    String sql = "SELECT * FROM hotels WHERE city LIKE ? OR city = ?";
    try (PreparedStatement st = connection.prepareStatement(sql)) {
        st.setString(1, "%" + name + "%");
        st.setString(2, name);
        try (ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                list.add(new hotels(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                ));
            }
        }
    } catch (Exception e) {
        System.out.println("Error: " + e.getMessage());
        e.printStackTrace();
    }
    return list;
}


    public void updateHotelById(hotels hotel) {
        String sql = "UPDATE hotels SET name=?, address=?, city=?, phone=?, email=?, website=?  WHERE hotel_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, hotel.getName());
            st.setString(2, hotel.getAddress());
            st.setString(3, hotel.getCity());
            st.setString(4, hotel.getPhone());
            st.setString(5, hotel.getEmail());
            st.setString(6, hotel.getWebsite());
            st.setInt(7, hotel.getHotel_id());
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace(); // Log exception
        }

    }
    

    public int getMaxHotelId() {
        int max = 0;
        String sql = "SELECT max(hotels.hotel_id) FROM hotels";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;

    }

   public void insertHotel(hotels hotel) {
    String sql = "INSERT INTO hotels (hotel_id, name, address, city, phone, email, website, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, hotel.getHotel_id());
        st.setString(2, hotel.getName());
        st.setString(3, hotel.getAddress());
        st.setString(4, hotel.getCity());
        st.setString(5, hotel.getPhone());
        st.setString(6, hotel.getEmail());
        st.setString(7, hotel.getWebsite());
        st.setString(8, hotel.getImage());
        st.executeUpdate();
    } catch (Exception e) {
        System.out.println(e);
    }
}



    public void DeleteHotel(int id) {
        RoomDAO d = new RoomDAO();
        
        
        d.deleteRoomByHotelId(id);

        String sql = "DELETE FROM hotels WHERE hotel_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public static void main(String[] args) {
        HotelDAO d = new HotelDAO();
        System.out.println(d.getHotelById(2));
    }

}
