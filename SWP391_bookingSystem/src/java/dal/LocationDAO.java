/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Location;

/**
 *
 * @author Admin
 */
public class LocationDAO extends DBContext {
     public boolean checkLocationExist(String city) {
        boolean check = true;
        int count = 0;
        String sql = "select * from location where locationName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, city);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                count++;
            }
            if (count != 0) {
                check = true;

            }

            if (count == 0) {
                check = false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return check;
    }


    public Location getLocationByID(int locationID) {
        Location location = null;
        String query = "SELECT * FROM location WHERE locationID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, locationID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                location = new Location();
                location.setLocationId(rs.getInt("locationID"));
                location.setLocationName(rs.getString("locationName"));
                location.setLocationDescription(rs.getString("locationDescription"));
                location.setLocationImage(rs.getString("locationImage"));
                location.setTourId(rs.getInt("tourID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return location;
    }

    public List<Location> getAllLocations() {
        List<Location> locations = new ArrayList<>();
        String query = "SELECT * FROM location";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("locationID"));
                location.setLocationName(rs.getString("locationName"));
                location.setLocationDescription(rs.getString("locationDescription"));
                location.setLocationImage(rs.getString("locationImage"));
                location.setTourId(rs.getInt("tourID"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public void addLocation(Location location) {
        String query = "INSERT INTO location (locationName, locationDescription, locationImage, tourID) VALUES (?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, location.getLocationName());
            ps.setString(2, location.getLocationDescription());
            ps.setString(3, location.getLocationImage());
            ps.setInt(4, location.getTourId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateLocation(Location location) {
        String query = "UPDATE location SET locationName = ?, locationDescription = ?, locationImage = ?, tourID = ? WHERE locationID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, location.getLocationName());
            ps.setString(2, location.getLocationDescription());
            ps.setString(3, location.getLocationImage());
            ps.setInt(4, location.getTourId());
            ps.setInt(5, location.getLocationId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteLocation(int locationID) {
        String query = "DELETE FROM location WHERE locationID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, locationID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Location> getLocationsWithPagination(int offset, int limit) {
        List<Location> locations = new ArrayList<>();
        String query = "SELECT * FROM location LIMIT ? OFFSET ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("locationID"));
                location.setLocationName(rs.getString("locationName"));
                location.setLocationDescription(rs.getString("locationDescription"));
                location.setLocationImage(rs.getString("locationImage"));
                location.setTourId(rs.getInt("tourID"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public int countAllLocations() {
        String query = "SELECT COUNT(*) AS total FROM location";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Location> searchLocations(String keyword, int offset, int limit) {
        List<Location> locations = new ArrayList<>();
        String query = "SELECT * FROM location WHERE locationName LIKE ? ORDER BY locationName ASC LIMIT ? OFFSET ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, "%" + keyword + "%");
            ps.setInt(2, limit);
            ps.setInt(3, offset);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("locationID"));
                location.setLocationName(rs.getString("locationName"));
                location.setLocationDescription(rs.getString("locationDescription"));
                location.setLocationImage(rs.getString("locationImage"));
                location.setTourId(rs.getInt("tourID"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public int countLocations(String search) {
        String query = "SELECT COUNT(*) AS total FROM location WHERE locationName LIKE ? OR locationDescription LIKE ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Location> getLocationsWithPaginationAndSorting(int page, int pageSize, String sortField, String sortOrder) {
        List<Location> locations = new ArrayList<>();
        int offset = (page - 1) * pageSize;
        String query = "SELECT * FROM location ORDER BY " + sortField + " " + sortOrder + " LIMIT ? OFFSET ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, pageSize);
            ps.setInt(2, offset);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("locationID"));
                location.setLocationName(rs.getString("locationName"));
                location.setLocationDescription(rs.getString("locationDescription"));
                location.setLocationImage(rs.getString("locationImage"));
                location.setTourId(rs.getInt("tourID"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public int getTotalLocations() {
        String query = "SELECT COUNT(*) AS total FROM location";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalSearchedLocations(String search) {
        String query = "SELECT COUNT(*) AS total FROM location WHERE locationName LIKE ? OR locationDescription LIKE ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
