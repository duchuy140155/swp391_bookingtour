/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import model.Feedback;
import model.Tour;
import model.User;

/**
 *
 * @author Admin
 */
public class FeedbackDAO extends DBContext {

    public List<Feedback> getFeedbackByTourID(int tourID) {
        UserDAO userDao = new UserDAO();
        TourDAO tourDao = new TourDAO();
        List<Feedback> feedbacks = new ArrayList<>();
        String query = "SELECT * FROM bookingsystem.feedback\n"
                + "where tourID =? \n"
                + "ORDER BY createDate DESC;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, tourID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("feedbackID");
                String feedbackContent = rs.getString("feedbackContent");
                int userID = rs.getInt("userID");
                Date createDate = rs.getDate("createDate");
                User user = userDao.getUserById(userID);
                Tour tour = tourDao.getTourByID(tourID);
                Feedback feedback = new Feedback(id, feedbackContent, user, tour, createDate);
                feedbacks.add(feedback);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbacks;
    }

    public void insertFeedback(String content, int userID, int tourID, Date createDate) {
        String sql = "INSERT INTO feedback (feedbackContent, userID, tourID, createDate) VALUES (?, ?, ?, ?)";
        try (PreparedStatement pst = connection.prepareStatement(sql)) {
            pst.setString(1, content);
            pst.setInt(2, userID);
            pst.setInt(3, tourID);
            pst.setDate(4, createDate);
            pst.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Feedback selectFeedback(int id) {
        UserDAO userDao = new UserDAO();
        TourDAO tourDao = new TourDAO();
        Feedback feedback = null;
        String sql = "SELECT * FROM bookingsystem.feedback\n"
                + "WHERE feedbackID = ?;";
        try (PreparedStatement pst = connection.prepareCall(sql)) {
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                String content = rs.getString("feedbackContent");
                int userId = rs.getInt("userID");
                int tourId = rs.getInt("tourID");
                Date createDate = rs.getDate("createDate");
                User user = userDao.getUserById(userId);
                Tour tour = tourDao.getTourByID(tourId);
                feedback = new Feedback(id, content, user, tour, createDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedback;
    }

    public boolean updateFeedback(Feedback feedback) {
        boolean rowUpdated = false;
        String sql = "UPDATE feedback SET feedbackContent = ? WHERE feedbackID = ?;";
        try (PreparedStatement pst = connection.prepareStatement(sql)) {
            pst.setString(1, feedback.getFeedbackContent());
            pst.setInt(2, feedback.getFeedbackID());
            rowUpdated = pst.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowUpdated;
    }

    public boolean deleteFeedback(int id) {
        boolean rowDeleted = false;
        String sql = "DELETE FROM feedback WHERE feedbackID = ?;";
        try (PreparedStatement pst = connection.prepareStatement(sql)) {
            pst.setInt(1, id);
            rowDeleted = pst.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowDeleted;
    }
}
