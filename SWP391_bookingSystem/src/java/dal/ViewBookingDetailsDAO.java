/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.BookingDetails;
import model.Discount;
import model.Ticket;
import model.Tour;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Discount;
import model.Ticket;
import model.TicketType;
import model.Tour;
import model.User;

/**
 *
 * @author Admin
 */
public class ViewBookingDetailsDAO extends DBContext {

    public BookingDetails getBookingDetailsById(int bookingID) {
        BookingDetails bookingDetails = null;
        String query = "SELECT bd.*, t.tourName, t.startLocation, t.endLocation, t.startDate, t.endDate, t.price, d.discountName "
                + "FROM bookingdetails bd "
                + "JOIN tour t ON bd.tourID = t.tourID "
                + "LEFT JOIN discount d ON bd.discountID = d.discountID "
                + "WHERE bd.bookingID = ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, bookingID);
            System.out.println("Executing query: " + ps);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    bookingDetails = new BookingDetails();
                    bookingDetails.setBookingID(rs.getInt("bookingID"));
                    bookingDetails.setCustomerName(rs.getString("customerName"));
                    bookingDetails.setEmail(rs.getString("email"));
                    bookingDetails.setPhoneNumber(rs.getInt("phoneNumber"));
                    bookingDetails.setCustomerAddress(rs.getString("customerAddress"));
                    bookingDetails.setNote(rs.getString("note"));
                    bookingDetails.setPaymentMethod(rs.getString("paymentMethod"));
                    bookingDetails.setFinalPrice(rs.getDouble("finalPrice"));
                    bookingDetails.setBookingDate(rs.getDate("bookingDate"));
                    bookingDetails.setStatus(rs.getInt("status"));
                    bookingDetails.setTotalPeople(rs.getInt("totalPeople"));

                    Tour tour = new Tour();
                    tour.setTourName(rs.getString("tourName"));
                    tour.setStartLocation(rs.getString("startLocation"));
                    tour.setEndLocation(rs.getString("endLocation"));
                    tour.setStartDate(rs.getDate("startDate"));
                    tour.setEndDate(rs.getDate("endDate"));
                    tour.setPrice(rs.getDouble("price"));
                    bookingDetails.setTour(tour);

                    Discount discount = new Discount();
                    discount.setDiscountName(rs.getString("discountName"));
                    bookingDetails.setDiscountID(discount);

                    // Lấy thông tin hành khách từ bảng ticket
                    List<Ticket> tickets = getTicketsByBookingID(bookingID);
                    bookingDetails.setTickets(tickets);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookingDetails;
    }

    private List<Ticket> getTicketsByBookingID(int bookingID) {
        List<Ticket> tickets = new ArrayList<>();
        String query = "SELECT * FROM ticket WHERE bookingID = ?";
        TicketDAO ticketDao = new TicketDAO();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, bookingID);
            System.out.println("Executing query: " + ps);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Ticket ticket = new Ticket();
                    ticket.setTicketID(rs.getInt("ticketID"));
                    ticket.setBookingID(rs.getInt("bookingID"));
                    int typeID = rs.getInt("typeID");
                    TicketType tickettype = ticketDao.getTicketByTypeID(typeID);
                    ticket.setTickettype(tickettype);
                    ticket.setName(rs.getString("name"));
                    ticket.setGender(rs.getBoolean("gender"));
                    ticket.setDob(rs.getDate("dob"));
                    tickets.add(ticket);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tickets;
    }

   public void updateBookingDetails(int bookingID, String customerName, String email, int phoneNumber, String customerAddress, String note, List<Ticket> tickets) {
        String query = "UPDATE bookingdetails SET customerName = ?, email = ?, phoneNumber = ?, customerAddress = ?, note = ? WHERE bookingID = ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, customerName);
            ps.setString(2, email);
            ps.setInt(3, phoneNumber);
            ps.setString(4, customerAddress);
            ps.setString(5, note);
            ps.setInt(6, bookingID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Ticket ticket : tickets) {
            updateTicket(ticket);
        }
    }

    private void updateTicket(Ticket ticket) {
        String query = "UPDATE ticket SET name = ?, gender = ?, dob = ?, typeID = ? WHERE ticketID = ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, ticket.getName());
            ps.setBoolean(2, ticket.isGender());
            ps.setDate(3, ticket.getDob());
            ps.setInt(4, ticket.getTickettype().getTypeID());
            ps.setInt(5, ticket.getTicketID());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
