/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import com.sun.jdi.connect.spi.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Ticket;
import model.Tour;
import model.TourTicket;
import model.TicketType;
import java.sql.Statement;
import java.sql.Date;

public class TicketDAO extends DBContext {

    public List<TicketType> getAllTicket() {
        List<TicketType> listTicket = new ArrayList<>();
        String query = "SELECT * FROM bookingsystem.tickettype";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                int typeID = rs.getInt("typeID");
                String typeName = rs.getString("typeName");
                String typeCode = rs.getString("typeCode");
                int ageMin = rs.getInt("ageMin");
                int ageMax = rs.getInt("ageMax");
                boolean isDefault = rs.getBoolean("isDefault");
                TicketType ticket = new TicketType(typeID, typeName, typeCode, ageMin, ageMax, isDefault);
                listTicket.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTicket;
    }

    public void insertTourTickets(int tourID, int typeID) {
        String query = "INSERT INTO bookingsystem.tourticket (tourID, typeID) VALUES (?, ?)";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, tourID);
            pst.setInt(2, typeID);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<TourTicket> getAllTicketByTourID(int tourID) {
        List<TourTicket> listTickets = new ArrayList<>();
        String query = "SELECT tt.typeID, tt.typeName, tt.typeCode, tt.ageMin, tt.ageMax, tt.isDefault, t.priceTicket "
                + "FROM tickettype tt "
                + "LEFT JOIN tourticket t ON tt.typeID = t.typeID AND t.tourID = ? "
                + "WHERE tt.isDefault = TRUE OR tt.tourID = ?";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, tourID);
            pst.setInt(2, tourID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                int typeID = rs.getInt("typeID");
                String typeName = rs.getString("typeName");
                String typeCode = rs.getString("typeCode");
                double priceTicket = rs.getObject("priceTicket") != null ? rs.getDouble("priceTicket") : -1;
                //check xem ageMin có null không nếu null cho nó =0
                int ageMin = rs.getInt("ageMin");
                if (rs.wasNull()) {
                    ageMin = 0;
                }
                //check xem ageMax có null không nếu null cho nó =100
                int ageMax = rs.getInt("ageMax");
                if (rs.wasNull()) {
                    ageMax = 100;
                }
                boolean isDefault = rs.getBoolean("isDefault");
                TicketType ticketType = new TicketType(typeID, typeName, typeCode, ageMin, ageMax, isDefault);

                TourTicket ticket = new TourTicket(tourID, typeID, priceTicket, ticketType);
                listTickets.add(ticket);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listTickets;
    }

    public void updatePriceTicket(int tourID, int typeID, double priceTicket) {
        String query = "UPDATE `bookingsystem`.`tourticket`\n"
                + "SET\n"
                + "`priceTicket` = ?\n"
                + "WHERE `tourID` = ? and `typeID` = ?  ;";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setDouble(1, priceTicket);
            pst.setInt(2, tourID);
            pst.setInt(3, typeID);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertPrice(int tourID, int typeID, double price) {
        String query = "UPDATE `bookingsystem`.`tourticket`\n"
                + "SET `priceTicket` = ?\n"
                + "WHERE `tourID` = ? AND `typeID` = ?;";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setDouble(1, price);
            pst.setInt(2, tourID);
            pst.setInt(3, typeID);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double getPriceTicket(int tourID, int typeID) {
        double priceTicket = -1;
        String query = "SELECT priceTicket FROM bookingsystem.tourticket\n"
                + "where tourID = ? and typeID = ?";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, tourID);
            pst.setInt(2, typeID);
            ResultSet rs = pst.executeQuery();
            priceTicket = rs.getDouble("priceTicket");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return priceTicket;
    }

    public int countTicket(int tourID, int bookingID) {
        int count = 0;
        String query = "SELECT COUNT(*) AS tongSoVe\n"
                + "FROM bookingsystem.ticket\n"
                + "WHERE tourID = ? AND bookingID = ?;";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, tourID);
            pst.setInt(2, bookingID);
            ResultSet rs = pst.executeQuery();
            count = rs.getInt("tongSoVe");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public void insertTickets(int bookingID, Map<String, List<Ticket>> tickets) throws SQLException {
        String sql = "INSERT INTO ticket (bookingID, typeID, name, gender, dob) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            for (String typeID : tickets.keySet()) {
                List<Ticket> ticketList = tickets.get(typeID);
                for (Ticket ticket : ticketList) {
                    stmt.setInt(1, bookingID);
                    stmt.setInt(2, Integer.parseInt(typeID));
                    stmt.setString(3, ticket.getName());
                    stmt.setBoolean(4, ticket.isGender());
                    stmt.setDate(5, new java.sql.Date(ticket.getDob().getTime()));
                    stmt.addBatch();
                }
            }
            stmt.executeBatch();
        }
    }

//    public List<Integer> insertTicket(Map<String, List<Ticket>> tickets) throws SQLException {
//        String sql = "INSERT INTO ticket (bookingID, typeID, name, gender, dob) VALUES (?, ?, ?, ?, ?)";
//        List<Integer> ticketIDs = new ArrayList<>();
//        try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
//            for (String typeID : tickets.keySet()) {
//                List<Ticket> ticketList = tickets.get(typeID);
//                for (Ticket ticket : ticketList) {
//                    stmt.setInt(1, ticket.getBookingID());
//                    stmt.setInt(2, Integer.parseInt(typeID));
//                    stmt.setString(3, ticket.getName());
//                    stmt.setBoolean(4, ticket.isGender());
//                    stmt.setDate(5, new java.sql.Date(ticket.getDob().getTime()));
//                    stmt.addBatch();
//                }
//            }
//            stmt.executeBatch();
//
//            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
//                while (generatedKeys.next()) {
//                    ticketIDs.add(generatedKeys.getInt(1));
//                }
//            }
//        }
//
//        return ticketIDs;
//    }
    public Ticket getTicketByID(int ticketID) throws SQLException {
        Ticket ticket = new Ticket();
        String query = "SELECT * FROM bookingsystem.ticket\n"
                + "where ticketID = ?";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, ticketID);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                int bookingID = rs.getInt("bookingID");
                int typeID = rs.getInt("typeID");
                TicketType tickettype = getTicketByTypeID(typeID);
                String name = rs.getString("name");
                boolean gender = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                ticket = new Ticket(ticketID, bookingID, tickettype, name, gender, dob);
            }
        }
        return ticket;
    }

    public void updateBookingID(int ticketID, int bookingID) throws SQLException {
        String query = "UPDATE ticket SET bookingID = ? WHERE ticketID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, bookingID);
            ps.setInt(2, ticketID);
            ps.executeUpdate();
        }
    }

    public List<Ticket> getAllTicketByBkIḌ̣(int bookingID) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        String query = "SELECT * FROM bookingsystem.ticket\n"
                + "where bookingID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, bookingID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ticketID = rs.getInt("ticketID");
                int typeID = rs.getInt("typeID");
                TicketType tickettype = getTicketByTypeID(typeID);
                String name = rs.getString("name");
                boolean gender = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                Ticket ticket = new Ticket(ticketID, bookingID, tickettype, name, gender, dob);
                tickets.add(ticket);
            }
        }

        return tickets;
    }

    public int sumTicket(int tourID) throws SQLException {
        int sum = 0;
        String query = "SELECT SUM(totalPeople) AS tongSoVe\n"
                + "FROM bookingsystem.bookingdetails\n"
                + "WHERE tourID = ?;";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                sum = rs.getInt("tongSoVe");
            }
        }
        return sum;
    }

    public void insertNewTypeTicket(TicketType newtype) throws SQLException {
        String query = "INSERT INTO tickettype (typeName, typeCode, ageMin, ageMax, isDefault,tourID) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement pst = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            pst.setString(1, newtype.getTypeName());
            pst.setString(2, newtype.getTypeCode());
            pst.setInt(3, newtype.getAgeMin());
            pst.setInt(4, newtype.getAgeMax());
            pst.setBoolean(5, newtype.isIsDefault());
            if (newtype.getTourID() == 0) {
            pst.setNull(6, java.sql.Types.INTEGER);
}
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteTicket(int typeID) throws SQLException {
        // Xóa tất cả các bản ghi liên quan trong bảng `tourticket` trước
        String deleteTourTicketQuery = "DELETE FROM tourticket WHERE typeID = ?";
        try (PreparedStatement pst = connection.prepareStatement(deleteTourTicketQuery)) {
            pst.setInt(1, typeID);
            pst.executeUpdate();
        }

        // Sau đó xóa bản ghi từ bảng `tickettype`
        String deleteTicketTypeQuery = "DELETE FROM tickettype WHERE typeID = ?";
        try (PreparedStatement pst = connection.prepareStatement(deleteTicketTypeQuery)) {
            pst.setInt(1, typeID);
            pst.executeUpdate();
        }
    }

    public TicketType getTicketByTypeID(int typeID) throws SQLException {
        TicketType type = new TicketType();
        String query = "SELECT * FROM bookingsystem.tickettype\n"
                + "where typeID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, typeID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String typeName = rs.getString("typeName");
                String typeCode = rs.getString("typeCode");
                int ageMin = rs.getInt("ageMin");
                int ageMax = rs.getInt("ageMax");
                boolean isDefault = rs.getBoolean("isDefault");
                int tourID = rs.getInt("tourID");
                type = new TicketType(typeID, typeName, typeCode, ageMin, ageMax, isDefault, tourID);
            }
        }
        return type;
    }
    
    public boolean updateTicket(TicketType ticket) {
        String sql = "UPDATE bookingsystem.tickettype SET typeName = ?, typeCode = ?, ageMin = ?, ageMax = ?, isDefault = ? WHERE typeID = ?";

        try (
             PreparedStatement stmt = connection.prepareStatement(sql)) {

            stmt.setString(1, ticket.getTypeName());
            stmt.setString(2, ticket.getTypeCode());
            stmt.setInt(3, ticket.getAgeMin());
            stmt.setInt(4, ticket.getAgeMax());
            stmt.setBoolean(5, ticket.isIsDefault());
            stmt.setInt(6, ticket.getTypeID());

            int rowsUpdated = stmt.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
