/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Tour;

/**
 *
 * @author Admin
 */
public class DisplayTourListDAO extends DBContext {

    public DisplayTourListDAO() {
        super(); // Gọi constructor của DBContext để kết nối cơ sở dữ liệu
    }

    public List<Tour> getAllTours() {
        List<Tour> tours = new ArrayList<>();
        String query = "SELECT * FROM tour";

        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                Tour tour = new Tour();
                tour.setTourID(rs.getInt("tourID"));
                tour.setTourName(rs.getString("tourName"));
                tour.setTourDescription(rs.getString("tourDescription"));
                tour.setStartLocation(rs.getString("startLocation"));
                tour.setEndLocation(rs.getString("endLocation"));
                tour.setStartDate(rs.getDate("startDate"));
                tour.setEndDate(rs.getDate("endDate"));
                tour.setPrice(rs.getDouble("price"));
                tour.setNumberOfPeople(rs.getInt("numberOfPeople"));
                tour.setThumbnails(rs.getString("thumbnails"));
                tour.setStatus(rs.getBoolean("status"));
                tours.add(tour);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tours;
    }

    public List<Tour> getAllValidTours() {
        List<Tour> tours = new ArrayList<>();
        String query = "SELECT * FROM tour WHERE startDate > ?";

        LocalDate currentDate = LocalDate.now();

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setDate(1, java.sql.Date.valueOf(currentDate));

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Tour tour = new Tour();
                    tour.setTourID(rs.getInt("tourID"));
                    tour.setTourName(rs.getString("tourName"));
                    tour.setTourDescription(rs.getString("tourDescription"));
                    tour.setStartLocation(rs.getString("startLocation"));
                    tour.setEndLocation(rs.getString("endLocation"));
                    tour.setStartDate(rs.getDate("startDate"));
                    tour.setEndDate(rs.getDate("endDate"));
                    tour.setPrice(rs.getDouble("price"));
                    tour.setNumberOfPeople(rs.getInt("numberOfPeople"));
                    tour.setThumbnails(rs.getString("thumbnails"));
                    tour.setStatus(rs.getBoolean("status"));
                    tours.add(tour);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tours;
    }

    public List<Tour> searchToursForHomepage(String startLocation, String endLocation, String startDate) {
        List<Tour> tours = new ArrayList<>();
        String query = "SELECT * FROM tour WHERE startDate > ?";
        List<Object> params = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        params.add(java.sql.Date.valueOf(currentDate));

        if (!startLocation.isEmpty()) {
            query += " AND startLocation = ?";
            params.add(startLocation);
        }

        if (!endLocation.isEmpty()) {
            query += " AND endLocation = ?";
            params.add(endLocation);
        }

        if (startDate != null && !startDate.isEmpty()) {
            query += " AND startDate >= ?";
            params.add(startDate);
        }

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            for (int i = 0; i < params.size(); i++) {
                stmt.setObject(i + 1, params.get(i));
            }

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Tour tour = new Tour();
                    tour.setTourID(rs.getInt("tourID"));
                    tour.setTourName(rs.getString("tourName"));
                    tour.setTourDescription(rs.getString("tourDescription"));
                    tour.setStartLocation(rs.getString("startLocation"));
                    tour.setEndLocation(rs.getString("endLocation"));
                    tour.setStartDate(rs.getDate("startDate"));
                    tour.setEndDate(rs.getDate("endDate"));
                    tour.setPrice(rs.getDouble("price"));
                    tour.setNumberOfPeople(rs.getInt("numberOfPeople"));
                    tour.setThumbnails(rs.getString("thumbnails"));
                    tour.setStatus(rs.getBoolean("status"));
                    tours.add(tour);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tours;
    }

    public List<Tour> searchToursWithFilter(String startLocation, String endLocation, String startDate, String daysFilter, double budget) {
        List<Tour> tours = new ArrayList<>();
        String query = "SELECT * FROM tour WHERE startDate > ?";
        List<Object> params = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        params.add(java.sql.Date.valueOf(currentDate));

        if (!startLocation.isEmpty()) {
            query += " AND startLocation = ?";
            params.add(startLocation);
        }

        if (!endLocation.isEmpty()) {
            query += " AND endLocation = ?";
            params.add(endLocation);
        }

        if (startDate != null && !startDate.isEmpty()) {
            query += " AND startDate >= ?";
            params.add(startDate);
        }

        if (daysFilter != null && !daysFilter.isEmpty()) {
            switch (daysFilter) {
                case "1-3":
                    query += " AND DATEDIFF(endDate, startDate) BETWEEN 1 AND 3";
                    break;
                case "4-7":
                    query += " AND DATEDIFF(endDate, startDate) BETWEEN 4 AND 7";
                    break;
                case "8-15":
                    query += " AND DATEDIFF(endDate, startDate) BETWEEN 8 AND 15";
                    break;
                case ">15":
                    query += " AND DATEDIFF(endDate, startDate) > 15";
                    break;
            }
        }

        if (budget > 0) {
            query += " AND price <= ?";
            params.add(budget);
        }

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            for (int i = 0; i < params.size(); i++) {
                stmt.setObject(i + 1, params.get(i));
            }

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Tour tour = new Tour();
                    tour.setTourID(rs.getInt("tourID"));
                    tour.setTourName(rs.getString("tourName"));
                    tour.setTourDescription(rs.getString("tourDescription"));
                    tour.setStartLocation(rs.getString("startLocation"));
                    tour.setEndLocation(rs.getString("endLocation"));
                    tour.setStartDate(rs.getDate("startDate"));
                    tour.setEndDate(rs.getDate("endDate"));
                    tour.setPrice(rs.getDouble("price"));
                    tour.setNumberOfPeople(rs.getInt("numberOfPeople"));
                    tour.setThumbnails(rs.getString("thumbnails"));
                    tour.setStatus(rs.getBoolean("status"));
                    tours.add(tour);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tours;
    }

    public void close() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
