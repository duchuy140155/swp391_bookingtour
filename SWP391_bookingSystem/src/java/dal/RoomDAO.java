


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Room;
import model.RoomBooking;
import model.RoomCancel;
import model.RoomType;

/**
 *
 * @author Administrator
 */
public class RoomDAO extends DBContext{
    public List<RoomBooking> loadBookedRoomForUser(int id){
    List<RoomBooking> list = new ArrayList<>();
    String sql = "select * from roomsbooking where userId =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs= st.executeQuery();
            while(rs.next()){
                list.add(new RoomBooking(rs.getInt(1),
                        rs.getInt(2), 
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getDate(10)
                ));
   
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    
    return list;
    }
    
    public void InsertRoom(Room room){
     String sql = "insert into rooms values (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, room.getRoom_id());
            st.setInt(2, room.getHotel_id());
            st.setInt(3, room.getRoom_type());
            st.setString(4, room.getName());
            st.setDouble(5, room.getPrice());
            st.setInt(6, room.getImageIdRoom());
            st.setInt(7, room.getMax_guests());
            st.setString(8, room.getDescription());
            st.setInt(9, room.getNumber_beds());
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    
    
    }
    public List<RoomBooking> getAllRoomBookings() {
        List<RoomBooking> roomBookings = new ArrayList<>();
        String sql = "SELECT * FROM roomsbooking";

        try (PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                int userId = rs.getInt("userId");
                int roomId = rs.getInt("room_id");
                int roomType = rs.getInt("room_type");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                int imageIdRoom = rs.getInt("imageIdRoom");
                int maxGuests = rs.getInt("max_guests");
                String description = rs.getString("description");
                int numberBeds = rs.getInt("number_beds");
                Date date = rs.getDate("date_book");
                RoomBooking roomBooking = new RoomBooking(userId, roomId, roomType, name, price, imageIdRoom, maxGuests, description, numberBeds,date);
                roomBookings.add(roomBooking);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return roomBookings;
    }
    
public void loadRoomBooking(int userid, Room room,String date) {
    String sql = "INSERT INTO roomsbooking (userId, room_id, room_type, name, price, imageIdRoom, max_guests, description, number_beds,date_book) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?)";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, userid);
        st.setInt(2, room.getRoom_id());
        st.setInt(3, room.getRoom_type());
        st.setString(4, room.getName());
        st.setDouble(5, room.getPrice());
        st.setInt(6, room.getImageIdRoom());
        st.setInt(7, room.getMax_guests());
        st.setString(8, room.getDescription());
        st.setInt(9, room.getNumber_beds());
        st.setString(10,  date);
        
        st.executeUpdate();
    } catch (Exception e) {
        e.printStackTrace();
    }
}


    
    public Room loadRoomById(int id){
    Room a = new Room();
     String sql = "select * from rooms where room_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Room(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8),rs.getInt(9)
                        );
                break;
            }

        } catch (Exception e) {
            System.out.println(e);
        }

    return a;
    }
    public void updateRoomById(Room room) {
    String sql = "UPDATE rooms SET hotel_id=?, room_type=?, price=?, max_guests=?, description=?, number_beds=? WHERE room_id=?";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, room.getHotel_id());
        st.setInt(2, room.getRoom_type());
        st.setDouble(3, room.getPrice());
        st.setInt(4, room.getMax_guests());
        st.setString(5, room.getDescription());
        st.setInt(6, room.getNumber_beds());
        st.setInt(7, room.getRoom_id());
        st.executeUpdate();
    } catch (Exception e) {
        e.printStackTrace(); // Log exception
    }
}

    
    public void DeleteRoom(int id){
     String sql = "DELETE  FROM rooms\n"
                + "  WHERE room_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    
    
    
    }
    public void deleteRoomByHotelId(int id){
    String sql = "delete from rooms where hotel_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.execute();
            
        } catch (Exception e) {
            System.out.println(e);
        }
    
    }
    public int getMaxId(){
    int max = 0;
    String sql = "SELECT max(rooms.room_id) FROM bookingsystem.rooms";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;
    }
    
    public List<Room> loadRoomByHotelId(int id){
    String sql = "select * from rooms where hotel_id = ?";
    List<Room> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
            list.add(new Room(rs.getInt(1),
                    rs.getInt(2),
                    rs.getInt(3),
                    rs.getString(4),
                    rs.getDouble(5),
                    rs.getInt(6),
                    rs.getInt(7),
                    rs.getString(8), 
                    rs.getInt(9)));
                
            
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    
    return list;
    }
    public List<RoomType> loadAllRoomType() {
        List<RoomType> list = new ArrayList<>();
        String sql = "select * from room_type";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new RoomType(rs.getInt(1),
                        rs.getString(2)
                ) );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
   
    public List<Room> loadAllRoom(){
    List<Room> list = new ArrayList<>();
    String sql = "select * from rooms";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
            list.add(new Room(rs.getInt(1),
                    rs.getInt(2),
                    rs.getInt(3),rs.getString(4),
                    rs.getDouble(5),
                    rs.getInt(6),
                   rs.getInt(7),
                    rs.getString(8),
                    rs.getInt(9)));
            
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        return list;
    }
    
      public int getMaxRomtypeId(){
    int max = 0;
    String sql = "SELECT max(rooms.imageIdRoom) FROM rooms";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;
    }
    
     public void insertRoomCancel(int userid, Room roomCancel) {
    String sql = "INSERT INTO RoomsCancel (userId, room_id, room_type, name, price, imageIdRoom, max_guests, description, number_beds, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, userid);
        st.setInt(2, roomCancel.getRoom_id());
        st.setInt(3, roomCancel.getRoom_type());
        st.setString(4, roomCancel.getName());
        st.setDouble(5, roomCancel.getPrice());
        st.setInt(6, roomCancel.getImageIdRoom());
        st.setInt(7, roomCancel.getMax_guests());
        st.setString(8, roomCancel.getDescription());
        st.setInt(9, roomCancel.getNumber_beds());
        st.setBoolean(10, false); 
        st.executeUpdate();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
     public int countTotalBookedRooms() {
    int count = 0;
    String sql = "SELECT COUNT(*) FROM roomsbooking";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            count = rs.getInt(1);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return count;
}


    public static void main(String[] args) {
        RoomDAO d = new RoomDAO();
       // Room room = new Room(1, 1, 1, "duy", 10, 1, 2, "hello", 1);
       //d. loadRoomBooking(16, room, "2024/7/14");
//       
//       List<RoomBooking > list = d.getAllRoomBookings();
//        RoomBooking m = new RoomBooking();
//        for(Object x : list){
//        m = (RoomBooking)x;
//        break;
//        }
//        System.out.println(m.getDate_booking());
System.out.println(d.loadBookedRoomForUser(16));
       
    }
 
}
