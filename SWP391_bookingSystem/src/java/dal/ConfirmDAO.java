/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.BookingDetails;
import model.Tour;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class ConfirmDAO extends DBContext {

//    public void saveBooking(BookingDetails bookingDetails) {
//        String query = "INSERT INTO bookingdetails (finalPrice, note, email, phoneNumber, address, discountID, tourID, userID, ticketID, status, bookingDate, customerName, paymentMethod) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//        try (PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
//            ps.setDouble(1, bookingDetails.getFinalPrice());
//            ps.setString(2, bookingDetails.getNote());
//            ps.setString(3, bookingDetails.getEmail());
//            ps.setInt(4, bookingDetails.getPhoneNumber());
//            ps.setString(5, bookingDetails.getCustomerAddress());
//            if (bookingDetails.getDiscountID() != null) {
//                ps.setInt(6, bookingDetails.getDiscountID().getDiscountID());
//            } else {
//                ps.setNull(6, java.sql.Types.INTEGER);
//            }
//            ps.setInt(7, bookingDetails.getTour().getTourID());
//            if (bookingDetails.getUserID() != null) {
//                ps.setInt(8, bookingDetails.getUserID().getUserID());
//            } else {
//                ps.setNull(8, java.sql.Types.INTEGER);
//            }
//            if (bookingDetails.getTicketID() != null) {
//                ps.setInt(9, bookingDetails.getTicketID().getTicketID());
//            } else {
//                ps.setNull(9, java.sql.Types.INTEGER);
//            }
//            ps.setInt(10, bookingDetails.getStatus());
//            ps.setDate(11, bookingDetails.getBookingDate());
//            ps.setString(12, bookingDetails.getCustomerName());
//            ps.setString(13, bookingDetails.getPaymentMethod());
//
//            ps.executeUpdate();
//
//            try (ResultSet rs = ps.getGeneratedKeys()) {
//                if (rs.next()) {
//                    bookingDetails.setBookingID(rs.getInt(1));
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
    public BookingDetails getBookingByID(int bookingID) {
        BookingDetails booking = null;
        String query = "SELECT * FROM bookingdetails WHERE bookingID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, bookingID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    booking = new BookingDetails();
                    booking.setBookingID(rs.getInt("bookingID"));
                    booking.setCustomerName(rs.getString("customerName"));
                    booking.setEmail(rs.getString("email"));
                    booking.setPhoneNumber(rs.getInt("phoneNumber"));
                    booking.setCustomerAddress(rs.getString("address"));
                    booking.setNote(rs.getString("note"));
                    booking.setBookingDate(rs.getDate("bookingDate"));
                    booking.setStatus(rs.getInt("status"));

                    // Lấy thông tin tour và set vào booking
                    int tourID = rs.getInt("tourID");
                    Tour tour = getTourByID(tourID);
                    booking.setTour(tour);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return booking;
    }

    public Tour getTourByID(int tourID) {
        Tour tour = null;
        String query = "SELECT * FROM tour WHERE tourID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    tour = new Tour();
                    tour.setTourID(rs.getInt("tourID"));
                    tour.setTourName(rs.getString("tourName"));
                    tour.setTourDescription(rs.getString("tourDescription"));
                    tour.setStartLocation(rs.getString("startLocation"));
                    tour.setEndLocation(rs.getString("endLocation"));
                    tour.setStartDate(rs.getDate("startDate"));
                    tour.setEndDate(rs.getDate("endDate"));
                    tour.setPrice(rs.getDouble("price"));
                    tour.setThumbnails(rs.getString("thumbnails"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tour;
    }

    public BookingDetails getSomethingByBkID(int bookingID) {
        BookingDetails bd = new BookingDetails();
        String query = "SELECT finalPrice, note, email, phoneNumber, customerAddress, status, bookingDate, customerName, paymentMethod, totalPeople\n"
                + "FROM bookingsystem.bookingdetails\n"
                + "where bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                double finalPrice = rs.getDouble("finalPrice");
                String email = rs.getString("email");
                String note = rs.getString("note");
                int phoneNumber = rs.getInt("phoneNumber");
                String customerAddress = rs.getString("customerAddress");
                int status = rs.getInt("status");
                Date bookingDate = rs.getDate("bookingDate");
                String customerName = rs.getString("customerName");
                String paymentMethod = rs.getString("paymentMethod");
                int totalPeople = rs.getInt("totalPeople");
                bd = new BookingDetails(finalPrice, note, email, phoneNumber, customerAddress, status, bookingDate, customerName, paymentMethod, totalPeople);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bd;

    }
}
