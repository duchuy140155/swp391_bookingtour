package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import model.ContactRequest;

public class ContactRequestDAO {

    private Connection con;

    public ContactRequestDAO(Connection con) {
        this.con = con;
    }

    public void saveContactRequest(String name, String email, String phone, String subject, String message, int tourID, ZonedDateTime createdAt) throws SQLException {
        String sql = "INSERT INTO ContactRequests (name, email, phone, subject, message, tourID, status, created_at, isNew) VALUES (?, ?, ?, ?, ?, ?, FALSE, ?, TRUE)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, phone);
            ps.setString(4, subject);
            ps.setString(5, message);
            ps.setInt(6, tourID);
            ps.setTimestamp(7, Timestamp.from(createdAt.toInstant()));
            ps.executeUpdate();
        }
    }

    public void updateContactStatus(int contactId) throws SQLException {
        String sql = "UPDATE ContactRequests SET status = TRUE, isNew = FALSE WHERE id = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, contactId);
            ps.executeUpdate();
        }
    }

    public List<ContactRequest> getContactRequestsByPartnerID(int partnerID) throws SQLException {
        String sql = "SELECT cr.* FROM ContactRequests cr INNER JOIN tour t ON cr.tourID = t.tourID WHERE t.partnerID = ? AND cr.status = FALSE";
        List<ContactRequest> contactRequests = new ArrayList<>();
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, partnerID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    ContactRequest contactRequest = new ContactRequest(
                            rs.getInt("tourID"),
                            rs.getString("name"),
                            rs.getString("email"),
                            rs.getString("phone"),
                            rs.getString("subject"),
                            rs.getString("message"),
                            rs.getBoolean("status"),
                            rs.getTimestamp("created_at").toInstant().atZone(ZoneId.systemDefault())
                    );
                    contactRequest.setId(rs.getInt("id"));
                    contactRequest.setNew(rs.getBoolean("isNew"));
                    contactRequests.add(contactRequest);
                }
            }
        }
        // In ra danh sách liên hệ
        for (ContactRequest cr : contactRequests) {
            System.out.println(cr);
        }
        return contactRequests;
    }

    public int getNewContactCountByPartnerID(int partnerID) throws SQLException {
        String sql = "SELECT COUNT(*) FROM ContactRequests cr INNER JOIN tour t ON cr.tourID = t.tourID WHERE t.partnerID = ? AND cr.isNew = TRUE";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, partnerID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        }
        return 0;
    }
}
