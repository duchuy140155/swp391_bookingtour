/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.NewsImage;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class NewsImageDAO extends DBContext {

    // Thêm một NewsImage mới
    public void addNewsImage(NewsImage newsImage) {
        String query = "INSERT INTO news_image (idNew, newsImageURL) VALUES (?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, newsImage.getIdNew());
            ps.setString(2, newsImage.getNewsImageURL());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Lấy tất cả các NewsImage theo idNew
    public List<NewsImage> getImagesByNewsId(int idNew) {
        List<NewsImage> images = new ArrayList<>();
        String query = "SELECT * FROM news_image WHERE idNew = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idNew);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                NewsImage image = new NewsImage();
                image.setNewImageID(rs.getInt("newImageID"));
                image.setIdNew(rs.getInt("idNew"));
                image.setNewsImageURL(rs.getString("newsImageURL"));
                images.add(image);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return images;
    }

    // Cập nhật một NewsImage
    public void updateNewsImage(NewsImage newsImage) {
        String query = "UPDATE news_image SET idNew = ?, newsImageURL = ? WHERE newImageID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, newsImage.getIdNew());
            ps.setString(2, newsImage.getNewsImageURL());
            ps.setInt(3, newsImage.getNewImageID());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Xóa một NewsImage theo ID
    public void deleteNewsImage(int newImageID) {
        String query = "DELETE FROM news_image WHERE newImageID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, newImageID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Xóa tất cả các NewsImage theo idNew
    public void deleteNewsImagesByNewsId(int idNew) {
        String query = "DELETE FROM news_image WHERE idNew = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idNew);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
