/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.Ticket;

import model.BookingDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ViewBookingDAO extends DBContext {

     public List<BookingDetails> getBookingsByTourID(int tourID, int offset, int recordsPerPage, String search, String sortStatus) {
        List<BookingDetails> bookings = new ArrayList<>();
        String query = "SELECT * FROM bookingdetails WHERE tourID = ? AND (customerName LIKE ? OR phoneNumber LIKE ?)";

        if ("approved".equals(sortStatus)) {
            query += " AND status = 1";
        } else if ("rejected".equals(sortStatus)) {
            query += " AND status = 0";
        }

        query += " LIMIT ?, ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ps.setInt(4, offset);
            ps.setInt(5, recordsPerPage);

            System.out.println("Executing query: " + ps);  // Debugging statement

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    BookingDetails booking = new BookingDetails();
                    booking.setBookingID(rs.getInt("bookingID"));
                    booking.setCustomerName(rs.getString("customerName"));
                    booking.setEmail(rs.getString("email"));
                    booking.setPhoneNumber(rs.getInt("phoneNumber"));
                    booking.setCustomerAddress(rs.getString("customerAddress"));
                    booking.setNote(rs.getString("note"));
                    booking.setPaymentMethod(rs.getString("paymentMethod"));
                    booking.setBookingDate(rs.getDate("bookingDate"));
                    booking.setStatus(rs.getInt("status"));
                    bookings.add(booking);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Bookings retrieved: " + bookings.size());  // Debugging statement
        return bookings;
    }

    public int getTotalBookingsCount(int tourID, String search, String sortStatus) {
        int count = 0;
        String query = "SELECT COUNT(*) FROM bookingdetails WHERE tourID = ? AND (customerName LIKE ? OR phoneNumber LIKE ?)";

        if ("approved".equals(sortStatus)) {
            query += " AND status = 1";
        } else if ("rejected".equals(sortStatus)) {
            query += " AND status = 0";
        }

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, tourID);
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");

            System.out.println("Executing query: " + ps);  // Debugging statement

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Total bookings count: " + count);  // Debugging statement
        return count;
    }

    public void updateBookingStatus(int bookingID, int status) {
        String query = "UPDATE bookingdetails SET status = ? WHERE bookingID = ?";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, status);
            ps.setInt(2, bookingID);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
