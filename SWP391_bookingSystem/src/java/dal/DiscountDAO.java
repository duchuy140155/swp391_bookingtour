package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Discount;
import model.LuckyDiscount;
import model.LuckySpin;
import model.Lucky_Discount_User;
import model.User;
import model.userLuckyDiscount;

public class DiscountDAO extends DBContext {

    public List<Discount> loadAllDiscount() {
        List<Discount> list = new ArrayList<>();
        String sql = "SELECT * FROM discount";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Discount(
                        rs.getInt("discountID"),
                        rs.getString("discountCode"),
                        rs.getString("discountName"),
                        rs.getDate("discountFrom"),
                        rs.getDate("discountTo"),
                        rs.getBoolean("status"),
                        rs.getInt("discountType"),
                        rs.getInt("discountPercentage"),
                        rs.getInt("discountReduce"),
                        rs.getInt("tourID")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void createUserLuckyDiscount(userLuckyDiscount a) {
        String sql = "insert  into user_lucky_discount values (?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getUserID());
            st.setInt(2, a.getDiscountID());
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public List<Lucky_Discount_User> getLuckyUser(int userID) {
        List<Lucky_Discount_User> list = new ArrayList<>();
        String sql = "SELECT uld.userID, uld.discountID, ld.discountCode, ld.discountName, ld.discountFrom, ld.discountTo, ld.status, ld.discountPercentage, ld.discountReduce\n"
                + "FROM user_lucky_discount uld\n"
                + "JOIN lucky_discount ld ON uld.discountID = ld.discountID\n"
                + "WHERE uld.userID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Lucky_Discount_User(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5),
                        rs.getDate(6),
                        rs.getBoolean(7),
                        rs.getInt(8),
                        rs.getInt(9)));

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public List<LuckyDiscount> loadAllLuckyDiscount() {
        List<LuckyDiscount> list = new ArrayList<>();
        String sql = "SELECT * FROM lucky_discount";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new LuckyDiscount(
                        rs.getInt("discountID"),
                        rs.getString("discountCode"),
                        rs.getString("discountName"),
                        rs.getDate("discountFrom"),
                        rs.getDate("discountTo"),
                        rs.getBoolean("status"),
                        rs.getInt("discountPercentage"),
                        rs.getInt("discountReduce")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void createTimeSpin(User user) {
        String sql = "INSERT INTO LuckySpins (userID, numberSpin)\n"
                + "SELECT ?, ?\n"
                + "FROM dual\n"
                + "WHERE NOT EXISTS (SELECT 1 FROM LuckySpins WHERE userID = ?);";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user.getUserID());
            st.setInt(2, 1);
            st.setInt(3, user.getUserID());
            st.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void UpdateTimeSpin(int id, String date) {
        String sql = "UPDATE .luckyspins \n"
                + "SET numberSpin = ?, TimeLastSpins= ?\n"
                + "WHERE userID = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, 1);
            st.setString(2, date);
            st.setInt(3, id);
            st.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public LuckySpin loadLuckyDiscountByUserId(int id) {
        String sql = "select * from luckyspins where userID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckySpin(rs.getInt(1), rs.getInt(2),
                        rs.getString(3));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public LuckyDiscount loadTop1LuckyDiscount() {
        String sql = "select * from lucky_discount where discountID=1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt("discountID"), rs.getString("discountCode"),
                        rs.getString("discountName"), rs.getDate("discountFrom"),
                        rs.getDate("discountTo"), rs.getBoolean("status"),
                        rs.getInt("discountPercentage"), rs.getInt("discountReduce"));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;

    }

    public LuckyDiscount loadTop2LuckyDiscount() {
        String sql = "select * from lucky_discount where discountID=2";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt("discountID"), rs.getString("discountCode"),
                        rs.getString("discountName"), rs.getDate("discountFrom"),
                        rs.getDate("discountTo"), rs.getBoolean("status"),
                        rs.getInt("discountPercentage"), rs.getInt("discountReduce"));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;

    }

    public LuckyDiscount loadTop3LuckyDiscount() {
        String sql = "select * from lucky_discount where discountID=3";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt("discountID"), rs.getString("discountCode"),
                        rs.getString("discountName"), rs.getDate("discountFrom"),
                        rs.getDate("discountTo"), rs.getBoolean("status"),
                        rs.getInt("discountPercentage"), rs.getInt("discountReduce"));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;

    }

    public LuckyDiscount loadTop4LuckyDiscount() {
        String sql = "select * from lucky_discount where discountID=4";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt("discountID"), rs.getString("discountCode"),
                        rs.getString("discountName"), rs.getDate("discountFrom"),
                        rs.getDate("discountTo"), rs.getBoolean("status"),
                        rs.getInt("discountPercentage"), rs.getInt("discountReduce"));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;

    }

    public List<Integer> loadTourId() {
        List<Integer> list = new ArrayList<>();
        String sql = "select tourID from tour";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt(1));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    public void deleteDiscount(int id) {
        String sql = "DELETE  FROM discount\n"
                + "  WHERE discountID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public LuckyDiscount loadLuckyDiscountById(int id) {
        LuckyDiscount d = new LuckyDiscount();
        String sql = "select * from lucky_discount where discountID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                d = new LuckyDiscount(
                        rs.getInt("discountID"),
                        rs.getString("discountCode"),
                        rs.getString("discountName"),
                        rs.getDate("discountFrom"),
                        rs.getDate("discountTo"),
                        rs.getBoolean("status"),
                        rs.getInt("discountPercentage"),
                        rs.getInt("discountReduce")
                );

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return d;

    }
   public Discount checkDiscount(String discountCode) {
        String sql = "SELECT * FROM discount WHERE discountCode = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, discountCode);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Date discountTo = rs.getDate(5);
                LocalDate currentDate = LocalDate.now();
                if (currentDate.isAfter(discountTo.toLocalDate())) {
                    return null;
                }
                return new Discount(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10));

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;

    }

    public Discount loadDiscountById(int id) {
        Discount d = new Discount();
        String sql = "select * from discount where discountID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                d = new Discount(
                        rs.getInt("discountID"),
                        rs.getString("discountCode"),
                        rs.getString("discountName"),
                        rs.getDate("discountFrom"),
                        rs.getDate("discountTo"),
                        rs.getBoolean("status"),
                        rs.getInt("discountType"),
                        rs.getInt("discountPercentage"),
                        rs.getInt("discountReduce"),
                        rs.getInt("tourID")
                );

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return d;

    }

    public void updateDiscount(Discount a) {
        String sql = "UPDATE discount SET discountCode=?, discountName=?, discountFrom=?, discountTo=?, status=?, discountType=?, discountPercentage=?, discountReduce=? ,tourID=? WHERE discountID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getDiscountCode());
            st.setString(2, a.getDiscountName());
            st.setDate(3, (Date) a.getDiscountFrom());
            st.setDate(4, (Date) a.getDiscountTo());
            st.setBoolean(5, a.isStatus());
            st.setInt(6, a.getDiscountType());
            st.setInt(7, a.getDiscountPercentage());
            st.setInt(8, a.getDiscountReduce());
            st.setInt(9, a.getTourID());
            st.setInt(10, a.getDiscountID());

            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace(); // Log exception
        }
    }

    public void updateLuckyDiscount(LuckyDiscount a) {
        String sql = "UPDATE lucky_discount SET discountCode=?, discountName=?, discountFrom=?, discountTo=?, status=?, discountPercentage=?, discountReduce=?  WHERE discountID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getDiscountCode());
            st.setString(2, a.getDiscountName());
            st.setDate(3, (Date) a.getDiscountFrom());
            st.setDate(4, (Date) a.getDiscountTo());
            st.setBoolean(5, a.isStatus());

            st.setInt(6, a.getDiscountPercentage());
            st.setInt(7, a.getDiscountReduce());

            st.setInt(8, a.getDiscountID());

            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace(); // Log exception
        }
    }

    public LuckyDiscount getLuckyDiscountById(int discountId) {
        String sql = "select * from bookingsystem.lucky_discount where discountID = ? ";
        LuckyDiscount d = new LuckyDiscount();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, discountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5), rs.getBoolean(6),
                        rs.getInt(7), rs.getInt(8));

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;

    }

    public LuckyDiscount checkLDiscount(String discountCode) {
        String sql = "select * from bookingsystem.lucky_discount where discountCode = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, discountCode);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new LuckyDiscount(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getInt(7),
                        rs.getInt(8));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;

    }

    public void insertDiscount(Discount a) {
        String sql = "insert into discount(discountID,discountCode,discountName,discountFrom,discountTo,status,discountType,discountPercentage,discountReduce,tourID)values(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getDiscountID());
            st.setString(2, a.getDiscountCode());
            st.setString(3, a.getDiscountName());
            st.setDate(4, (Date) a.getDiscountFrom());
            st.setDate(5, (Date) a.getDiscountTo());
            st.setBoolean(6, a.isStatus());
            st.setInt(7, a.getDiscountType());
            st.setInt(8, a.getDiscountPercentage());
            st.setInt(9, a.getDiscountReduce());
            st.setInt(10, a.getTourID());
            st.execute();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public int getMaxId() {
        int max = 0;
        String sql = "SELECT max(discount.discountID) FROM discount";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;
    }

    public static void main(String[] args) {
        DiscountDAO d = new DiscountDAO();
        System.out.println(d.loadAllDiscount());

    }
}
