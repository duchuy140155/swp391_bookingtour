package dal;

import model.ReviewReply;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReviewReplyDAO {
    private Connection connection;

    public ReviewReplyDAO(Connection connection) {
        this.connection = connection;
    }

    public void addReply(ReviewReply reply) throws SQLException {
        String query = "INSERT INTO ReviewReply (reviewID, partnerID, replyContent, replyDate) VALUES (?, ?, ?, ?)";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, reply.getReviewID());
            stmt.setInt(2, reply.getPartnerID());
            stmt.setString(3, reply.getReplyContent());
            stmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            stmt.executeUpdate();
        }
    }

    public void updateReply(ReviewReply reply) throws SQLException {
        String query = "UPDATE ReviewReply SET replyContent = ? WHERE replyID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, reply.getReplyContent());
            stmt.setInt(2, reply.getReplyID());
            stmt.executeUpdate();
        }
    }

    public void deleteReply(int replyID) throws SQLException {
        String query = "DELETE FROM ReviewReply WHERE replyID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, replyID);
            stmt.executeUpdate();
        }
    }

    public List<ReviewReply> getRepliesByReviewID(int reviewID) throws SQLException {
        List<ReviewReply> replies = new ArrayList<>();
        String query = "SELECT * FROM ReviewReply WHERE reviewID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, reviewID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int replyID = rs.getInt("replyID");
                    int partnerID = rs.getInt("partnerID");
                    String replyContent = rs.getString("replyContent");
                    Timestamp replyDate = rs.getTimestamp("replyDate");
                    replies.add(new ReviewReply(replyID, reviewID, partnerID, replyContent, replyDate));
                }
            }
        }
        return replies;
    }

    public List<ReviewReply> getRepliesByPartnerID(int partnerID) throws SQLException {
        List<ReviewReply> replies = new ArrayList<>();
        String query = "SELECT * FROM ReviewReply WHERE partnerID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, partnerID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int replyID = rs.getInt("replyID");
                    int reviewID = rs.getInt("reviewID");
                    String replyContent = rs.getString("replyContent");
                    Timestamp replyDate = rs.getTimestamp("replyDate");
                    replies.add(new ReviewReply(replyID, reviewID, partnerID, replyContent, replyDate));
                }
            }
        }
        return replies;
    }
}
