package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * DAO class for User
 *
 */
public class UserDAO extends DBContext {

    private Connection con;
    private ArrayList<User> users = new ArrayList<>();

    public String status = "";

    public UserDAO() {
        con = new DBContext().connection;
    }

    public UserDAO(Connection connection) {
        this.con = connection;
    }

    public void setPassword(String email, String newpass) {
        String sql = "UPDATE user SET password = ? WHERE email = ?";
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, newpass);
            st.setString(2, email);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkOldPass(String email, String oldpass) {
        User a = new User();
        List<User> list = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE email = ?";
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new User(rs.getInt(1),
                        rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getDate(5),
                        rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getInt(9), rs.getBoolean(10));
            }

            if (a.getPassword().equals(oldpass)) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return false;
    }

    public void createUser(String userName, String email, String password, Date userDOB, String phoneNumber, String address) {
        String sql = "INSERT INTO user (userName, email, password, userDOB, phoneNumber, address, roleID, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, userName);
            ps.setString(2, email);
            ps.setString(3, password);
            ps.setDate(4, userDOB);
            ps.setString(5, phoneNumber);
            ps.setString(6, address);
            ps.setInt(7, 1);
            ps.setBoolean(8, true);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error creating user: " + e.getMessage(), e);
        }
    }

    public boolean checkEmail(String email) {
        String sql = "SELECT * FROM user WHERE email = ?";
        boolean check = false;
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }

    public User loadAccount(String email, String password) {
        String sql = "SELECT * FROM user WHERE email = ? AND password = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = new User(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getBoolean(10)
                );
                return user;
            }
        } catch (SQLException e) {
            System.err.println("Error at reading User: " + e.getMessage());

        }
        return null;
    }

    public boolean updatePassword(String email, String hashedPassword) throws SQLException {
        String sql = "UPDATE user SET password = ? WHERE email = ?";
        try (PreparedStatement pst = con.prepareStatement(sql)) {
            pst.setString(1, hashedPassword);
            pst.setString(2, email);
            int rowsUpdated = pst.executeUpdate();
            return rowsUpdated > 0;
        }
    }

    public void saveUserData(String userName, String email, String password, String userDOB, String phoneNumber, String address, String userPicture, int roleID, int status) {
        String sql = "INSERT INTO user (userName, email, password, userDOB, phoneNumber, address, userPicture, roleID, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, userName);
            ps.setString(2, email);
            ps.setString(3, password);
            ps.setString(4, userDOB);
            ps.setString(5, phoneNumber);
            ps.setString(6, address);
            ps.setString(7, userPicture);
            ps.setInt(8, roleID);
            ps.setInt(9, status);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error at saving User data: " + e.getMessage());
            e.printStackTrace(); // In ra stack trace để xem chi tiết lỗi
        }
    }

    public List<User> getAllUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM user"; // Sửa lại tên bảng thành `user`
        try (PreparedStatement statement = con.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setUserID(resultSet.getInt("userID"));
                user.setUserName(resultSet.getString("userName"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setUserDOB(resultSet.getDate("userDOB"));
                user.setPhoneNumber(resultSet.getString("phoneNumber"));
                user.setAddress(resultSet.getString("address"));
                user.setUserPicture(resultSet.getString("userPicture"));
                user.setRoleID(resultSet.getInt("roleID"));
                user.setStatus(resultSet.getBoolean("status"));
                users.add(user);
            }
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
            throw e;
        }
        // In ra console để kiểm tra
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }

    public User getUserById(int userId) throws SQLException {
        User user = null;
        String query = "SELECT * FROM user WHERE userID = ?";
        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setUserID(resultSet.getInt("userID"));
                user.setUserName(resultSet.getString("userName"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setUserDOB(resultSet.getDate("userDOB"));
                user.setPhoneNumber(resultSet.getString("phoneNumber"));
                user.setAddress(resultSet.getString("address"));
                user.setUserPicture(resultSet.getString("userPicture"));
                user.setRoleID(resultSet.getInt("roleID"));
                user.setStatus(resultSet.getBoolean("status"));
            }
        }
        return user;
    }

    public void updateUserRole(int userId, int roleId) throws SQLException {
        String query = "UPDATE user SET roleID = ? WHERE userID = ?";
        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setInt(1, roleId);
            statement.setInt(2, userId);
            statement.executeUpdate();
        }
    }

    public boolean deleteUser(int userId) throws SQLException {
        String deleteFeedbackQuery = "DELETE FROM feedback WHERE userID = ?";
        String deleteBookingDetailsQuery = "DELETE FROM bookingdetails WHERE userID = ?";
        String deleteAdminContactQuery = "DELETE FROM admin_contact WHERE userID = ?";
        String deleteUserQuery = "DELETE FROM user WHERE userID = ?";

        try (PreparedStatement deleteFeedbackStmt = con.prepareStatement(deleteFeedbackQuery); PreparedStatement deleteBookingDetailsStmt = con.prepareStatement(deleteBookingDetailsQuery); PreparedStatement deleteAdminContactStmt = con.prepareStatement(deleteAdminContactQuery); PreparedStatement deleteUserStmt = con.prepareStatement(deleteUserQuery)) {

            con.setAutoCommit(false);

            deleteFeedbackStmt.setInt(1, userId);
            deleteFeedbackStmt.executeUpdate();

            deleteBookingDetailsStmt.setInt(1, userId);
            deleteBookingDetailsStmt.executeUpdate();

            deleteAdminContactStmt.setInt(1, userId);
            deleteAdminContactStmt.executeUpdate();

            deleteUserStmt.setInt(1, userId);
            int rowsDeleted = deleteUserStmt.executeUpdate();

            con.commit();
            return rowsDeleted > 0;
        } catch (SQLException e) {
            con.rollback();
            e.printStackTrace();
            throw e;
        } finally {
            con.setAutoCommit(true);
        }
    }

    public List<User> getAllUsers(int page, int pageSize) throws SQLException {
        String query = "SELECT * FROM user LIMIT ? OFFSET ?";
        List<User> userList = new ArrayList<>();
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, pageSize);
            ps.setInt(2, (page - 1) * pageSize);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("password"));
                    user.setUserDOB(rs.getDate("userDOB"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setUserPicture(rs.getString("userPicture"));
                    user.setRoleID(rs.getInt("roleID"));
                    user.setStatus(rs.getBoolean("status"));
                    userList.add(user);
                }
            }
        }
        return userList;
    }

    public int getTotalUsers() throws SQLException {
        String query = "SELECT COUNT(*) FROM user";
        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    public List<User> searchUsers(String keyword, int page, int pageSize) throws SQLException {
        String query = "SELECT * FROM user WHERE userName LIKE ? OR email LIKE ? LIMIT ? OFFSET ?";
        List<User> userList = new ArrayList<>();
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, "%" + keyword + "%");
            ps.setString(2, "%" + keyword + "%");
            ps.setInt(3, pageSize);
            ps.setInt(4, (page - 1) * pageSize);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("password"));
                    user.setUserDOB(rs.getDate("userDOB"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setUserPicture(rs.getString("userPicture"));
                    user.setRoleID(rs.getInt("roleID"));
                    user.setStatus(rs.getBoolean("status"));
                    userList.add(user);
                }
            }
        }
        return userList;
    }

    public boolean isAdmin(int userId) throws SQLException {
        String query = "SELECT roleID FROM user WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("roleID") == 1; // Giả sử roleID = 1 là admin
                }
            }
        }
        return false;
    }

    public boolean isSuperAdmin(int userId) throws SQLException {
        String query = "SELECT roleID FROM user WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("roleID") == 4;
                }
            }
        }
        return false;
    }

    public int getRoleId(int userId) throws SQLException {

        String query = "SELECT roleID FROM user WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("roleID");
                }
            }
        }
        return Integer.MAX_VALUE;
    }

    public boolean isAdminExists() throws SQLException {
        String query = "SELECT COUNT(*) FROM user WHERE roleID = 1";
        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        }

        return false;
    }

    public boolean canManageRole(int managerId, int userId) throws SQLException {
        int managerRoleId = getRoleId(managerId);
        int userRoleId = getRoleId(userId);

        return (managerRoleId == 4 && userRoleId != 4) || (managerRoleId == 1 && userRoleId > 1 && userRoleId != 4);
    }

    public void toggleStatus(int userID) throws SQLException {
        String query = "UPDATE user SET status = NOT status WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, userID);
            ps.executeUpdate();
        }
    }

    public void updateUser(int userID, String userName, String email, String phoneNumber, String address, boolean status) throws SQLException {
        String query = "UPDATE user SET userName = ?, email = ?, phoneNumber = ?, address = ?, status = ? WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, userName);
            ps.setString(2, email);
            ps.setString(3, phoneNumber);
            ps.setString(4, address);
            ps.setBoolean(5, status);
            ps.setInt(6, userID);
            ps.executeUpdate();
        }
    }

    public User getUserDetails(int userID) throws SQLException {
        User user = null;
        String query = "SELECT * FROM user WHERE userID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, userID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    user = new User(
                            rs.getInt("userID"),
                            rs.getString("userName"),
                            rs.getString("email"),
                            rs.getString("password"),
                            rs.getDate("userDOB"),
                            rs.getString("phoneNumber"),
                            rs.getString("address"),
                            rs.getString("userPicture"),
                            rs.getInt("roleID"),
                            rs.getBoolean("status")
                    );
                }
            }
        }
        return user;
    }
    private static final String SELECT_USERNAME_BY_USER_ID = "SELECT username FROM user WHERE userID = ?";

    public String getUsernameByUserId(int userID) throws SQLException {
        String username = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try {
            preparedStatement = connection.prepareStatement(SELECT_USERNAME_BY_USER_ID);
            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                username = rs.getString("username");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return username;
    }

    public List<User> getAllUsersWithRole(int roleID) throws SQLException {
        String query = "SELECT * FROM user WHERE roleID = ?";
        List<User> userList = new ArrayList<>();
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, roleID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    User user = new User();
                    user.setUserID(rs.getInt("userID"));
                    user.setUserName(rs.getString("userName"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("password"));
                    user.setUserDOB(rs.getDate("userDOB"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setUserPicture(rs.getString("userPicture"));
                    user.setRoleID(rs.getInt("roleID"));
                    user.setStatus(rs.getBoolean("status"));
                    userList.add(user);
                }
            }
        }
        return userList;
    }
    
     public boolean checkEmailExists(String email) {
        String queryUser = "SELECT COUNT(*) FROM user WHERE email = ?";
        String queryPartner = "SELECT COUNT(*) FROM partner WHERE email = ?";
        try (
             PreparedStatement psUser = connection.prepareStatement(queryUser);
             PreparedStatement psPartner = connection.prepareStatement(queryPartner)) {

            psUser.setString(1, email);
            try (ResultSet rsUser = psUser.executeQuery()) {
                if (rsUser.next() && rsUser.getInt(1) > 0) {
                    return true;
                }
            }

            psPartner.setString(1, email);
            try (ResultSet rsPartner = psPartner.executeQuery()) {
                if (rsPartner.next() && rsPartner.getInt(1) > 0) {
                    return true;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
