package dal;

import model.Review;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReviewTourDAO {
    private Connection connection;

    public ReviewTourDAO(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("Connection cannot be null");
        }
        this.connection = connection;
    }

    // Phương thức lấy danh sách top 3 hướng dẫn viên có rating cao nhất trong tháng hiện tại
    public List<Review> getTop3GuidesCurrentMonth() throws SQLException {
        List<Review> topGuidesReviews = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = "SELECT r.guideID, AVG(r.rating) AS avg_rating, g.name, g.experience, g.imageURL "
                     + "FROM review r "
                     + "JOIN guide g ON r.guideID = g.guideID "
                     + "WHERE MONTH(r.reviewDate) = MONTH(CURRENT_DATE()) AND YEAR(r.reviewDate) = YEAR(CURRENT_DATE()) "
                     + "GROUP BY r.guideID, g.name, g.experience, g.imageURL "
                     + "ORDER BY avg_rating DESC "
                     + "LIMIT 4";

        try {
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int guideID = rs.getInt("guideID");
                double avgRating = rs.getDouble("avg_rating");
                String name = rs.getString("name");
                String experience = rs.getString("experience");
                String imageURL = rs.getString("imageURL");
                
                Review review = new Review();
                review.setGuideID(guideID);
                review.setRating((int) avgRating); 
                review.setComment(name); 
                review.setTitle(experience); 
                review.setImageURL(imageURL); 

                topGuidesReviews.add(review);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return topGuidesReviews;
    }

    // Các phương thức khác...

    public List<Review> getReviewsByGuideId(int guideId) throws SQLException {
        List<Review> reviews = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        UserDAO userDAO = new UserDAO(connection);
        String query = "SELECT r.reviewID, r.tourID, r.userID, r.rating, r.comment, r.reviewDate, r.title, t.tourName, t.startDate, t.endDate, g.name AS guideName "
                     + "FROM review r "
                     + "JOIN tour t ON r.tourID = t.tourID "
                     + "JOIN guide g ON r.guideID = g.guideID "
                     + "WHERE r.guideID = ?";

        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, guideId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int reviewID = rs.getInt("reviewID");
                int tourID = rs.getInt("tourID");
                int userID = rs.getInt("userID");
                int rating = rs.getInt("rating");
                String comment = rs.getString("comment");
                String title = rs.getString("title");
                java.sql.Date reviewDate = rs.getDate("reviewDate");
                String tourName = rs.getString("tourName");
                java.sql.Date startDate = rs.getDate("startDate");
                java.sql.Date endDate = rs.getDate("endDate");
                String guideName = rs.getString("guideName");
                String username = userDAO.getUsernameByUserId(userID); // Lấy username từ userID
                reviews.add(new Review(reviewID, tourID, guideId, userID, rating, comment, reviewDate, title, tourName, startDate, endDate, username));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return reviews;
    }

    public boolean saveReview(Review review) {
        String query = "INSERT INTO review (tourID, guideID, userID, rating, comment, reviewDate, title) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, review.getTourID());
            preparedStatement.setInt(2, review.getGuideID());
            preparedStatement.setInt(3, review.getUserID());
            preparedStatement.setInt(4, review.getRating());
            preparedStatement.setString(5, review.getComment());
            preparedStatement.setDate(6, new java.sql.Date(review.getReviewDate().getTime()));
            preparedStatement.setString(7, review.getTitle());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updateReview(int reviewID, String title, String comment, int rating) throws SQLException {
        String query = "UPDATE review SET title = ?, comment = ?, rating = ? WHERE reviewID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, comment);
            preparedStatement.setInt(3, rating);
            preparedStatement.setInt(4, reviewID);
            preparedStatement.executeUpdate();
        }
    }

    public void deleteReview(int reviewID) throws SQLException {
        String query = "DELETE FROM review WHERE reviewID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, reviewID);
            preparedStatement.executeUpdate();
        }
    }
}
