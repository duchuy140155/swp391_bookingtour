/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.util.ArrayList;
import model.Report;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.ReportTypes;
/**
 *
 * @author MSI
 */
public class ReportDAO {
     private Connection con;
    private ArrayList<Report> report = new ArrayList<>();

    public String status = "";

    public ReportDAO() {
        con = new DBContext().connection;
    }

    public ReportDAO(Connection con) {
        this.con = con;
    }
    
    
      public ArrayList<ReportTypes> getReportTypes() {
        ArrayList<ReportTypes> reportTypes = new ArrayList<>();
        String query = "SELECT typeID, typeName FROM ReportTypes";
        try (PreparedStatement ps = con.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                ReportTypes type = new ReportTypes();
                type.setTypeID(rs.getInt("typeID"));
                type.setTypeName(rs.getString("typeName"));
                reportTypes.add(type);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reportTypes;
    }

    public boolean insertReport(Report report) {
        String query = "INSERT INTO reports (reason, createdAt, blogID, userID, typeID) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, report.getReason());
            ps.setTimestamp(2, report.getCreatedAt());
            ps.setInt(3, report.getBlog().getBlogID());
            ps.setInt(4, report.getUser().getUserID());
            ps.setInt(5, report.getType().getTypeID());

            System.out.println("Executing query: " + ps);
            int rowsAffected = ps.executeUpdate();
            System.out.println("Rows affected: " + rowsAffected);
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ReportTypes getReportTypeById(int typeID) {
        String query = "SELECT typeName FROM ReportTypes WHERE typeID = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, typeID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    ReportTypes type = new ReportTypes();
                    type.setTypeID(typeID);
                    type.setTypeName(rs.getString("typeName"));
                    return type;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    
    
}
