/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Post;


/**
 *
 * @author Administrator
 */
public class PostDAO extends DBContext{
     public boolean createPost(int postId, String postTitle, String createAt, int partnerID, String imagePath, String postContent) {
        String sql = "INSERT INTO post (postID, postTitle, createAt, partnerID, image, postContent) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postId);
            st.setString(2, postTitle);
            st.setString(3, createAt);
            st.setInt(4, partnerID);
            st.setString(5, imagePath);
            st.setString(6, postContent);
            int rowsInserted = st.executeUpdate();
            return rowsInserted > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public int getMaxPostId() {
    int maxId = 0;
    String sql = "SELECT MAX(postID) AS max_id FROM post";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            maxId = rs.getInt("max_id"); // Sử dụng alias "max_id"
        }
    } catch (Exception e) {
        System.out.println(e);
    }
    return maxId;
}
    public List<Post> getAllPosts() {
        List<Post> postList = new ArrayList<>();
        try {
             PreparedStatement st = connection.prepareStatement("SELECT * FROM post");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post post = new Post();
                post.setPostId(rs.getInt("postID"));
                post.setPostTitle(rs.getString("postTitle"));
                post.setPostContent(rs.getString("postContent"));
                post.setCreateAt(rs.getDate("createAt"));
                post.setPartnerID(rs.getInt("partnerID"));
                post.setImage(rs.getString("image"));
                postList.add(post);
            }
        } catch (Exception e) {
                System.out.println(e);
        }
            
        
        return postList;
    }
    
    public void deletePost(int postId) {
        String sql = "DELETE FROM post WHERE postID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postId);
            st.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
    public Post getPostByID(int postId) {
        Post post = null;
        String sql = "SELECT * FROM post WHERE postID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                post = new Post();
                post.setPostId(rs.getInt("postID"));
                post.setPostTitle(rs.getString("postTitle"));
                post.setPostContent(rs.getString("postContent"));
                post.setCreateAt(rs.getDate("createAt"));
                post.setPartnerID(rs.getInt("partnerID"));
                post.setImage(rs.getString("image"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return post;
    }
    
    
     public void updatePost(int postId, String postTitle, String postContent, String image) {
        String sql = "UPDATE post SET postTitle = ?, postContent = ?, image = ? WHERE postID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, postTitle);
            st.setString(2, postContent);
            st.setString(3, image);
            st.setInt(4, postId);
           st.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
      public List<Post> getTop4Posts() {
        List<Post> postList = new ArrayList<>();
        String sql = "SELECT * FROM post ORDER BY createAt DESC LIMIT 4";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post post = new Post();
                post.setPostId(rs.getInt("postID"));
                post.setPostTitle(rs.getString("postTitle"));
                post.setPostContent(rs.getString("postContent"));
                post.setCreateAt(rs.getDate("createAt"));
                post.setPartnerID(rs.getInt("partnerID"));
                post.setImage(rs.getString("image"));
                postList.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postList;
    }
    public static void main(String[] args) {
        PostDAO d = new PostDAO();
        System.out.println(d.getAllPosts());
    }

    
    
    
}
