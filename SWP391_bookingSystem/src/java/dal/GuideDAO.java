package dal;

import model.Guide;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Tour;

public class GuideDAO {

    private DBContext dbContext;

    public GuideDAO() {
        dbContext = new DBContext();
    }

    // Phương thức kiểm tra tồn tại của tourID
    private boolean isTourExists(int tourID) {
        String sql = "SELECT 1 FROM tour WHERE tourID = ?";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(sql)) {
            ps.setInt(1, tourID);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Phương thức lấy thông tin hướng dẫn viên theo ID
    private static final String SELECT_GUIDE_BY_ID = "SELECT * FROM guide WHERE guideID = ?";

    public Guide selectGuideByID(int guideID) {
        Guide guide = null;
        try (PreparedStatement preparedStatement = dbContext.connection.prepareStatement(SELECT_GUIDE_BY_ID)) {
            preparedStatement.setInt(1, guideID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phoneNumber = rs.getString("phoneNumber");
                String experience = rs.getString("experience");
                String imageURL = rs.getString("imageURL");
                String address = rs.getString("address");
                boolean status = rs.getBoolean("status");
                guide = new Guide(guideID, name, email, phoneNumber, experience, imageURL, address, status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return guide;
    }

    // Phương thức lấy danh sách hướng dẫn viên theo tour ID
    public List<Guide> selectGuidesByTourID(int tourID) {
        List<Guide> guides = new ArrayList<>();
        String SELECT_GUIDES_BY_TOUR_ID = "SELECT tg.tourType, g.guideID, g.name, g.email, g.phoneNumber, g.experience, g.imageURL, g.address, g.status "
                + "FROM guide g "
                + "JOIN tour_guide tg ON g.guideID = tg.guideID "
                + "WHERE tg.tourID = ?";
        try (PreparedStatement preparedStatement = dbContext.connection.prepareStatement(SELECT_GUIDES_BY_TOUR_ID)) {
            preparedStatement.setInt(1, tourID);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int guideID = rs.getInt("guideID");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phoneNumber = rs.getString("phoneNumber");
                String experience = rs.getString("experience");
                String imageURL = rs.getString("imageURL");
                String address = rs.getString("address");
                boolean status = rs.getBoolean("status");
                String tourType = rs.getString("tourType");
                guides.add(new Guide(guideID, name, email, phoneNumber, experience, imageURL, address, status, tourType));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return guides;
    }

    public List<Guide> getAllGuides() {
        List<Guide> guides = new ArrayList<>();
        String sql = "SELECT * FROM guide";

        try (PreparedStatement ps = dbContext.connection.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                int guideID = rs.getInt("guideID");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phoneNumber = rs.getString("phoneNumber");
                String experience = rs.getString("experience");
                String imageURL = rs.getString("imageURL");
                String address = rs.getString("address");
                boolean status = rs.getBoolean("status");
                guides.add(new Guide(guideID, name, email, phoneNumber, experience, imageURL, address, status));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return guides;
    }

    public List<Guide> getInactiveGuides() {
        List<Guide> guides = new ArrayList<>();
        String sql = "SELECT * FROM guide WHERE status = 0";

        try (PreparedStatement ps = dbContext.connection.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                int guideID = rs.getInt("guideID");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phoneNumber = rs.getString("phoneNumber");
                String experience = rs.getString("experience");
                String imageURL = rs.getString("imageURL");
                String address = rs.getString("address");
                boolean status = rs.getBoolean("status");
                guides.add(new Guide(guideID, name, email, phoneNumber, experience, imageURL, address, status));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return guides;
    }

    public boolean addGuideToTour(int guideID, int tourID, String tourType) {
        if (!isTourExists(tourID)) {
            System.out.println("Tour ID không tồn tại: " + tourID);
            return false;
        }

        String updateGuideSql = "UPDATE guide SET status = 1 WHERE guideID = ?";
        String insertTourGuideSql = "INSERT INTO tour_guide (tourID, guideID, tourType) VALUES (?, ?, ?)";

        try (PreparedStatement updatePs = dbContext.connection.prepareStatement(updateGuideSql); PreparedStatement insertPs = dbContext.connection.prepareStatement(insertTourGuideSql)) {

            updatePs.setInt(1, guideID);
            updatePs.executeUpdate();

            insertPs.setInt(1, tourID);
            insertPs.setInt(2, guideID);
            insertPs.setString(3, tourType);

            return insertPs.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateGuide(Guide guide) {
        String sql = "UPDATE guide SET name = ?, email = ?, phoneNumber = ?, experience = ?, imageURL = ?, address = ?, status = ? WHERE guideID = ?";

        try (PreparedStatement ps = dbContext.connection.prepareStatement(sql)) {
            ps.setString(1, guide.getName());
            ps.setString(2, guide.getEmail());
            ps.setString(3, guide.getPhoneNumber());
            ps.setString(4, guide.getExperience());
            ps.setString(5, guide.getImageURL());
            ps.setString(6, guide.getAddress());
            ps.setBoolean(7, guide.isStatus());
            ps.setInt(8, guide.getGuideID());
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    // Phương thức xóa hướng dẫn viên
    public boolean deleteGuide(int guideID) {
        String sql = "DELETE FROM guide WHERE guideID = ?";

        try (PreparedStatement ps = dbContext.connection.prepareStatement(sql)) {
            ps.setInt(1, guideID);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    // Phương thức xóa hướng dẫn viên khỏi tour
    public boolean removeGuideFromTour(int guideID, int tourID) {
        String deleteTourGuideSql = "DELETE FROM tour_guide WHERE guideID = ? AND tourID = ?";
        String updateGuideSql = "UPDATE guide SET status = 0 WHERE guideID = ?";

        try (PreparedStatement deletePs = dbContext.connection.prepareStatement(deleteTourGuideSql); PreparedStatement updatePs = dbContext.connection.prepareStatement(updateGuideSql)) {

            deletePs.setInt(1, guideID);
            deletePs.setInt(2, tourID);
            deletePs.executeUpdate();

            updatePs.setInt(1, guideID);
            return updatePs.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void updateGuideInfo(int userID, String experience, String imageURL, String address, int status) throws SQLException {
        String query = "INSERT INTO guide (userID, experience, imageURL, address, status) VALUES (?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE experience = ?, imageURL = ?, address = ?, status = ?";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            ps.setString(2, experience);
            ps.setString(3, imageURL);
            ps.setString(4, address);
            ps.setInt(5, status);
            ps.setString(6, experience);
            ps.setString(7, imageURL);
            ps.setString(8, address);
            ps.setInt(9, status);
            ps.executeUpdate();
        }
    }

    public int insertGuide(String name, String email, String phoneNumber, String experience, String imageURL, String address, int status) throws SQLException {
        String query = "INSERT INTO guide (name, email, phoneNumber, experience, imageURL, address, status) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, phoneNumber);
            ps.setString(4, experience);
            ps.setString(5, imageURL);
            ps.setString(6, address);
            ps.setInt(7, status);
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        }
        return -1;
    }

    public void insertUserGuide(int userID, int guideID) throws SQLException {
        String query = "INSERT INTO user_guides (userID, guideID) VALUES (?, ?)";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            ps.setInt(2, guideID);
            ps.executeUpdate();
        }
    }

    public List<Map<String, Object>> getToursByGuideId(int guideID) throws SQLException {
        String query = "SELECT t.tourID, t.tourName, t.startDate, t.endDate, tg.tourType "
                + "FROM tour t "
                + "JOIN tour_guide tg ON t.tourID = tg.tourID "
                + "WHERE tg.guideID = ?";
        List<Map<String, Object>> tours = new ArrayList<>();
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query)) {
            ps.setInt(1, guideID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Map<String, Object> tour = new HashMap<>();
                    tour.put("tourID", rs.getInt("tourID"));
                    tour.put("tourName", rs.getString("tourName"));
                    tour.put("startDate", rs.getDate("startDate"));
                    tour.put("endDate", rs.getDate("endDate"));
                    tour.put("tourType", rs.getString("tourType"));
                    tours.add(tour);
                }
            }
        }
        return tours;
    }

    public int getGuideIDByUserID(int userID) throws SQLException {
        String query = "SELECT guideID FROM user_guides WHERE userID = ?";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("guideID");
                }
            }
        }
        return -1; // hoặc giá trị mặc định nếu không tìm thấy
    }

    public boolean hasGuideInfo(int userId) throws SQLException {
        String query = "SELECT COUNT(*) FROM user_guides WHERE userID = ?";
        try (PreparedStatement ps = dbContext.connection.prepareStatement(query)) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int count = rs.getInt(1);
                    System.out.println("Count for userID " + userId + ": " + count); // Log số lượng hàng trả về
                    return count > 0;
                }
            }
        }
        return false;
    }

}
