package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Guide;
import model.Schedule;
import model.ScheduleDetails;
import model.Tour;
import model.TourDetails;
import model.TourImage;

/**
 *
 * @author MSI
 */
public class TourDAO extends DBContext {

    private Connection con;
    private ArrayList<Tour> tour = new ArrayList<>();

    public String status = "";

    public TourDAO() {
        con = new DBContext().connection;
    }

    public TourDAO(Connection con) {
        
        this.con = con;
    }
    
public Tour getTourIdWithHotel(int id) {
    String sql = "select * from bookingsystem.tour where tourID =?";
    Tour tour = new Tour();
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, id);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            tour = new Tour(
                rs.getInt("tourID"),
                rs.getString("tourName"),
                rs.getString("tourDescription"),
                rs.getString("startLocation"),
                rs.getString("endLocation"),
                rs.getDate("startDate"),
                rs.getDate("endDate"),
                rs.getDouble("price"),
                rs.getInt("numberOfPeople"),
                rs.getString("thumbnails"),
                rs.getBoolean("status"),
                rs.getInt("hotel_id") // Retrieve and set the hotel_id
            );
        }
    } catch (Exception e) {
        System.out.println(e);
    }
    return tour;
}
    public int insertTourWHotel(String tourName, String tourDescription, String startLocation, String endLocation, Date startDate, Date endDate, Double price, int numberOfPeople, String thumbnails, int partnerID, int hotelid) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int tourID = 0;
        try {
            String sqlTour = "INSERT INTO Tour (tourName, tourDescription, startLocation, endLocation, startDate, endDate, price, numberOfPeople, thumbnails, partnerID,status,hotel_id) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
            ps = con.prepareStatement(sqlTour, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, tourName);
            ps.setString(2, tourDescription);
            ps.setString(3, startLocation);
            ps.setString(4, endLocation);
            ps.setDate(5, startDate);
            ps.setDate(6, endDate);
            ps.setDouble(7, price);
            ps.setInt(8, numberOfPeople);
            ps.setString(9, thumbnails);
            ps.setInt(10, partnerID);
            ps.setBoolean(11, true);
            ps.setInt(12, hotelid);
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                tourID = rs.getInt(1);
            }
            System.out.println("Tour inserted with ID: " + tourID);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error inserting tour: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tourID;
    }

    

    public int insertTour(String tourName, String tourDescription, String startLocation, String endLocation, Date startDate, Date endDate, Double price, int numberOfPeople, String thumbnails, int partnerID) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int tourID = 0;
        try {
            String sqlTour = "INSERT INTO Tour (tourName, tourDescription, startLocation, endLocation, startDate, endDate, price, numberOfPeople, thumbnails, partnerID,status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
            ps = con.prepareStatement(sqlTour, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, tourName);
            ps.setString(2, tourDescription);
            ps.setString(3, startLocation);
            ps.setString(4, endLocation);
            ps.setDate(5, startDate);
            ps.setDate(6, endDate);
            ps.setDouble(7, price);
            ps.setInt(8, numberOfPeople);
            ps.setString(9, thumbnails);
            ps.setInt(10, partnerID);
            ps.setBoolean(11, true);
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                tourID = rs.getInt(1);
            }
            System.out.println("Tour inserted with ID: " + tourID);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error inserting tour: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tourID;
    }

    public boolean confirmTour(int tourID) {
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE Tour SET status = ? WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setBoolean(1, true);
            ps.setInt(2, tourID);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean insertTourImages(int tourID, ArrayList<String> imageURLs) {
        PreparedStatement ps = null;
        try {
            String sqlImage = "INSERT INTO tour_images (imageURL, tourID) VALUES (?, ?)";
            ps = con.prepareStatement(sqlImage);
            for (String imageURL : imageURLs) {
                ps.setString(1, imageURL);
                ps.setInt(2, tourID);
                ps.addBatch();
            }
            ps.executeBatch();
            System.out.println("Tour images inserted for tour ID: " + tourID);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error inserting tour images: " + e.getMessage());
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
 public List<Integer> loadTourId() {
        List<Integer> list = new ArrayList<>();
        String sql = "select tourID from tour";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt(1));

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }
    public ArrayList<Tour> getToursByPartnerIDWithPagination(int partnerID, int page, int pageSize) {
        ArrayList<Tour> tours = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int start = (page - 1) * pageSize;
            String sql = "SELECT * FROM Tour WHERE partnerID = ? LIMIT ?, ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            ps.setInt(2, start);
            ps.setInt(3, pageSize);
            rs = ps.executeQuery();
            while (rs.next()) {
                Tour tour = new Tour(
                        rs.getInt("tourID"),
                        rs.getString("tourName"),
                        rs.getString("tourDescription"),
                        rs.getString("startLocation"),
                        rs.getString("endLocation"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getDouble("price"),
                        rs.getInt("numberOfPeople"),
                        rs.getString("thumbnails"),
                        rs.getBoolean("status")
                );
                tours.add(tour);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tours;
    }

    public int getTotalToursByPartnerID(int partnerID) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int total = 0;
        try {
            String sql = "SELECT COUNT(*) FROM Tour WHERE partnerID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    public ArrayList<Tour> getAllTours() {
        ArrayList<Tour> tours = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM Tour WHERE status = true";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Tour tour = new Tour(
                        rs.getInt("tourID"),
                        rs.getString("tourName"),
                        rs.getString("tourDescription"),
                        rs.getString("startLocation"),
                        rs.getString("endLocation"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getDouble("price"),
                        rs.getInt("numberOfPeople"),
                        rs.getString("thumbnails"),
                        rs.getBoolean("status")
                );
                tours.add(tour);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tours;
    }

    public Tour getTourByID(int tourID) {
        Tour tour = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM Tour WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            rs = ps.executeQuery();
            if (rs.next()) {
                tour = new Tour(
                        rs.getInt("tourID"),
                        rs.getString("tourName"),
                        rs.getString("tourDescription"),
                        rs.getString("startLocation"),
                        rs.getString("endLocation"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getDouble("price"),
                        rs.getInt("numberOfPeople"),
                        rs.getString("thumbnails"),
                        rs.getBoolean("status")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tour;
    }

    public Date[] getTourDates(int tourID) {
        Date[] dates = new Date[2];
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT startDate, endDate FROM Tour WHERE tourID = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, tourID);
            rs = ps.executeQuery();

            if (rs.next()) {
                dates[0] = rs.getDate("startDate");
                dates[1] = rs.getDate("endDate");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return dates;
    }

    // New method to get start date of a tour
    public Date getTourStartDate(int tourID) throws SQLException {
        String sql = "SELECT startDate FROM Tour WHERE tourID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, tourID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getDate("startDate");
                }
            }
        }
        return null;
    }

    public List<TourImage> getTourImagesByTourID(int tourID) {
        List<TourImage> images = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM tour_images WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            rs = ps.executeQuery();
            while (rs.next()) {
                TourImage image = new TourImage(
                        rs.getInt("imageID"),
                        rs.getString("imageURL"),
                        rs.getInt("tourID")
                );
                images.add(image);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return images;
    }

    public boolean updateTour(int tourID, String tourName, String tourDescription, String startLocation, String endLocation, Date startDate, Date endDate, Double price, int numberOfPeople, String thumbnails) {
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE Tour SET tourName = ?, tourDescription = ?, startLocation = ?, endLocation = ?, startDate = ?, endDate = ?, price = ?, numberOfPeople = ?, thumbnails = ? WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, tourName);
            ps.setString(2, tourDescription);
            ps.setString(3, startLocation);
            ps.setString(4, endLocation);
            ps.setDate(5, startDate);
            ps.setDate(6, endDate);
            ps.setDouble(7, price);
            ps.setInt(8, numberOfPeople);
            ps.setString(9, thumbnails);
            ps.setInt(10, tourID);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean updateTourImages(int tourID, ArrayList<String> imageURLs) {
        PreparedStatement ps = null;
        try {
            // First, delete existing images
            String sqlDelete = "DELETE FROM tour_images WHERE tourID = ?";
            ps = con.prepareStatement(sqlDelete);
            ps.setInt(1, tourID);
            ps.executeUpdate();
            ps.close();

            // Then, insert new images
            String sqlInsert = "INSERT INTO tour_images (imageURL, tourID) VALUES (?, ?)";
            ps = con.prepareStatement(sqlInsert);
            for (String imageURL : imageURLs) {
                ps.setString(1, imageURL);
                ps.setInt(2, tourID);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteTourImages(int tourID) {
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM tour_images WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteTour(int tourID) {
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM Tour WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Tour> searchToursByPartnerIDWithPagination(int partnerID, String search, int page, int pageSize) {
        ArrayList<Tour> tours = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int start = (page - 1) * pageSize;
            String sql = "SELECT * FROM Tour WHERE partnerID = ? AND tourName LIKE ? LIMIT ?, ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            ps.setString(2, "%" + search + "%");
            ps.setInt(3, start);
            ps.setInt(4, pageSize);
            rs = ps.executeQuery();
            while (rs.next()) {
                Tour tour = new Tour(
                        rs.getInt("tourID"),
                        rs.getString("tourName"),
                        rs.getString("tourDescription"),
                        rs.getString("startLocation"),
                        rs.getString("endLocation"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getDouble("price"),
                        rs.getInt("numberOfPeople"),
                        rs.getString("thumbnails"),
                        rs.getBoolean("status")
                );
                tours.add(tour);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tours;
    }

    public int getTotalSearchedToursByPartnerID(int partnerID, String search) {
        int total = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT COUNT(*) FROM Tour WHERE partnerID = ? AND tourName LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    public boolean deleteTourImage(int tourID, String imageUrl) {
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM tour_images WHERE tourID = ? AND imageURL = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            ps.setString(2, imageUrl);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean updateTourImages(int tourID, List<String> imageURLs) {
        PreparedStatement ps = null;
        try {
            String sqlInsert = "INSERT INTO tour_images (imageURL, tourID) VALUES (?, ?)";
            ps = con.prepareStatement(sqlInsert);
            for (String imageURL : imageURLs) {
                ps.setString(1, imageURL);
                ps.setInt(2, tourID);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean addNewTourImages(int tourID, List<String> imageURLs) {
        PreparedStatement ps = null;
        try {
            String sqlInsert = "INSERT INTO tour_images (imageURL, tourID) VALUES (?, ?)";
            ps = con.prepareStatement(sqlInsert);
            for (String imageURL : imageURLs) {
                ps.setString(1, imageURL);
                ps.setInt(2, tourID);
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public TourDetails getTourDetailsByID(int tourID) {
        Tour tour = getTourByID(tourID);
        List<Schedule> schedules = getSchedulesByTourID(tourID);
        List<TourImage> images = getTourImagesByTourID(tourID);
        return new TourDetails(tour, schedules, images);
    }

    private List<Schedule> getSchedulesByTourID(int tourID) {
        List<Schedule> schedules = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM Schedule WHERE tourID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tourID);
            rs = ps.executeQuery();
            while (rs.next()) {
                Schedule schedule = new Schedule(
                        rs.getInt("scheduleID"),
                        rs.getString("scheduleTitle"),
                        rs.getString("scheduleContent"),
                        rs.getInt("tourID"),
                        rs.getDate("scheduleDate")
                );
                schedule.setDetails(getScheduleDetailsByScheduleID(rs.getInt("scheduleID")));
                schedules.add(schedule);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return schedules;
    }

    private List<ScheduleDetails> getScheduleDetailsByScheduleID(int scheduleID) {
        List<ScheduleDetails> details = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ScheduleDetails WHERE scheduleID = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, scheduleID);
            rs = ps.executeQuery();
            while (rs.next()) {
                ScheduleDetails detail = new ScheduleDetails(
                        rs.getInt("detailID"),
                        rs.getInt("scheduleID"),
                        rs.getString("detailTitle"),
                        rs.getString("detailContent"),
                        rs.getString("note")
                );
                details.add(detail);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return details;
    }

    public String getPartnerEmailByTourID(int tourID) throws SQLException {
        String query = "SELECT p.email FROM partner p INNER JOIN tour t ON p.partnerID = t.partnerID WHERE t.tourID = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, tourID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString("email");
            }
        }
        return null;
    }
     public List<Tour> getToursByPartnerID(int partnerID) {
        List<Tour> tours = new ArrayList<>();
        String query = "SELECT * FROM tour WHERE partnerID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, partnerID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tour tour = new Tour();
                // Set tour properties
                tour.setTourID(resultSet.getInt("tourID"));
                tour.setTourName(resultSet.getString("tourName"));
                tour.setStartDate(resultSet.getDate("startDate"));
                tour.setEndDate(resultSet.getDate("endDate"));
                tour.setPrice(resultSet.getDouble("price"));
                // Add more fields as necessary

                tours.add(tour);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tours;
    }

      private List<Guide> getGuidesByTourID(int tourID) {
        List<Guide> guides = new ArrayList<>();
        String sql = "SELECT g.* FROM guide g JOIN tour_guide tg ON g.guideID = tg.guideID WHERE tg.tourID = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, tourID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Guide guide = new Guide(
                    rs.getInt("guideID"),
                    rs.getString("name"),
                    rs.getString("email"),
                    rs.getString("phoneNumber"),
                    rs.getString("experience"),
                    rs.getString("imageURL"),
                    rs.getString("address"),
                    rs.getBoolean("status"),
                         rs.getString("tourType")
                );
                guides.add(guide);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return guides;
    }
}
