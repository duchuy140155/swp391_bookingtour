package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.TourCode;

public class BookingCodeDAO extends DBContext {

    private static final String SELECT_BOOKINGS_BY_STATUS = "SELECT bookingID FROM bookingdetails WHERE status = 1";
    private static final String INSERT_BOOKING_CODE_SQL = "INSERT INTO booking_codes (bookingID, tourCode) VALUES (?, ?)";
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int CODE_LENGTH = 10;

    public BookingCodeDAO() {
        super();  // Gọi constructor của DBContext để đảm bảo connection được khởi tạo
    }

    private String generateRandomCode() {
        StringBuilder code = new StringBuilder(CODE_LENGTH);
        Random random = new Random();

        for (int i = 0; i < CODE_LENGTH; i++) {
            code.append(CHARACTERS.charAt(random.nextInt(CHARACTERS.length())));
        }

        return code.toString();
    }

    public List<Integer> getBookingIDsWithStatusOne() throws SQLException {
        List<Integer> bookingIDs = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try {
            preparedStatement = connection.prepareStatement(SELECT_BOOKINGS_BY_STATUS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                bookingIDs.add(rs.getInt("bookingID"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return bookingIDs;
    }

    public void generateAndSaveBookingCodes() throws SQLException {
        List<Integer> bookingIDs = getBookingIDsWithStatusOne();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(INSERT_BOOKING_CODE_SQL);
            for (int bookingID : bookingIDs) {
                String tourCode = generateRandomCode();
                preparedStatement.setInt(1, bookingID);
                preparedStatement.setString(2, tourCode);
                preparedStatement.executeUpdate();
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public TourCode getCodebyBkID(int bookingID) {
        TourCode code = null;
        String query = "SELECT * FROM bookingsystem.booking_codes\n"
                + "where bookingID = ?";
        try(PreparedStatement pst= connection.prepareStatement(query)) {
            pst.setInt(1, bookingID);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                String tourCode = rs.getString("tourCode");
                code = new TourCode(id, bookingID, tourCode);
            }
        } catch (Exception e) {
        }
        return code;
    }
}
