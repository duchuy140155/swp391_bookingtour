package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BookingDetails;
import model.CancelledBooking;
import model.Tour;
import model.User;

public class BookingDetailsDAO extends DBContext {

    private static final Logger LOGGER = Logger.getLogger(BookingDetailsDAO.class.getName());

    public BookingDetails getContentForFeedback(int userID, int tourID) {
        BookingDetails bookingtour = null;
        UserDAO userDao = new UserDAO();
        TourDAO tourDao = new TourDAO();
        String query = "SELECT b.bookingID, b.userID, b.tourID, t.startDate, t.endDate FROM bookingsystem.bookingdetails b "
                + "INNER JOIN bookingsystem.tour t "
                + "ON b.tourID = t.tourID "
                + "WHERE b.userID = ? AND b.tourID = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, userID);
            ps.setInt(2, tourID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("bookingID");
                User user = userDao.getUserById(userID);
                Tour tour = tourDao.getTourByID(tourID);
                bookingtour = new BookingDetails(id, tour, user);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in getContentForFeedback: ", e);
        }
        return bookingtour;
    }

    public List<BookingDetails> getAllBookingDetails() {
        List<BookingDetails> bookingList = new ArrayList<>();
        String query = "SELECT b.bookingID, b.finalPrice, b.note, b.email, b.phoneNumber, b.status, b.bookingDate, "
                + "t.tourID, t.tourName, t.startDate, t.endDate "
                + "FROM bookingdetails b "
                + "INNER JOIN tour t ON b.tourID = t.tourID";

        try (PreparedStatement stmt = connection.prepareStatement(query); ResultSet rs = stmt.executeQuery()) {

            while (rs.next()) {
                BookingDetails booking = new BookingDetails();
                booking.setBookingID(rs.getInt("bookingID"));
                booking.setFinalPrice(rs.getDouble("finalPrice"));
                booking.setNote(rs.getString("note"));
                booking.setEmail(rs.getString("email"));
                booking.setPhoneNumber(rs.getInt("phoneNumber"));
                booking.setStatus(rs.getInt("status"));
                booking.setBookingDate(rs.getDate("bookingDate"));

                Tour tour = new Tour();
                tour.setTourID(rs.getInt("tourID"));
                tour.setTourName(rs.getString("tourName"));
                tour.setStartDate(rs.getDate("startDate"));
                tour.setEndDate(rs.getDate("endDate"));

                booking.setTour(tour);

                bookingList.add(booking);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookingList;
    }

    public BookingDetails getBookingDetailsById(int bookingID) {
        BookingDetails booking = null;
        String query = "SELECT bookingID, finalPrice, note, email, phoneNumber, status, bookingDate,CustomerName FROM bookingdetails WHERE bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                booking = new BookingDetails();
                booking.setBookingID(rs.getInt("bookingID"));
                booking.setFinalPrice(rs.getDouble("finalPrice"));
                booking.setNote(rs.getString("note"));
                booking.setEmail(rs.getString("email"));
                booking.setPhoneNumber(rs.getInt("phoneNumber"));
                booking.setStatus(rs.getInt("status"));
                booking.setBookingDate(rs.getDate("bookingDate"));
                booking.setCustomerName(rs.getString("CustomerName"));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in getBookingDetailsById: ", e);
        }
        return booking;
    }

    public double calculatePenalty(int bookingID) {
        double penaltyPercentage = 0.0;
        try {
            String query = "SELECT t.startDate FROM bookingdetails b INNER JOIN tour t ON b.tourID = t.tourID WHERE b.bookingID = ?";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Date startDate = rs.getDate("startDate");
                LocalDate today = LocalDate.now();
                long daysBetween = java.time.temporal.ChronoUnit.DAYS.between(today, startDate.toLocalDate());

                if (daysBetween >= 14) {
                    penaltyPercentage = 0.20;
                } else if (daysBetween >= 10) {
                    penaltyPercentage = 0.30;
                } else if (daysBetween >= 7) {
                    penaltyPercentage = 0.50;
                } else if (daysBetween >= 6) {
                    penaltyPercentage = 0.60;
                } else if (daysBetween >= 5) {
                    penaltyPercentage = 0.70;
                } else if (daysBetween >= 4) {
                    penaltyPercentage = 0.80;
                } else if (daysBetween >= 3) {
                    penaltyPercentage = 0.90;
                } else {
                    penaltyPercentage = 1.00;
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in calculatePenalty: ", e);
        }
        return penaltyPercentage;
    }

    public double calculateRefund(int bookingID, double penaltyPercentage) {
        double refundAmount = 0.0;
        try {
            String query = "SELECT finalPrice FROM bookingdetails WHERE bookingID = ?";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                double finalPrice = rs.getDouble("finalPrice");
                refundAmount = finalPrice * (1 - penaltyPercentage);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in calculateRefund: ", e);
        }
        return refundAmount;
    }

    public void cancelBooking(int bookingID, String cancelReason, double penaltyPercentage, double refundAmount) {
        try {

            String updateQuery = "UPDATE bookingdetails SET status = 2 WHERE bookingID = ?";
            PreparedStatement updateStmt = connection.prepareStatement(updateQuery);
            updateStmt.setInt(1, bookingID);
            updateStmt.executeUpdate();

            String insertQuery = "INSERT INTO cancelledbookings (bookingID, cancelDate, cancelReason, penaltyPercentage, refundAmount, status) VALUES (?, ?, ?, ?, ?, 0)"; 
            PreparedStatement insertStmt = connection.prepareStatement(insertQuery);
            insertStmt.setInt(1, bookingID);
            insertStmt.setDate(2, Date.valueOf(LocalDate.now()));
            insertStmt.setString(3, cancelReason);
            insertStmt.setDouble(4, penaltyPercentage);
            insertStmt.setDouble(5, refundAmount);
            insertStmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in cancelBooking: ", e);
        }
    }

  public void confirmCancel(int cancelID, int bookingID) {
    try {
        
        String updateBookingQuery = "UPDATE bookingdetails SET status = 2 WHERE bookingID = ?"; // status 2: Đã hủy
        PreparedStatement updateBookingStmt = connection.prepareStatement(updateBookingQuery);
        updateBookingStmt.setInt(1, bookingID);
        updateBookingStmt.executeUpdate();

      
        String updateCancelQuery = "UPDATE cancelledbookings SET status = 1 WHERE cancelID = ?"; // status 1: Đã xác nhận hủy
        PreparedStatement updateCancelStmt = connection.prepareStatement(updateCancelQuery);
        updateCancelStmt.setInt(1, cancelID);
        updateCancelStmt.executeUpdate();
    } catch (SQLException e) {
        LOGGER.log(Level.SEVERE, "SQL error in confirmCancel: ", e);
    }
}


    public List<CancelledBooking> getAllCancelledBookings() {
        List<CancelledBooking> cancelList = new ArrayList<>();
        String query = "SELECT * FROM cancelledbookings";
        try (PreparedStatement stmt = connection.prepareStatement(query); ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                CancelledBooking cancel = new CancelledBooking();
                cancel.setCancelID(rs.getInt("cancelID"));
                cancel.setBookingID(rs.getInt("bookingID"));
                cancel.setCancelDate(rs.getDate("cancelDate"));
                cancel.setCancelReason(rs.getString("cancelReason"));
                cancel.setPenaltyPercentage(rs.getDouble("penaltyPercentage"));
                cancel.setRefundAmount(rs.getDouble("refundAmount"));
                cancel.setStatus(rs.getInt("status"));
                cancelList.add(cancel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cancelList;
    }

    public void updateCancelStatus(int cancelID, int status) {
        String query = "UPDATE cancelledbookings SET status = ? WHERE cancelID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, status);
            stmt.setInt(2, cancelID);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<CancelledBooking> getCancelledBookingsByPartner(int partnerID) {
        List<CancelledBooking> cancelList = new ArrayList<>();
        String query = "SELECT cb.cancelID, cb.bookingID, cb.cancelDate, cb.cancelReason, cb.penaltyPercentage, cb.refundAmount, cb.status, "
                + "t.tourName, t.startDate, t.endDate "
                + "FROM cancelledbookings cb "
                + "INNER JOIN bookingdetails bd ON cb.bookingID = bd.bookingID "
                + "INNER JOIN tour t ON bd.tourID = t.tourID "
                + "WHERE t.partnerID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, partnerID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                CancelledBooking cancel = new CancelledBooking();
                cancel.setCancelID(rs.getInt("cancelID"));
                cancel.setBookingID(rs.getInt("bookingID"));
                cancel.setCancelDate(rs.getDate("cancelDate"));
                cancel.setCancelReason(rs.getString("cancelReason"));
                cancel.setPenaltyPercentage(rs.getDouble("penaltyPercentage"));
                cancel.setRefundAmount(rs.getDouble("refundAmount"));
                cancel.setStatus(rs.getInt("status"));
                cancel.setTourName(rs.getString("tourName"));
                cancel.setStartDate(rs.getDate("startDate"));
                cancel.setEndDate(rs.getDate("endDate"));
                cancelList.add(cancel);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in getCancelledBookingsByPartner: ", e);
        }
        return cancelList;
    }

    public LocalDate getStartTourDate(int bookingID) {
        LocalDate startDate = null;
        String query = "SELECT t.startDate FROM bookingdetails b INNER JOIN tour t ON b.tourID = t.tourID WHERE b.bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                startDate = rs.getDate("startDate").toLocalDate();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in getStartTourDate: ", e);
        }
        return startDate;
    }

    public String getCustomerEmail(int bookingID) {
        String email = "";
        String query = "SELECT email FROM bookingdetails WHERE bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                email = rs.getString("email");
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL error in getCustomerEmail: ", e);
        }
        return email;
    }

    public Date getTourStartDate(int bookingID) {
        Date startDate = null;
        String query = "SELECT t.startDate FROM bookingdetails b INNER JOIN tour t ON b.tourID = t.tourID WHERE b.bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                startDate = rs.getDate("startDate");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public List<BookingDetails> getCancelledBookings() {
        List<BookingDetails> cancelledBookings = new ArrayList<>();
        String sql = "SELECT tourName, startDate, endDate, cancelDate, cancelReason, penaltyPercentage, refundAmount, status FROM BookingDetails WHERE status = 'Cancelled'";

        return cancelledBookings;
    }

    public boolean isBookingExist(int bookingID) {
        String query = "SELECT 1 FROM BookingDetails WHERE bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isBookingActive(int bookingID) {
        String query = "SELECT status FROM BookingDetails WHERE bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("status") == 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Date getBookingStartDate(int bookingID) {
        String query = "SELECT startDate FROM Tour WHERE tourID = (SELECT tourID FROM BookingDetails WHERE bookingID = ?)";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getDate("startDate");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public double getFinalPrice(int bookingID) {
        String query = "SELECT finalPrice FROM BookingDetails WHERE bookingID = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getDouble("finalPrice");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    private int noOfRecords;

    public List<BookingDetails> getBookerTourbyUserID(int userID, int offset, int noOfRecords) {
        List<BookingDetails> bookingDetails = new ArrayList<>();
        TourDAO tourDao = new TourDAO();
        String query = "SELECT SQL_CALC_FOUND_ROWS bookingID, finalPrice, tourID, status, bookingDate, totalPeople "
                + "FROM bookingsystem.bookingdetails "
                + "WHERE userID = ? "
                + "ORDER BY bookingDate DESC "
                + "LIMIT ?, ?";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, userID);
            pst.setInt(2, offset);
            pst.setInt(3, noOfRecords);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("bookingID");
                double finalPrice = rs.getDouble("finalPrice");
                int tourID = rs.getInt("tourID");
                Tour tour = tourDao.getTourByID(tourID);
                int status = rs.getInt("status");
                Date bookingDate = rs.getDate("bookingDate");
                int totalPeople = rs.getInt("totalPeople");
                BookingDetails book = new BookingDetails(id, finalPrice, tour, status, bookingDate, totalPeople);
                bookingDetails.add(book);
            }
            rs.close();
            rs = pst.executeQuery("SELECT FOUND_ROWS()");
            if (rs.next()) {
                this.noOfRecords = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingDetails;
    }

    public int getNoOfRecords() {
        return noOfRecords;
    }

    public BookingDetails getBookedTourbyBkID(int bookingID) {
        TourDAO tourDao = new TourDAO();
        BookingDetails bookingDetails = null;
        String query = "SELECT bookingID, finalPrice, tourID, status,bookingDate, totalPeople \n"
                + "FROM bookingsystem.bookingdetails \n"
                + "where bookingID = ?";
        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setInt(1, bookingID);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                double finalPrice = rs.getDouble("finalPrice");
                int tourID = rs.getInt("tourID");
                Tour tour = tourDao.getTourByID(tourID);
                int status = rs.getInt("status");
                Date bookingDate = rs.getDate("bookingDate");
                int totalPeople = rs.getInt("totalPeople");
                bookingDetails = new BookingDetails(bookingID, finalPrice, tour, status, bookingDate, totalPeople);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingDetails;

    }

  
   
  public BookingDetails getBookingDetailsByTourCode(String tourCode) {
    BookingDetails bookingDetails = null;
    String query = "SELECT bd.bookingID, bc.tourCode, bd.customerName, bd.bookingDate " +
                   "FROM bookingdetails bd " +
                   "INNER JOIN booking_codes bc ON bd.bookingID = bc.bookingID " +
                   "WHERE bc.tourCode = ?";
    try (PreparedStatement ps = connection.prepareStatement(query)) {
        ps.setString(1, tourCode);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            bookingDetails = new BookingDetails();
            bookingDetails.setBookingID(rs.getInt("bookingID"));
            bookingDetails.setTourCode(rs.getString("tourCode"));
            bookingDetails.setCustomerName(rs.getString("customerName"));
            bookingDetails.setBookingDate(rs.getDate("bookingDate"));
        }
    } catch (SQLException e) {
        LOGGER.log(Level.SEVERE, "SQL error in getBookingDetailsByTourCode: ", e);
    }
    return bookingDetails;
}

public CancelledBooking getCancelledBookingByBookingID(int bookingID) {
    CancelledBooking cancelledBooking = null;
    String query = "SELECT * FROM cancelledbookings WHERE bookingID = ?";
    try (PreparedStatement ps = connection.prepareStatement(query)) {
        ps.setInt(1, bookingID);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            cancelledBooking = new CancelledBooking();
            cancelledBooking.setCancelID(rs.getInt("cancelID"));
            cancelledBooking.setBookingID(rs.getInt("bookingID"));
            cancelledBooking.setCancelDate(rs.getDate("cancelDate"));
            cancelledBooking.setCancelReason(rs.getString("cancelReason"));
            cancelledBooking.setPenaltyPercentage(rs.getDouble("penaltyPercentage"));
            cancelledBooking.setRefundAmount(rs.getDouble("refundAmount"));
            cancelledBooking.setStatus(rs.getInt("status"));
        }
    } catch (SQLException e) {
        LOGGER.log(Level.SEVERE, "SQL error in getCancelledBookingByBookingID: ", e);
    }
    return cancelledBooking;
}


   public List<CancelledBooking> getProcessingCancelledBookingsByPartner(int partnerID) {
    List<CancelledBooking> cancelledBookings = new ArrayList<>();
    String query = "SELECT cb.cancelID, cb.bookingID, cb.cancelDate, cb.cancelReason, cb.penaltyPercentage, cb.refundAmount, cb.status, "
            + "t.tourName, t.startDate, t.endDate "
            + "FROM cancelledbookings cb "
            + "INNER JOIN bookingdetails bd ON cb.bookingID = bd.bookingID "
            + "INNER JOIN tour t ON bd.tourID = t.tourID "
            + "WHERE t.partnerID = ? AND cb.status = 0";

    try (PreparedStatement stmt = connection.prepareStatement(query)) {
        stmt.setInt(1, partnerID);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            CancelledBooking booking = new CancelledBooking();
            booking.setCancelID(rs.getInt("cancelID"));
            booking.setBookingID(rs.getInt("bookingID"));
            booking.setTourName(rs.getString("tourName"));
            booking.setStartDate(rs.getDate("startDate"));
            booking.setEndDate(rs.getDate("endDate"));
            booking.setCancelDate(rs.getDate("cancelDate"));
            booking.setCancelReason(rs.getString("cancelReason"));
            booking.setPenaltyPercentage(rs.getDouble("penaltyPercentage"));
            booking.setRefundAmount(rs.getDouble("refundAmount"));
            booking.setStatus(rs.getInt("status"));
            cancelledBookings.add(booking);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return cancelledBookings;
}

public List<CancelledBooking> getCancelledBookings(int partnerID) {
    List<CancelledBooking> cancelList = new ArrayList<>();
    String query = "SELECT cb.cancelID, cb.bookingID, cb.cancelDate, cb.cancelReason, cb.penaltyPercentage, cb.refundAmount, cb.status, "
            + "t.tourName, t.startDate, t.endDate "
            + "FROM cancelledbookings cb "
            + "INNER JOIN bookingdetails bd ON cb.bookingID = bd.bookingID "
            + "INNER JOIN tour t ON bd.tourID = t.tourID "
            + "WHERE t.partnerID = ? AND cb.status = 1"; 

    try (PreparedStatement stmt = connection.prepareStatement(query)) {
        stmt.setInt(1, partnerID);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            CancelledBooking cancel = new CancelledBooking();
            cancel.setCancelID(rs.getInt("cancelID"));
            cancel.setBookingID(rs.getInt("bookingID"));
            cancel.setTourName(rs.getString("tourName"));
            cancel.setStartDate(rs.getDate("startDate"));
            cancel.setEndDate(rs.getDate("endDate"));
            cancel.setCancelDate(rs.getDate("cancelDate"));
            cancel.setCancelReason(rs.getString("cancelReason"));
            cancel.setPenaltyPercentage(rs.getDouble("penaltyPercentage"));
            cancel.setRefundAmount(rs.getDouble("refundAmount"));
            cancel.setStatus(rs.getInt("status"));
            cancelList.add(cancel);
        }
    } catch (SQLException e) {
        LOGGER.log(Level.SEVERE, "SQL error in getCancelledBookingsByPartner: ", e);
    }
    return cancelList;
}
}
