package dal;

import model.Partner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PartnerDAO {

    private Connection con;
    private ArrayList<Partner> partners = new ArrayList<>();

    public PartnerDAO() {
        con = new DBContext().connection;
    }

    public void createPartner(String partnerName, String email, String password, int phoneNumber,
            String address, String certificate, Boolean status) {
        try {
            String sql = "INSERT INTO `bookingsystem`.`partner`"
                    + "(`partnerName`, `email`, `password`, `phoneNumber`, `address`, `certificate`, `status`, `roleID`)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, partnerName);
            ps.setString(2, email);
            ps.setString(3, password);
            ps.setInt(4, phoneNumber);
            ps.setString(5, address);
            ps.setString(6, certificate);
            ps.setBoolean(7, false);
            ps.setInt(8, 3);
            int rowsAffected = ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Error creating user: " + e.getMessage(), e);
        }
    }

    public boolean checkEmail(String email) {
        String sql = "SELECT * FROM `bookingsystem`.`partner` WHERE email = ?";
        boolean check = false;
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }

    public Partner PartnerAccount(String email, String password) {
        String sql = "SELECT * FROM partner WHERE email = ? AND password = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Partner partner = new Partner(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getBoolean(8),
                        rs.getInt(9)
                );
                return partner;
            }
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
        }
        return null;
    }

    public List<Partner> getPendingPartners() {
        List<Partner> pendingPartners = new ArrayList<>();
        String sql = "SELECT * FROM partner WHERE status = false";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Partner partner = new Partner(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getBoolean(8),
                        rs.getInt(9)
                );
                pendingPartners.add(partner);
            }
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
        }
        return pendingPartners;
    }

    public void activatePartner(int partnerID) {
        String sql = "UPDATE partner SET status = true WHERE partnerID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
        }
    }

    public Partner getPartnerById(int partnerID) {
        String sql = "SELECT * FROM partner WHERE partnerID = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, partnerID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Partner(
                        rs.getInt("partnerID"),
                        rs.getString("partnerName"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getInt("phoneNumber"),
                        rs.getString("address"),
                        rs.getString("certificate"),
                        rs.getBoolean("status"),
                        rs.getInt("roleID")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Partner> getPendingPartners(String search, int start, int total) {
        List<Partner> pendingPartners = new ArrayList<>();
        String sql = "SELECT * FROM partner WHERE status = false AND (partnerName LIKE ? OR email LIKE ?) LIMIT ?, ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setInt(3, start);
            ps.setInt(4, total);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Partner partner = new Partner(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getBoolean(8),
                        rs.getInt(9)
                );
                pendingPartners.add(partner);
            }
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
        }
        return pendingPartners;
    }

    public int getTotalPendingPartners(String search) {
        String sql = "SELECT COUNT(*) FROM partner WHERE status = false AND (partnerName LIKE ? OR email LIKE ?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.err.println("SQL Error: " + e.getMessage());
        }
        return 0;
    }
       public boolean updatePartner(int partnerID, String partnerName, String email, String phoneNumber, String address, String certificate) {
        String sql = "UPDATE partner SET partnerName=?, email=?, phoneNumber=?, address=?, certificate=? WHERE partnerID=?";
        try (PreparedStatement pstmt = con.prepareStatement(sql)) {
            pstmt.setString(1, partnerName);
            pstmt.setString(2, email);
            pstmt.setString(3, phoneNumber);
            pstmt.setString(4, address);
            pstmt.setString(5, certificate);
            pstmt.setInt(6, partnerID);

            int rowsUpdated = pstmt.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
             System.err.println("SQL Error: " + e.getMessage());
            return false;
        }
    }

}
