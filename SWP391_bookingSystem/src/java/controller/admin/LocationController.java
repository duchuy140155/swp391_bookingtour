/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dal.LocationDAO;
import model.Location;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.util.List;

@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15 // 15 MB
)
public class LocationController extends HttpServlet {

    private static final String UPLOAD_DIR = "uploads";
    private static final int ITEMS_PER_PAGE = 5;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        LocationDAO locationDAO = new LocationDAO();

        if (action != null) {
            switch (action) {
                case "add":
                    showAddForm(request, response);
                    return;
                case "edit":
                    showEditForm(request, response);
                    return;
                case "delete":
                    deleteLocation(request, response);
                    return;
            }
        }

        String search = request.getParameter("search");
        String sortField = request.getParameter("sortField") != null ? request.getParameter("sortField") : "locationName";
        String sortOrder = request.getParameter("sortOrder") != null ? request.getParameter("sortOrder") : "ASC";
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int pageSize = ITEMS_PER_PAGE;

        List<Location> locations;
        int totalLocations;
        if (search != null && !search.isEmpty()) {
            locations = locationDAO.searchLocations(search, (page - 1) * pageSize, pageSize);
            totalLocations = locationDAO.countLocations(search);
        } else {
            locations = locationDAO.getLocationsWithPaginationAndSorting(page, pageSize, sortField, sortOrder);
            totalLocations = locationDAO.getTotalLocations();
        }

        int totalPages = (int) Math.ceil((double) totalLocations / pageSize);

        request.setAttribute("locations", locations);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("search", search);
        request.setAttribute("sortField", sortField);
        request.setAttribute("sortOrder", sortOrder);

        String success = request.getParameter("success");
        if (success != null) {
            if (success.equals("true")) {
                request.setAttribute("success", "Địa điểm đã được thêm thành công.");
            } else if (success.equals("delete")) {
                request.setAttribute("success", "Địa điểm đã được xóa thành công.");
            }
        }
        request.getRequestDispatcher("adminLocation.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null && action.equals("update")) {
            updateLocation(request, response);
        } else {
            addLocation(request, response);
        }
    }

    private void showAddForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("addLocation.jsp").forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int locationId = Integer.parseInt(request.getParameter("locationId"));
        LocationDAO locationDAO = new LocationDAO();
        Location location = locationDAO.getLocationByID(locationId);
        request.setAttribute("location", location);
        request.getRequestDispatcher("editLocation.jsp").forward(request, response);
    }

    private void deleteLocation(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int locationId = Integer.parseInt(request.getParameter("locationId"));
        LocationDAO locationDAO = new LocationDAO();
        locationDAO.deleteLocation(locationId);
        response.sendRedirect("locations?success=delete");
    }

    private void updateLocation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int locationId = Integer.parseInt(request.getParameter("locationId"));
        String locationName = request.getParameter("locationName");
        String locationDescription = request.getParameter("locationDescription");
        Part filePart = request.getPart("locationImage");

        LocationDAO locationDAO = new LocationDAO();
        Location location = locationDAO.getLocationByID(locationId);

        if (filePart != null && filePart.getSize() > 0) {
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR;
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String filePath = uploadPath + File.separator + fileName;
            try (InputStream fileContent = filePart.getInputStream()) {
                Files.copy(fileContent, Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
            }
            location.setLocationImage(fileName);
        }

        location.setLocationName(locationName);
        location.setLocationDescription(locationDescription);

        locationDAO.updateLocation(location);
        response.sendRedirect("locations?success=true");
    }

    private void addLocation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String locationName = request.getParameter("locationName");
        String locationDescription = request.getParameter("locationDescription");
        Part filePart = request.getPart("locationImage");

        if (filePart == null || filePart.getSubmittedFileName().isEmpty()) {
            request.setAttribute("error", "File hình ảnh không được để trống.");
            request.getRequestDispatcher("addLocation.jsp").forward(request, response);
            return;
        }

        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String applicationPath = getServletContext().getRealPath("");
        String uploadPath = applicationPath + File.separator + UPLOAD_DIR;
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        String filePath = uploadPath + File.separator + fileName;
        try (InputStream fileContent = filePart.getInputStream()) {
            Files.copy(fileContent, Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
        }

        int tourId = 1; // Giá trị mặc định cho tourId

        Location location = new Location(0, locationName, locationDescription, fileName, tourId);
        LocationDAO locationDAO = new LocationDAO();
        locationDAO.addLocation(location);

        response.sendRedirect("locations?success=true");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
