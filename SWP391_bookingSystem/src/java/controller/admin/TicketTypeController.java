/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dal.TicketDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TicketType;


/**
 *
 * @author Admin
 */
public class TicketTypeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        try {
            if (action == null) {
                 listTicketType(request, response);
            } else {
                switch (action) {
                    case "editTicket":
                        editTicket(request, response);
                        break;
                    case "deleteTicket":
                        deleteTicket(request, response);
                        break;
                    default:
                        listTicketType(request, response);
                        break;
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         int typeID =Integer.parseInt(request.getParameter("typeID"));
        String typeName = request.getParameter("typeName");
        String typeCode = request.getParameter("typeCode");
        int ageMin = Integer.parseInt(request.getParameter("ageMin"));
        int ageMax = Integer.parseInt(request.getParameter("ageMax"));
        boolean isDefault = Boolean.parseBoolean(request.getParameter("isDefault"));

        // Update the ticket in the database
        TicketDAO ticketDAO = new TicketDAO();
        TicketType ticket = new TicketType();
        ticket.setTypeID(typeID);
        ticket.setTypeName(typeName);
        ticket.setTypeCode(typeCode);
        ticket.setAgeMin(ageMin);
        ticket.setAgeMax(ageMax);
        ticket.setIsDefault(isDefault);

        boolean success = ticketDAO.updateTicket(ticket);

        if (success) {
            response.sendRedirect("/SWP391_bookingSystem/listTicket");
        } else {
            request.setAttribute("errorMessage", "Error updating ticket");
            request.getRequestDispatcher("editTicket.jsp").forward(request, response);
        }
    }   

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void editTicket(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        TicketDAO dal = new TicketDAO();
         int typeID = Integer.parseInt(request.getParameter("typeID"));
         TicketType  ticket = dal.getTicketByTypeID(typeID);
         request.setAttribute("ticket", ticket);
         request.getRequestDispatcher("editTicket.jsp").forward(request, response);
    }

    private void deleteTicket(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int typeID = Integer.parseInt(request.getParameter("typeID"));
        TicketDAO dal = new TicketDAO();
        try {
            dal.deleteTicket(typeID);
            response.sendRedirect("/SWP391_bookingSystem/listTicket");
        } catch (SQLException ex) {
            Logger.getLogger(ManagerTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listTicketType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TicketDAO dal = new TicketDAO();
        List<TicketType> tickets = dal.getAllTicket();
        request.setAttribute("tickets", tickets);
        request.getRequestDispatcher("ticketList.jsp").forward(request, response);
    }

}
