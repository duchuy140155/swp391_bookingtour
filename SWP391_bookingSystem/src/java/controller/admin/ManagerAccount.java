package controller.admin;

import dal.UserDAO;
import dal.RoleDAO;
import dal.DBContext;
import model.User;
import model.Role;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


public class ManagerAccount extends HttpServlet {

    private UserDAO userDAO;
    private RoleDAO roleDAO;

    @Override
    public void init() throws ServletException {
        DBContext dbContext = new DBContext();
        Connection connection = dbContext.connection;
        userDAO = new UserDAO(connection);
        roleDAO = new RoleDAO(connection);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Integer currentID = (Integer) session.getAttribute("currentUserID");
            if (currentID == null) {
                response.sendRedirect("login");
                return;
            }

            int currentRoleID = userDAO.getRoleId(currentID);

            Map<Integer, Date> lastAccessTimes = (Map<Integer, Date>) session.getServletContext().getAttribute("lastAccessTimes");
            if (lastAccessTimes == null) {
                lastAccessTimes = new HashMap<>();
                session.getServletContext().setAttribute("lastAccessTimes", lastAccessTimes);
            }
            lastAccessTimes.put(currentID, new Date());

            int page = 1;
            int pageSize = 5;
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            String search = request.getParameter("search");
            List<User> userList;
            int totalUsers;

            if (search != null && !search.trim().isEmpty()) {
                userList = userDAO.searchUsers(search, page, pageSize);
                totalUsers = userDAO.getTotalUsers();
            } else {
                userList = userDAO.getAllUsers(page, pageSize);
                totalUsers = userDAO.getTotalUsers();
            }

            List<User> userListWithRole2 = userDAO.getAllUsersWithRole(2);

            int totalPages = (int) Math.ceil((double) totalUsers / pageSize);
            List<Role> roleList = roleDAO.getAllRoles();

            request.setAttribute("userList", userList);
            request.setAttribute("userListWithRole2", userListWithRole2);
            request.setAttribute("roleList", roleList);
            request.setAttribute("currentPage", page);
            request.setAttribute("totalPages", totalPages);
            request.setAttribute("search", search);
            request.setAttribute("currentRoleID", currentRoleID);
            request.setAttribute("lastAccessTimes", lastAccessTimes);

            request.getRequestDispatcher("ManagerAccount.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Lỗi cơ sở dữ liệu");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        try {
            HttpSession session = request.getSession();
            Integer currentID = (Integer) session.getAttribute("currentUserID");
            if (currentID == null) {
                response.sendRedirect("login");
                return;
            }

            int userID = Integer.parseInt(request.getParameter("userID"));
            int currentRoleID = userDAO.getRoleId(currentID);

            if ("promoteToAdmin".equals(action)) {
                try {
                    int promoteUserID = Integer.parseInt(request.getParameter("promoteUserID"));
                    userDAO.updateUserRole(promoteUserID, 1);
                    response.sendRedirect("manageRoles");
                } catch (NumberFormatException e) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Định dạng ID không hợp lệ");
                }
                return;
            }

            if (!userDAO.canManageRole(currentID, userID)) {
                request.setAttribute("error", "Bạn không có quyền quản lý tài khoản này.");
                doGet(request, response);
                return;
            }

            // Check if the current role can delete the user based on roleID
            boolean canDelete = false;
            if (currentRoleID == 4 && (userDAO.getRoleId(userID) == 1 || userDAO.getRoleId(userID) == 2 || userDAO.getRoleId(userID) == 3 || userDAO.getRoleId(userID) == 5)) {
                canDelete = true;
            } else if (currentRoleID == 1 && (userDAO.getRoleId(userID) == 2 || userDAO.getRoleId(userID) == 3 || userDAO.getRoleId(userID) == 5)) {
                canDelete = true;
            }

            if ("delete".equals(action) && canDelete) {
                userDAO.deleteUser(userID);
                response.sendRedirect("manageRoles");
            } else if ("toggleStatus".equals(action)) {
                userDAO.toggleStatus(userID);
                response.sendRedirect("manageRoles");
            } else {
                request.setAttribute("error", "Bạn không có quyền thực hiện hành động này.");
                doGet(request, response);
            }
        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Định dạng ID không hợp lệ");
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Lỗi cơ sở dữ liệu");
        }
    }
}
