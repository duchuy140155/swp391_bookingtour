/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.partner;

import dal.DiscountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Discount;

/**
 *
 * @author Administrator
 */
public class ActionDiscount extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ActionDiscount</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ActionDiscount at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        DiscountDAO d = new DiscountDAO();
        
        HttpSession session = request.getSession();
        String type = request.getParameter("type");

        String id = request.getParameter("id");
        if (type.equals("1")) {
            int Id = Integer.parseInt(id);
            d.deleteDiscount(Id);
            session.setAttribute("loadDiscount", d.loadAllDiscount());
            response.sendRedirect("discountManage.jsp");

        }
        if (type.equals("2")) {
            try {
                int Id = Integer.parseInt(id);
                Discount discount = d.loadDiscountById(Id);
                session.setAttribute("discount", discount);
                // Debugging: Print the discount object
                List<Integer> list = d.loadTourId();
                session.setAttribute("loadTourId", list);
                System.out.println("Set Discount in Session: " + discount);
                response.sendRedirect("editDiscount.jsp");
            } catch (NumberFormatException e) {
                e.printStackTrace();  // Print stack trace for debugging
                request.setAttribute("error", "Invalid input format.");
                request.getRequestDispatcher("errorPage.jsp").forward(request, response);
            }

        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        DiscountDAO d = new DiscountDAO();
        String id = request.getParameter("id");
        int Id = Integer.parseInt(id);
        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String dateFrom = request.getParameter("from");
        String dateTo = request.getParameter("to");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String percent = request.getParameter("percent");
        String reduce = request.getParameter("reduce");
        String tourId = request.getParameter("tourid");
        int Type = Integer.parseInt(type);
        int Percent = Integer.parseInt(percent);
        int Reduce = Integer.parseInt(reduce);
        int Tourid = Integer.parseInt(tourId);
        Boolean Status;
        if (status.equals("1")) {
            Status = true;

        } else {
            Status = false;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {

            Date from = dateFormat.parse(dateFrom);
            Date to = dateFormat.parse(dateTo);
// Convert java.util.Date to java.sql.Date
            java.sql.Date sqlFromDate = new java.sql.Date(from.getTime());
            java.sql.Date sqlToDate = new java.sql.Date(to.getTime());

            Discount a = new Discount(Id, code,
                    name, sqlFromDate, sqlToDate,
                    Status, Type, Percent,
                    Reduce, Tourid);
            d.updateDiscount(a);
            session.setAttribute("loadDiscount", d.loadAllDiscount());
            response.sendRedirect("discountManage.jsp");
        } catch (ParseException ex) {
            Logger.getLogger(ActionDiscount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
