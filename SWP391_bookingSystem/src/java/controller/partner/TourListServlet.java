package controller.partner;

import dal.GuideDAO;
import dal.TourDAO;
import model.Tour;
import model.Guide;
import com.google.gson.Gson;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TourListServlet extends HttpServlet {

    private TourDAO tourDAO;
    private GuideDAO guideDAO;

    public void init() {
        tourDAO = new TourDAO();
        guideDAO = new GuideDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        Integer partnerID = (Integer) session.getAttribute("partnerID");

        if (partnerID == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        String tourIDParam = request.getParameter("tourID");
        if (tourIDParam != null) {
            try {
                int tourID = Integer.parseInt(tourIDParam);
                List<Guide> guides = guideDAO.selectGuidesByTourID(tourID);
                String json = new Gson().toJson(guides);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                return;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        List<Tour> tours = tourDAO.getToursByPartnerID(partnerID);
        Map<Integer, Boolean> tourGuideStatus = new HashMap<>();
        for (Tour tour : tours) {
            List<Guide> guides = guideDAO.selectGuidesByTourID(tour.getTourID());
            tourGuideStatus.put(tour.getTourID(), guides != null && !guides.isEmpty());
        }

        request.setAttribute("tours", tours);
        request.setAttribute("tourGuideStatus", tourGuideStatus);

        List<Guide> inactiveGuides = guideDAO.getInactiveGuides();
        request.setAttribute("inactiveGuides", inactiveGuides);

        request.getRequestDispatcher("ManagerGuide.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
