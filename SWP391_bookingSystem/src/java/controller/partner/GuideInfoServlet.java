package controller.partner;

import dal.GuideDAO;
import model.User;
import java.io.IOException;
import java.sql.SQLException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.logging.Logger;


public class GuideInfoServlet extends HttpServlet {

    private GuideDAO guideDAO;
    private static final Logger logger = Logger.getLogger(GuideInfoServlet.class.getName());

    @Override
    public void init() throws ServletException {
        guideDAO = new GuideDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        if (user == null) {
            logger.info("User not logged in, redirecting to login page.");
            response.sendRedirect("login");
            return;
        }

        int userID = user.getUserID();
        logger.info("Checking if user with ID " + userID + " has guide info");

        try {
            boolean hasGuideInfo = guideDAO.hasGuideInfo(userID);
            logger.info("User with ID " + userID + " guide info status: " + hasGuideInfo);
            if (hasGuideInfo) {
                logger.info("User with ID " + userID + " already has guide info. Redirecting to GuideWorkInfoServlet.");
                response.sendRedirect("GuideWorkInfoServlet");
            } else {
                logger.info("User with ID " + userID + " does not have guide info. Showing guide info form.");
                request.setAttribute("user", user);
                request.getRequestDispatcher("guide_info_form.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            logger.severe("Database error while checking guide info for user with ID " + userID + ": " + e.getMessage());
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        if (user == null) {
            response.sendRedirect("login");
            return;
        }

        int userID = user.getUserID();
        String name = user.getUserName();
        String email = user.getEmail();
        String phoneNumber = user.getPhoneNumber();
        String experience = request.getParameter("experience");
        String imageURL = request.getParameter("imageURL");
        String address = request.getParameter("address");
        int status = 0;

        logger.info("Received guide info for user ID " + userID + ": Name=" + name + ", Email=" + email + 
                    ", Phone Number=" + phoneNumber + ", Experience=" + experience + 
                    ", Image URL=" + imageURL + ", Address=" + address);

        try {
            int guideID = guideDAO.insertGuide(name, email, phoneNumber, experience, imageURL, address, status);
            guideDAO.insertUserGuide(userID, guideID);
            logger.info("Inserted guide with ID " + guideID + " and associated it with user ID " + userID);
            response.sendRedirect("GuideWorkInfoServlet");
        } catch (SQLException e) {
            logger.severe("Database error while inserting guide info for user with ID " + userID + ": " + e.getMessage());
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
    }
}
