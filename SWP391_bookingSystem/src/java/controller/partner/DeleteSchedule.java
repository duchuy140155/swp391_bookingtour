package controller.partner;

import dal.ScheduleDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class DeleteSchedule extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String scheduleIDParam = request.getParameter("scheduleID");
        String tourIDParam = request.getParameter("tourID");

        if (scheduleIDParam == null || tourIDParam == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid scheduleID or tourID");
            return;
        }

        int scheduleID;
        int tourID;
        try {
            scheduleID = Integer.parseInt(scheduleIDParam);
            tourID = Integer.parseInt(tourIDParam);
        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid scheduleID or tourID");
            return;
        }

        ScheduleDAO dao = new ScheduleDAO();
        try {
            dao.deleteSchedule(scheduleID);
            response.sendRedirect("tourDetails?tourID=" + tourID);
        } catch (SQLException e) {
            throw new ServletException("Error deleting schedule: " + e.getMessage(), e);
        }
    }
}
