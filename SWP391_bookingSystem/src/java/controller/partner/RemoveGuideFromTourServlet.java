package controller.partner;

import dal.GuideDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;


public class RemoveGuideFromTourServlet extends HttpServlet {
    private GuideDAO guideDAO;

    @Override
    public void init() throws ServletException {
        guideDAO = new GuideDAO();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int guideID = Integer.parseInt(request.getParameter("guideID"));
            int tourID = Integer.parseInt(request.getParameter("tourID"));
            boolean result = guideDAO.removeGuideFromTour(guideID, tourID);

            System.out.println("removeGuideFromTour - result: " + result);

            if (result) {
                response.sendRedirect("tourList"); // Chuyển hướng đến danh sách tour sau khi xóa thành công
            } else {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Không thể xóa hướng dẫn viên.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Có lỗi xảy ra khi xóa hướng dẫn viên.");
        }
    }
}
