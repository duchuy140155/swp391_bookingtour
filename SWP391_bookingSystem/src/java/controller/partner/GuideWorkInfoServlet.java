package controller.partner;

import dal.GuideDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class GuideWorkInfoServlet extends HttpServlet {

    private GuideDAO guideDAO;

    @Override
    public void init() throws ServletException {
        guideDAO = new GuideDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer guideID = (Integer) session.getAttribute("guideID");

        if (guideID == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        try {
            List<Map<String, Object>> tours = guideDAO.getToursByGuideId(guideID);
            request.setAttribute("tours", tours);
            request.getRequestDispatcher("guide_work_info.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Lỗi cơ sở dữ liệu");
        }
    }
}
