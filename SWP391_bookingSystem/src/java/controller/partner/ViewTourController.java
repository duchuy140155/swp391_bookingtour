package controller.partner;

import dal.ScheduleDAO;
import dal.TourDAO;
import model.Schedule;
import model.Tour;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class ViewTourController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");

        if (partnerID == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }

        int page = 1;
        int pageSize = 3;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        String search = request.getParameter("search");
        TourDAO tourDAO = new TourDAO();
        ArrayList<Tour> tours;
        int totalTours;

        if (search != null && !search.trim().isEmpty()) {
            tours = tourDAO.searchToursByPartnerIDWithPagination(partnerID, search, page, pageSize);
            totalTours = tourDAO.getTotalSearchedToursByPartnerID(partnerID, search);
        } else {
            tours = tourDAO.getToursByPartnerIDWithPagination(partnerID, page, pageSize);
            totalTours = tourDAO.getTotalToursByPartnerID(partnerID);
        }

        int totalPages = (int) Math.ceil((double) totalTours / pageSize);

        ScheduleDAO scheduleDAO = new ScheduleDAO();
        Map<Integer, List<Schedule>> tourSchedulesMap = new HashMap<>();
        for (Tour tour : tours) {
            try {
                List<Schedule> schedules = scheduleDAO.getTourSchedules(tour.getTourID());
                tourSchedulesMap.put(tour.getTourID(), schedules);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        request.setAttribute("tours", tours);
        request.setAttribute("tourSchedulesMap", tourSchedulesMap);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("search", search);
        request.getRequestDispatcher("listTour.jsp").forward(request, response);
    }
}
