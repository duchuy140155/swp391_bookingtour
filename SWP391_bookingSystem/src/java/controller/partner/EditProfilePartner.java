package controller.partner;

import dal.PartnerDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Partner;


public class EditProfilePartner extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");
        
        if (partnerID == null) {
            response.getWriter().println("Partner ID not found in session.");
            return;
        }

        PartnerDAO partnerDAO = new PartnerDAO();
        Partner partner = partnerDAO.getPartnerById(partnerID);

        if (partner != null) {
            request.setAttribute("partner", partner);
            request.getRequestDispatcher("editProfile.jsp").forward(request, response);
        } else {
            response.getWriter().println("Partner not found.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");
        
        if (partnerID == null) {
            response.getWriter().println("Partner ID not found in session.");
            return;
        }

        String partnerName = request.getParameter("partnerName");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");
        String address = request.getParameter("address");
        String certificate = request.getParameter("certificate");

        PartnerDAO partnerDAO = new PartnerDAO();
        boolean isUpdated = partnerDAO.updatePartner(partnerID, partnerName, email, phoneNumber, address, certificate);

        if (isUpdated) {
            response.sendRedirect("profileUpdated.jsp");
        } else {
            response.getWriter().println("Update failed.");
        }
    }
}
