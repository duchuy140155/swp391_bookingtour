/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.partner;

import dal.HotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Room;

/**
 *
 * @author Administrator
 */
public class EditRoom extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditRoom</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditRoom at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String id = request.getParameter("id");
        HttpSession session = request.getSession();
        RoomDAO d = new RoomDAO();
        Room a = d.loadRoomById(Integer.parseInt(id));

        session.setAttribute("listroomtype", d.loadAllRoomType());
        session.setAttribute("roomid", a.getRoom_id());
        session.setAttribute("price", a.getPrice());
        session.setAttribute("description", a.getDescription());
        session.setAttribute("guest", a.getMax_guests());
        session.setAttribute("bed", a.getNumber_beds());
        session.setAttribute("image", a.getImageIdRoom());
        session.setAttribute("hotel", a.getHotel_id());
        session.setAttribute("name", a.getName());
        
        response.sendRedirect("EditRoom.jsp");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         HttpSession session = request.getSession();
        RoomDAO d = new RoomDAO();
        HotelDAO h = new HotelDAO();
        String hotelid = request.getParameter("hotelid");
        String imageId = request.getParameter("imageid");
        String roomid = request.getParameter("roomid");
        String roomType = request.getParameter("roomType");
        String price = request.getParameter("price");
        String roomname = request.getParameter("roomname");
         String numberBeds = request.getParameter("numberBeds").replaceAll("[^0-9]", "");
        String description = request.getParameter("description");
       String maxGuests = request.getParameter("maxGuests").replaceAll("[^0-9]", "");
        int ImageID = Integer.parseInt(imageId);
        int RoomID = Integer.parseInt(roomid);
        int RoomType = Integer.parseInt(roomType);
        int HotleID = Integer.parseInt(hotelid);
        double Price = Double.parseDouble(price);
       int NumberBed = Integer.parseInt(numberBeds);
       int MaxGuests  = Integer.parseInt(maxGuests);
        Room a = new Room(RoomID, HotleID, RoomType,roomname, Price, ImageID, MaxGuests, description, NumberBed);
        d.updateRoomById(a);
        session.setAttribute("listroom",d.loadAllRoom() );
        session.setAttribute("listhotel",h.loadAllHotel());
        response.sendRedirect("ManageRoom.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
