package controller.partner;

import dal.PartnerDAO;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Partner;


public class LoginPartnerController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String email = null;
        String password = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("partner_cuser".equals(cookie.getName())) {
                    email = cookie.getValue();
                    System.out.println("Email from cookie: " + email);
                }
                if ("partner_cpass".equals(cookie.getName())) {
                    password = cookie.getValue();
                    System.out.println("Password from cookie: " + password);
                }
            }
        }

        request.setAttribute("email", email);
        request.setAttribute("password", password);
        request.getRequestDispatcher("loginPartner.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");

        String passwordWithSpecialChars = addSpecialChars(password);
        String hashedPassword = md5(passwordWithSpecialChars);

        Cookie cu = new Cookie("partner_cuser", email);
        Cookie cp = new Cookie("partner_cpass", password);
        if ("on".equals(remember)) {
            cu.setMaxAge(60 * 60 * 24 * 7);
            cp.setMaxAge(60 * 60 * 24 * 7);
        } else {
            cu.setMaxAge(0);
            cp.setMaxAge(0);
        }
        response.addCookie(cu);
        response.addCookie(cp);

        PartnerDAO userDAO = new PartnerDAO();
        Partner partner = userDAO.PartnerAccount(email, hashedPassword);
        if (partner == null) {
            request.setAttribute("error", "Kiểm tra email hoặc mật khẩu");
            request.getRequestDispatcher("loginPartner.jsp").forward(request, response);
        } else if (!partner.isStatus()) {
            request.setAttribute("error", "Tài khoản chưa được kích hoạt");
            request.getRequestDispatcher("loginPartner.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("partner", partner);
            session.setAttribute("partnerID", partner.getPartnerID());
            response.sendRedirect("PartnerInfo");
        }
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String addSpecialChars(String password) {
        String specialChars = "!@#$%^&*()-_=+";
        return password + specialChars;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
