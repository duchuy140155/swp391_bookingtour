package controller.partner;

import dal.PartnerDAO;
import model.Partner;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class PendingPartners extends HttpServlet {

    private static final int RECORDS_PER_PAGE = 10; 

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PartnerDAO partnerDAO = new PartnerDAO();
        


        // Lấy từ khóa tìm kiếm từ request

        String search = request.getParameter("search");
        if (search == null) {
            search = "";
        }

      
        int currentPage = 1;
        String pageStr = request.getParameter("page");
        if (pageStr != null && !pageStr.isEmpty()) {
            currentPage = Integer.parseInt(pageStr);
        }

    
        int start = (currentPage - 1) * RECORDS_PER_PAGE;
        List<Partner> pendingPartners = partnerDAO.getPendingPartners(search, start, RECORDS_PER_PAGE);

        
        int totalRecords = partnerDAO.getTotalPendingPartners(search);

     
        int totalPages = (int) Math.ceil((double) totalRecords / RECORDS_PER_PAGE);

       
        request.setAttribute("pendingPartners", pendingPartners);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("search", search);

    
        request.getRequestDispatcher("pendingPartnerDetails.jsp").forward(request, response);
    }
}
