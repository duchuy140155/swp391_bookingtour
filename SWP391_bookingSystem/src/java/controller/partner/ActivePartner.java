package controller.partner;

import dal.PartnerDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ActivePartner extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int partnerID = Integer.parseInt(request.getParameter("partnerID"));
        PartnerDAO partnerDAO = new PartnerDAO();
        partnerDAO.activatePartner(partnerID);
        response.sendRedirect("pendingPartners");
    }
}
