/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.TicketDAO;
import dal.ViewBookingDetailsDAO;
import model.Ticket;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BookingDetails;
import model.TicketType;

/**
 *
 * @author Admin
 */
public class ViewBookingDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        ViewBookingDetailsDAO viewBookingDetailsDAO = new ViewBookingDetailsDAO();
        BookingDetails bookingDetails = viewBookingDetailsDAO.getBookingDetailsById(bookingID);

        request.setAttribute("bookingDetails", bookingDetails);
        request.getRequestDispatcher("viewBookingDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        String customerName = request.getParameter("customerName");
        String email = request.getParameter("email");
        int phoneNumber = Integer.parseInt(request.getParameter("phoneNumber"));
        String customerAddress = request.getParameter("customerAddress");
        String note = request.getParameter("note");

        TicketDAO ticketDao = new TicketDAO();
        List<Ticket> tickets = new ArrayList<>();
        for (int i = 0;; i++) {
            try {
                String ticketIDParam = request.getParameter("ticketID_" + i);
                if (ticketIDParam == null) {
                    break;
                }
                int ticketID = Integer.parseInt(ticketIDParam);
                String name = request.getParameter("name_" + i);
                boolean gender = Integer.parseInt(request.getParameter("gender_" + i)) == 1;
                Date dob = Date.valueOf(request.getParameter("dob_" + i));
                int typeID = Integer.parseInt(request.getParameter("typeID_" + i));
                TicketType tickettype = ticketDao.getTicketByTypeID(typeID);
                Ticket ticket = new Ticket(ticketID, bookingID, tickettype, name, gender, dob);
                tickets.add(ticket);
            } catch (SQLException ex) {
                Logger.getLogger(ViewBookingDetailsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        ViewBookingDetailsDAO viewBookingDetailsDAO = new ViewBookingDetailsDAO();
        viewBookingDetailsDAO.updateBookingDetails(bookingID, customerName, email, phoneNumber, customerAddress, note, tickets);

        response.sendRedirect("viewBookingDetails?bookingID=" + bookingID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
