/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.RevenueByPartnerDAO;
import model.RevenueByPartner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RevenueByPartners extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RevenueByPartnerDAO revenueDAO = new RevenueByPartnerDAO();

        // Lấy tháng và năm hiện tại
        int currentMonth = LocalDate.now().getMonthValue();
        int currentYear = LocalDate.now().getYear();

        // Lấy dữ liệu doanh thu từ DAO
        List<RevenueByPartner> revenueByPartnerList = revenueDAO.getRevenueByPartner();
        request.setAttribute("revenueByPartnerList", revenueByPartnerList);

        // Lấy dữ liệu số tour đã bán trong tháng
        List<RevenueByPartner> toursSoldByMonth = revenueDAO.getToursSoldByMonth(currentMonth, currentYear);
        request.setAttribute("toursSoldByMonth", toursSoldByMonth);

        // Lấy dữ liệu tổng số khách hàng trong tháng
        List<RevenueByPartner> totalCustomersByMonth = revenueDAO.getTotalCustomersByMonth(currentMonth, currentYear);
        request.setAttribute("totalCustomersByMonth", totalCustomersByMonth);

        // Lấy dữ liệu số tour đã bán trong năm
        List<RevenueByPartner> toursSoldByYear = revenueDAO.getToursSoldByYear(currentYear);
        request.setAttribute("toursSoldByYear", toursSoldByYear);

        // Lấy dữ liệu tổng số khách hàng trong năm
        List<RevenueByPartner> totalCustomersByYear = revenueDAO.getTotalCustomersByYear(currentYear);
        request.setAttribute("totalCustomersByYear", totalCustomersByYear);

        // Thêm dữ liệu doanh thu của từng partner theo tháng
        List<RevenueByPartner> revenueByPartnerByMonthList = revenueDAO.getRevenueByPartnerByMonth();
        request.setAttribute("revenueByPartnerByMonthList", revenueByPartnerByMonthList);

        // Xác định loại biểu đồ yêu cầu
        String chartType = request.getParameter("chartType");
        if (chartType != null) {
            generateChart(response, chartType, revenueDAO, currentYear);
        } else {
            // Forward tới view
            request.getRequestDispatcher("viewRevenue.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // Lớp tạo biểu đồ cho các loại biểu đồ khác nhau
    private void generateChart(HttpServletResponse response, String chartType, RevenueByPartnerDAO dao, int year) throws IOException {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        switch (chartType) {
            case "revenueByPartner":
                List<RevenueByPartner> revenues = dao.getRevenueByPartner();
                for (RevenueByPartner revenue : revenues) {
                    dataset.addValue(revenue.getTotalRevenue(), revenue.getPartnerName(), "");
                }
                break;
            case "chartA":
                // Dữ liệu cho biểu đồ số tour đã bán trong năm
                List<RevenueByPartner> toursSoldByYear = dao.getToursSoldByYear(year);
                for (RevenueByPartner tour : toursSoldByYear) {
                    dataset.addValue(tour.getTourCount(), "Tours Sold", "Tháng " + tour.getMonth());
                }
                break;
            case "chartB":
                // Dữ liệu cho biểu đồ tổng số khách hàng trong năm
                List<RevenueByPartner> totalCustomersByYear = dao.getTotalCustomersByYear(year);
                for (RevenueByPartner customer : totalCustomersByYear) {
                    dataset.addValue(customer.getTotalCustomers(), "Total Customers", "Tháng " + customer.getMonth());
                }
                break;
            case "chartC":
                // Lấy dữ liệu cho biểu đồ Doanh thu của từng partner theo tháng
                RevenueByPartnerDAO daoC = new RevenueByPartnerDAO();
                List<RevenueByPartner> revenueByMonth = daoC.getRevenueByPartnerByMonth();
                for (RevenueByPartner data : revenueByMonth) {
                    dataset.addValue(data.getTotalRevenue(), data.getPartnerName(), "Tháng " + data.getMonth());
                }
                break;
        }

        JFreeChart barChart = ChartFactory.createBarChart(
                "",
                "",
                "",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        response.setContentType("image/png");
        ChartUtilities.writeChartAsPNG(response.getOutputStream(), barChart, 800, 600);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response); // Chuyển xử lý từ doPost về doGet
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
