package controller.partner;

import dal.BookingDetailsDAO;
import model.EmailUtility;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConfirmCancelServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(ConfirmCancelServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cancelIDStr = request.getParameter("cancelID");
        String bookingIDStr = request.getParameter("bookingID");

        if (cancelIDStr == null || cancelIDStr.isEmpty() || bookingIDStr == null || bookingIDStr.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid cancelID or bookingID");
            return;
        }

        try {
            int cancelID = Integer.parseInt(cancelIDStr);
            int bookingID = Integer.parseInt(bookingIDStr);

            BookingDetailsDAO dao = new BookingDetailsDAO();
            dao.confirmCancel(cancelID, bookingID);

            // Send email notification to the customer
            String email = dao.getCustomerEmail(bookingID);
            String subject = "Xác nhận hủy tour";

            String body = "<html><body>"
                    + "<p>Xin chào,</p>"
                    + "<p>Yêu cầu hủy tour của bạn đã được xác nhận.</p>"
                    + "<p>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi.</p>"
                    + "</body></html>";

            try {
                EmailUtility.sendEmail(email, subject, body);
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error sending email", e);
            }

            response.sendRedirect("PartnerProcessCancellationServlet"); 
        } catch (NumberFormatException e) {
            LOGGER.log(Level.SEVERE, "Invalid cancelID or bookingID format", e);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid cancelID or bookingID format");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error processing cancellation", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An error occurred while processing the cancellation");
        }
    }
}
