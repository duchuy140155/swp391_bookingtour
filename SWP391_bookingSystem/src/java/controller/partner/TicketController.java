package controller.partner;

import dal.TicketDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import model.TourTicket;
import model.TicketType;

public class TicketController extends HttpServlet {

    private TicketDAO dal;

    @Override
    public void init() throws ServletException {
        dal = new TicketDAO(); // Khởi tạo TicketDAO
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int tourID = Integer.parseInt(request.getParameter("tourID"));

        // Insert all ticket types for the tour if not already exists
        List<TicketType> listType = dal.getAllTicket();
        for (TicketType type : listType) {
            dal.insertTourTickets(tourID, type.getTypeID());
        }

        // Get all tickets for the tour
        List<TourTicket> listTickets = dal.getAllTicketByTourID(tourID);

        request.setAttribute("tourID", tourID);
        request.setAttribute("listTickets", listTickets);
        request.removeAttribute("message");
        // Clear any existing message
        
        request.getRequestDispatcher("tourTicket.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        String[] typeIDs = request.getParameterValues("typeIDs");
        List<String> errors = new ArrayList<>();

        if (typeIDs != null) {
            for (String typeID : typeIDs) {
                String price = request.getParameter("price[" + typeID + "]");
                if (price != null && !price.isEmpty()) {
                    if (!validateDouble(price)) {
                        errors.add("Giá vé cho loại vé " + typeID + " không hợp lệ. Chỉ có thể nhập số và dấu chấm thập phân.");
                    } else {
                        // Cập nhật giá vé trong database
                        dal.insertPrice(tourID, Integer.parseInt(typeID), Double.parseDouble(price));
                    }
                } else {
                    errors.add("Giá vé cho loại vé " + typeID + " không được bỏ trống.");
                }
            }
        } else {
            errors.add("Không có loại vé nào được chọn.");
        }

        if (!errors.isEmpty()) {
            request.setAttribute("errors", errors);
        } else {
            request.setAttribute("message", "Cập nhật giá vé thành công.");
        }

        // Lấy lại dữ liệu để hiển thị trên trang JSP
        List<TourTicket> listTickets = dal.getAllTicketByTourID(tourID);
        request.setAttribute("tourID", tourID);
        request.setAttribute("listTickets", listTickets);
        request.getRequestDispatcher("tourTicket.jsp").forward(request, response);
    }

    private boolean validateDouble(String input) {
        return Pattern.matches("^[+-]?([0-9]*[.])?[0-9]+$", input);
    }
}
