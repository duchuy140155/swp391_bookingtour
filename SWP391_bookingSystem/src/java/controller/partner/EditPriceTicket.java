package controller.partner;

import dal.TicketDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TicketType;

public class EditPriceTicket extends HttpServlet {

    private TicketDAO dal;

    @Override
    public void init() throws ServletException {
        dal = new TicketDAO(); // Khởi tạo TicketDAO
    }

     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("typeID");
        String tourID = request.getParameter("tourID");

        HttpSession session = request.getSession();
        
        if (id != null && !id.isEmpty()) {
            int typeID = Integer.parseInt(id);
            try {
                dal.deleteTicket(typeID);
                session.setAttribute("message", "Vé đã được xóa thành công!");
            } catch (SQLException ex) {
                Logger.getLogger(EditPriceTicket.class.getName()).log(Level.SEVERE, null, ex);
                session.setAttribute("errors", "Có lỗi xảy ra khi xóa vé!");
            }
        } else {
            session.setAttribute("errors", "Có lỗi xảy ra khi xóa vé!");
        }
        response.sendRedirect("ticketController?tourID=" + tourID);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int tourID = Integer.parseInt(request.getParameter("tourID"));
            int typeID = Integer.parseInt(request.getParameter("typeIDs"));
            String priceStr = request.getParameter("price");
            
            List<String> errors = new ArrayList<>();
            HttpSession session = request.getSession();
            TicketType ticket = dal.getTicketByTypeID(typeID);
            
            if (priceStr == null || priceStr.isEmpty()) {
                errors.add("Giá vé cho loại vé " + ticket.getTypeCode() + " không được bỏ trống.");
            } else {
                try {
                    double price = Double.parseDouble(priceStr);
                    dal.insertPrice(tourID, typeID, price);
                } catch (NumberFormatException e) {
                    errors.add("Giá vé cho loại vé " + ticket.getTypeCode() + " không hợp lệ. Chỉ có thể nhập số và dấu chấm thập phân.");
                }
            }
            
            if (!errors.isEmpty()) {
                session.setAttribute("errors", errors);
                session.removeAttribute("message"); // Đảm bảo rằng thông báo thành công bị xóa nếu có lỗi
            } else {
                session.setAttribute("message", "Cập nhật giá vé thành công.");
                session.removeAttribute("errors"); // Đảm bảo rằng lỗi bị xóa nếu không có lỗi
            }
            
            response.sendRedirect("ticketController?tourID=" + tourID);
        } catch (SQLException ex) {
            Logger.getLogger(EditPriceTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
