package controller.partner;

import dal.ScheduleDAO;
import dal.TourDAO;
import model.Schedule;
import model.ScheduleDetails;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CreateSchedule extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String scheduleTitle = request.getParameter("scheduleTitle").trim();
        String scheduleDateStr = request.getParameter("scheduleDate").trim();
        String detailTitle = request.getParameter("detailTitle").trim();
        String detailContent = request.getParameter("detailContent").trim();
        String note = request.getParameter("note").trim();

        String tourIDStr = request.getParameter("tourID").trim();

        if (scheduleTitle.isEmpty() || scheduleDateStr.isEmpty() || detailTitle.isEmpty() || (detailContent.isEmpty() && !detailContent.isEmpty())) {
            request.setAttribute("error", "Các trường không được để trống hoặc chỉ chứa dấu cách.");
            request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
            return;
        }

        int tourID;
        try {
            tourID = Integer.parseInt(tourIDStr);
        } catch (NumberFormatException e) {
            request.setAttribute("error", "ID tour không hợp lệ.");
            request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
            return;
        }

        Date scheduleDate;
        try {
            scheduleDate = new SimpleDateFormat("yyyy-MM-dd").parse(scheduleDateStr);
        } catch (ParseException e) {
            request.setAttribute("error", "Định dạng ngày không hợp lệ.");
            request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
            return;
        }

        TourDAO tourDAO = new TourDAO();
        Date tourStartDate;
        try {
            tourStartDate = tourDAO.getTourStartDate(tourID);
            if (tourStartDate == null) {
                request.setAttribute("error", "Không tìm thấy ngày khởi hành của tour.");
                request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
                return;
            }

            if (scheduleDate.before(tourStartDate)) {
                request.setAttribute("error", "Ngày của lịch trình phải sau ngày khởi hành của tour.");
                request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
                return;
            }
        } catch (SQLException e) {
            request.setAttribute("error", "Lỗi khi lấy thông tin ngày khởi hành của tour.");
            request.getRequestDispatcher("createSchedule.jsp").forward(request, response);
            return;
        }

        ScheduleDAO dao = new ScheduleDAO();
        try {
            List<Schedule> existingSchedules = dao.getTourSchedules(tourID);
            Schedule existingSchedule = null;
            for (Schedule schedule : existingSchedules) {
                if (schedule.getScheduleDate().equals(scheduleDate)) {
                    existingSchedule = schedule;
                    break;
                }
            }

            if (existingSchedule != null) {
                ScheduleDetails detail = new ScheduleDetails();
                detail.setScheduleID(existingSchedule.getScheduleID());
                detail.setDetailTitle(detailTitle);
                detail.setDetailContent(detailContent);
                detail.setNote(note);
                dao.addScheduleDetails(detail);
            } else {
                Schedule schedule = new Schedule();
                schedule.setScheduleTitle(scheduleTitle);
                schedule.setScheduleDate(scheduleDate);
                schedule.setTourID(tourID);

                int scheduleID = dao.addSchedule(schedule);
                ScheduleDetails details = new ScheduleDetails();
                details.setScheduleID(scheduleID);
                details.setDetailTitle(detailTitle);
                details.setDetailContent(detailContent);
                details.setNote(note);
                dao.addScheduleDetails(details);
            }

            response.sendRedirect("tourDetails?tourID=" + tourID);
        } catch (SQLException e) {
            throw new ServletException("Error: " + e.getMessage());
        }
    }
}
