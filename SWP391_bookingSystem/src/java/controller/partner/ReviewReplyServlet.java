package controller.partner;

import dal.ReviewReplyDAO;
import dal.DBContext;
import model.ReviewReply;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReviewReplyServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ReviewReplyServlet.class.getName());
    private Connection connection;

    @Override
    public void init() throws ServletException {
        super.init();
        DBContext dbContext = new DBContext();
        connection = dbContext.getConnection();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int reviewID = Integer.parseInt(request.getParameter("reviewID"));
        int partnerID = Integer.parseInt(request.getParameter("partnerID"));
        String replyContent = request.getParameter("replyContent");

        ReviewReplyDAO reviewReplyDAO = new ReviewReplyDAO(connection);
        ReviewReply reply = new ReviewReply();
        reply.setReviewID(reviewID);
        reply.setPartnerID(partnerID);
        reply.setReplyContent(replyContent);

        try {
            reviewReplyDAO.addReply(reply);
            response.setContentType("application/json");
            response.getWriter().write("{\"success\":true}");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Database error", e);
            response.setContentType("application/json");
            response.getWriter().write("{\"success\":false}");
        }
    }

    @Override
    public void destroy() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Database connection close error", e);
        }
    }
}
