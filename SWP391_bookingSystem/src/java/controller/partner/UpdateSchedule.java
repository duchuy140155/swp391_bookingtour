package controller.partner;

import dal.ScheduleDAO;
import model.Schedule;
import model.ScheduleDetails;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UpdateSchedule extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int scheduleID = Integer.parseInt(request.getParameter("scheduleID"));
        ScheduleDAO dao = new ScheduleDAO();
        try {
            Schedule schedule = dao.getScheduleByID(scheduleID);
            if (schedule == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Schedule not found");
                return;
            }
            List<ScheduleDetails> details = dao.getDetailsByScheduleID(scheduleID);
            schedule.setDetails(details);
            request.setAttribute("schedule", schedule);
            request.getRequestDispatcher("updateSchedule.jsp").forward(request, response);
        } catch (SQLException e) {
            throw new ServletException(" error: " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        int scheduleID = Integer.parseInt(request.getParameter("scheduleID"));
        String scheduleTitle = request.getParameter("scheduleTitle").trim();
        String scheduleDateStr = request.getParameter("scheduleDate").trim();
        int tourID = Integer.parseInt(request.getParameter("tourID"));

        if (scheduleTitle.isEmpty() || scheduleDateStr.isEmpty()) {
            request.setAttribute("error", "Các trường không được để trống hoặc chỉ chứa dấu cách.");
            doGet(request, response);
            return;
        }

        Date scheduleDate;
        try {
            scheduleDate = Date.valueOf(scheduleDateStr);
        } catch (IllegalArgumentException e) {
            request.setAttribute("error", "Định dạng ngày không hợp lệ.");
            doGet(request, response);
            return;
        }

        ScheduleDAO dao = new ScheduleDAO();
        try {
            Date startDate = dao.getStartDateByTourID(tourID);
            if (startDate == null) {
                request.setAttribute("error", "Không tìm thấy thông tin chuyến đi.");
                doGet(request, response);
                return;
            }

            if (!scheduleDate.after(startDate)) {
                request.setAttribute("error", "Ngày sửa phải sau ngày bắt đầu chuyến đi.");
                doGet(request, response);
                return;
            }

            Schedule schedule = new Schedule();
            schedule.setScheduleID(scheduleID);
            schedule.setScheduleTitle(scheduleTitle);
            schedule.setScheduleDate(scheduleDate);
            schedule.setTourID(tourID);

            List<ScheduleDetails> details = new ArrayList<>();
            for (String paramName : request.getParameterMap().keySet()) {
                if (paramName.startsWith("detailTitle-")) {
                    int detailID = Integer.parseInt(paramName.split("-")[1]);
                    ScheduleDetails detail = new ScheduleDetails();
                    detail.setDetailID(detailID);
                    detail.setScheduleID(scheduleID);
                    detail.setDetailTitle(request.getParameter("detailTitle-" + detailID).trim());
                    detail.setDetailContent(request.getParameter("detailContent-" + detailID).trim());
                    detail.setNote(request.getParameter("note-" + detailID).trim());
                    if (detail.getDetailTitle().isEmpty()) {
                        request.setAttribute("error", "Tiêu đề chi tiết không được để trống hoặc chỉ chứa dấu cách.");
                        doGet(request, response);
                        return;
                    }
                    if (!detail.getDetailContent().isEmpty() && detail.getDetailContent().trim().isEmpty()) {
                        request.setAttribute("error", "Nội dung chi tiết không được chỉ chứa dấu cách.");
                        doGet(request, response);
                        return;
                    }
                    details.add(detail);
                }
            }
            schedule.setDetails(details);

            dao.updateSchedule(schedule);
            for (ScheduleDetails detail : details) {
                dao.updateScheduleDetails(detail);
            }

            response.sendRedirect("tourDetails?tourID=" + tourID);
        } catch (SQLException e) {
            throw new ServletException(" error: " + e.getMessage());
        }
    }
}
