package controller.partner;

import dal.PartnerDAO;
import model.Partner;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class GetPartnerDetailsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String partnerIDStr = request.getParameter("partnerID");

        if (partnerIDStr == null || partnerIDStr.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Partner ID is missing or invalid.");
            return;
        }
        try {
            int partnerID = Integer.parseInt(partnerIDStr);
            PartnerDAO partnerDAO = new PartnerDAO();
            Partner partner = partnerDAO.getPartnerById(partnerID);

            if (partner != null) {
                request.setAttribute("partner", partner);
                request.getRequestDispatcher("partnerDetails.jsp").forward(request, response);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Partner not found.");
            }
        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Partner ID format.");
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An unexpected error occurred.");
        }
    }
}
