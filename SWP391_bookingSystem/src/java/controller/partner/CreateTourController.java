package controller.partner;

import dal.HotelDAO;
import dal.LocationDAO;
import dal.TourDAO;
import java.io.IOException;
import java.sql.Date;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import model.Location;

/**
 *
 * @author MSI
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class CreateTourController extends HttpServlet {

    private static final String UPLOAD_DIR = "images"; // Directory within the webapp

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        HotelDAO d = new HotelDAO();
        if (session.getAttribute("partnerID") == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }
         LocationDAO locationDAO = new LocationDAO();
        List<Location> locations = locationDAO.getAllLocations();
        request.setAttribute("locations", locations);
        session.setAttribute("hotels", d.loadAllHotel());
        request.getRequestDispatcher("createTour.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
         TourDAO tourDAO = new TourDAO();
        Integer partnerID = (Integer) session.getAttribute("partnerID");

        if (partnerID == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }

        try {
            String tourName = request.getParameter("tourName").trim();
            String tourDescription = request.getParameter("tourDescription").trim();
            String startLocation = request.getParameter("startLocation");
            String endLocation = request.getParameter("endLocation");
            Date startDate = Date.valueOf(request.getParameter("startDate"));
            Date endDate = Date.valueOf(request.getParameter("endDate"));
            String hotelid = request.getParameter("hotel");
            int numberOfPeople = 0;
             int tourID =0;
             Double price = 0.0;
             String thumbnails ="";
              Part thumbnailPart = request.getPart("thumbnail");
            if(hotelid==null || hotelid.equals("") || hotelid.isEmpty()){
             price = Double.parseDouble(request.getParameter("price").trim());
             numberOfPeople = Integer.parseInt(request.getParameter("numberOfPeople").trim());
           
             thumbnails = uploadFile(thumbnailPart);
            System.out.println("Thumbnail uploaded: " + thumbnails);
               // Insert tour into database
           price = Double.parseDouble(request.getParameter("price").trim());
             numberOfPeople = Integer.parseInt(request.getParameter("numberOfPeople").trim());
            
             thumbnails = uploadFile(thumbnailPart);
             tourID = tourDAO.insertTour(tourName, tourDescription, startLocation, endLocation, startDate, endDate, price, numberOfPeople, thumbnails, partnerID);
            System.out.println("Tour ID: " + tourID);
            }else{
                 price = Double.parseDouble(request.getParameter("price").trim());
                   numberOfPeople = Integer.parseInt(request.getParameter("numberOfPeople").trim());
                 thumbnails = uploadFile(thumbnailPart);
            tourID = tourDAO.insertTourWHotel(tourName, tourDescription, startLocation, endLocation, startDate, endDate, price, numberOfPeople, thumbnails, partnerID,Integer.parseInt(hotelid));
            
            
            }
            

            // Log values
            System.out.println("Tour Name: " + tourName);
            System.out.println("Tour Description: " + tourDescription);
            System.out.println("Start Location: " + startLocation);
            System.out.println("End Location: " + endLocation);
            System.out.println("Start Date: " + startDate);
            System.out.println("End Date: " + endDate);
           // System.out.println("Price: " + price);
            //System.out.println("Number of People: " + numberOfPeople);

            // Upload single thumbnail
           

            

            if (tourID > 0) {
                // Upload multiple images
                ArrayList<String> imageURLs = new ArrayList<>();
                for (Part part : request.getParts()) {
                    if (part.getName().equals("images")) {
                        String imageURL = uploadFile(part);
                        if (imageURL != null) {
                            imageURLs.add(imageURL);
                        }
                    }
                }

                boolean areImagesInserted = tourDAO.insertTourImages(tourID, imageURLs);
                System.out.println("Images inserted: " + areImagesInserted);

                if (areImagesInserted) {
                    response.sendRedirect("listTour");
                    return;
                } else {
                    request.setAttribute("message", "Failed to insert tour images.");
                }
            } else {
                request.setAttribute("message", "Failed to create tour.");
            }
        } catch (Exception e) {
            request.setAttribute("message", "An error occurred: " + e.getMessage());
            e.printStackTrace();
        }

        // If we reach this point, it means there was an error
        LocationDAO locationDAO = new LocationDAO();
        List<Location> locations = locationDAO.getAllLocations();
        request.setAttribute("locations", locations);

        request.getRequestDispatcher("createTour.jsp").forward(request, response);
    }

    private String uploadFile(Part part) throws IOException {
        String fileName = extractFileName(part);
        String savePath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR + File.separator + fileName;
        File fileSaveDir = new File(getServletContext().getRealPath("") + File.separator + UPLOAD_DIR);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }

        System.out.println("Saving file to: " + savePath); // Debug line

        part.write(savePath);
        return UPLOAD_DIR + "/" + fileName; // Return the relative path
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
