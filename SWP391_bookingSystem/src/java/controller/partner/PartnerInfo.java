package controller.partner;

import dal.DashboardDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class PartnerInfo
 */
public class PartnerInfo extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private DashboardDAO dashboardDAO;
    private static final Logger logger = Logger.getLogger(PartnerInfo.class.getName());

    @Override
    public void init() throws ServletException {
        super.init();
        dashboardDAO = new DashboardDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        Integer partnerID = (Integer) session.getAttribute("partnerID");

        if (partnerID == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        logger.log(Level.INFO, "Fetching data for partner ID: {0}", partnerID);
        Double totalRevenue = dashboardDAO.getTotalRevenue(partnerID);
        int totalCustomers = dashboardDAO.getTotalCustomers(partnerID);
        int totalToursSold = dashboardDAO.getTotalToursSold(partnerID);
          String mostPopularTour = dashboardDAO.getMostPopularTour(partnerID);

        logger.log(Level.INFO, "Total Tours Sold: {0}", totalToursSold);
        logger.log(Level.INFO, "Total Revenue: {0}", totalRevenue);
        logger.log(Level.INFO, "Total Customers: {0}", totalCustomers);
        

        request.setAttribute("totalRevenue", totalRevenue);
        request.setAttribute("totalCustomers", totalCustomers);
        request.setAttribute("totalToursSold", totalToursSold);
          request.setAttribute("mostPopularTour", mostPopularTour);

        request.getRequestDispatcher("partner.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.INFO, "Handling POST request");
        doGet(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
