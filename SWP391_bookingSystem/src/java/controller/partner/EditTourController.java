/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.LocationDAO;
import dal.TourDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import model.Location;
import model.Tour;
import model.TourImage;

/**
 *
 * @author MSI
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class EditTourController extends HttpServlet {

    // private static final long serialVersionUID = 1L;
    private static final String UPLOAD_DIR = "images"; // Directory within the webapp

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("partnerID") == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }

        int tourID = Integer.parseInt(request.getParameter("tourID"));

        TourDAO tourDAO = new TourDAO();
        LocationDAO locationDAO = new LocationDAO();

        Tour tour = tourDAO.getTourByID(tourID);
        List<Location> locations = locationDAO.getAllLocations();
        List<TourImage> tourImages = tourDAO.getTourImagesByTourID(tourID);

        request.setAttribute("tour", tour);
        request.setAttribute("locations", locations);
        request.setAttribute("tourImages", tourImages);

        request.getRequestDispatcher("editTour.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");

        if (partnerID == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }

        int tourID = Integer.parseInt(request.getParameter("tourID"));
        TourDAO tourDAO = new TourDAO();

        try {
            String tourName = request.getParameter("tourName").trim();
            String tourDescription = request.getParameter("tourDescription").trim();
            String startLocation = request.getParameter("startLocation");
            String endLocation = request.getParameter("endLocation");
            Date startDate = Date.valueOf(request.getParameter("startDate"));
            Date endDate = Date.valueOf(request.getParameter("endDate"));
            Double price = Double.parseDouble(request.getParameter("price").trim());
            int numberOfPeople = Integer.parseInt(request.getParameter("numberOfPeople").trim());

            // Handle thumbnail
            String thumbnails = request.getParameter("currentThumbnail"); // default to the current thumbnail
            Part thumbnailPart = request.getPart("thumbnail");
            if (thumbnailPart != null && thumbnailPart.getSize() > 0) {
                thumbnails = uploadFile(thumbnailPart);
            }

            boolean isUpdated = tourDAO.updateTour(tourID, tourName, tourDescription, startLocation, endLocation, startDate, endDate, price, numberOfPeople, thumbnails);

            if (isUpdated) {
                // Handle image deletions
                String[] imagesToDelete = request.getParameterValues("deleteImages");
                if (imagesToDelete != null) {
                    for (String imageUrl : imagesToDelete) {
                        tourDAO.deleteTourImage(tourID, imageUrl);
                    }
                }

                // Handle new image uploads
                ArrayList<String> imageURLs = new ArrayList<>();
                for (Part part : request.getParts()) {
                    if (part.getName().equals("images") && part.getSize() > 0) {
                        String imageURL = uploadFile(part);
                        if (imageURL != null) {
                            imageURLs.add(imageURL);
                        }
                    }
                }

                if (!imageURLs.isEmpty()) {
                    boolean areImagesUpdated = tourDAO.addNewTourImages(tourID, imageURLs);
                    if (!areImagesUpdated) {
                        request.setAttribute("message", "Failed to update tour images.");
                    }
                }

                request.setAttribute("message", "Tour updated successfully!");
            } else {
                request.setAttribute("message", "Failed to update tour.");
            }
        } catch (Exception e) {
            request.setAttribute("message", "An error occurred: " + e.getMessage());
            e.printStackTrace();
        }

        response.sendRedirect("listTour");
    }

    private String uploadFile(Part part) throws IOException {
        String fileName = extractFileName(part);
        String savePath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR + File.separator + fileName;
        File fileSaveDir = new File(getServletContext().getRealPath("") + File.separator + UPLOAD_DIR);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }

        System.out.println("Saving file to: " + savePath); // Debug line

        part.write(savePath);
        return UPLOAD_DIR + "/" + fileName; // Return the relative path
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
