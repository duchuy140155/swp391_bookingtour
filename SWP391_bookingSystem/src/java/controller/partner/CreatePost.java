package controller.partner;

import dal.PostDAO;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
    maxFileSize = 1024 * 1024 * 10, // 10 MB
    maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class CreatePost extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("CreatePost.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDAO d = new PostDAO();
        HttpSession session = request.getSession();
        String postTitle = request.getParameter("postTitle");
        int partnerID = (int) session.getAttribute("partnerID");

        // Lấy mô tả ảnh
        String imageDescription = request.getParameter("imageDescription");

        // Gộp các section lại thành một chuỗi duy nhất
        StringBuilder postContentBuilder = new StringBuilder();
        for (int i = 1; i <= 3; i++) {
            String sectionTitle = request.getParameter("sectionTitle" + i);
            String sectionContent = request.getParameter("sectionContent" + i);
            if (sectionTitle != null && !sectionTitle.trim().isEmpty() && sectionContent != null && !sectionContent.trim().isEmpty()) {
                postContentBuilder.append("Section ").append(i).append(": ").append(sectionTitle)
                        .append("\n").append(sectionContent).append("\n\n");
            }
        }
        String postContent = postContentBuilder.toString().trim();

        // Tạo bài đăng
        d.createPost(d.getMaxPostId() + 1, postTitle, getCurrentDate(), partnerID, imageDescription, postContent);

        String message = "Bài đăng đã được tạo thành công!";
        request.setAttribute("message", message);

        // Chuyển hướng về trang tạo bài đăng với thông báo
        request.getRequestDispatcher("CreatePost.jsp").forward(request, response);
    }

    private String getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return currentDate.format(formatter);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
