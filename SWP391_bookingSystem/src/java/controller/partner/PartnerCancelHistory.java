package controller.partner;

import dal.BookingDetailsDAO;
import model.CancelledBooking;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;


public class PartnerCancelHistory extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int partnerID = (int) request.getSession().getAttribute("partnerID");

        BookingDetailsDAO dao = new BookingDetailsDAO();
        List<CancelledBooking> cancelledHistory = dao.getCancelledBookings(partnerID);

        request.setAttribute("cancelledHistory", cancelledHistory);
        request.getRequestDispatcher("partnerCancelHistory.jsp").forward(request, response);
    }
}
