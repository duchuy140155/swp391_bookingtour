/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.partner;

import dal.HotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Room;

/**
 *
 * @author Administrator
 */
public class CreateRoom extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateRoom</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateRoom at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        HotelDAO d= new HotelDAO();
        RoomDAO r= new RoomDAO();
        session.setAttribute("listhotel", d.loadAllHotel());
        session.setAttribute("listroomtype", r.loadAllRoomType());
        
        response.sendRedirect("CreateRoom.jsp");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         RoomDAO d = new RoomDAO();
        HttpSession session = request.getSession();
        String roomType = request.getParameter("roomType");
        String hotelId = request.getParameter("hotelId");
        int HotelID = Integer.parseInt(hotelId);
        int RoomType = Integer.parseInt(roomType);
        String roomname = request.getParameter("roomname");
        String price = request.getParameter("price");
        double Price = Double.parseDouble(price);
        int numberImageId = d.getMaxRomtypeId()+1;
        String maxGuests = request.getParameter("maxGuests");
        String description = request.getParameter("description");
        String numberBeds = request.getParameter("numberBeds");
        int guests = Integer.parseInt(maxGuests.replaceAll("[^0-9]", ""));
        int beds = Integer.parseInt(numberBeds.replaceAll("[^0-9]", ""));
        int id = d.getMaxId()+1;
        Room room = new Room(id, HotelID, RoomType,roomname, Price, numberImageId, guests, description, beds);
        d.InsertRoom(room);
        session.setAttribute("listroom", d.loadAllRoom());
        response.sendRedirect("ManageRoom.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
