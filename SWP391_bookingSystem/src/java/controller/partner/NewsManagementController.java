/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.NewsDAO;
import model.News;
import model.NewsImage;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Admin
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class NewsManagementController extends HttpServlet {

    private static final String UPLOAD_DIR = "uploads/";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private NewsDAO newsDAO;

    @Override
    public void init() throws ServletException {
        newsDAO = new NewsDAO();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null && action.equals("edit")) {
            handleEditNews(request, response);
        } else {
            handleListNews(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null) {
            switch (action) {
                case "create":
                    handleCreateNews(request, response);
                    break;
                case "update":
                    handleUpdateNews(request, response);
                    break;
                case "delete":
                    handleDeleteNews(request, response);
                    break;
                default:
                    response.sendRedirect("manageNews");
                    break;
            }
        }
    }

    private void handleListNews(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String query = request.getParameter("query");
        int page = 1;
        int pageSize = 10;

        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        List<News> newsList;
        int totalNews;
        if (query != null && !query.isEmpty()) {
            newsList = newsDAO.searchNewsByTitle(query, page, pageSize);
            totalNews = newsDAO.getSearchNewsCount(query);
        } else {
            newsList = newsDAO.getNewsByPage(page, pageSize);
            totalNews = newsDAO.getNewsCount();
        }

        int totalPages = (int) Math.ceil((double) totalNews / pageSize);

        request.setAttribute("newsList", newsList);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("query", query);

        request.getRequestDispatcher("partnerNewsManagement.jsp").forward(request, response);
    }

    private void handleCreateNews(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String shortDescription = request.getParameter("shortDescription");
        String longDescription = request.getParameter("longDescription");
        Part imageThemePart = request.getPart("imageTheme");
        List<Part> additionalImagesParts = request.getParts().stream()
                .filter(part -> "newsImages".equals(part.getName()) && part.getSize() > 0)
                .toList();

        String imageTheme = uploadFile(imageThemePart);
        int partnerID = 1; // Set this based on your authentication mechanism

        News news = new News();
        news.setTitle(title);
        news.setShortDescription(shortDescription);
        news.setLongDescription(longDescription);
        news.setPublishDate(new Date());
        news.setImageTheme(imageTheme);
        news.setPartnerID(partnerID);

        int newsId = newsDAO.addNews(news);

        if (newsId != -1) {
            for (Part additionalImagePart : additionalImagesParts) {
                String imageUrl = uploadFile(additionalImagePart);
                NewsImage newsImage = new NewsImage();
                newsImage.setIdNew(newsId);
                newsImage.setNewsImageURL(imageUrl);
                newsDAO.addNewsImage(newsImage);
            }
        }

        response.sendRedirect("manageNews");
    }

    private void handleUpdateNews(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idNew = Integer.parseInt(request.getParameter("idNew"));
        String title = request.getParameter("title");
        String shortDescription = request.getParameter("shortDescription");
        String longDescription = request.getParameter("longDescription");
        Part imageThemePart = request.getPart("imageTheme");
        List<Part> additionalImagesParts = request.getParts().stream()
                .filter(part -> "newsImages".equals(part.getName()) && part.getSize() > 0)
                .toList();

        News news = newsDAO.getNewsById(idNew);
        if (news != null) {
            news.setTitle(title);
            news.setShortDescription(shortDescription);
            news.setLongDescription(longDescription);

            if (imageThemePart != null && imageThemePart.getSize() > 0) {
                String imageTheme = uploadFile(imageThemePart);
                news.setImageTheme(imageTheme);
            }

            newsDAO.updateNews(news);

            for (Part additionalImagePart : additionalImagesParts) {
                String imageUrl = uploadFile(additionalImagePart);
                NewsImage newsImage = new NewsImage();
                newsImage.setIdNew(idNew);
                newsImage.setNewsImageURL(imageUrl);
                newsDAO.addNewsImage(newsImage);
            }
        }

        response.sendRedirect("manageNews");
    }

    private void handleDeleteNews(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idNew = Integer.parseInt(request.getParameter("id"));
        newsDAO.deleteNews(idNew);
        response.sendRedirect("manageNews");
    }

    private void handleEditNews(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idNew = Integer.parseInt(request.getParameter("idNew"));
        News news = newsDAO.getNewsById(idNew);
        request.setAttribute("news", news);
        request.getRequestDispatcher("editNews.jsp").forward(request, response);
    }

    private String uploadFile(Part filePart) throws IOException {
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String uploadDir = getServletContext().getRealPath("/") + UPLOAD_DIR;
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        String uniqueFileName = UUID.randomUUID().toString() + "_" + fileName;
        Path filePath = uploadPath.resolve(uniqueFileName);

        Files.copy(filePart.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        return UPLOAD_DIR + uniqueFileName;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
