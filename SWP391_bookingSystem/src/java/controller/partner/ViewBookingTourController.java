package controller.partner;

import controller.customer.CodeGenerator;
import dal.BookingDAO;
import dal.BookingDetailsDAO;
import dal.ViewBookingDAO;
import model.BookingDetails;
import model.EmailUtility;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Controller for viewing and managing booking tours.
 */
public class ViewBookingTourController extends HttpServlet {

    private static final int RECORDS_PER_PAGE = 3;
    private Connection connection; // Assume you have initialized this connection

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Implement as needed
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        int page = 1;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        String search = request.getParameter("search") != null ? request.getParameter("search") : "";
        String sortStatus = request.getParameter("sortStatus") != null ? request.getParameter("sortStatus") : "all";

        ViewBookingDAO viewBookingDAO = new ViewBookingDAO();
        List<BookingDetails> bookings = viewBookingDAO.getBookingsByTourID(tourID, (page - 1) * RECORDS_PER_PAGE, RECORDS_PER_PAGE, search, sortStatus);
        int totalRecords = viewBookingDAO.getTotalBookingsCount(tourID, search, sortStatus);
        int totalPages = (int) Math.ceil((double) totalRecords / RECORDS_PER_PAGE);

        request.setAttribute("bookings", bookings);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("tourID", tourID);
        request.setAttribute("search", search);
        request.setAttribute("sortStatus", sortStatus);
        request.getRequestDispatcher("viewBooking.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        int status = Integer.parseInt(request.getParameter("status"));
        int tourID = Integer.parseInt(request.getParameter("tourID"));

        ViewBookingDAO viewBookingDAO = new ViewBookingDAO();
        BookingDAO bookingDAO = new BookingDAO();
        BookingDetailsDAO bookingDetailsDAO = new BookingDetailsDAO();
        viewBookingDAO.updateBookingStatus(bookingID, status);

        // Generate and save tour code if status is approved
        if (status == 1) {
            String tourCode = CodeGenerator.generateTourCode();
            try {
                bookingDAO.saveTourCode(bookingID, tourCode);

                // Lấy thông tin email người dùng
                BookingDetails bookingDetails = bookingDetailsDAO.getBookingDetailsById(bookingID);
                String toEmail = bookingDetails.getEmail();
                String subject = "Thông tin Đặt Tour và Mã Code";
                String body = "Chào bạn,\n\nThông tin đặt tour của bạn như sau:\n"
                        + "Tên khách hàng: " + bookingDetails.getCustomerName() + "\n"
                        + "Mã Code: " + tourCode + "\n\n"
                        + "Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi.";

//                EmailUtility.sendEmail(toEmail, subject, body);
                String url = "sendNotification?bookingID=" + bookingID + "&tourID=" + tourID + "&tourCode=" + tourCode;
                response.sendRedirect(url);
            } catch (SQLException e) {
                throw new ServletException("Error saving tour code", e);
            }
        }

        response.sendRedirect("viewBooking?tourID=" + tourID + "&search=" + request.getParameter("search") + "&sortStatus=" + request.getParameter("sortStatus"));
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "View Booking Tour Controller";
    }
}
