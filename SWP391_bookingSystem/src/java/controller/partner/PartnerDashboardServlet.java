package controller.partner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import dal.DashboardDAO;
import model.TourStatistic;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.logging.Logger;

public class PartnerDashboardServlet extends HttpServlet {

    private DashboardDAO dashboardDAO;
    private static final Logger logger = Logger.getLogger(PartnerDashboardServlet.class.getName());

    public void init() {
        dashboardDAO = new DashboardDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer partnerId = (Integer) session.getAttribute("partnerID");
        if (partnerId == null) {
            response.sendRedirect("loginPartner.jsp");
            return;
        }

        String action = request.getParameter("action");
        if (action == null) {
            action = "viewDashboard";
        }

        switch (action) {
            case "viewDashboard":
                viewDashboard(partnerId, request, response);
                break;
            case "monthlyRevenue":
                String yearStr = request.getParameter("year");
                int year = (yearStr != null && !yearStr.isEmpty()) ? Integer.parseInt(yearStr) : 2024; // Default year
                response.setContentType("image/png");
                OutputStream outMonthlyRevenue = response.getOutputStream();
                generateMonthlyRevenueChart(partnerId, outMonthlyRevenue, year);
                outMonthlyRevenue.close();
                break;
            case "yearlyRevenue":
                response.setContentType("image/png");
                OutputStream outYearlyRevenue = response.getOutputStream();
                generateYearlyRevenueChart(partnerId, outYearlyRevenue);
                outYearlyRevenue.close();
                break;
            case "monthlyTourCount":
                yearStr = request.getParameter("year");
                year = (yearStr != null && !yearStr.isEmpty()) ? Integer.parseInt(yearStr) : 2024; // Default year 2024
                response.setContentType("image/png");
                OutputStream outMonthlyTourCount = response.getOutputStream();
                generateMonthlyTourCountChart(partnerId, outMonthlyTourCount, year);
                outMonthlyTourCount.close();
                break;
            case "yearlyTourCount":
                response.setContentType("image/png");
                OutputStream outYearlyTourCount = response.getOutputStream();
                generateYearlyTourCountChart(partnerId, outYearlyTourCount);
                outYearlyTourCount.close();
                break;
            case "customerCountByTour":
                response.setContentType("image/png");
                OutputStream outCustomerCountByTour = response.getOutputStream();
                generateCustomerCountByTourChart(partnerId, outCustomerCountByTour);
                outCustomerCountByTour.close();
                break;
            case "generateRevenueByTourChart":
                response.setContentType("image/png");
                OutputStream outRevenueByTour = response.getOutputStream();
                generateRevenueByTourChart(partnerId, outRevenueByTour);
                outRevenueByTour.close();
                break;
            default:
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid action");
                break;
        }
    }

    private void viewDashboard(int partnerId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Double totalRevenue = dashboardDAO.getTotalRevenue(partnerId);
        int totalCustomers = dashboardDAO.getTotalCustomers(partnerId);
        int totalToursSold = dashboardDAO.getTotalToursSold(partnerId);

        request.setAttribute("totalRevenue", totalRevenue);
        request.setAttribute("totalCustomers", totalCustomers);
        request.setAttribute("totalToursSold", totalToursSold);

        request.getRequestDispatcher("partner.jsp").forward(request, response);
    }

    private void generateMonthlyRevenueChart(int partnerId, OutputStream out, int year) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getMonthlyRevenue(partnerId, year);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalRevenue(), "Doanh thu", "Tháng " + stat.getMonth() + " - " + stat.getYear());
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Doanh thu hàng tháng",
                "Tháng/Năm",
                "Doanh thu",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void generateYearlyRevenueChart(int partnerId, OutputStream out) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getYearlyRevenue(partnerId);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalRevenue(), "Doanh thu", "Năm " + stat.getYear());
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Doanh thu hàng năm",
                "Năm",
                "Doanh thu",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void generateMonthlyTourCountChart(int partnerId, OutputStream out, int year) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getMonthlyTourCount(partnerId, year);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalTours(), "Tours", "Tháng " + stat.getMonth() + " - " + stat.getYear());
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Số lượng tour đã bán hàng tháng",
                "Tháng/Năm",
                "Số lượng tour",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void generateYearlyTourCountChart(int partnerId, OutputStream out) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getYearlyTourCount(partnerId);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalTours(), "Tours", "Năm " + stat.getYear());
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Số lượng tour đã bán hàng năm",
                "Năm",
                "Số lượng tour",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void generateCustomerCountByTourChart(int partnerId, OutputStream out) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getCustomerCountByTour(partnerId);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalCustomers(), "Người dùng", truncateLabel(stat.getTourName()));
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Số lượng khách của từng tour",
                "Tour",
                "Số lượng khách",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void generateRevenueByTourChart(int partnerId, OutputStream out) throws IOException {
        List<TourStatistic> stats = dashboardDAO.getHighestRevenueTours(partnerId);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (TourStatistic stat : stats) {
            dataset.addValue(stat.getTotalRevenue(), "Doanh thu", truncateLabel(stat.getTourName()));
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Doanh thu của từng tour",
                "Tour",
                "Doanh thu",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        customizeBarChart(barChart);
        ChartUtilities.writeChartAsPNG(out, barChart, 800, 600);
    }

    private void customizeBarChart(JFreeChart chart) {
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);

        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        domainAxis.setMaximumCategoryLabelWidthRatio(0.8f); 
        domainAxis.setLabelFont(new Font("Montserrat", Font.BOLD, 12));
        domainAxis.setTickLabelFont(new Font("Montserrat", Font.PLAIN, 10));

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setNumberFormatOverride(NumberFormat.getInstance()); 
        rangeAxis.setLabelFont(new Font("Montserrat", Font.BOLD, 12));
        rangeAxis.setTickLabelFont(new Font("Montserrat", Font.PLAIN, 10));

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, new Color(79, 129, 189));
        renderer.setDrawBarOutline(false);
        renderer.setBarPainter(new StandardBarPainter()); 
        renderer.setShadowVisible(false);
        renderer.setItemMargin(0.02); 

        chart.getTitle().setFont(new Font("Montserrat", Font.BOLD, 14));
        chart.getTitle().setPaint(new Color(31, 78, 120));
        if (chart.getLegend() != null) {
            chart.getLegend().setItemFont(new Font("Montserrat", Font.PLAIN, 12));
        }
    }

    private String truncateLabel(String label) {
        if (label.length() > 20) {
            return label.substring(0, 20) + "...";
        }
        return label;
    }

    @Override
    public String getServletInfo() {
        return "Partner Dashboard Servlet";
    }
}
