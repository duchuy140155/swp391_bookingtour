package controller.partner;

import dal.ContactRequestDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.ContactRequest;

public class ViewContacts extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);

        if (session == null || session.getAttribute("partnerID") == null) {
            response.sendRedirect("loginPartner.jsp?error=not_logged_in");
            return;
        }

        Integer partnerID = (Integer) session.getAttribute("partnerID");

        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem?useSSL=false", "root", "123456789");
            ContactRequestDAO contactRequestDAO = new ContactRequestDAO(con);
            List<ContactRequest> contactRequests = contactRequestDAO.getContactRequestsByPartnerID(partnerID);
            int newContactCount = contactRequestDAO.getNewContactCountByPartnerID(partnerID);

            request.setAttribute("contactRequests", contactRequests);
            request.setAttribute("newContactCount", newContactCount);
            request.getRequestDispatcher("viewContacts.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
            response.sendRedirect("errorPage.jsp");
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("updateStatus".equals(action)) {
            int contactId = Integer.parseInt(request.getParameter("contactId"));

            Connection con = null;
            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem?useSSL=false", "root", "123456789");
                ContactRequestDAO contactRequestDAO = new ContactRequestDAO(con);
                contactRequestDAO.updateContactStatus(contactId);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (con != null) {
                        con.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        doGet(request, response);
    }
}
