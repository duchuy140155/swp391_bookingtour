/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.PartnerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author MSI
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
public class RegisterPartnerController extends HttpServlet {

    private static final String UPLOAD_DIR = "C:\\Image";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("registerPartner.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            InputStream inputStream = null;
            Part filePart = request.getPart("certificate");
            String fileName = null;

            if (filePart != null) {
                fileName = filePart.getSubmittedFileName();
                inputStream = filePart.getInputStream();

                // Use the defined upload path
                String uploadPath = UPLOAD_DIR;
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs(); // Create the directory if it does not exist
                }

                try (OutputStream outputStream = new FileOutputStream(new File(uploadPath + File.separator + fileName))) {
                    int bytesRead;
                    byte[] buffer = new byte[4096];
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                }
                inputStream.close();
            }

            String filePath = "images" + File.separator + fileName;
            String partnerName = request.getParameter("partnerName").trim();
            String email = request.getParameter("email").trim();
            String password = request.getParameter("password").trim();
            int phoneNumber = Integer.parseInt(request.getParameter("phoneNumber").trim());
            String address = request.getParameter("address").trim();
            String certificate = filePath;

            String passwordWithSpecialChars = addSpecialChars(password);
            String hashedPassword = md5(passwordWithSpecialChars);

            PartnerDAO partnerDAO = new PartnerDAO();
            boolean check = partnerDAO.checkEmail(email);
            if (!check) {
                try {
                    partnerDAO.createPartner(partnerName, email, hashedPassword, phoneNumber, address, certificate, false);
                    request.setAttribute("message", "Registration successful!");
                    request.getRequestDispatcher("registerPartner.jsp").forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    request.setAttribute("message", "An error occurred while creating partner: " + e.getMessage());
                    request.getRequestDispatcher("registerPartner.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("message", "Account already exists.");
                request.getRequestDispatcher("registerPartner.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("message", "An error occurred: " + e.getMessage());
            request.getRequestDispatcher("registerPartner.jsp").forward(request, response);
        }
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String addSpecialChars(String password) {
        String specialChars = "!@#$%^&*()-_=+";
        StringBuilder sb = new StringBuilder(password);
        sb.append(specialChars);
        return sb.toString();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
