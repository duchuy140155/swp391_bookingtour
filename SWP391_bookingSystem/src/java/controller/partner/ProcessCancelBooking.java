package controller.partner;

import dal.BookingDetailsDAO;
import model.EmailUtility;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class ProcessCancelBooking extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        String cancelReason = request.getParameter("cancelReason");

        BookingDetailsDAO dao = new BookingDetailsDAO();
        double penaltyPercentage = dao.calculatePenalty(bookingID);
        double refundAmount = dao.calculateRefund(bookingID, penaltyPercentage);

        dao.cancelBooking(bookingID, cancelReason, penaltyPercentage, refundAmount);

        String email = dao.getCustomerEmail(bookingID);

        String subject = "Xác nhận hủy tour";

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
        symbols.setGroupingSeparator('.');
        DecimalFormat currencyFormatter = new DecimalFormat("#,### đ", symbols);

        String formattedRefundAmount = currencyFormatter.format(refundAmount);
        String formattedPenaltyPercentage = String.format("%.1f", penaltyPercentage * 100);

        String body = "<html><body>"
                + "<p>Xin chào,</p>"
                + "<p>Chúng tôi đã gửi yêu cầu hủy tour của bạn đến đối tác, chúng tôi sẽ phản hồi bạn trong thời gian sớm nhất:</p>"
                + "<p><strong>Lý do hủy:</strong> " + cancelReason + "</p>"
                + "<p>Nhưng vì bạn đã hủy tour sau thời gian quy định của chúng tôi, nên chúng tôi không thể hoàn 100% tiền tour của bạn</p>"
                + "<p>Tỉ lệ phạt và tiền hoàn sẽ để chi tiết thông tin bên dưới</p>"
                + "<p><strong>Bạn sẽ bị phạt</strong> " + formattedPenaltyPercentage + "% so với giá trị của tour</p>"
                + "<p><strong>Số tiền hoàn lại:</strong> " + formattedRefundAmount + "</p>"
                + "<p>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi.</p>"
                + "</body></html>";

        boolean emailSent = false;
        try {
            EmailUtility.sendEmail(email, subject, body);
            emailSent = true;
        } catch (Exception e) {
            e.printStackTrace(); // Log the exception
        }

        // Set attribute to indicate email sent success
        request.setAttribute("emailSent", emailSent);
        request.setAttribute("bookingID", bookingID);

        // Forward to JSP
        request.getRequestDispatcher("checkPin.jsp").forward(request, response);
    }
}
