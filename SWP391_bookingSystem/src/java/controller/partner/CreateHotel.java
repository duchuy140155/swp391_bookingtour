/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.partner;

import dal.HotelDAO;
import dal.LocationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.hotels;

/**
 *
 * @author Administrator
 */
public class CreateHotel extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateHotel</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateHotel at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       HttpSession session = request.getSession();
        LocationDAO d = new LocationDAO();
        session.setAttribute("location", d.getAllLocations());
        response.sendRedirect("CreateHotel.jsp");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       HttpSession session = request.getSession();
        HotelDAO d = new HotelDAO();
        LocationDAO j = new LocationDAO();
        int id = d.getMaxHotelId() + 1;
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String city1 = request.getParameter("city");
        String city = request.getParameter("newCityCheckbox") != null ? request.getParameter("newCity") : request.getParameter("city");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String website = request.getParameter("website");
        String image = request.getParameter("image");
        
        if(city1!= null && !city1.trim().isEmpty()){
        hotels hotel = new hotels(id, name, address, city, phone, email, website,image);
        d.insertHotel(hotel);
                    session.setAttribute("message", "Tạo khách sạn thành công!");
        
        
        
        }else{
         if (city != null && !city.trim().isEmpty()) {
            boolean check = j.checkLocationExist(city.trim());
            if (check == false) {
                hotels hotel = new hotels(id, name, address, city, phone, email, website,image);
                try {
                    d.insertHotel(hotel);
                    session.setAttribute("message", "Tạo khách sạn thành công!");
                } catch (Exception e) {
                    session.setAttribute("errorMessage", "Tạo khách sạn không thành công. Lỗi: " + e.getMessage());
                }
                response.sendRedirect("partner.jsp");
            } else {
                session.setAttribute("errorMessage", "Tên vị trí đã có sẵn!");
                response.sendRedirect("CreateHotel.jsp");
            }
        } else {
            session.setAttribute("errorMessage", "Tên thành phố không hợp lệ!");
            response.sendRedirect("CreateHotel.jsp");
        }
        }
        response.sendRedirect("partner.jsp");
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
