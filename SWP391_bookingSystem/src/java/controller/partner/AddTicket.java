/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dal.TicketDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import model.TicketType;

/**
 *
 * @author Admin
 */
public class AddTicket extends HttpServlet {

    private TicketDAO dal;

    @Override
    public void init() throws ServletException {
        dal = new TicketDAO(); // Khởi tạo TicketDAO
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("addTickets.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String typeName = request.getParameter("typeName");
        String typeCode = request.getParameter("typeCode");
        String ageMinStr = request.getParameter("ageMin");
        String ageMaxStr = request.getParameter("ageMax");

        TicketType newtype = new TicketType();
        newtype.setTypeName(typeName);
        newtype.setTypeCode(typeCode);
        newtype.setAgeMin(Integer.parseInt(ageMinStr));
        newtype.setAgeMax(Integer.parseInt(ageMaxStr));
        newtype.setTourID(0);
        newtype.setIsDefault(true);
        HttpSession session = request.getSession();
        try {
            dal.insertNewTypeTicket(newtype);
             response.sendRedirect("/SWP391_bookingSystem/listTicket");
        } catch (Exception e) {
            e.printStackTrace();
            session.setAttribute("errors", "Đã xảy ra lỗi khi tạo vé mới! Hãy thử lại!");
            request.getRequestDispatcher("addTickets.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
