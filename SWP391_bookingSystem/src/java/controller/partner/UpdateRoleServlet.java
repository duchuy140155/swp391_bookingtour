package controller.partner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class UpdateRoleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIDStr = request.getParameter("promoteUserID");

        
        if (userIDStr == null || userIDStr.isEmpty()) {
            response.getWriter().println("Error: promoteUserID is missing or empty");
            return;
        }

        int userID;
        try {
            userID = Integer.parseInt(userIDStr);
        } catch (NumberFormatException e) {
            response.getWriter().println("Error: promoteUserID is not a valid number");
            return;
        }

        String jdbcURL = "jdbc:mysql://localhost:3306/bookingsystem?useSSL=false";
        String dbUser = "root";
        String dbPassword = "123456789";
        
        Connection connection = null;
        PreparedStatement updateUserStmt = null;
        PreparedStatement insertGuideStmt = null;
        PreparedStatement insertUserGuideStmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);

            
            String updateUserSQL = "UPDATE user SET roleID = 5 WHERE userID = ?";
            updateUserStmt = connection.prepareStatement(updateUserSQL);
            updateUserStmt.setInt(1, userID);
            updateUserStmt.executeUpdate();
            response.getWriter().println("User promoted to Guide successfully!");
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println("Error: " + e.getMessage());
        } finally {
            try {
                if (updateUserStmt != null) updateUserStmt.close();
                if (insertGuideStmt != null) insertGuideStmt.close();
                if (insertUserGuideStmt != null) insertUserGuideStmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
