package controller.partner;

import dal.GuideDAO;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public class AddGuideToTourServlet extends HttpServlet {
    private GuideDAO guideDAO;

    @Override
    public void init() throws ServletException {
        guideDAO = new GuideDAO();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int guideID = Integer.parseInt(request.getParameter("guideID"));
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        String tourType = request.getParameter("tourType");
        
        boolean result = guideDAO.addGuideToTour(guideID, tourID,tourType);

        response.sendRedirect("tourList");
    }
}
