/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;

/**
 *
 * @author MSI
 */
@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
    maxFileSize = 1024 * 1024 * 10, // 10 MB
    maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class UploadImageController extends HttpServlet {

   
    private static final String UPLOAD_DIR = "images"; // Directory within the webapp

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try {
            Part filePart = request.getPart("upload");
            if (filePart != null && filePart.getSize() > 0) {
                String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
                String uploadDir = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR;
                File uploadDirFile = new File(uploadDir);
                if (!uploadDirFile.exists()) {
                    uploadDirFile.mkdir();
                }
                File file = new File(uploadDir, fileName);
                filePart.write(file.getAbsolutePath());

                // Construct the URL for the uploaded image
                String imageUrl = request.getContextPath() + "/" + UPLOAD_DIR + "/" + fileName;

                // Create a JSON response manually
                String jsonResponse = String.format("{\"uploaded\": true, \"url\": \"%s\"}", imageUrl);

                response.getWriter().write(jsonResponse);
            } else {
                throw new Exception("Invalid file upload");
            }
        } catch (Exception e) {
            e.printStackTrace();
            String jsonResponse = "{\"uploaded\": false, \"error\": {\"message\": \"Image upload failed\"}}";
            response.getWriter().write(jsonResponse);
        }
    }

    @Override
    public String getServletInfo() {
        return "Handles image uploads";
    }
}
