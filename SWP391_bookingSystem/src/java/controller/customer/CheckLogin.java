/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author TranT
 */
public class CheckLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CheckLogin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CheckLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    String tourID = request.getParameter("tourID");
    HttpSession session = request.getSession();
    
    // Logging để kiểm tra giá trị của tourID và account trong session
    System.out.println("TourID: " + tourID);
    System.out.println("Session Account: " + session.getAttribute("account"));
    
    if (session.getAttribute("account") == null) {
        // Nếu chưa đăng nhập, chuyển hướng đến trang đăng nhập
        System.out.println("Redirecting to login page");
        response.sendRedirect("login.jsp"); // Thay đổi đường dẫn nếu cần
    } else {
        // Nếu đã đăng nhập
        if (tourID != null && !tourID.isEmpty()) {
            // Nếu tourID tồn tại và không rỗng, chuyển hướng đến trang chi tiết tour với tourID
            System.out.println("Redirecting to tourDetails with tourID=" + tourID);
            response.sendRedirect("tourDetails.jsp?tourID=" + tourID); // Thay đổi đường dẫn nếu cần
        } else {
            // Xử lý trường hợp không có tourID
            System.out.println("TourID is null or empty, redirecting to error page");
            response.sendRedirect("error.jsp"); // Chuyển hướng đến trang lỗi nếu cần
        }
    }
}


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
