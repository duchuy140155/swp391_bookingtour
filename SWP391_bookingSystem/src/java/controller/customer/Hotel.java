/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.customer;

import dal.HotelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.hotels;

/**
 *
 * @author Administrator
 */
public class Hotel extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Hotel</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Hotel at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.sendRedirect("SearchHotel.jsp");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
      HotelDAO d = new HotelDAO();
        
        HttpSession session = request.getSession();
        String searchOption = request.getParameter("searchOption");
        String destination = request.getParameter("destination");
        String checkin = request.getParameter("checkin");
        String checkout = request.getParameter("checkout");
        String guestsString = request.getParameter("guests");
        String roomsString = request.getParameter("rooms");

       
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date checkinDate = null;
        Date checkoutDate = null;
        Date today = new Date();
        try {
            checkinDate = sdf.parse(checkin);
            checkoutDate = sdf.parse(checkout);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        
        if (checkinDate != null && checkoutDate != null) {
            if (checkinDate.before(today) || checkoutDate.before(today)) {
                session.setAttribute("errorMessage", "Không thể nhập ngày nhỏ hơn ngày hiện tại");
                response.sendRedirect("SearchHotel.jsp");
                return;
            }
        }

        int adults = 0;
        int children = 0;
        int rooms = 0;
        if (guestsString != null && !guestsString.isEmpty()) {
            Pattern guestsPattern = Pattern.compile("(\\d+) Người lớn, (\\d+) Trẻ em");
            Matcher guestsMatcher = guestsPattern.matcher(guestsString);
            if (guestsMatcher.find()) {
                try {
                    adults = Integer.parseInt(guestsMatcher.group(1));
                    children = Integer.parseInt(guestsMatcher.group(2));
                } catch (NumberFormatException e) {
                    adults = 1;
                    children = 0; 
                }
            }
        }
        if (roomsString != null && !roomsString.isEmpty()) {
            Pattern roomsPattern = Pattern.compile("(\\d+) phòng");
            Matcher roomsMatcher = roomsPattern.matcher(roomsString);
            if (roomsMatcher.find()) {
                try {
                    rooms = Integer.parseInt(roomsMatcher.group(1));
                } catch (NumberFormatException e) {
                    rooms = 1; 
                }
            }
        }

        List<hotels> list = new ArrayList<>();
        if("hotelName".equals(searchOption)){
            list = d.getHotelByName(destination);
        } else if("location".equals(searchOption)){
            list = d.getHotelByLocation(destination);
        }

        session.setAttribute("hotelList", list);
        session.setAttribute("destination", destination);
        session.setAttribute("checkin", checkin);
        session.setAttribute("checkout", checkout);
        session.setAttribute("guestsString", guestsString);
        session.setAttribute("roomsString", roomsString);
        response.sendRedirect("ViewListRoom.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
