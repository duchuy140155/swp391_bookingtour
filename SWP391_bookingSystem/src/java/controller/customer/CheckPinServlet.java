package controller.customer;

import dal.BookingDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;


public class CheckPinServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private BookingDAO bookingDAO;

    public void init() {
        bookingDAO = new BookingDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        
        
        String tourIDStr = request.getParameter("tourID");
        String pin = request.getParameter("pin");
        
        if (tourIDStr == null || pin == null || tourIDStr.isEmpty() || pin.isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            out.print("{\"pinValid\":false, \"errorMessage\":\"Thiếu thông tin tourID hoặc pin.\"}");
            out.flush();
            out.close();
            return;
        }

       
        int tourID;
        try {
            tourID = Integer.parseInt(tourIDStr);
        } catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            out.print("{\"pinValid\":false, \"errorMessage\":\"tourID không hợp lệ.\"}");
            out.flush();
            out.close();
            return;
        }

        System.out.println("Received PIN: " + pin + ", Tour ID: " + tourID);  // Log received pin and tourID

       
        boolean isValid = bookingDAO.checkPin(pin, tourID);

        if (isValid) {
            out.print("{\"pinValid\":true}");
        } else {
            out.print("{\"pinValid\":false, \"errorMessage\":\"Mã PIN không hợp lệ. Vui lòng thử lại.\"}");
        }
        out.flush();
        out.close();
    }
}
