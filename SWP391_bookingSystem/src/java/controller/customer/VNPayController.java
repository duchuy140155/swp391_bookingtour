package controller.customer;

import dal.BookingDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.BookingDetails;
import model.Tour;
import model.User;
import vnPay.VNPayUtils;

public class VNPayController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String customerName = request.getParameter("customerName");
        String customerEmail = request.getParameter("customerEmail");
        String customerPhone = request.getParameter("customerPhone");
        String customerAddress = request.getParameter("customerAddress");
        String note = request.getParameter("note");
        String tourIDStr = request.getParameter("tourID");
        String paymentMethod = request.getParameter("paymentMethod");
        String totalpriceStr = request.getParameter("totalPrice");
        String totalpeople = request.getParameter("totalPeople");

        // Debugging output
        System.out.println("customerName: " + customerName);
        System.out.println("customerEmail: " + customerEmail);
        System.out.println("customerPhone: " + customerPhone);
        System.out.println("customerAddress: " + customerAddress);
        System.out.println("note: " + note);
        System.out.println("tourIDStr: " + tourIDStr);
        System.out.println("paymentMethod: " + paymentMethod);

        if (tourIDStr == null || tourIDStr.isEmpty()) {
            response.sendRedirect("error.jsp");
            return;
        }

        int tourID;
        try {
            tourID = Integer.parseInt(tourIDStr);
        } catch (NumberFormatException e) {
            response.sendRedirect("error.jsp");
            return;
        }

        if (customerName == null || customerEmail == null || customerPhone == null || customerAddress == null) {
            response.sendRedirect("error.jsp");
            return;
        }

        BookingDAO bookingDAO = new BookingDAO();
        Tour tour = null;
        try {
            tour = bookingDAO.getTourByID(tourID);
        } catch (Exception ex) {
            Logger.getLogger(VNPayController.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect("error.jsp");
            return;
        }

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");

        BookingDetails bookingDetails = new BookingDetails();
        bookingDetails.setCustomerName(customerName);
        bookingDetails.setEmail(customerEmail);
        bookingDetails.setPhoneNumber(Integer.parseInt(customerPhone));
        bookingDetails.setCustomerAddress(customerAddress);
        bookingDetails.setTour(tour);
        bookingDetails.setNote(note);
        bookingDetails.setDiscountID(null);
        bookingDetails.setTotalPeople(Integer.parseInt(totalpeople));
        if (user != null) {
            bookingDetails.setUserID(user);
        } else {
            bookingDetails.setUserID(null);
        }

        double totalPriceDouble;
        try {
            totalPriceDouble = Double.parseDouble(totalpriceStr);
        } catch (NumberFormatException e) {
            response.sendRedirect("error.jsp");
            return;
        }

        bookingDetails.setStatus(1); // Set initial status to 1 (Pending)
        bookingDetails.setFinalPrice(totalPriceDouble);
        bookingDetails.setBookingDate(new java.sql.Date(System.currentTimeMillis()));
        bookingDetails.setPaymentMethod(paymentMethod);

        int bookingID;
        try {
            bookingID = bookingDAO.saveBooking(bookingDetails);
        } catch (Exception e) {
            Logger.getLogger(VNPayController.class.getName()).log(Level.SEVERE, null, e);
            response.sendRedirect("error.jsp");
            return;
        }

        bookingDetails.setBookingID(bookingID);

        if ("vnpay".equals(paymentMethod)) {
            try {
                long totalPriceLong = (long) totalPriceDouble;
                String paymentUrl = VNPayUtils.createPaymentUrl(totalPriceLong, bookingID);
                response.sendRedirect(paymentUrl);
            } catch (Exception e) {
                e.printStackTrace();
                response.sendRedirect("error.jsp");
            }
        } else {
            request.setAttribute("bookingDetails", bookingDetails);
            request.setAttribute("tour", tour);
            request.getRequestDispatcher("confirmation.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Handle GET requests if necessary
    }
}
