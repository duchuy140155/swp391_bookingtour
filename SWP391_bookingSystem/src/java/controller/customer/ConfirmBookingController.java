/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.ConfirmDAO;
import dal.TicketDAO;
import dal.TourDAO;
import model.BookingDetails;
import model.Tour;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Ticket;

/**
 *
 * @author Admin
 */
public class ConfirmBookingController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String bookingIDStr = request.getParameter("bookingID");

        try {
            int bookingID = Integer.parseInt(bookingIDStr);
            ConfirmDAO confirmDAO = new ConfirmDAO();
            BookingDetails booking = confirmDAO.getBookingByID(bookingID);
            if (booking != null) {
                TourDAO tourDAO = new TourDAO();
                Tour tour = tourDAO.getTourByID(booking.getTour().getTourID());
                booking.setTour(tour);  // Set the tour object in booking details
                request.setAttribute("tour", tour);
                request.setAttribute("booking", booking);
                request.getRequestDispatcher("confirmation.jsp").forward(request, response);
            } else {
                response.sendRedirect("error.jsp");
            }
        } catch (NumberFormatException e) {
            response.sendRedirect("error.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));

        ConfirmDAO confirmDAO = new ConfirmDAO();
        Tour tour = confirmDAO.getTourByID(tourID);
        TicketDAO ticketDAO = new TicketDAO();
        BookingDetails bookingDetails = confirmDAO.getSomethingByBkID(bookingID);

        List<Ticket> tickets = new ArrayList<>();
        try {
            tickets = ticketDAO.getAllTicketByBkIḌ̣(bookingID);
        } catch (SQLException ex) {
            Logger.getLogger(ConfirmBookingController.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("tickets", tickets);
        request.setAttribute("bookingDetails", bookingDetails);
        request.setAttribute("tour", tour);
        request.getRequestDispatcher("confirmation.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
