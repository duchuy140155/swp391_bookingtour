package controller.customer;

import dal.ReviewTourDAO;
import dal.TourDAO;
import dal.DBContext;
import model.Review;
import model.Tour;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 */
public class HomepageController extends HttpServlet {

    private Connection connection;

    @Override
    public void init() throws ServletException {
        super.init();
        // Kết nối tới cơ sở dữ liệu
        connection = new DBContext().getConnection();
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TourDAO tourDAO = new TourDAO();
        ArrayList<Tour> listP = tourDAO.getAllTours();
        request.setAttribute("listP", listP);

        // Lấy danh sách top 3 hướng dẫn viên
        ReviewTourDAO reviewTourDAO = new ReviewTourDAO(connection);
        try {
            List<Review> topGuides = reviewTourDAO.getTop3GuidesCurrentMonth();
            request.setAttribute("topGuides", topGuides);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("homepage.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @Override
    public void destroy() {
        super.destroy();
        // Đóng kết nối cơ sở dữ liệu
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
