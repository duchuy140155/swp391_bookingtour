package controller.customer;
import java.sql.SQLException;
import dal.DiscountDAO;
import dal.GuideDAO;
import dal.TourDAO;
import dal.UserDAO;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import model.LuckySpin;
import model.User;

public class loginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String email = null;
        String password = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("customer_cuser".equals(cookie.getName())) {
                    email = cookie.getValue();
                    System.out.println("Email from cookie: " + email);
                }
                if ("customer_cpass".equals(cookie.getName())) {
                    password = cookie.getValue();
                    System.out.println("Password from cookie: " + password);
                }
            }
        }

        request.setAttribute("email", email);
        request.setAttribute("password", password);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");

        if (email == null || email.trim().isEmpty()) {
            request.setAttribute("error", "Email không hợp lệ");
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        if (password == null || password.trim().isEmpty()) {
            request.setAttribute("error", "Mật khẩu không được để trống");
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        String passwordWithSpecialChars = addSpecialChars(password);
        String hashedPassword = md5(passwordWithSpecialChars);

        Cookie cu = new Cookie("customer_cuser", email);
        Cookie cp = new Cookie("customer_cpass", password);
        if ("on".equals(remember)) {
            cu.setMaxAge(60 * 60 * 24 * 7);
            cp.setMaxAge(60 * 60 * 24 * 7);
        } else {
            cu.setMaxAge(0);
            cp.setMaxAge(0);
        }
        response.addCookie(cu);
        response.addCookie(cp);

        DiscountDAO d = new DiscountDAO();
        TourDAO b = new TourDAO();
        UserDAO userDAO = new UserDAO();
          GuideDAO guideDAO = new GuideDAO();
        User user = userDAO.loadAccount(email, hashedPassword);
        if (user == null) {
            request.setAttribute("error", "Kiểm tra email hoặc mật khẩu");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else if (!user.isStatus()) {
            request.setAttribute("error", "Tài khoản của bạn đã bị khóa");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("currentUserID", user.getUserID());
            session.setAttribute("account", user);
            session.setAttribute("loadLuckyDiscount", d.loadAllLuckyDiscount());
            session.setAttribute("luckyDiscountTop1", d.loadTop1LuckyDiscount());
            session.setAttribute("luckyDiscountTop2", d.loadTop2LuckyDiscount());
            session.setAttribute("luckyDiscountTop3", d.loadTop3LuckyDiscount());
            session.setAttribute("luckyDiscountTop4", d.loadTop4LuckyDiscount());
            session.setAttribute("loadDiscount", d.loadAllDiscount());
            session.setAttribute("loadTourId", b.loadTourId());


            d.createTimeSpin(user);
            session.setAttribute("currentUserID", user.getUserID());

//            LuckySpin lucky = d.loadLuckyDiscountByUserId(user.getUserID());
//            session.setAttribute("dateSpin", lucky.getTimeLastSpins());
//            d.createTimeSpin(user);
            session.setAttribute("currentUserID", user.getUserID()); // Lưu currentUserID vào session
            session.setAttribute("account", user);
            session.setAttribute("userID", user.getUserID());
            int role = user.getRoleID();
            session.setAttribute("role", role);
            
             if (role == 5) {
                try {
                    int guideID = guideDAO.getGuideIDByUserID(user.getUserID());
                   session.setAttribute("guideID", guideID);
                } catch (SQLException e) {
                    e.printStackTrace();
                    // Bạn có thể thêm xử lý khác nếu cần
                }
            }

           

            switch (role) {
                case 1:
                    response.sendRedirect("admin.jsp");
                    break;
                case 2:
                    response.sendRedirect("homepage");
                    break;
                case 3:
                    response.sendRedirect("partner.jsp");
                    break;
                case 4:
                   response.sendRedirect("admin.jsp");
                    break;
                    case 5:
                   response.sendRedirect("GuideInfoServlet");
                    break;
                default:
                   
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                    break;
            }
        }
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String addSpecialChars(String password) {
        String specialChars = "!@#$%^&*()-_=+";
        return password + specialChars;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
