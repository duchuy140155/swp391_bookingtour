package controller.customer;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.GoogleAccount;
import dal.UserDAO;

public class LoginGoogleServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            response.sendRedirect("login.jsp");
            return;
        }

        GoogleLogin gg = new GoogleLogin();
        String accessToken = null;
        try {
            accessToken = gg.getToken(code);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("login.jsp");
            return;
        }

        if (accessToken == null || accessToken.isEmpty()) {
            response.sendRedirect("login.jsp");
            return;
        }

        GoogleAccount acc = null;
        try {
            acc = gg.getUserInfo(accessToken);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("login.jsp");
            return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("account", acc);

        UserDAO userDao = new UserDAO();
        if (!userDao.checkEmail(acc.getEmail())) {
            
            session.setAttribute("googleAccount", acc);
            response.sendRedirect("fillDetails.jsp"); 
        } else {
            response.sendRedirect("homepage.jsp"); 
        }
        response.sendRedirect("homepage");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
