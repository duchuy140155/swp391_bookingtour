package controller.customer;

import dal.TicketDAO;
import dal.ViewBookingDetailsDAO;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.WriteListener;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BookingDetails;
import model.Ticket;

public class SendNotificationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        String tourCode = request.getParameter("tourCode");

        ViewBookingDetailsDAO viewBookingDetailsDAO = new ViewBookingDetailsDAO();
        BookingDetails bookingDetails = viewBookingDetailsDAO.getBookingDetailsById(bookingID);

        TicketDAO ticketDAO = new TicketDAO();
        List<Ticket> tickets = null;
        try {
            tickets = ticketDAO.getAllTicketByBkIḌ̣(bookingID);
        } catch (SQLException ex) {
            Logger.getLogger(SendNotificationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }


        String subject = "Xác nhận đặt tour thành công từ Booking Tour!";
        String htmlContent = getEmailContent(request, response, bookingDetails, tickets, tourCode);
        sendEmail(bookingDetails.getEmail(), subject, htmlContent);

        response.sendRedirect("viewBooking?tourID=" + tourID);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private String getEmailContent(HttpServletRequest request, HttpServletResponse response, BookingDetails bookingdetails, List<Ticket> tickets, String tourCode)
            throws ServletException, IOException {
        request.setAttribute("bookingDetails", bookingdetails);
        request.setAttribute("tickets", tickets);
        request.setAttribute("tourCode", tourCode);
        
        StringWriter stringWriter = new StringWriter();
        ServletOutputStream out = new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                stringWriter.write(b);
            }

            @Override
            public boolean isReady() {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            }

            @Override
            public void setWriteListener(WriteListener wl) {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            }
        };
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/emailContent.jsp");
        dispatcher.include(request, new HttpServletResponseWrapper(response) {
            @Override
            public ServletOutputStream getOutputStream() {
                return out;
            }

            @Override
            public PrintWriter getWriter() {
                return new PrintWriter(stringWriter);
            }
        });

        return stringWriter.toString();
    }

    private boolean sendEmail(String email, String subject, String htmlContent) {
        String to = email;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("nguyenhai2k1@gmail.com", "yonlulrhmbnxkrpm");
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("nguyenhai2k1@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject, "UTF-8");

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(htmlContent, "text/html; charset=UTF-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }
}
