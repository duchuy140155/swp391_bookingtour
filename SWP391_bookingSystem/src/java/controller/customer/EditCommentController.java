package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import model.BlogComment;
import model.User;

public class EditCommentController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        
        try {
            int commentID = Integer.parseInt(request.getParameter("commentID"));
            String commentText = request.getParameter("comment");
            
            BlogDAO blogDAO = new BlogDAO();
            BlogComment comment = blogDAO.getCommentByID(commentID);
            
            if (comment != null && comment.getUser().getUserID() == user.getUserID() && commentText != null && !commentText.trim().isEmpty()) {
                comment.setComment(commentText.trim());
                blogDAO.updateComment(comment);
            }
            
            response.sendRedirect("viewBlog?blogID=" + comment.getBlog().getBlogID());
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("viewBlog?blogID=" + request.getParameter("blogID"));
        }
    }
}
