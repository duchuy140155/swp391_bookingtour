package controller.customer;

import dal.GuideDAO;
import dal.ScheduleDAO;
import dal.TourDAO;
import dal.ReviewTourDAO;
import dal.DBContext;
import model.Guide;
import model.Schedule;
import model.TourDetails;
import model.Review;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TourDetailsController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(TourDetailsController.class.getName());

    private Connection connection;

    @Override
    public void init() throws ServletException {
        super.init();
        // Lấy kết nối từ DBContext
        DBContext dbContext = new DBContext();
        connection = dbContext.getConnection();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String tourIDStr = request.getParameter("tourID");
        String guideIDStr = request.getParameter("guideId");
        if (tourIDStr != null && !tourIDStr.isEmpty()) {
            int tourID = Integer.parseInt(tourIDStr);
            TourDAO tourDAO = new TourDAO();
            GuideDAO guideDAO = new GuideDAO();
            ReviewTourDAO reviewTourDAO = new ReviewTourDAO(connection);
            TourDetails tourDetails = null;
            List<Schedule> schedules = null;
            List<Guide> listGuide = null;
            List<Review> reviews = null;

            try {
                tourDetails = tourDAO.getTourDetailsByID(tourID);
                ScheduleDAO scheduleDAO = new ScheduleDAO();
                schedules = scheduleDAO.getTourSchedules(tourID);
                listGuide = guideDAO.selectGuidesByTourID(tourID);
                

                if (guideIDStr != null && !guideIDStr.isEmpty()) {
                    int guideID = Integer.parseInt(guideIDStr);
                    reviews = reviewTourDAO.getReviewsByGuideId(guideID);
                    request.setAttribute("reviews", reviews);
                }

            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, "Database error", e);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error.");
                return;
            }
            request.setAttribute("tourDetails", tourDetails);
            request.setAttribute("schedules", schedules);
            request.setAttribute("listGuide", listGuide);
            request.setAttribute("tourID", tourID);
            request.setAttribute("guideID", guideIDStr);

            request.getRequestDispatcher("tourDetails.jsp").forward(request, response);
        } else {
            response.sendRedirect("error.jsp");
        }
    }
   @Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    String action = request.getParameter("action");
    ReviewTourDAO reviewTourDAO = new ReviewTourDAO(connection);

    if ("edit".equals(action)) {
        int reviewID = Integer.parseInt(request.getParameter("reviewID"));
        String title = request.getParameter("title");
        String comment = request.getParameter("comment");
        int rating = Integer.parseInt(request.getParameter("rating"));
        
        try {
            reviewTourDAO.updateReview(reviewID, title, comment, rating);
            response.sendRedirect("tourDetails?tourID=" + request.getParameter("tourID") + "&guideID=" + request.getParameter("guideID"));
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Database error", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error.");
        }
    } else if ("delete".equals(action)) {
        int reviewID = Integer.parseInt(request.getParameter("reviewID"));
        
        try {
            reviewTourDAO.deleteReview(reviewID);
            response.sendRedirect("tourDetails?tourID=" + request.getParameter("tourID") + "&guideID=" + request.getParameter("guideID"));
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Database error", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error.");
        }
    }
}

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
