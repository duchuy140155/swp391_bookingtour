/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.LocationDAO;
import model.Location;

import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15 // 15 MB
)
public class CustomerLocationController extends HttpServlet {

    private static final int ITEMS_PER_PAGE = 5;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LocationDAO locationDAO = new LocationDAO();

        String search = request.getParameter("search");
        String sortField = request.getParameter("sortField") != null ? request.getParameter("sortField") : "locationName";
        String sortOrder = request.getParameter("sortOrder") != null ? request.getParameter("sortOrder") : "ASC";
        int page = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int offset = (page - 1) * ITEMS_PER_PAGE;

        List<Location> locations;
        int totalLocations;

        if (search != null && !search.isEmpty()) {
            locations = locationDAO.searchLocations(search, offset, ITEMS_PER_PAGE);
            totalLocations = locationDAO.getTotalSearchedLocations(search);
        } else {
            locations = locationDAO.getLocationsWithPaginationAndSorting(page, ITEMS_PER_PAGE, sortField, sortOrder);
            totalLocations = locationDAO.getTotalLocations();
        }

        int totalPages = (int) Math.ceil((double) totalLocations / ITEMS_PER_PAGE);

        request.setAttribute("locations", locations);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("search", search);
        request.setAttribute("sortField", sortField);
        request.setAttribute("sortOrder", sortOrder);

        request.getRequestDispatcher("customerLocation.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
