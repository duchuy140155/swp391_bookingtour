package controller.customer;

import dal.BookingDAO;
import dal.BookingDetailsDAO;
import model.BookingDetails;
import model.CancelledBooking;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;


public class CheckPin extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private BookingDAO bookingDAO;
    private BookingDetailsDAO bookingDetailsDAO;

    public void init() {
        bookingDAO = new BookingDAO();
        bookingDetailsDAO = new BookingDetailsDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pin = request.getParameter("pin");

        if (pin == null || pin.isEmpty()) {
            request.setAttribute("pinValid", "false");
            request.setAttribute("errorMessage", "Thiếu thông tin PIN.");
            request.getRequestDispatcher("checkPin.jsp").forward(request, response);
            return;
        }

        boolean isValid = bookingDAO.checkPin(pin);

        if (isValid) {
            BookingDetails bookingDetails = bookingDetailsDAO.getBookingDetailsByTourCode(pin);

            if (bookingDetails == null) {
                request.setAttribute("pinValid", "false");
                request.setAttribute("errorMessage", "Không tìm thấy tour nào tương ứng với mã PIN đã nhập.");
            } else {
                CancelledBooking cancelledBooking = bookingDetailsDAO.getCancelledBookingByBookingID(bookingDetails.getBookingID());

                if (cancelledBooking == null) {
                    request.setAttribute("pinValid", "true");
                    request.setAttribute("bookingDetails", bookingDetails);
                } else if (cancelledBooking.getStatus() == 0) {  // Status 0: Cancellation request sent
                    request.setAttribute("pinValid", "false");
                    request.setAttribute("errorMessage", "Yêu cầu hủy tour đã được gửi và đang chờ xác nhận.");
                } else if (cancelledBooking.getStatus() == 1) {  // Status 1: Tour cancelled
                    request.setAttribute("pinValid", "false");
                    request.setAttribute("errorMessage", "Tour này đã bị hủy.");
                }
            }
        } else {
            request.setAttribute("pinValid", "false");
            request.setAttribute("errorMessage", "Mã PIN không hợp lệ. Vui lòng thử lại.");
        }

        request.getRequestDispatcher("checkPin.jsp").forward(request, response);
    }
}
