package controller.customer;

import dal.UserDAO;
import dal.DBContext;
import model.User;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import com.google.gson.Gson;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class GetUserDetails extends HttpServlet {

    private UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        DBContext dbContext = new DBContext();
        Connection connection = dbContext.connection;
        userDAO = new UserDAO(connection);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int userID = Integer.parseInt(request.getParameter("userID"));
            User user = userDAO.getUserDetails(userID);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(new Gson().toJson(user));
        } catch (NumberFormatException | SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Định dạng ID không hợp lệ hoặc lỗi cơ sở dữ liệu");
        }
    }
}
