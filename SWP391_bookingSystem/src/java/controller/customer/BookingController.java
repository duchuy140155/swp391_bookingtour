package controller.customer;

import dal.BookingDAO;
import dal.DiscountDAO;
import dal.TicketDAO;
import model.BookingDetails;
import model.Ticket;
import model.Tour;
import model.TourTicket;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Discount;
import model.TicketType;
import model.User;

public class BookingController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String tourIDStr = request.getParameter("tourID");
        TicketDAO ticketDAO = new TicketDAO();

        try {
            int tourID = Integer.parseInt(tourIDStr);
            BookingDAO bookingDAO = new BookingDAO();
            Tour tour = bookingDAO.getTourByID(tourID);
            List<TourTicket> listTicket = ticketDAO.getAllTicketByTourID(tourID);
            int sumTicket = ticketDAO.sumTicket(tourID);
            int remainsTicket = tour.getNumberOfPeople() - sumTicket;
            if (tour != null) {
                request.setAttribute("remainsTicket", remainsTicket);
                request.setAttribute("tour", tour);
                request.setAttribute("listTicket", listTicket);
                request.getRequestDispatcher("booking.jsp").forward(request, response);
            } else {
                response.sendRedirect("error.jsp");
            }
        } catch (NumberFormatException | SQLException e) {
            response.sendRedirect("error.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String customerName = request.getParameter("customerName");
        String customerEmail = request.getParameter("customerEmail");
        String customerPhone = request.getParameter("customerPhone");
        String customerAddress = request.getParameter("customerAddress");
        String note = request.getParameter("note");
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        String discountCode = request.getParameter("discountCode");
        String option = request.getParameter("option");

        System.out.println("customerName: " + customerName);
        System.out.println("customerEmail: " + customerEmail);
        System.out.println("customerPhone: " + customerPhone);
        System.out.println("customerAddress: " + customerAddress);
        System.out.println("note: " + note);
        System.out.println("tourID: " + tourID);
        System.out.println("discountCode: " + discountCode);
        System.out.println("option: " + option);
        DiscountDAO discountDAO = new DiscountDAO();
        int totalPeople = 0;
        double totalPrice = 0;
        if ("customerList".equals(option)) {
            totalPeople = Integer.parseInt(request.getParameter("totalPeople"));
            totalPrice = Double.parseDouble(request.getParameter("totalPrice"));

            Discount discount = discountDAO.checkDiscount(discountCode.trim());
            int discountPrice = 0;
            if (discount != null) {
                if (discount.getDiscountReduce() > 0) {
                    discountPrice = discount.getDiscountReduce();
                } else {
                    discountPrice = (int) (totalPrice * discount.getDiscountPercentage() / 100);
                }
                totalPrice -= discountPrice;
            }
            

            System.out.println("totalPeople: " + totalPeople);
            System.out.println("totalPrice: " + totalPrice);

            BookingDAO bookingDAO = new BookingDAO();
            Tour tour = bookingDAO.getTourByID(tourID);

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("account");

            List<Integer> ticketIDs = new ArrayList<>();
            List<Ticket> ticketsList = new ArrayList<>();

            BookingDetails bookingDetails = new BookingDetails();
            bookingDetails.setCustomerName(customerName);
            bookingDetails.setEmail(customerEmail);
            bookingDetails.setPhoneNumber(Integer.parseInt(customerPhone));
            bookingDetails.setCustomerAddress(customerAddress);
            bookingDetails.setFinalPrice(totalPrice);
            bookingDetails.setTour(tour);
            bookingDetails.setNote(note);
            if (discount!=null) {
                 bookingDetails.setDiscountID(discount);
            } else {
                 bookingDetails.setDiscountID(null);
            }
            if (user != null) {
                bookingDetails.setUserID(user);
            } else {
                bookingDetails.setUserID(null);
            }
            bookingDetails.setStatus(0);
            bookingDetails.setBookingDate(new java.sql.Date(System.currentTimeMillis()));
            bookingDetails.setTotalPeople(totalPeople);
            
            int bookingID = bookingDAO.saveBooking(bookingDetails);

            System.out.println("Booking ID: " + bookingID);

            if ("customerList".equals(option)) {
                TicketDAO ticketDAO = new TicketDAO();
                String[] typeIDs = request.getParameter("typeIDs").split(",");
                Map<String, List<Ticket>> tickets = new HashMap<>();

                for (String typeID : typeIDs) {
                    int count = 1;
                    List<Ticket> ticketList = new ArrayList<>();
                    while (true) {
                        try {
                            String name = request.getParameter("passenger[" + typeID + "][" + count + "][name]");
                            String gender = request.getParameter("passenger[" + typeID + "][" + count + "][gender]");

                            String dobDay = request.getParameter("passenger[" + typeID + "][" + count + "][dob-day]");
                            String dobMonth = request.getParameter("passenger[" + typeID + "][" + count + "][dob-month]");
                            String dobYear = request.getParameter("passenger[" + typeID + "][" + count + "][dob-year]");

                            if (name == null || gender == null || dobDay == null || dobMonth == null || dobYear == null) {
                                break;
                            }

                            String dob = dobYear + "-" + (dobMonth.length() == 1 ? "0" + dobMonth : dobMonth) + "-" + (dobDay.length() == 1 ? "0" + dobDay : dobDay);

                            Ticket ticket = new Ticket();
                            ticket.setBookingID(bookingID);
                            TicketType tickettype = ticketDAO.getTicketByTypeID(Integer.parseInt(typeID));

                            ticket.setTickettype(tickettype);
                            ticket.setName(name);
                            ticket.setGender("male".equals(gender));
                            try {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                java.util.Date utilDate = sdf.parse(dob);
                                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                                ticket.setDob(sqlDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            ticketList.add(ticket);
                            count++;
                        } catch (SQLException ex) {
                            Logger.getLogger(BookingController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (!ticketList.isEmpty()) {
                        tickets.put(typeID, ticketList);
                    }
                }

                try {
                    ticketDAO.insertTickets(bookingID, tickets);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            request.setAttribute("user", user);
            request.setAttribute("bookingID", bookingID);
            request.setAttribute("tour", tour);
            request.setAttribute("bookingDetails", bookingDetails);
            request.getRequestDispatcher("payment.jsp").forward(request, response);
        }
    }
}
