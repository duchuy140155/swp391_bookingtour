package controller.customer;

import dal.BookingDetailsDAO;
import model.BookingDetails;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Date;

public class BookingDetailsServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BookingDetailsDAO dao = new BookingDetailsDAO();
        List<BookingDetails> bookingList = dao.getAllBookingDetails();

        // Set the attribute and forward to JSP
        request.setAttribute("bookingList", bookingList);
        request.setAttribute("now", new Date());
        request.getRequestDispatcher("bookingDetails.jsp").forward(request, response);
    }
}
