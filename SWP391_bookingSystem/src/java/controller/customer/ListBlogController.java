package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Blog;
import model.User;

public class ListBlogController extends HttpServlet {

    private static final int BLOGS_PER_PAGE = 2;
    private static final Logger LOGGER = Logger.getLogger(ListBlogController.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BlogDAO blogDAO = new BlogDAO();
        int page = 1;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        User user = (User) request.getSession().getAttribute("account");
        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        try {
            int totalBlogs = blogDAO.getTotalBlogs();
            List<Blog> blogs = blogDAO.getAllBlogs(user.getUserID(), (page - 1) * BLOGS_PER_PAGE, BLOGS_PER_PAGE);

            request.setAttribute("blogs", blogs);
            request.setAttribute("totalBlogs", totalBlogs);
            request.setAttribute("currentPage", page);
            request.setAttribute("blogsPerPage", BLOGS_PER_PAGE);

            if ("true".equals(request.getParameter("ajax"))) {
                request.getRequestDispatcher("blogListFragment.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("listBlog.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQL Error while retrieving blogs", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database access error");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Unexpected error", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unexpected server error");
        }
    }

    @Override
    public String getServletInfo() {
        return "Handles the retrieval and display of blogs";
    }
}
