package controller.customer;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.Period;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.GoogleAccount;
import dal.UserDAO;


public class SaveDetails extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(SaveDetails.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        GoogleAccount acc = (GoogleAccount) session.getAttribute("googleAccount");

        if (acc == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");
        String userDOB = request.getParameter("userDOB");
        String phoneNumber = request.getParameter("phoneNumber");
        String address = request.getParameter("address");

        // Kiểm tra xem mật khẩu và xác nhận mật khẩu có khớp nhau hay không
        if (!password.equals(confirmPassword)) {
            request.setAttribute("error", "mật khẩu không khớp");
            request.getRequestDispatcher("fillDetails.jsp").forward(request, response);
            return;
        }

     
        if (!isValidAge(userDOB)) {
            request.setAttribute("error", "bạn chưa đủ 18 tuổi");
            request.getRequestDispatcher("fillDetails.jsp").forward(request, response);
            return;
        }

        String passwordWithSpecialChars = addSpecialChars(password);
        String hashedPassword = md5(passwordWithSpecialChars);

        UserDAO userDao = new UserDAO();
        try {
            userDao.saveUserData(acc.getName(), acc.getEmail(), hashedPassword, userDOB, phoneNumber, address, acc.getPicture(), 2, 1);
            session.removeAttribute("googleAccount");
            session.setAttribute("account", acc);
            response.sendRedirect("homepage.jsp");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error saving user data", e);
            response.sendRedirect("error.jsp");
        }
    }

    private boolean isValidAge(String dob) {
        LocalDate birthDate = LocalDate.parse(dob);
        LocalDate today = LocalDate.now();
        return Period.between(birthDate, today).getYears() >= 18;
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String addSpecialChars(String password) {
        String specialChars = "!@#$%^&*()-_=+";
        return password + specialChars;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
