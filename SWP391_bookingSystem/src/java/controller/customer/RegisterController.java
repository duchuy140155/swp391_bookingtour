package controller.customer;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.Calendar;

public class RegisterController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName").trim();
        String email = request.getParameter("email").trim();
        String password = request.getParameter("password").trim();
        String dobString = request.getParameter("userDOB");
        String phoneNumber = request.getParameter("phoneNumber").trim();
        String address = request.getParameter("address").trim();
        System.out.println("a");
        Date userDOB;
        try {
            userDOB = Date.valueOf(dobString);
        } catch (IllegalArgumentException e) {
            request.setAttribute("abc", "Invalid date format for Date of Birth");
            request.getRequestDispatcher("register.jsp").forward(request, response);

            return;
        }

        Calendar dob = Calendar.getInstance();
        dob.setTime(userDOB);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        if (age < 18) {
            request.setAttribute("abc", "You must be at least 18 years old to register");
            request.getRequestDispatcher("register.jsp").forward(request, response);
            return;
        }

        String passwordWithSpecialChars = addSpecialChars(password);
        String hashedPassword = md5(passwordWithSpecialChars);

        UserDAO userDAO = new UserDAO();
        boolean check;
        try {
            check = userDAO.checkEmail(email);
        } catch (Exception e) {
            request.setAttribute("abc", "Error checking email: " + e.getMessage());
            request.getRequestDispatcher("register.jsp").forward(request, response);
            return;
        }

        if (!check) {
            try {
                userDAO.createUser(userName, email, hashedPassword, userDOB, phoneNumber, address);
                request.setAttribute("abc", "Registration successful");
                request.getRequestDispatcher("homepage.jsp").forward(request, response); // Redirect to login page with a success message
            } catch (Exception e) {
                request.setAttribute("abc", "Error creating user: " + e.getMessage());
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("abc", "Account already exists");
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
    }

    private String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String addSpecialChars(String password) {
        String specialChars = "!@#$%^&*()-_=+";
        return password + specialChars;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
