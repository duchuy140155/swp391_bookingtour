package controller.customer;

import dal.BookingDetailsDAO;
import dal.FeedbackDAO;
import dal.TourDAO;
import dal.UserDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Date;
import java.util.List;
import model.BookingDetails;
import model.Feedback;
import model.Partner;
import model.Tour;
import model.User;

public class FeedbackController extends HttpServlet {

    private FeedbackDAO feedbackDAO = new FeedbackDAO();
    private UserDAO userDao;
    private TourDAO tourDao;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        try {
            if (action == null) {
                listFeedback(request, response);
            } else {
                switch (action) {
                    case "updateFeedback":
                        showEditForm(request, response);
                        break;
                    case "deleteFeedback":
                        deleteFeedback(request, response);
                        break;
                    default:
                        listFeedback(request, response);
                        break;
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        try {
            if ("insertFeedback".equals(action)) {
                insertFeedback(request, response);
            } else if ("updateFeedback".equals(action)) {
                updateFeedback(request, response);
            } else {
                listFeedback(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listFeedback(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        Partner partner = (Partner) session.getAttribute("partner");
        BookingDetailsDAO bd = new BookingDetailsDAO();
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        if (user != null) {
            int userId = user.getUserID();
            Date currentDate = new java.sql.Date(System.currentTimeMillis());
            BookingDetails bookedtour = bd.getContentForFeedback(userId, tourID);
            if (bookedtour != null) {
                boolean isCheck = currentDate.after(bookedtour.getTour().getEndDate());
                request.setAttribute("bookedtour", bookedtour);
                request.setAttribute("isCheck", isCheck);
            }
            request.setAttribute("user", user);
        }
        List<Feedback> feedbackList = feedbackDAO.getFeedbackByTourID(tourID);

        request.setAttribute("feedbackList", feedbackList);
        request.setAttribute("tourID", tourID);
        request.getRequestDispatcher("feedbackList.jsp").forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStr = request.getParameter("feedbackID");
//        int tourID = Integer.parseInt(request.getParameter("tourID"));
        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            Feedback existingFeedback = feedbackDAO.selectFeedback(id);
            request.setAttribute("editFeedback", existingFeedback);
        }
        listFeedback(request, response);
    }

    private void insertFeedback(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        String content = request.getParameter("feedbackContent");
        int userID = Integer.parseInt(request.getParameter("userID"));
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        Date createDate = new java.sql.Date(System.currentTimeMillis());

        // Insert feedback into the database
        feedbackDAO.insertFeedback(content, userID, tourID, createDate);

        // Redirect back to the feedback page for the tour
        response.sendRedirect("feedback?tourID=" + tourID);
    }

    private void updateFeedback(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStr = request.getParameter("feedbackID");
        String content = request.getParameter("feedbackContent");
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        if (idStr != null && content != null && !idStr.isEmpty() && !content.isEmpty()) {
            int id = Integer.parseInt(idStr);
            Feedback feedback = new Feedback(id, content);
            feedbackDAO.updateFeedback(feedback);
            request.getSession().setAttribute("successMessage", "Phản hồi đã được cập nhật thành công!");
        } else {
            request.getSession().setAttribute("errorMessage", "Có lỗi xảy ra khi cập nhật phản hồi!");
        }
        response.sendRedirect("feedback?tourID=" + tourID);
    }

    private void deleteFeedback(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStr = request.getParameter("feedbackID");
        int tourID = Integer.parseInt(request.getParameter("tourID"));
        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            feedbackDAO.deleteFeedback(id);
            request.getSession().setAttribute("successMessage", "Phản hồi đã được xóa thành công!");
        } else {
            request.getSession().setAttribute("errorMessage", "Có lỗi xảy ra khi xóa phản hồi!");
        }
        response.sendRedirect("feedback?tourID=" + tourID);
    }
}
