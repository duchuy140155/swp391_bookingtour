package controller.customer;

import dal.BlogDAO;
import dal.ReportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Blog;
import model.Report;
import model.ReportTypes;
import model.User;

public class ReportController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int blogID = Integer.parseInt(request.getParameter("blogID"));
            int typeID = Integer.parseInt(request.getParameter("typeID"));
            String reason = request.getParameter("reason");

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("account");

            if (user == null) {
                response.getWriter().println("User not logged in.");
                return;
            }

            BlogDAO blogDAO = new BlogDAO();
            Blog blog = blogDAO.getBlogByID(blogID);

            ReportDAO reportDAO = new ReportDAO();
            ReportTypes type = reportDAO.getReportTypeById(typeID);

            Report report = new Report();
            report.setBlog(blog);
            report.setUser(user);
            report.setType(type);
            report.setReason(reason);
            report.setCreatedAt(new Timestamp(System.currentTimeMillis()));

            boolean success = reportDAO.insertReport(report);

            if (success) {
                response.getWriter().println("Report submitted successfully.");
            } else {
                response.getWriter().println("Failed to submit report.");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().println("SQL Error: " + ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().println("Error: " + ex.getMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReportController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReportController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
