package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Blog;
import model.User;

public class FavoriteBlogController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        String blogIDStr = request.getParameter("blogID");
        String action = request.getParameter("action");
        User user = (User) request.getSession().getAttribute("account");

        if (blogIDStr == null || blogIDStr.isEmpty() || user == null || action == null) {
            out.write("{\"success\": false, \"message\": \"Invalid request\"}");
            return;
        }

        int blogID = Integer.parseInt(blogIDStr);
        BlogDAO blogDAO = new BlogDAO();

        try {
            Blog blog = blogDAO.getBlogByID(blogID);
            if (blog == null) {
                out.write("{\"success\": false, \"message\": \"Blog not found\"}");
                return;
            }

            if (blog.getUser().getUserID() == user.getUserID()) {
                out.write("{\"success\": false, \"message\": \"You cannot favorite your own blog\"}");
                return;
            }

            if ("favorite".equals(action)) {
                blogDAO.addFavorite(blogID, user.getUserID());
            } else if ("unfavorite".equals(action)) {
                blogDAO.removeFavorite(blogID, user.getUserID());
            }
            boolean isFavorited = blogDAO.isFavorite(blogID, user.getUserID());
            out.write("{\"success\": true, \"favorited\": " + isFavorited + "}");
        } catch (Exception ex) {
            out.write("{\"success\": false, \"message\": \"" + ex.getMessage() + "\"}");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
