package controller.customer;

import dal.ContactRequestDAO;
import dal.TourDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.util.Properties;
import jakarta.mail.*;
import jakarta.mail.internet.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Tour;

public class SendEmail extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String subject = request.getParameter("subject");
        String messageContent = request.getParameter("message");
        String tourID = request.getParameter("tourID");

        if (name == null || name.isEmpty() || email == null || email.isEmpty() || phone == null || phone.isEmpty() || subject == null || subject.isEmpty() || messageContent == null || messageContent.isEmpty() || tourID == null || tourID.isEmpty()) {
            response.sendRedirect("contactUs.jsp?status=error");
            return;
        }

        int tourIDInt = Integer.parseInt(tourID);
        Tour tour = null;
        String partnerEmail = null;
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem?useSSL=false", "root", "123456789");
            TourDAO tourDAO = new TourDAO(con);
            ContactRequestDAO contactRequestDAO = new ContactRequestDAO(con);

            tour = tourDAO.getTourByID(tourIDInt);
            partnerEmail = tourDAO.getPartnerEmailByTourID(tourIDInt);
            ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("UTC")); // Lấy thời gian theo múi giờ UTC
            contactRequestDAO.saveContactRequest(name, email, phone, subject, messageContent, tourIDInt, createdAt);

        } catch (SQLException e) {
            e.printStackTrace();
            response.sendRedirect("contactUs.jsp?status=error");
            return;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (partnerEmail == null) {
            response.sendRedirect("contactUs.jsp?status=error");
            return;
        }

        String host = "smtp.gmail.com";
        final String user = "trantientn03@gmail.com";
        final String password = "ihzk ezth zhmz tfvp";

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(partnerEmail));
            message.setSubject(subject, "UTF-8");

            String htmlContent = "<h2>Thông tin khách hàng</h2>"
                    + "<p><b>Tên:</b> " + name + "</p>"
                    + "<p><b>Email:</b> " + email + "</p>"
                    + "<p><b>Số điện thoại:</b> " + phone + "</p>"
                    + "<h2>Nội dung tin nhắn</h2>"
                    + "<p>" + messageContent + "</p>"
                    + "<h2>Thông tin tour</h2>"
                    + "<p><b>Tên Tour:</b> " + tour.getTourName() + "</p>"
                    + "<p><b>Mô tả:</b> " + tour.getTourDescription() + "</p>"
                    + "<p><b>Điểm đi:</b> " + tour.getStartLocation() + "</p>"
                    + "<p><b>Điểm đến:</b> " + tour.getEndLocation() + "</p>"
                    + "<p><b>Ngày đi:</b> " + tour.getStartDate() + "</p>"
                    + "<p><b>Ngày về:</b> " + tour.getEndDate() + "</p>"
                    + "<p><b>Giá:</b> " + tour.getPrice() + "</p>"
                    + "<p><b>Số người:</b> " + tour.getNumberOfPeople() + "</p>";

            message.setContent(htmlContent, "text/html; charset=UTF-8");
            Transport.send(message);
            response.sendRedirect("contactUs.jsp?status=success");
        } catch (MessagingException e) {
            e.printStackTrace();
            response.sendRedirect("contactUs.jsp?status=error");
        }
    }
}
