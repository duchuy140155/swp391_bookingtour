package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.BlogComment;
import model.User;

public class DeleteCommentController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        
        try {
            int commentID = Integer.parseInt(request.getParameter("commentID"));
            
            BlogDAO blogDAO = new BlogDAO();
            BlogComment comment = blogDAO.getCommentByID(commentID);
            
            if (comment != null && comment.getUser().getUserID() == user.getUserID()) {
                blogDAO.deleteComment(commentID);
            }
            
            response.sendRedirect("viewBlog?blogID=" + comment.getBlog().getBlogID());
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("viewBlog?blogID=" + request.getParameter("blogID"));
        }
    }
}
