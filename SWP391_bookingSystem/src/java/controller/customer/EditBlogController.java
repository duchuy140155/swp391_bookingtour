package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;
import java.sql.Timestamp;
import model.Blog;
import model.BlogPicture;
import model.User;

@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
    maxFileSize = 1024 * 1024 * 10, // 10 MB
    maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class EditBlogController extends HttpServlet {

    private static final String UPLOAD_DIR = "images"; // Directory within the webapp

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int blogID = Integer.parseInt(request.getParameter("blogID"));
        BlogDAO blogDAO = new BlogDAO();
        try {
            Blog blog = blogDAO.getBlogByID(blogID);
            request.setAttribute("blog", blog);
            request.getRequestDispatcher("editBlog.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("listBlog");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        try {
            int blogID = Integer.parseInt(request.getParameter("blogID"));
            String title = request.getParameter("title");
            String content = request.getParameter("content");

            if (title == null || title.trim().isEmpty() || content == null || content.trim().isEmpty()) {
                session.setAttribute("message", "Title and content cannot be empty.");
                response.sendRedirect("editBlog?blogID=" + blogID);
                return;
            }

            title = title.trim();
            content = content.trim();
            Timestamp updatedAt = new Timestamp(System.currentTimeMillis());

            Blog blog = new Blog();
            blog.setBlogID(blogID);
            blog.setTitle(title);
            blog.setContent(content);
            blog.setCreatedAt(updatedAt);
            blog.setUser(user);

            BlogDAO blogDAO = new BlogDAO();
            blogDAO.updateBlogWithoutPictures(blog);

            // Handle picture upload
            for (Part filePart : request.getParts()) {
                if (filePart.getName().equals("pictures") && filePart.getSize() > 0) {
                    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
                    String uploadDir = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR;
                    File uploadDirFile = new File(uploadDir);
                    if (!uploadDirFile.exists()) {
                        uploadDirFile.mkdir();
                    }
                    File file = new File(uploadDir, fileName);
                    filePart.write(file.getAbsolutePath());

                    BlogPicture blogPicture = new BlogPicture();
                    blogPicture.setPictureURL(UPLOAD_DIR + "/" + fileName);
                    blogPicture.setCreatedAt(updatedAt);
                    blogPicture.setBlog(blog); // Set the blog for the picture

                    blogDAO.addBlogPicture(blogPicture);
                }
            }

            response.sendRedirect("viewBlog?blogID=" + blogID);
        } catch (Exception e) {
            session.setAttribute("message", "An error occurred: " + e.getMessage());
            e.printStackTrace();
            response.sendRedirect("editBlog?blogID=" + request.getParameter("blogID"));
        }
    }

    @Override
    public String getServletInfo() {
        return "Handles blog editing";
    }
}
