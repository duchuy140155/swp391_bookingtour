package controller.customer;

import dal.UserDAO;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class ForgotPassword extends HttpServlet {

    private UserDAO userDao;

    public ForgotPassword() {
        this.userDao = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");

        RequestDispatcher dispatcher = null;
        int otpvalue = 0;
        HttpSession mySession = request.getSession();

        boolean emailExists = userDao.checkEmailExists(email);

        if (email != null && !email.equals("") && emailExists) {
            Random rand = new Random();
            otpvalue = rand.nextInt(900000) + 100000;

            String to = email;
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            Session session = Session.getDefaultInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("nguyenhai2k1@gmail.com", "yonlulrhmbnxkrpm");
                }
            });

            mySession.setAttribute("otpCreationTime", System.currentTimeMillis());

            try {
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress("nguyenhai2k1@gmail.com"));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("Xác nhận đổi mật khẩu từ Booking Tour!", "UTF-8");

                String htmlContent = "<html>"
                        + "<body style='font-family: Arial, sans-serif; margin: 0; padding: 0; background-color: #f4f4f4;'>"
                        + "<div style='max-width: 600px; margin: auto; background: white; padding: 20px; border-radius: 10px;'>"
                        + "<div style='text-align: center;'>"
                        + "<h1 style='font-size: 24px; color: #333; margin-bottom: 20px;'>Booking Tour</h1>"
                        + "</div>"
                        + "<h2 style='text-align: center; color: #333;'>Mã OTP Đổi Mật Khẩu</h2>"
                        + "<p>Chào bạn,</p>"
                        + "<p>Chúng tôi đã nhận được yêu cầu đổi mật khẩu cho tài khoản của bạn. Vui lòng sử dụng mã OTP sau để hoàn tất quá trình đổi mật khẩu:</p>"
                        + "<div style='text-align: center; margin: 20px 0;'>"
                        + "<span style='font-size: 24px; color: #4CAF50; font-weight: bold;'>" + otpvalue + "</span>"
                        + "</div>"
                        + "<p><strong>Lưu ý:</strong> Mã OTP của bạn sẽ hết hạn sau 1 phút!</p>"
                        + "<p>Nếu bạn không yêu cầu đổi mật khẩu, vui lòng bỏ qua email này hoặc liên hệ với bộ phận hỗ trợ của chúng tôi.</p>"
                        + "<div style='text-align: center; margin: 30px 0;'>"
                        + "<a href='#' style='text-decoration: none; color: white; background-color: #4CAF50; padding: 10px 20px; border-radius: 5px;'>Liên Hệ Hỗ Trợ</a>"
                        + "</div>"
                        + "<p>Trân trọng,</p>"
                        + "<p>Đội ngũ hỗ trợ của Booking Tour</p>"
                        + "<hr style='border: none; border-top: 1px solid #ddd;' />"
                        + "<p style='font-size: 12px; color: #999;'>"
                        + "Booking Tour - xxxxxx<br />"
                        + "Email: support@bookingtour.com<br />"
                        + "Hotline: 1900 123 456"
                        + "</p>"
                        + "</div>"
                        + "</body>"
                        + "</html>";

                message.setContent(htmlContent, "text/html; charset=UTF-8");

                Transport.send(message);
                System.out.println("Gửi thành công");
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }

            dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
            request.setAttribute("message", "Mã OTP đã được gửi đến Email của bạn!");
            mySession.setAttribute("otp", otpvalue);
            mySession.setAttribute("email", email);
            dispatcher.forward(request, response);
        } else {
            dispatcher = request.getRequestDispatcher("/forgotPassword.jsp");
            request.setAttribute("message", "Email không tồn tại trong hệ thống, vui lòng nhập lại!");
            dispatcher.forward(request, response);
        }
    }

    public String validateInput(String input, String regex, List<String> errors) {
        if (input.isEmpty()) {
            errors.add("Chưa điền Email!");
        } else {
            if (!regex.isEmpty() && !input.matches(regex)) {
                errors.add("Email không hợp lệ!");
            }
        }
        return input;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
