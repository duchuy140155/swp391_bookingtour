package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;
import java.sql.Timestamp;
import model.Blog;
import model.BlogPicture;
import model.User;

@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
    maxFileSize = 1024 * 1024 * 10, // 10 MB
    maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class CreateBlogController extends HttpServlet {

    private static final String UPLOAD_DIR = "images"; // Directory within the webapp

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        request.getRequestDispatcher("createBlog.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        try {
            String title = request.getParameter("title");
            String content = request.getParameter("content");

            if (title == null || title.trim().isEmpty() || content == null || content.trim().isEmpty()) {
                session.setAttribute("message", "Title and content cannot be empty.");
                response.sendRedirect("createBlog.jsp");
                return;
            }

            title = title.trim();
            content = content.trim();
            Timestamp createdAt = new Timestamp(System.currentTimeMillis());

            Blog blog = new Blog();
            blog.setTitle(title);
            blog.setContent(content);
            blog.setCreatedAt(createdAt);
            blog.setUser(user);

            BlogDAO blogDAO = new BlogDAO();
            blogDAO.createBlog(blog);

            // Handle picture upload
            for (Part filePart : request.getParts()) {
                if (filePart.getName().equals("pictures") && filePart.getSize() > 0) {
                    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
                    String uploadDir = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR;
                    File uploadDirFile = new File(uploadDir);
                    if (!uploadDirFile.exists()) {
                        uploadDirFile.mkdir();
                    }
                    File file = new File(uploadDir, fileName);
                    filePart.write(file.getAbsolutePath());

                    BlogPicture blogPicture = new BlogPicture();
                    blogPicture.setPictureURL(UPLOAD_DIR + "/" + fileName);
                    blogPicture.setCreatedAt(createdAt);
                    blogPicture.setBlog(blog); // Set the blog for the picture

                    blogDAO.addBlogPicture(blogPicture);
                }
            }

            response.sendRedirect("viewBlog?blogID=" + blog.getBlogID());
        } catch (Exception e) {
            session.setAttribute("message", "An error occurred: " + e.getMessage());
            e.printStackTrace();
            response.sendRedirect("listBlog");
        }
    }

    @Override
    public String getServletInfo() {
        return "Handles blog creation";
    }
}
