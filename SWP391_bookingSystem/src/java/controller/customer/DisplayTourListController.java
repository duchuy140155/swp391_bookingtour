/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.DisplayTourListDAO;
import model.Tour;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Admin
 */
public class DisplayTourListController extends HttpServlet {

    private DisplayTourListDAO tourListDAO;

    public DisplayTourListController() {
        this.tourListDAO = new DisplayTourListDAO(); // Khởi tạo đối tượng DAO
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private void searchToursForHomepage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String startLocation = request.getParameter("startLocation");
        String endLocation = request.getParameter("endLocation");
        String startDate = request.getParameter("startDate");

        if (startLocation == null || startLocation.equals("Không rõ") || startLocation.isEmpty()) {
            startLocation = "";
        }
        if (endLocation == null || endLocation.equals("Không rõ") || endLocation.isEmpty()) {
            endLocation = "";
        }
        if (startDate == null || startDate.isEmpty()) {
            startDate = LocalDate.now().toString();
        }

        List<Tour> tourList = tourListDAO.searchToursForHomepage(startLocation, endLocation, startDate);

        if (tourList.isEmpty()) {
            request.setAttribute("message", "Rất tiếc! Chúng tôi không có tour bạn đang cần tìm.");
        }

        setRequestAttributes(request, tourList, startLocation, endLocation, startDate, null, 0);

        request.getRequestDispatcher("displayTourList.jsp").forward(request, response);
    }

    private void searchToursWithFilter(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String startLocation = request.getParameter("startLocation");
        String endLocation = request.getParameter("endLocation");
        String startDate = request.getParameter("startDate");
        String daysFilter = request.getParameter("daysFilter");
        String budgetStr = request.getParameter("budget");

        double budget = budgetStr != null && !budgetStr.isEmpty() ? Double.parseDouble(budgetStr) : 0;

        if (startLocation == null || startLocation.equals("Không rõ") || startLocation.isEmpty()) {
            startLocation = "";
        }
        if (endLocation == null || endLocation.equals("Không rõ") || endLocation.isEmpty()) {
            endLocation = "";
        }
        if (startDate == null || startDate.isEmpty()) {
            startDate = LocalDate.now().toString();
        }

        List<Tour> tourList = tourListDAO.searchToursWithFilter(startLocation, endLocation, startDate, daysFilter, budget);

        if (tourList.isEmpty()) {
            request.setAttribute("message", "Rất tiếc! Chúng tôi không có tour bạn đang cần tìm.");
        }

        setRequestAttributes(request, tourList, startLocation, endLocation, startDate, daysFilter, budget);

        request.getRequestDispatcher("displayTourList.jsp").forward(request, response);
    }

    private void setRequestAttributes(HttpServletRequest request, List<Tour> tourList, String startLocation,
            String endLocation, String startDate, String daysFilter, double budget) {
        request.setAttribute("tourList", tourList);
        request.setAttribute("startLocation", startLocation);
        request.setAttribute("endLocation", endLocation);
        request.setAttribute("startDate", startDate);
        request.setAttribute("daysFilter", daysFilter);
        request.setAttribute("budget", budget);

        List<String> locations = Arrays.asList(
                "Hà Nội", "TP Hồ Chí Minh", "An Giang", "Bà Rịa – Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh",
                "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau",
                "Cần Thơ", "Cao Bằng", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai",
                "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương",
                "Hải Phòng", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang",
                "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định",
                "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên", "Quảng Bình",
                "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La",
                "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang",
                "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái"
        );
        request.setAttribute("locations", locations);

        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String formattedDate = currentDate.format(formatter);
        request.setAttribute("currentDate", formattedDate);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action != null && action.equals("filter")) {
            searchToursWithFilter(request, response);
        } else {
            searchToursForHomepage(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
