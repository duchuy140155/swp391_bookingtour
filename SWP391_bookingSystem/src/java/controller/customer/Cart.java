/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.customer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Room;
import model.RoomQuantity;

/**
 *
 * @author Administrator
 */
public class Cart extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Cart</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Cart at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String cartItemsJson = request.getParameter("cartItems");

        List<RoomQuantity> list = new ArrayList<>();
        HttpSession session = request.getSession();
        RoomDAO d = new RoomDAO();

        if (cartItemsJson == null || cartItemsJson.isEmpty()) {
            response.getWriter().write("No cart items received");
            return;
        }

        Gson gson = new Gson();
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        List<Integer> cartItems = gson.fromJson(cartItemsJson, listType);
        String total = request.getParameter("total");
        Map<Integer, Integer> roomCountMap = new HashMap<>();
        for (Integer roomId : cartItems) {
            roomCountMap.put(roomId, roomCountMap.getOrDefault(roomId, 0) + 1);
        }
        for (Map.Entry<Integer, Integer> entry : roomCountMap.entrySet()) {
            int roomId = entry.getKey();
            int quantity = entry.getValue();
            Room room = d.loadRoomById(roomId);
            RoomQuantity roomWithQuantity = new RoomQuantity(room, quantity);
            list.add(roomWithQuantity);
        }
        session.setAttribute("cartItems", list);
        System.out.println(list.size());

        response.sendRedirect("CartRoom.jsp");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
