/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Admin
 */
public class ValidateOtp extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String otpString = request.getParameter("otp");
        HttpSession session = request.getSession();

        RequestDispatcher dispatcher = null;

        // Kiểm tra nếu OTP là chuỗi rỗng hoặc không thể chuyển đổi thành số nguyên
        try {
            if (otpString == null || otpString.isEmpty()) {
                request.setAttribute("message", "Bạn cần phải nhập mã OTP!");
                dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
                dispatcher.forward(request, response);
                return;
            }
            int value = Integer.parseInt(otpString);
            Integer otp = (Integer) session.getAttribute("otp");
            Long otpCreationTime = (Long) session.getAttribute("otpCreationTime");

            if (otp != null && otpCreationTime != null) {
                long currentTime = System.currentTimeMillis();
                long otpValidityDuration = 1 * 60 * 1000;  // 1 phút sẽ hết hiệu lực OTP

                if (currentTime - otpCreationTime <= otpValidityDuration) {
                    if (value == otp) {
                        request.setAttribute("email", request.getParameter("email"));
                        request.setAttribute("status", "Xác nhận thành công!");
                        dispatcher = request.getRequestDispatcher("/newPassword.jsp");
                    } else {
                        request.setAttribute("message", "Mã OTP sai!");
                        dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
                    }
                } else {
                    request.setAttribute("message", "OTP has expired");
                    dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
                }
            } else {
                request.setAttribute("message", "Không tìm thấy mã OTP");
                dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
            }
        } catch (NumberFormatException e) {
            request.setAttribute("message", "Mã OTP phải là chuỗi 6 chữ số");
            dispatcher = request.getRequestDispatcher("/EnterOtp.jsp");
        }

        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
