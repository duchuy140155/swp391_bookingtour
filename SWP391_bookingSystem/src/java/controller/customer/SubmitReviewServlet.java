package controller.customer;

import dal.ReviewTourDAO;
import model.Review;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SubmitReviewServlet extends HttpServlet {
    private ReviewTourDAO reviewTourDAO;
    private static final Logger logger = Logger.getLogger(SubmitReviewServlet.class.getName());

    @Override
    public void init() throws ServletException {
        try {
            // Initialize the database connection here
            String jdbcURL = "jdbc:mysql://localhost:3306/bookingsystem?useSSL=false";
            String dbUser = "root";
            String dbPassword = "123456789";
            Connection connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);
            reviewTourDAO = new ReviewTourDAO(connection);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        Integer userID = (Integer) session.getAttribute("userID");

        // Kiểm tra xem userID có tồn tại trong session hay không
        if (userID == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            out.write("{\"message\":\"sessionExpired\"}");
            return;
        }

        String tourIDStr = request.getParameter("tourID");
        String guideIDStr = request.getParameter("guideID");
        String ratingStr = request.getParameter("rating");
        String comment = request.getParameter("reviewBody");
        String title = request.getParameter("reviewTitle");

        // Ghi log các giá trị từ form
        logger.log(Level.INFO, "Received form data - tourID: {0}, guideID: {1}, userID: {2}, rating: {3}, comment: {4}, title: {5}", 
                new Object[]{tourIDStr, guideIDStr, userID, ratingStr, comment, title});

        // Kiểm tra các tham số không được trống
        if (tourIDStr == null || tourIDStr.isEmpty() ||
            guideIDStr == null || guideIDStr.isEmpty() ||
            ratingStr == null || ratingStr.isEmpty() ||
            comment == null || comment.isEmpty() ||
            title == null || title.isEmpty()) {

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            out.write("{\"message\":\"missingParameters\"}");
            return;
        }

        try {
            // Chuyển đổi các tham số sang kiểu dữ liệu tương ứng
            int tourID = Integer.parseInt(tourIDStr);
            int guideID = Integer.parseInt(guideIDStr);
            int rating = Integer.parseInt(ratingStr);
            Date reviewDate = new Date(System.currentTimeMillis()); // Giả sử reviewDate là ngày hiện tại

            // Tạo đối tượng Review và lưu vào cơ sở dữ liệu
            Review review = new Review(tourID, guideID, userID, rating, comment, reviewDate, title);
            boolean success = reviewTourDAO.saveReview(review);

            if (success) {
                out.write("{\"message\":\"success\"}");
            } else {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                out.write("{\"message\":\"error\"}");
            }
        } catch (NumberFormatException e) {
            logger.log(Level.SEVERE, "Error parsing form data", e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            out.write("{\"message\":\"errorParsing\"}");
        }
    }
}
