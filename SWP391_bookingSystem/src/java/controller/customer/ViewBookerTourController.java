package controller.customer;

import dal.BookingCodeDAO;
import dal.BookingDetailsDAO;
import dal.ScheduleDAO;
import dal.TicketDAO;
import dal.TourDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BookingDetails;
import model.Schedule;
import model.Ticket;
import model.Tour;
import model.TourCode;
import model.TourDetails;
import model.User;

public class ViewBookerTourController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        BookingDetailsDAO bd = new BookingDetailsDAO();
        if (user != null) {
            Date currentDate = new java.sql.Date(System.currentTimeMillis());

            // Get page parameter from request
            int page = 1;
            int recordsPerPage = 3;
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }

            List<BookingDetails> list = bd.getBookerTourbyUserID(user.getUserID(), (page - 1) * recordsPerPage, recordsPerPage);
            int noOfRecords = bd.getNoOfRecords();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            request.setAttribute("bookingDetails", list);
            request.setAttribute("currentDate", currentDate);
            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("currentPage", page);
            request.getRequestDispatcher("viewListBookedTour.jsp").forward(request, response);
        } else {
            response.sendRedirect("login.jsp"); // Redirect to login if user is not logged in
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int bookingID = Integer.parseInt(request.getParameter("bookingID"));
        BookingDetailsDAO bd = new BookingDetailsDAO();
        TourDAO tourDAO = new TourDAO();
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        BookingCodeDAO bkdao = new BookingCodeDAO();
        BookingDetails bookingDetails = bd.getBookedTourbyBkID(bookingID);
        TicketDAO ticketDAO = new TicketDAO();
        List<Ticket> tickets = null;
        try {
            tickets = ticketDAO.getAllTicketByBkIḌ̣(bookingID);
            TourDetails tourDetails = tourDAO.getTourDetailsByID(bookingDetails.getTour().getTourID());
            List<Schedule> schedules = scheduleDAO.getTourSchedules(bookingDetails.getTour().getTourID());
            TourCode tourCode = bkdao.getCodebyBkID(bookingID);
            request.setAttribute("tickets", tickets);
            request.setAttribute("tourDetails", tourDetails);
            request.setAttribute("schedules", schedules);
            request.setAttribute("tourCode", tourCode);
            request.getRequestDispatcher("bookedTourDetails.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SendNotificationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Handle post request logic here
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
