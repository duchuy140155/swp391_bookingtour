/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.BlogDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Blog;
import model.User;

/**
 *
 * @author MSI
 */
public class ProfileBlogController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfileBlogController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfileBlogController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     private static final int BLOGS_PER_PAGE = 2;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        BlogDAO blogDAO = new BlogDAO();
        try {
            List<Blog> userBlogs = blogDAO.getBlogsByUser(user.getUserID(), 0, BLOGS_PER_PAGE);
            List<Blog> favoriteBlogs = blogDAO.getFavoriteBlogsByUser(user.getUserID(), 0, BLOGS_PER_PAGE);

            request.setAttribute("userBlogs", userBlogs);
            request.setAttribute("favoriteBlogs", favoriteBlogs);
            request.setAttribute("blogsPerPage", BLOGS_PER_PAGE);
            request.getRequestDispatcher("profileBlog.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        int offset = Integer.parseInt(request.getParameter("offset"));
        boolean isFavorite = Boolean.parseBoolean(request.getParameter("isFavorite"));

        BlogDAO blogDAO = new BlogDAO();
        try {
            List<Blog> blogs;
            if (isFavorite) {
                blogs = blogDAO.getFavoriteBlogsByUser(user.getUserID(), offset, BLOGS_PER_PAGE);
            } else {
                blogs = blogDAO.getBlogsByUser(user.getUserID(), offset, BLOGS_PER_PAGE);
            }

            request.setAttribute("blogs", blogs);
            request.getRequestDispatcher("loadMoreBlogs.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
