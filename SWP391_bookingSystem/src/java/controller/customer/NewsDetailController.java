/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.NewsDAO;
import dal.NewsImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.News;
import model.NewsImage;

/**
 *
 * @author Admin
 */
public class NewsDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idNewStr = request.getParameter("idNew");
        int idNew = -1;
        try {
            idNew = Integer.parseInt(idNewStr);
        } catch (NumberFormatException e) {
            // Xử lý khi idNew không hợp lệ
            request.setAttribute("error", "ID không hợp lệ");
            request.getRequestDispatcher("error.jsp").forward(request, response);
            return;
        }

        NewsDAO newsDAO = new NewsDAO();
        News news = newsDAO.getNewsById(idNew);

        if (news == null) {
            request.setAttribute("error", "Tin tức không tồn tại");
            request.getRequestDispatcher("error.jsp").forward(request, response);
            return;
        }

        NewsImageDAO newsImageDAO = new NewsImageDAO();
        List<NewsImage> images = newsImageDAO.getImagesByNewsId(idNew);

        request.setAttribute("news", news);
        request.setAttribute("images", images);
        request.getRequestDispatcher("newsDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
