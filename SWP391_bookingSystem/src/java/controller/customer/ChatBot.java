package controller.customer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class ChatBot extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Map<String, String> predefinedResponses;

    public ChatBot() {
        predefinedResponses = new HashMap<>();
        predefinedResponses.put("xin chào", "Chào bạn! Tôi có thể giúp gì cho bạn hôm nay?");
        predefinedResponses.put("bạn khỏe không", "Tôi chỉ là một bot, nhưng tôi rất khỏe! Còn bạn thì sao?");
        predefinedResponses.put("bạn tên là gì", "Tôi là một chatbot được tạo ra để giúp bạn.");
        predefinedResponses.put("tạm biệt", "Tạm biệt! Chúc bạn một ngày tốt lành!");
        predefinedResponses.put("làm thế nào để đặt tour", "Bạn có thể đặt tour bằng cách truy cập trang đặt tour của chúng tôi và chọn tour bạn muốn.");
        predefinedResponses.put("chính sách hủy tour là gì", "Chính sách hủy tour: Trước 14 ngày chịu phạt 20%, trước 10 ngày chịu phạt 30%, trước 7 ngày chịu phạt 50%, trước 6 ngày chịu phạt 60%, trước 5 ngày chịu phạt 70%, trước 4 ngày chịu phạt 80%, trước 3 ngày chịu phạt 100%.");
        predefinedResponses.put("tour có bảo hiểm không", "Các tour của chúng tôi đều bao gồm bảo hiểm du lịch.");
        predefinedResponses.put("thanh toán như thế nào", "Bạn có thể thanh toán qua thẻ tín dụng, chuyển khoản ngân hàng hoặc ví điện tử.");
        predefinedResponses.put("có khuyến mãi gì không", "Hiện tại chúng tôi đang có nhiều chương trình khuyến mãi. Vui lòng truy cập trang khuyến mãi của chúng tôi để biết thêm chi tiết.");
        predefinedResponses.put("thời gian khởi hành", "Thời gian khởi hành tùy thuộc vào tour bạn chọn. Vui lòng xem chi tiết trong trang mô tả tour.");
        predefinedResponses.put("cần chuẩn bị gì cho tour", "Bạn cần chuẩn bị giấy tờ tùy thân, trang phục phù hợp, và các vật dụng cá nhân cần thiết.");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userQuestion = request.getParameter("question").toLowerCase().trim();
        String responseMessage = predefinedResponses.getOrDefault(userQuestion, "Xin lỗi, tôi không hiểu câu hỏi của bạn.");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("{\"responseMessage\": \"" + responseMessage + "\"}");
    }
}
