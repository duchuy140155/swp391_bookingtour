/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Admin
 */
public class ChangePassController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePassController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePassController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDAO d = new UserDAO();
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String cfpass = request.getParameter("cfpass");
        //String email = (String) session.getAttribute("account");
        String email = "duy1@gmail.com";
        String passwordWithSpecialChars1 = addSpecialChars(oldpass);
        String hashedPassword1 = md5(passwordWithSpecialChars1);
        if (d.checkOldPass(email, hashedPassword1) == false) {
            request.setAttribute("err", "Old Password is wrong!");
            request.getRequestDispatcher("changePassword.jsp").forward(request, response);

        } else {

            if (newpass.equals(cfpass)) {
                if (newpass.length() < 8 || !newpass.matches(".*[!@#$%^&*()-_=+].*")) {
                    request.setAttribute("err", "Password must be at least 8 characters long and include special characters");
                    request.getRequestDispatcher("changePassword.jsp").forward(request, response);
                } else {
                    String passwordWithSpecialChars = addSpecialChars(newpass);
                    String hashedPassword = md5(passwordWithSpecialChars);
                    d.setPassword(email, hashedPassword);
                    request.setAttribute("success", "Change Pass Successfully!");
                    request.getRequestDispatcher("homepage.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("err", "New Pass and Confirm Pass do not match. Please check again!");
                request.getRequestDispatcher("changePassword.jsp").forward(request, response);
            }
        }
    }

    private String md5(String input) {
        try {
            // Create MD5 Hashing instance
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Update message digest with input byte array
            md.update(input.getBytes());
            // Generate MD5 hash
            byte[] digest = md.digest();
            // Convert byte array to signum representation
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // Method to add special characters to the password
    private String addSpecialChars(String password) {
        // Add your desired special characters here
        String specialChars = "!@#$%^&*()-_=+";

        // Append special characters to the password
        StringBuilder sb = new StringBuilder(password);
        sb.append(specialChars);
        return sb.toString();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
