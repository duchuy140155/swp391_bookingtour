/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Administrator
 */
public class LuckySpin {
    private int userID;
    private int numberSpin;
    private String TimeLastSpins;

    public LuckySpin() {
    }

    public LuckySpin(int userID, int numberSpin, String TimeLastSpins) {
        this.userID = userID;
        this.numberSpin = numberSpin;
        this.TimeLastSpins = TimeLastSpins;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getNumberSpin() {
        return numberSpin;
    }

    public void setNumberSpin(int numberSpin) {
        this.numberSpin = numberSpin;
    }

    public String getTimeLastSpins() {
        return TimeLastSpins;
    }

    public void setTimeLastSpins(String TimeLastSpins) {
        this.TimeLastSpins = TimeLastSpins;
    }
    
}
