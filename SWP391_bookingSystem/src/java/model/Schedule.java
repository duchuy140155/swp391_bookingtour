package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Schedule {

    private int scheduleID;
    private String scheduleTitle;
    private String scheduleContent;
    private int tourID;
    private Date scheduleDate; // Sử dụng java.util.Date hoặc java.sql.Date tùy thuộc vào nhu cầu
    private List<ScheduleDetails> details;

    public Schedule(int scheduleID, String scheduleTitle, String scheduleContent, int tourID, Date scheduleDate) {
        this.scheduleID = scheduleID;
        this.scheduleTitle = scheduleTitle;
        this.scheduleContent = scheduleContent;
        this.tourID = tourID;
        this.scheduleDate = scheduleDate;
        this.details = new ArrayList<>();
    }

    // Các phương thức getter và setter
    public Schedule() {
    }

    public int getScheduleID() {
        return scheduleID;
    }

    public String getScheduleTitle() {
        return scheduleTitle;
    }

    public String getScheduleContent() {
        return scheduleContent;
    }

    public int getTourID() {
        return tourID;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public List<ScheduleDetails> getDetails() {
        return details;
    }

    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

    public void setScheduleTitle(String scheduleTitle) {
        this.scheduleTitle = scheduleTitle;
    }

    public void setScheduleContent(String scheduleContent) {
        this.scheduleContent = scheduleContent;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public void setDetails(List<ScheduleDetails> details) {
        this.details = details;
    }

}
