package model;

public class TourCode {
    private int id;
    private int bookingID;
    private String tourCode;

    public TourCode() {
    }

    public TourCode(int id, int bookingID, String tourCode) {
        this.id = id;
        this.bookingID = bookingID;
        this.tourCode = tourCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public String getTourCode() {
        return tourCode;
    }

    public void setTourCode(String tourCode) {
        this.tourCode = tourCode;
    }
}
