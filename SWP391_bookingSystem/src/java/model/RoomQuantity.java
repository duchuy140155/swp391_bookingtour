/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Administrator
 */
public class RoomQuantity {
    private Room room;
    private int quantity;

    public RoomQuantity(Room room, int quantity) {
        this.room = room;
        this.quantity = quantity;
    }

    public RoomQuantity() {
    }

    public Room getRoom() {
        return room;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
