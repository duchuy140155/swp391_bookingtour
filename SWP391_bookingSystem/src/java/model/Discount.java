/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class Discount {
     private int discountID;
    private String discountCode;
    private String discountName;
    private Date discountFrom;
    private Date discountTo;
    private boolean status;
    private int discountType;
    private int discountPercentage;
    private int discountReduce;
    private int tourID;

    public Discount() {
    }

    public Discount(int discountID, String discountCode, String discountName, Date discountFrom, Date discountTo, boolean status, int discountType, int discountPercentage, int discountReduce, int tourId) {
        this.discountID = discountID;
        this.discountCode = discountCode;
        this.discountName = discountName;
        this.discountFrom = discountFrom;
        this.discountTo = discountTo;
        this.status = status;
        this.discountType = discountType;
        this.discountPercentage = discountPercentage;
        this.discountReduce = discountReduce;
        this.tourID = tourId;
    }

    public int getDiscountID() {
        return discountID;
    }

    public void setDiscountID(int discountID) {
        this.discountID = discountID;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Date getDiscountFrom() {
        return discountFrom;
    }

    public void setDiscountFrom(Date discountFrom) {
        this.discountFrom = discountFrom;
    }

    public Date getDiscountTo() {
        return discountTo;
    }

    public void setDiscountTo(Date discountTo) {
        this.discountTo = discountTo;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getDiscountType() {
        return discountType;
    }

    public void setDiscountType(int discountType) {
        this.discountType = discountType;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public int getDiscountReduce() {
        return discountReduce;
    }

    public void setDiscountReduce(int discountReduce) {
        this.discountReduce = discountReduce;
    }

    public int getTourID() {
        return tourID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

   
    
}
