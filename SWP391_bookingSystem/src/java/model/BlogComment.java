/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author MSI
 */
public class BlogComment {
    private int commentID;
    private String comment;
    private Date createdAt;
    private int parentCommentID;
    private Blog blog;
    private User user;
    private List<BlogComment> replies;
    public BlogComment() {
    }

    public BlogComment(int commentID, String comment, Date createdAt, int parentCommentID, Blog blog, User user, List<BlogComment> replies) {
        this.commentID = commentID;
        this.comment = comment;
        this.createdAt = createdAt;
        this.parentCommentID = parentCommentID;
        this.blog = blog;
        this.user = user;
        this.replies = replies;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getParentCommentID() {
        return parentCommentID;
    }

    public void setParentCommentID(int parentCommentID) {
        this.parentCommentID = parentCommentID;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BlogComment> getReplies() {
        return replies;
    }

    public void setReplies(List<BlogComment> replies) {
        this.replies = replies;
    }

    
    
}
