/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author MSI
 */
public class Report {
    private int reportID; 
    private String reason;
    private Timestamp createdAt;
    private Blog blog;
    private User user;
    private ReportTypes type;

    public Report() {
    }

    public Report(int reportID, String reason, Timestamp createdAt, Blog blog, User user, ReportTypes type) {
        this.reportID = reportID;
        this.reason = reason;
        this.createdAt = createdAt;
        this.blog = blog;
        this.user = user;
        this.type = type;
    }

    public int getReportID() {
        return reportID;
    }

    public void setReportID(int reportID) {
        this.reportID = reportID;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ReportTypes getType() {
        return type;
    }

    public void setType(ReportTypes type) {
        this.type = type;
    }
    
    

}
    

    