/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class Handbook {

    private int handbookID;
    private int partnerID;
    private String title;
    private String shortContent;
    private String createdAt;
    private String imageTheme;

    public Handbook() {
    }

    public Handbook(int handbookID, int partnerID, String title, String shortContent, String createdAt, String imageTheme) {
        this.handbookID = handbookID;
        this.partnerID = partnerID;
        this.title = title;
        this.shortContent = shortContent;
        this.createdAt = createdAt;
        this.imageTheme = imageTheme;
    }

    public int getHandbookID() {
        return handbookID;
    }

    public void setHandbookID(int handbookID) {
        this.handbookID = handbookID;
    }

    public int getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortContent() {
        return shortContent;
    }

    public void setShortContent(String shortContent) {
        this.shortContent = shortContent;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getImageTheme() {
        return imageTheme;
    }

    public void setImageTheme(String imageTheme) {
        this.imageTheme = imageTheme;
    }

    @Override
    public String toString() {
        return "handbook{" + "handbookID=" + handbookID + ", partnerID=" + partnerID + ", title=" + title + ", shortContent=" + shortContent + ", createdAt=" + createdAt + ", imageTheme=" + imageTheme + '}';
    }

}
