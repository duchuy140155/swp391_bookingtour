package model;

import java.sql.Date;
import java.util.List;

public class Review {
    // Fields
    private int reviewID;
    private int tourID;
    private int guideID;
    private int userID;
    private int rating;
    private String comment;
    private Date reviewDate;
    private String title;
    private String tourName;
    private Date startDate;
    private Date endDate;
    private String username;
      private String imageURL;
      private List<ReviewReply> replies; 

    // Existing Constructor
    public Review(int reviewID, int tourID, int guideID, int userID, int rating, String comment, Date reviewDate, String title, String tourName, Date startDate, Date endDate, String username) {
        this.reviewID = reviewID;
        this.tourID = tourID;
        this.guideID = guideID;
        this.userID = userID;
        this.rating = rating;
        this.comment = comment;
        this.reviewDate = reviewDate;
        this.title = title;
        this.tourName = tourName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.username = username;
    }

    // New Constructor
    public Review(int tourID, int guideID, int userID, int rating, String comment, Date reviewDate, String title) {
        this.tourID = tourID;
        this.guideID = guideID;
        this.userID = userID;
        this.rating = rating;
        this.comment = comment;
        this.reviewDate = reviewDate;
        this.title = title;
    }

    // Getters and Setters for all fields
    // ...

    public int getReviewID() {
        return reviewID;
    }

    public int getTourID() {
        return tourID;
    }

    public int getGuideID() {
        return guideID;
    }

    public int getUserID() {
        return userID;
    }

    public int getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public String getTitle() {
        return title;
    }

    public String getTourName() {
        return tourName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getUsername() {
        return username;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Review(int reviewID, int tourID, int guideID, int userID, int rating, String comment, Date reviewDate, String title, String tourName, Date startDate, Date endDate, String username, List<ReviewReply> replies) {
        this.reviewID = reviewID;
        this.tourID = tourID;
        this.guideID = guideID;
        this.userID = userID;
        this.rating = rating;
        this.comment = comment;
        this.reviewDate = reviewDate;
        this.title = title;
        this.tourName = tourName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.username = username;
        this.replies = replies;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Review() {
    }
    
}
