/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class RevenueByPartner {

    private String partnerName;
    private double totalRevenue;
    private int tourCount;
    private int month;
    private String customerName;
    private double highestRevenue;
    private int totalCustomers;

    public RevenueByPartner() {
    }

    public RevenueByPartner(String partnerName, double totalRevenue) {
        this.partnerName = partnerName;
        this.totalRevenue = totalRevenue;
    }

    public RevenueByPartner(String partnerName, double totalRevenue, int tourCount, int month, String customerName, double highestRevenue, int totalCustomers) {
        this.partnerName = partnerName;
        this.totalRevenue = totalRevenue;
        this.tourCount = tourCount;
        this.month = month;
        this.customerName = customerName;
        this.highestRevenue = highestRevenue;
        this.totalCustomers = totalCustomers;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public void setTotalRevenue(double totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public void setTourCount(int tourCount) {
        this.tourCount = tourCount;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setHighestRevenue(double highestRevenue) {
        this.highestRevenue = highestRevenue;
    }

    public void setTotalCustomers(int totalCustomers) {
        this.totalCustomers = totalCustomers;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public double getTotalRevenue() {
        return totalRevenue;
    }

    public int getTourCount() {
        return tourCount;
    }

    public int getMonth() {
        return month;
    }

    public String getCustomerName() {
        return customerName;
    }

    public double getHighestRevenue() {
        return highestRevenue;
    }

    public int getTotalCustomers() {
        return totalCustomers;
    }


    @Override
    public String toString() {
        return "RevenueByPartner{" + "partnerName=" + partnerName + ", totalRevenue=" + totalRevenue + ", tourCount=" + tourCount + ", month=" + month + ", customerName=" + customerName + ", highestRevenue=" + highestRevenue + ", totalCustomers=" + totalCustomers + '}';
    }
}
