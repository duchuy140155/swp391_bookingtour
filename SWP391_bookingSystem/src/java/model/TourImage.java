/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author MSI
 */
public class TourImage {

    private int imageID;
    private String imageURL;
    private int tourID;

    public TourImage() {
    }

    public TourImage(int imageID, String imageURL, int tourID) {
        this.imageID = imageID;
        this.imageURL = imageURL;
        this.tourID = tourID;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getTourID() {
        return tourID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    @Override
    public String toString() {
        return "TourImage{" + "imageID=" + imageID + ", imageURL=" + imageURL + ", tourID=" + tourID + '}';
    }

}
