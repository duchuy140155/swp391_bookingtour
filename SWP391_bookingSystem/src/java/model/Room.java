/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Administrator
 */
public class Room {
    private int room_id;
    private int hotel_id;
    private int room_type;
    private String name;
    private double price;
    private int imageIdRoom;
    private int max_guests;
    private String description;
    private int number_beds;

    public Room() {
    }

    public Room(int room_id, int hotel_id, int room_type, String name, double price, int imageIdRoom, int max_guests, String description, int number_beds) {
        this.room_id = room_id;
        this.hotel_id = hotel_id;
        this.room_type = room_type;
        this.name = name;
        this.price = price;
        this.imageIdRoom = imageIdRoom;
        this.max_guests = max_guests;
        this.description = description;
        this.number_beds = number_beds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    public int getRoom_type() {
        return room_type;
    }

    public void setRoom_type(int room_type) {
        this.room_type = room_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getImageIdRoom() {
        return imageIdRoom;
    }

    public void setImageIdRoom(int imageIdRoom) {
        this.imageIdRoom = imageIdRoom;
    }

    public int getMax_guests() {
        return max_guests;
    }

    public void setMax_guests(int max_guests) {
        this.max_guests = max_guests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumber_beds() {
        return number_beds;
    }

    public void setNumber_beds(int number_beds) {
        this.number_beds = number_beds;
    }
    
    
             
}
