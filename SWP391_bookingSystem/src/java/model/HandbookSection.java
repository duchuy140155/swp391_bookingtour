/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class HandbookSection {

    private int sectionID;
    private int handbookID;
    private int sectionOrder;
    private String createdAt;

    public HandbookSection() {
    }

    public HandbookSection(int sectionID, int handbookID, int sectionOrder, String createdAt) {
        this.sectionID = sectionID;
        this.handbookID = handbookID;
        this.sectionOrder = sectionOrder;
        this.createdAt = createdAt;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getHandbookID() {
        return handbookID;
    }

    public void setHandbookID(int handbookID) {
        this.handbookID = handbookID;
    }

    public int getSectionOrder() {
        return sectionOrder;
    }

    public void setSectionOrder(int sectionOrder) {
        this.sectionOrder = sectionOrder;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "HandbookSection{" + "sectionID=" + sectionID + ", handbookID=" + handbookID + ", sectionOrder=" + sectionOrder + ", createdAt=" + createdAt + '}';
    }

}
