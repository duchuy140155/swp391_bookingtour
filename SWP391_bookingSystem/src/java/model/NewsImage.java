/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class NewsImage {

    private int newImageID;
    private int idNew; // FK to News table
    private String newsImageURL;

    public NewsImage() {
    }

    // Getters and Setters
    public int getNewImageID() {
        return newImageID;
    }

    public void setNewImageID(int newImageID) {
        this.newImageID = newImageID;
    }

    public int getIdNew() {
        return idNew;
    }

    public void setIdNew(int idNew) {
        this.idNew = idNew;
    }

    public String getNewsImageURL() {
        return newsImageURL;
    }

    public void setNewsImageURL(String newsImageURL) {
        this.newsImageURL = newsImageURL;
    }
}
