/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class HandbookContent {

    private int contentID;
    private int sectionID;
    private String title;
    private String content;
    private String image;
    private String imageDescription;
    private String createdAt;

    public HandbookContent() {
    }

    public HandbookContent(int contentID, int sectionID, String title, String content, String image, String imageDescription, String createdAt) {
        this.contentID = contentID;
        this.sectionID = sectionID;
        this.title = title;
        this.content = content;
        this.image = image;
        this.imageDescription = imageDescription;
        this.createdAt = createdAt;
    }

    public int getContentID() {
        return contentID;
    }

    public void setContentID(int contentID) {
        this.contentID = contentID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "HandbookContent{" + "contentID=" + contentID + ", sectionID=" + sectionID + ", title=" + title + ", content=" + content + ", image=" + image + ", imageDescription=" + imageDescription + ", createdAt=" + createdAt + '}';
    }

}
