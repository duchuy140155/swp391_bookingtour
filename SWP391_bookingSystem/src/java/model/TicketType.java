/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class TicketType {

    private int typeID;
    private String typeName;
    private String typeCode;
    private int ageMin;
    private int ageMax;
    private boolean isDefault;
    private int tourID;

    public TicketType() {
    }

    public TicketType(int typeID, String typeName, String typeCode, int ageMin, int ageMax, boolean isDefault) {
        this.typeID = typeID;
        this.typeName = typeName;
        this.typeCode = typeCode;
        this.ageMin = ageMin;
        this.ageMax = ageMax;
        this.isDefault = isDefault;
    }

    public TicketType(int typeID, String typeName, String typeCode, int ageMin, int ageMax, boolean isDefault, int tourID) {
        this.typeID = typeID;
        this.typeName = typeName;
        this.typeCode = typeCode;
        this.ageMin = ageMin;
        this.ageMax = ageMax;
        this.isDefault = isDefault;
        this.tourID = tourID;
    }

    

    // Getters and Setters
    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public int getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(int ageMin) {
        this.ageMin = ageMin;
    }

    public int getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(int ageMax) {
        this.ageMax = ageMax;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public int getTourID() {
        return tourID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }
}
