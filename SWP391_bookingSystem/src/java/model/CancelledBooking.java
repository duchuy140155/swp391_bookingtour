package model;

import java.sql.Date;

public class CancelledBooking {

    private int cancelID;
    private int bookingID;
    private Date cancelDate;
    private String cancelReason;
    private double penaltyPercentage;
    private double refundAmount;
    private int status;
    private String tourName;
    private Date startDate;
    private Date endDate;

    // Getters and setters...
    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // Other getters and setters...
    public int getCancelID() {
        return cancelID;
    }

    public int getBookingID() {
        return bookingID;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public double getPenaltyPercentage() {
        return penaltyPercentage;
    }

    public double getRefundAmount() {
        return refundAmount;
    }

    public int getStatus() {
        return status;
    }

    public void setCancelID(int cancelID) {
        this.cancelID = cancelID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public void setPenaltyPercentage(double penaltyPercentage) {
        this.penaltyPercentage = penaltyPercentage;
    }

    public void setRefundAmount(double refundAmount) {
        this.refundAmount = refundAmount;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CancelledBooking() {
    }

    public CancelledBooking(int cancelID, int bookingID, Date cancelDate, String cancelReason, double penaltyPercentage, double refundAmount, int status, String tourName, Date startDate, Date endDate) {
        this.cancelID = cancelID;
        this.bookingID = bookingID;
        this.cancelDate = cancelDate;
        this.cancelReason = cancelReason;
        this.penaltyPercentage = penaltyPercentage;
        this.refundAmount = refundAmount;
        this.status = status;
        this.tourName = tourName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

}
