/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class RoomType {
    private int room_type;
    private String roomTypeName;

    public RoomType(int room_type, String roomTypeName) {
        this.room_type = room_type;
        this.roomTypeName = roomTypeName;
    }

    

    public RoomType() {
    }

    public int getRoom_type() {
        return room_type;
    }

    public void setRoom_type(int room_type) {
        this.room_type = room_type;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    
    
    
    
}
