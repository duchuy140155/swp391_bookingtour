/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MSI
 */
public class Blog {
    private int blogID;
    private String title;
    private String content;
    private Timestamp createdAt;
    private User user;
    private List<BlogLike> likes;
    private List<BlogComment> comments;
    private List<BlogPicture> pictures;
    private boolean liked;
    private boolean favorite;

    public Blog() {
    }

    public Blog(int blogID, String title, String content, Timestamp createdAt, User user, List<BlogLike> likes, List<BlogComment> comments, List<BlogPicture> pictures, boolean liked, boolean favorite) {
        this.blogID = blogID;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
        this.user = user;
        this.likes = likes;
        this.comments = comments;
        this.pictures = pictures;
        this.liked = liked;
        this.favorite = favorite;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BlogLike> getLikes() {
        return likes;
    }

    public void setLikes(List<BlogLike> likes) {
        this.likes = likes;
    }

    public List<BlogComment> getComments() {
        return comments;
    }

    public void setComments(List<BlogComment> comments) {
        this.comments = comments;
    }

    public List<BlogPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<BlogPicture> pictures) {
        this.pictures = pictures;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
    
    public List<String> getPictureURLs() {
        List<String> urls = new ArrayList<>();
        for (BlogPicture picture : pictures) {
            urls.add(picture.getPictureURL());
        }
        return urls;
    }

   
}
