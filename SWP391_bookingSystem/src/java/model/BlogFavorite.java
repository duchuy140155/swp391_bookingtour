/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author MSI
 */
public class BlogFavorite {
    private int favoriteID;
    private Timestamp create_at;
    private Blog blog;
    private User user;

    public BlogFavorite() {
    }

    public BlogFavorite(int favoriteID, Timestamp create_at, Blog blog, User user) {
        this.favoriteID = favoriteID;
        this.create_at = create_at;
        this.blog = blog;
        this.user = user;
    }

    public int getFavoriteID() {
        return favoriteID;
    }

    public void setFavoriteID(int favoriteID) {
        this.favoriteID = favoriteID;
    }

    public Timestamp getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Timestamp create_at) {
        this.create_at = create_at;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
