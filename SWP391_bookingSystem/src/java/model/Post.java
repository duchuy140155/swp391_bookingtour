/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author MSI
 */
public class Post {

    private int postId;
    private String postTitle;
    private String postContent;
  
    private Date createAt;
    private int partnerID;
      private String image;

    public Post() {
    }

    public Post(int postId, String postTitle, String postContent, Date createAt, int partnerID) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.createAt = createAt;
        this.partnerID = partnerID;
    }

    public Post(int postId, String postTitle, String postContent, Date createAt, int partnerID, String image) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.createAt = createAt;
        this.partnerID = partnerID;
        this.image = image;
    }

   

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public int getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

}
