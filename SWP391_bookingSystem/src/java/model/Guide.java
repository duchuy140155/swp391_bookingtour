package model;

public class Guide {
    private int guideID;
    private String name;
    private String email;
    private String phoneNumber;
    private String experience;
    private String imageURL;
    private String address;
    private boolean status;
    private String tourType; // Tour Type, nếu cần thiết

    public Guide(int guideID, String name, String email, String phoneNumber, String experience, String imageURL, String address, boolean status, String tourType) {
        this.guideID = guideID;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.experience = experience;
        this.imageURL = imageURL;
        this.address = address;
        this.status = status;
        this.tourType = tourType;
    }

    public Guide(int guideID, String name, String email, String phoneNumber, String experience, String imageURL, String address, boolean status) {
        this(guideID, name, email, phoneNumber, experience, imageURL, address, status, null);
    }

    // Các getter và setter cho tất cả các trường
    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTourType() {
        return tourType;
    }

    public void setTourType(String tourType) {
        this.tourType = tourType;
    }
}
