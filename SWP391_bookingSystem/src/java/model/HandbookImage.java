/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class HandbookImage {

    private int imageID;
    private int handbookID;
    private String handbookImageURL;
    private String imageDescription;
    private String createdAt;

    public HandbookImage() {
    }

    public HandbookImage(int imageID, int handbookID, String handbookImageURL, String imageDescription, String createdAt) {
        this.imageID = imageID;
        this.handbookID = handbookID;
        this.handbookImageURL = handbookImageURL;
        this.imageDescription = imageDescription;
        this.createdAt = createdAt;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public int getHandbookID() {
        return handbookID;
    }

    public void setHandbookID(int handbookID) {
        this.handbookID = handbookID;
    }

    public String getHandbookImageURL() {
        return handbookImageURL;
    }

    public void setHandbookImageURL(String handbookImageURL) {
        this.handbookImageURL = handbookImageURL;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "HandbookImage{" + "imageID=" + imageID + ", handbookID=" + handbookID + ", handbookImageURL=" + handbookImageURL + ", imageDescription=" + imageDescription + ", createdAt=" + createdAt + '}';
    }

}
