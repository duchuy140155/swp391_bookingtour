/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author MSI
 */
public class Partner {

    private int partnerID;
    private String partnerName;
    private String email;
    private String password;
    private int phoneNumber;
    private String address;
    private String certificate;
    private boolean status;
    private int roleID;

    public Partner(int partnerID, String partnerName, String email, String password, int phoneNumber, String address, String certificate, boolean status, int roleID) {
        this.partnerID = partnerID;
        this.partnerName = partnerName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.certificate = certificate;
        this.status = status;
        this.roleID = roleID;
    }

    public Partner() {
    }

    public int getPartnerID() {
        return partnerID;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getCertificate() {
        return certificate;
    }

    public boolean isStatus() {
        return status;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

}
