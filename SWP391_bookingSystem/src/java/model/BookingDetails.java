package model;

import java.sql.Date;
import java.util.List;

public class BookingDetails {

    private int bookingID;
    private double finalPrice;
    private String note;
    private String email;
    private int phoneNumber;
    private String customerAddress;
    private Discount discountID;
    private Tour tour;
    private User userID;
    private List<Ticket> tickets;  
    private int status;
    private Date bookingDate;
    private String customerName;
    private String paymentMethod;
    private int totalPeople;
     private String tourCode;
    public BookingDetails() {
    }

    public BookingDetails(int bookingID, double finalPrice, String note, String email, int phoneNumber, String customerAddress, Discount discountID, Tour tour, User userID, List<Ticket> tickets, int status, Date bookingDate, String customerName, String paymentMethod, String tourName, Date startDate, Date endDate, int totalPeople) {
        this.bookingID = bookingID;
        this.finalPrice = finalPrice;
        this.note = note;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.customerAddress = customerAddress;
        this.discountID = discountID;
        this.tour = tour;
        this.userID = userID;
        this.tickets = tickets;
        this.status = status;
        this.bookingDate = bookingDate;
        this.customerName = customerName;
        this.paymentMethod = paymentMethod;
        this.totalPeople = totalPeople;
    }


    public BookingDetails(double finalPrice, String note, String email, int phoneNumber, String customerAddress, int status, Date bookingDate, String customerName, String paymentMethod, int totalPeople) {
        this.finalPrice = finalPrice;
        this.note = note;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.customerAddress = customerAddress;
        this.status = status;
        this.bookingDate = bookingDate;
        this.customerName = customerName;
        this.paymentMethod = paymentMethod;
        this.totalPeople = totalPeople;
    }

    public BookingDetails(int bookingID, Tour tour, User userID) {
        this.bookingID = bookingID;
        this.tour = tour;
        this.userID = userID;
    }

    public BookingDetails(int bookingID, double finalPrice, Tour tour, int status, Date bookingDate, int totalPeople) {
        this.bookingID = bookingID;
        this.finalPrice = finalPrice;
        this.tour = tour;
        this.status = status;
        this.bookingDate = bookingDate;
        this.totalPeople = totalPeople;
    }

    
    

    // Getter và Setter
    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Discount getDiscountID() {
        return discountID;
    }

    public void setDiscountID(Discount discountID) {
        this.discountID = discountID;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public User getUserID() {
        return userID;
    }

    public void setUserID(User userID) {
        this.userID = userID;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }



    public int getTotalPeople() {
        return totalPeople;
    }

    public void setTotalPeople(int totalPeople) {
        this.totalPeople = totalPeople;
    }

    public void setTourCode(String tourCode) {
        this.tourCode = tourCode;
    }

    public String getTourCode() {
        return tourCode;
    }
    

}
