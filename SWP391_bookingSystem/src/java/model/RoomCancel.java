package model;

public class RoomCancel {
    private int userId;
   
    private int roomId;
    private int roomType;
    private String name;
    private double price;
    private int imageIdRoom;
    private int maxGuests;
    private String description;
    private int numberBeds;
    private boolean status;

    // Constructors, getters, and setters

    public RoomCancel() {
    }

    public RoomCancel(int userId, int roomId, int roomType, String name, double price, int imageIdRoom, int maxGuests, String description, int numberBeds, boolean status) {
        this.userId = userId;
        this.roomId = roomId;
        this.roomType = roomType;
        this.name = name;
        this.price = price;
        this.imageIdRoom = imageIdRoom;
        this.maxGuests = maxGuests;
        this.description = description;
        this.numberBeds = numberBeds;
        this.status = status;
    }

   

    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

  

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getImageIdRoom() {
        return imageIdRoom;
    }

    public void setImageIdRoom(int imageIdRoom) {
        this.imageIdRoom = imageIdRoom;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberBeds() {
        return numberBeds;
    }

    public void setNumberBeds(int numberBeds) {
        this.numberBeds = numberBeds;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

   
}
