/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author MSI
 */
public class TourDetails {

    private Tour tour;
    private List<Schedule> schedules;
    private List<TourImage> images;

    public TourDetails() {
    }

    public TourDetails(Tour tour, List<Schedule> schedules, List<TourImage> images) {
        this.tour = tour;
        this.schedules = schedules;
        this.images = images;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public List<TourImage> getImages() {
        return images;
    }

    public void setImages(List<TourImage> images) {
        this.images = images;
    }

}
