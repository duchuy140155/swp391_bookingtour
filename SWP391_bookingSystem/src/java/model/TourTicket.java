/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class TourTicket {
    private int tourID;
    private int typeID;
    private double priceTicket;
    private TicketType tickettype;
    public TourTicket() {
    }
    
//    public TourTicket(int tourID, int typeID, double priceTicket, String typeName, String typeCode) {
//        this.tourID = tourID;
//        this.typeID = typeID;
//        this.priceTicket = priceTicket;
//        this.typeName = typeName;
//        this.typeCode = typeCode;
//    }

    public TourTicket(int tourID, int typeID, double priceTicket) {
        this.tourID = tourID;
        this.typeID = typeID;
        this.priceTicket = priceTicket;
    }

//    public TourTicket(int tourID, int typeID, double priceTicket, String typeName, String typeCode, int ageMin, int ageMax) {
//        this.tourID = tourID;
//        this.typeID = typeID;
//        this.priceTicket = priceTicket;
//        this.typeName = typeName;
//        this.typeCode = typeCode;
//        this.ageMin = ageMin;
//        this.ageMax = ageMax;
//    }

    public TourTicket(int tourID, int typeID, double priceTicket, TicketType tickettype) {
        this.tourID = tourID;
        this.typeID = typeID;
        this.priceTicket = priceTicket;
        this.tickettype = tickettype;
    }



    public int getTourID() {
        return tourID;
    }

    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public double getPriceTicket() {
        return priceTicket;
    }

    public void setPriceTicket(double priceTicket) {
        this.priceTicket = priceTicket;
    }

    public TicketType getTickettype() {
        return tickettype;
    }

    public void setTickettype(TicketType tickettype) {
        this.tickettype = tickettype;
    }
}
