/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;
import java.sql.Date;

/**
 *
 * @author MSI
 */
public class BlogPicture {
    private int pictureID;
    private String pictureURL;
    private Timestamp createdAt;
    private Blog blog;

    public BlogPicture() {
    }

    public BlogPicture(int pictureID, String pictureURL, Timestamp createdAt, Blog blog) {
        this.pictureID = pictureID;
        this.pictureURL = pictureURL;
        this.createdAt = createdAt;
        this.blog = blog;
    }

    public int getPictureID() {
        return pictureID;
    }

    public void setPictureID(int pictureID) {
        this.pictureID = pictureID;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    
    
}
