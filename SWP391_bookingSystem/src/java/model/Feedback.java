/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author MSI
 */
public class Feedback {

    private int feedbackID;
    private String feedbackContent;
    private User user;
    private Tour tour;
    private Date createDate;
    
    public Feedback() {
    }

//    public Feedback(int feedbackID, String feedbackContent, int UserID, int TourID, Date createDate) {
//        this.feedbackID = feedbackID;
//        this.feedbackContent = feedbackContent;
//        this.UserID = UserID;
//        this.TourID = TourID;
//        this.createDate = createDate;
//    }
//
//    public Feedback(int feedbackID, String feedbackContent, int UserID, int TourID, Date createDate, String userName, String userPicture) {
//        this.feedbackID = feedbackID;
//        this.feedbackContent = feedbackContent;
//        this.UserID = UserID;
//        this.TourID = TourID;
//        this.createDate = createDate;
//        this.userName = userName;
//        this.userPicture = userPicture;
//    }
//

    public Feedback(String feedbackContent, User user, Tour tour, Date createDate) {
        this.feedbackContent = feedbackContent;
        this.user = user;
        this.tour = tour;
        this.createDate = createDate;
    }


    public Feedback(int feedbackID, String feedbackContent, User user, Tour tour, Date createDate) {
        this.feedbackID = feedbackID;
        this.feedbackContent = feedbackContent;
        this.user = user;
        this.tour = tour;
        this.createDate = createDate;
    }

    public Feedback(int feedbackID, String feedbackContent) {
        this.feedbackID = feedbackID;
        this.feedbackContent = feedbackContent;
    }

    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    public String getFeedbackContent() {
        return feedbackContent;
    }

    public void setFeedbackContent(String feedbackContent) {
        this.feedbackContent = feedbackContent;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }


}
