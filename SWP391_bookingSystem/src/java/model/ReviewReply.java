/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author TranT
 */
public class ReviewReply {
    private int replyID;
    private int reviewID;
    private int partnerID;
    private String replyContent;
    private Date replyDate;

    // Constructor, getters, and setters

    public ReviewReply() {
    }

    public ReviewReply(int replyID, int reviewID, int partnerID, String replyContent, Date replyDate) {
        this.replyID = replyID;
        this.reviewID = reviewID;
        this.partnerID = partnerID;
        this.replyContent = replyContent;
        this.replyDate = replyDate;
    }

    public int getReplyID() {
        return replyID;
    }

    public int getReviewID() {
        return reviewID;
    }

    public int getPartnerID() {
        return partnerID;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public Date getReplyDate() {
        return replyDate;
    }

    public void setReplyID(int replyID) {
        this.replyID = replyID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public void setReplyDate(Date replyDate) {
        this.replyDate = replyDate;
    }
    
}
