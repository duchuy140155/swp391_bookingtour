/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author MSI
 */
public class Ticket {
    private int ticketID;
    private int bookingID;
    private TicketType tickettype;
    private String name;
    private boolean gender;
    private Date dob;

    public Ticket() {
    }

    public Ticket(int ticketID, int bookingID, TicketType tickettype, String name, boolean gender, Date dob) {
        this.ticketID = ticketID;
        this.bookingID = bookingID;
        this.tickettype = tickettype;
        this.name = name;
        this.gender = gender;
        this.dob = dob;
    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public TicketType getTickettype() {
        return tickettype;
    }

    public void setTickettype(TicketType tickettype) {
        this.tickettype = tickettype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    
}
