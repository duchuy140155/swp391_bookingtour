package model;

public class ScheduleDetails {

    private int detailID;
    private int scheduleID;
    private String detailTitle;
    private String detailContent;
    private String note; // Thêm biến lưu ý

    public ScheduleDetails(int detailID, int scheduleID, String detailTitle, String detailContent, String note) {
        this.detailID = detailID;
        this.scheduleID = scheduleID;
        this.detailTitle = detailTitle;
        this.detailContent = detailContent;
        this.note = note;
    }

    // Các getter và setter
    public int getDetailID() {
        return detailID;
    }

    public void setDetailID(int detailID) {
        this.detailID = detailID;
    }

    public int getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

    public String getDetailTitle() {
        return detailTitle;
    }

    public void setDetailTitle(String detailTitle) {
        this.detailTitle = detailTitle;
    }

    public String getDetailContent() {
        return detailContent;
    }

    public void setDetailContent(String detailContent) {
        this.detailContent = detailContent;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ScheduleDetails() {
    }

}
