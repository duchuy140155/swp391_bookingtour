<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Create New Hotel</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            max-width: 800px;
            margin: 50px auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
        }
        .top-bar {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 10px 30px;
            background-color: #007bff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
        }
        .top-bar button {
            padding: 10px 20px;
            background-color: #ffffff;
            border: none;
            color: #007bff;
            cursor: pointer;
            margin-left: 10px;
            border-radius: 4px;
            transition: background-color 0.3s ease;
        }
        .top-bar button:hover {
            background-color: #e0e0e0;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin: 10px 0 5px;
            color: #555;
        }
        input[type="text"],
        input[type="email"],
        textarea,
        select {
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
            width: 100%;
            box-sizing: border-box;
            margin-bottom: 15px;
        }
        input[type="submit"] {
            padding: 10px 20px;
            background-color: #28a745;
            border: none;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
        }
        input[type="submit"]:hover {
            background-color: #218838;
        }
        .message {
            text-align: center;
            padding: 10px;
            margin-top: 10px;
            border-radius: 4px;
        }
        .error {
            background-color: #f8d7da;
            color: #721c24;
        }
        .success {
            background-color: #d4edda;
            color: #155724;
        }
        .info {
            background-color: #e7f3fe;
            color: #31708f;
            border: 1px solid #bce8f1;
        }
        .validation-message {
            color: red;
            font-size: 12px;
            display: none;
        }
        #newCityField {
            display: none;
        }
    </style>
</head>
<body>
    <div class="top-bar">
        <button onclick="history.back()">Back</button>
        <button onclick="location.href='partner.jsp'">Back to Partner</button>
    </div>
    <div class="container">
        <h1>Tạo Khách Sạn</h1>
        <div class="message info">
            Việc sửa đổi khách sạn diễn ra phức tạp, yêu cầu kiểm tra kĩ thông tin trước khi ấn nút tạo khách sạn.
        </div>
        <c:if test="${not empty message}">
            <p class="message ${message.startsWith('Failed') ? 'error' : 'success'}">${message}</p>
        </c:if>
        <form action="createhotel" method="post" onsubmit="return confirmSubmit()">
            <label for="name">Tên Khách Sạn:</label>
            <input type="text" id="name" name="name" required minlength="3" maxlength="100" title="Hotel name must be between 3 and 100 characters and cannot have more than 2 consecutive spaces." oninput="validateInput(this)">
            <span class="validation-message" id="nameError">Tên khách sạn không hợp lệ. Không được có nhiều hơn 2 khoảng trắng liên tiếp hoặc chứa ký tự đặc biệt.</span>
            
            <label for="address">Địa Chỉ:</label>
            <textarea id="address" name="address" required minlength="5" maxlength="255" title="Address must be between 5 and 255 characters and cannot have more than 2 consecutive spaces." oninput="validateInput(this)"></textarea>
            <span class="validation-message" id="addressError">Địa chỉ không hợp lệ. Không được có nhiều hơn 2 khoảng trắng liên tiếp hoặc chứa ký tự đặc biệt.</span>
            
            <label for="city">Thành Phố:</label>
            <select id="city" name="city">
                <c:forEach items="${sessionScope.location}" var="p">
                    <option value="${p.locationName}">${p.locationName}</option>
                </c:forEach>
            </select>
            <input type="checkbox" id="newCityCheckbox" name="newCityCheckbox" onchange="toggleNewCityField()"> Bạn muốn thêm thành phố mới
            <textarea id="newCityField" name="newCity" placeholder="Nhập thành phố mới..."></textarea>
            <span class="validation-message" id="cityError">Thành phố không hợp lệ. Không được có nhiều hơn 2 khoảng trắng liên tiếp hoặc chứa ký tự đặc biệt.</span>
            
            <label for="phone">Liên Hệ:</label>
            <input type="text" id="phone" name="phone" required title="Phone number must be exactly 10 digits." oninput="validatePhone(this)">
            <span class="validation-message" id="phoneError">Số điện thoại phải có đúng 10 chữ số.</span>
            
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" required maxlength="100" title="Please enter a valid email address." oninput="validateEmail(this)">
            <span class="validation-message" id="emailError">Email không hợp lệ.</span>
            
            <label for="website">Website:</label>
            <input type="text" id="website" name="website" required minlength="5" maxlength="100" title="Website must be a valid URL." oninput="validateWebsite(this)">
            <span class="validation-message" id="websiteError">Website phải là một URL hợp lệ.</span>
            
            <label for="image">URL Ảnh Khách Sạn:</label>
            <input type="text" id="image" name="image" required minlength="5" maxlength="255" title="Image URL must be a valid URL.">
           
            <input type="submit" value="Tạo Khách Sạn">
        </form>
    </div>

    <script>
        const forbiddenCharacters = /[.,?><;:{}[\]\/\\=+!@#$%^&*()]/;

        function validateInput(input) {
            const value = input.value;
            const errorElement = document.getElementById(input.id + 'Error');
            
            if (forbiddenCharacters.test(value) || value.includes("  ")) {
                errorElement.style.display = 'block';
            } else {
                errorElement.style.display = 'none';
            }
        }

        function validatePhone(input) {
            const value = input.value;
            const errorElement = document.getElementById(input.id + 'Error');
            const phonePattern = /^\d{10}$/;
            
            if (!phonePattern.test(value)) {
                errorElement.style.display = 'block';
            } else {
                errorElement.style.display = 'none';
            }
        }

        function validateEmail(input) {
            const value = input.value;
            const errorElement = document.getElementById(input.id + 'Error');
            const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            
            if (!emailPattern.test(value)) {
                errorElement.style.display = 'block';
            } else {
                errorElement.style.display = 'none';
            }
        }

        function validateWebsite(input) {
            const value = input.value;
            const errorElement = document.getElementById(input.id + 'Error');
            const websitePattern = /^(https?:\/\/)?([\w-]+\.)+[\w-]+(\/[\w-]*)?$/;
            
            if (!websitePattern.test(value)) {
                errorElement.style.display = 'block';
            } else {
                errorElement.style.display = 'none';
            }
        }

        function validateForm() {
            const inputs = document.querySelectorAll('input[required], textarea[required]');
            let isValid = true;
            
            inputs.forEach(input => {
                const value = input.value;
                const errorElement = document.getElementById(input.id + 'Error');
                
                if (input.type === 'email') {
                    validateEmail(input);
                    if (errorElement.style.display === 'block') {
                        isValid = false;
                    }
                } else if (input.id === 'website' || input.id === 'image') {
                    validateWebsite(input);
                    if (errorElement.style.display === 'block') {
                        isValid = false;
                    }
                } else if (input.id === 'phone') {
                    validatePhone(input);
                    if (errorElement.style.display === 'block') {
                        isValid = false;
                    }
                } else {
                    validateInput(input);
                    if (errorElement.style.display === 'block') {
                        isValid = false;
                    }
                }
            });
            
            return isValid;
        }

        function confirmSubmit() {
            if (validateForm()) {
                const checkbox = document.getElementById('newCityCheckbox');
                const citySelect = document.getElementById('city');
                if (checkbox.checked) {
                    citySelect.disabled = true;
                } else {
                    citySelect.disabled = false;
                }
                return confirm("Bạn có chắc chắn muốn tạo khách sạn không?");
            }
            return false;
        }

        function toggleNewCityField() {
            const checkbox = document.getElementById('newCityCheckbox');
            const newCityField = document.getElementById('newCityField');
            const citySelect = document.getElementById('city');

            if (checkbox.checked) {
                newCityField.style.display = 'block';
                citySelect.disabled = true;
            } else {
                newCityField.style.display = 'none';
                citySelect.disabled = false;
            }
        }
    </script>
</body>
</html>
