<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List of Ticket</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                max-width: 90%;
                margin: 50px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h1 {
                text-align: center;
                color: #333;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            table, th, td {
                border: 1px solid #ddd;
            }
            th, td {
                padding: 8px;
                text-align: left;
            }
            th {
                background-color: #f2f2f2;
            }
            img.thumbnail {
                display: block;
                margin: 0 auto;
                max-width: 100px;
                max-height: 100px;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin: 2px;
                font-size: 14px;
                font-weight: normal;
                line-height: 1.428571429;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                border: 1px solid transparent;
                border-radius: 4px;
                text-decoration: none;
            }
            .btn-edit {
                color: #fff;
                background-color: #5bc0de;
                border-color: #46b8da;
            }
            .btn-delete {
                color: #fff;
                background-color: #d9534f;
                border-color: #d43f3a;
            }
            .btn-edit:hover {
                background-color: #31b0d5;
                border-color: #269abc;
            }
            .btn-delete:hover {
                background-color: #c9302c;
                border-color: #ac2925;
            }
            .pagination {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }
            .pagination a {
                color: #333;
                padding: 8px 16px;
                text-decoration: none;
                border: 1px solid #ddd;
                margin: 0 4px;
                border-radius: 4px;
            }
            .pagination a.active {
                background-color: #007bff;
                color: white;
                border: 1px solid #007bff;
            }
            .pagination a:hover:not(.active) {
                background-color: #ddd;
            }
            .search-form {
                text-align: center;
                margin-bottom: 20px;
            }
            .search-input {
                padding: 8px;
                font-size: 16px;
                width: 300px;
            }
            .search-button {
                padding: 8px 16px;
                font-size: 16px;
                cursor: pointer;
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }
            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }
            .dropdown-content a:hover {
                background-color: #f1f1f1;
            }
            .dropdown:hover .dropdown-content {
                display: block;
            }
            .nested-dropdown {
                position: relative;
            }
            .nested-dropdown .nested-dropdown-content {
                display: none;
                position: absolute;
                left: 100%;
                top: 0;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }
            .nested-dropdown:hover .nested-dropdown-content {
                display: block;
            }
            .btn-ticket {
                background-color: #008CBA; /* Blue */
                color: yellow;
            }
            .btn-view {
                background-color: #31b0d5; /* Blue */
                color: yellow;
            }

        </style>
        <script>
            function confirmDeleteTour(typeID) {
                if (confirm('Bạn có chắc chắn muốn xóa vé này không?')) {
                    window.location.href = 'listTicket?action=deleteTicket&typeID=' + typeID;
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="back-button">
                <button onclick="window.location.href = 'partner.jsp'" class="btn">Trở về trang quản lý</button>
            </div>
            <h1>Danh sách các loại vé</h1>
            <div class="search-form">
                <form action="${pageContext.request.contextPath}/listTour" method="get">
                    <input type="text" name="search" class="search-input" placeholder="Tìm kiếm vé..." value="${fn:escapeXml(search)}">
                    <button type="submit" class="search-button">Tìm kiếm</button>
                    <a href="${pageContext.request.contextPath}/addTicket" class="btn btn-ticket">Thêm vé mới</a>

                </form>
            </div>
            <table>
                <tr>
                    <th>Loại vé</th>
                    <th>Mã vé</th>
                    <th>Độ tuổi</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </tr>
                <c:forEach var="ticket" items="${tickets}">
                    <tr>
                        <td>${ticket.typeName}</a></td>
                        <td>${ticket.typeCode}</td>
                        <td>Từ ${ticket.ageMin} đến ${ticket.ageMax}</td>
                        <td>
                            <c:if test="${ticket.isDefault}">
                                Hiển thị lên trang
                            </c:if>
                            <c:if test="${!ticket.isDefault}">
                                Không hiển thị lên trang
                            </c:if>
                       </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/listTicket?action=editTicket&typeID=${ticket.typeID}" class="btn btn-edit">Chỉnh sửa</a>
                            <a href="javascript:void(0);" onclick="confirmDeleteTour(${ticket.typeID})" class="btn btn-delete">Xóa</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
