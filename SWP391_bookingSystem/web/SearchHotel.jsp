<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hotel Search</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Icons CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.2/font/bootstrap-icons.min.css" rel="stylesheet">
    <!-- jQuery UI CSS -->
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .search-container {
            border: 2px solid #ffc107;
            border-radius: 10px;
            padding: 30px;
            background-color: #fff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            margin-top: 50px;
        }
        .search-container .form-control, .search-container .btn {
            border-radius: 5px;
            border: 1px solid #ffc107;
        }
        .search-container .btn {
            background: linear-gradient(to right, #ff9800, #ffc107);
            color: white;
            font-weight: bold;
        }
        .search-container .form-control:focus, .search-container .btn:focus {
            box-shadow: none;
            border-color: #ffc107;
        }
        .search-container .input-group-text {
            background: transparent;
            border: none;
            color: #ffc107;
        }
        .search-container label {
            font-weight: bold;
            color: #333;
        }
        .navbar {
            background: linear-gradient(to right, #ff9800, #ffc107);
        }
        .navbar-brand {
            font-weight: bold;
            color: white;
        }
        .navbar-brand i {
            color: white;
        }
        .navbar-nav .nav-link {
            color: white;
        }
        .navbar-nav .nav-link:hover {
            color: #333;
        }
        .footer {
            background: linear-gradient(to right, #ff9800, #ffc107);
            padding: 40px 0;
            border-top: 1px solid #e9ecef;
            color: white;
            margin-top: 50px;
        }
        .footer a {
            color: white;
            text-decoration: none;
        }
        .footer a:hover {
            color: #333;
            text-decoration: none;
        }
        .footer .social-icons a {
            color: white;
            margin-right: 15px;
            font-size: 20px;
        }
        .footer .social-icons a:hover {
            color: #333;
        }
        .footer .contact-item {
            margin-bottom: 10px;
        }
        .footer .quick-links ul {
            list-style: none;
            padding-left: 0;
        }
        .footer .quick-links ul li {
            margin-bottom: 10px;
        }
        .footer .newsletter {
            background: #333;
            color: white;
            padding: 20px;
            border-radius: 5px;
        }
        .footer .newsletter input {
            border-radius: 5px;
            border: none;
            margin-right: 10px;
            padding: 10px;
        }
        .footer .newsletter button {
            border-radius: 5px;
            border: none;
            padding: 10px 20px;
            background-color: #ffc107;
            color: white;
            font-weight: bold;
        }
        .dropdown-menu {
            padding: 20px;
            width: 300px;
        }
        .counter {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
        }
        .counter span {
            font-size: 18px;
            font-weight: bold;
        }
        .counter button {
            border: none;
            background-color: #ffc107;
            color: white;
            font-size: 18px;
            width: 30px;
            height: 30px;
            border-radius: 50%;
            cursor: pointer;
        }
        .pulse {
            animation: pulse 1s infinite;
            position: relative;
        }
        .pulse::after {
            content: "";
            position: absolute;
            top: -20px;
            right: -20px;
            width: 40px;
            height: 40px;
            background: url('https://upload.wikimedia.org/wikipedia/commons/1/17/Hand_cursor_icon.png') no-repeat center center;
            background-size: contain;
            animation: pulse-hand 1s infinite;
        }
        @keyframes pulse {
            0% {
                transform: scale(1);
            }
            50% {
                transform: scale(1.1);
            }
            100% {
                transform: scale(1);
            }
        }
        @keyframes pulse-hand {
            0% {
                transform: translateY(0);
            }
            50% {
                transform: translateY(-5px);
            }
            100% {
                transform: translateY(0);
            }
        }
    </style>
</head>
<body>
    <!-- Header Start -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="#">
                <i class="bi bi-geo-alt-fill"></i> Travela
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="homepage.jsp">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="historybookroom">Lịch sử đặt phòng</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Tours</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Liên Hệ</a>
                    </li>
                    <c:if test="${sessionScope.account == null || sessionScope.account.roleID != 2}">
                        <li class="nav-item">
                            <a class="nav-link" id="loginLink" href="login.jsp">Login</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header End -->

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-md-8 col-lg-6">
                <div class="search-container">
                  
                    <c:if test="${not empty sessionScope.errorMessage}">
                        <div class="alert alert-danger" role="alert">
                            ${sessionScope.errorMessage}
                            <c:remove var="errorMessage" scope="session"/>
                        </div>
                    </c:if>
                    <form onsubmit="return validateDates()" method="post" action="hotel">
                        <div class="mb-3">
                            <label for="searchOption" class="form-label">Tìm kiếm theo</label>
                            <div>
                                <input type="radio" id="hotelNameOption" name="searchOption" value="hotelName" checked>
                                <label for="hotelNameOption">Nhập tên khách sạn</label>
                                <input type="radio" id="locationOption" name="searchOption" value="location">
                                <label for="locationOption">Nhập địa điểm</label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="searchInput" class="form-label">Tên khách sạn hoặc địa điểm</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-geo-alt"></i></span>
                                <input type="text" class="form-control" id="searchInput" name="destination" placeholder="Nhập tên khách sạn hoặc địa điểm">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="checkin" class="form-label">Ngày đi</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control" id="checkin" name="checkin" placeholder="Chọn ngày đi">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="checkout" class="form-label">Ngày về</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control" id="checkout" name="checkout" placeholder="Chọn ngày về">
                            </div>
                        </div>
                        <div class="mb-3 dropdown">
                            <label for="guests" class="form-label">Số người</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-people"></i></span>
                                <input type="text" class="form-control" id="guests" name="guests" placeholder="1 Người lớn" readonly data-bs-toggle="dropdown">
                                <ul class="dropdown-menu">
                                    <div class="counter">
                                        <span>Người lớn</span>
                                        <div>
                                            <button type="button" onclick="decrement('adults')">-</button>
                                            <span id="adults">1</span>
                                            <button type="button" onclick="increment('adults')">+</button>
                                        </div>
                                    </div>
                                    <div class="counter">
                                        <span>Trẻ em</span>
                                        <div>
                                            <button type="button" onclick="decrement('children')">-</button>
                                            <span id="children">0</span>
                                            <button type="button" onclick="increment('children')">+</button>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="mb-3 dropdown">
                            <label for="rooms" class="form-label">Số phòng</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-door-open"></i></span>
                                <input type="text" class="form-control" id="rooms" name="rooms" placeholder="1 phòng" readonly data-bs-toggle="dropdown">
                                <ul class="dropdown-menu">
                                    <div class="counter">
                                        <span>Phòng</span>
                                        <div>
                                            <button type="button" onclick="decrement('roomCount')">-</button>
                                            <span id="roomCount">1</span>
                                            <button type="button" onclick="increment('roomCount')">+</button>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h5>Contact Us</h5>
                    <p class="contact-item"><i class="bi bi-geo-alt-fill"></i> 123 Street, New York, USA</p>
                    <p class="contact-item"><i class="bi bi-envelope-fill"></i> info@example.com</p>
                    <p class="contact-item"><i class="bi bi-phone-fill"></i> +012 345 67890</p>
                </div>
                <div class="col-md-4 quick-links">
                    <h5>Quick Links</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Destinations</a></li>
                        <li><a href="#">Tours</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>Follow Us</h5>
                    <div class="social-icons">
                        <a href="#"><i class="bi bi-facebook"></i></a>
                        <a href="#"><i class="bi bi-twitter"></i></a>
                        <a href="#"><i class="bi bi-instagram"></i></a>
                        <a href="#"><i class="bi bi-linkedin"></i></a>
                    </div>
                    <h5 class="mt-4">Newsletter</h5>
                    <div class="newsletter">
                        <input type="email" placeholder="Your Email">
                        <button type="submit">Subscribe</button>
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <p>&copy; 2024 Travela. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <!-- Footer End -->

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <!-- jQuery UI JS -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            var today = new Date();
            $("#checkin, #checkout").datepicker({
                dateFormat: "dd/mm/yy",
                minDate: today // Ngày không thể dưới ngày hiện tại
            });

            $(".dropdown-menu").click(function (e) {
                e.stopPropagation();
            });

            <% if (session.getAttribute("errorMessage") != null) { %>
                $("#loginLink").addClass("pulse");
            <% } %>

            updateGuests();
            updateRooms();
        });

        function increment(id) {
            var element = document.getElementById(id);
            var value = parseInt(element.innerText);
            element.innerText = value + 1;
            updateGuests();
            updateRooms();
        }

        function decrement(id) {
            var element = document.getElementById(id);
            var value = parseInt(element.innerText);
            if (value > 0) {
                element.innerText = value - 1;
            }
            updateGuests();
            updateRooms();
        }

        function updateGuests() {
            var adults = document.getElementById("adults").innerText;
            var children = document.getElementById("children").innerText;
            document.getElementById("guests").value = adults + " Người lớn, " + children + " Trẻ em";
        }

        function updateRooms() {
            var roomCount = document.getElementById("roomCount").innerText;
            document.getElementById("rooms").value = roomCount + " phòng";
        }

        function validateDates() {
            var checkin = $("#checkin").datepicker("getDate");
            var checkout = $("#checkout").datepicker("getDate");
            if (!checkin || !checkout) {
                alert("Please select both check-in and check-out dates.");
                return false;
            }
            var sixMonthsLater = new Date(checkin);
            sixMonthsLater.setMonth(sixMonthsLater.getMonth() + 6);
            if (checkout > sixMonthsLater) {
                alert("The check-out date cannot be more than 6 months after the check-in date.");
                return false;
            }
            return true;
        }
    </script>
</body>
</html>
