<%-- 
    Document   : createHandbook
    Created on : May 25, 2024, 4:37:47 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Create Handbook</title>
    </head>
    <body>
        <h2>Create New Handbook</h2>
        <form action="${pageContext.request.contextPath}/manageHandbook?action=create" method="post" enctype="multipart/form-data">
            <label for="title">Title:</label>
            <input type="text" id="title" name="title" required><br>

            <label for="shortContent">Short Content:</label>
            <textarea id="shortContent" name="shortContent" required></textarea><br>

            <label for="imageTheme">Image Theme:</label>
            <input type="file" id="imageTheme" name="imageTheme" required><br>

            <button type="submit">Create</button>
        </form>
        <a href="${pageContext.request.contextPath}/manageHandbook?action=list">Back to List</a>
    </body>
</html>
