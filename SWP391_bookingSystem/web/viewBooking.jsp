<%-- 
    Document   : viewBooking
    Created on : Jun 11, 2024, 11:45:33 PM
    Author     : Admin
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thông tin Đặt Tour</title>
        <link href="css/viewBooking.css" rel="stylesheet" type="text/css"/>
        <script src="js/viewBooking.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <h2>Thông tin Đặt Tour</h2>
            <a href="listTour" class="btn btn-back">Trở về</a>
            <form class="search-form" action="viewBooking" method="GET">
                <input type="hidden" name="tourID" value="${tourID}">
                <input type="text" name="search" placeholder="Tìm kiếm..." value="${search}">
                <select name="sortStatus">
                    <option value="all" ${sortStatus == 'all' ? 'selected' : ''}>Tất cả</option>
                    <option value="approved" ${sortStatus == 'approved' ? 'selected' : ''}>Đã duyệt</option>
                    <option value="rejected" ${sortStatus == 'rejected' ? 'selected' : ''}>Chưa duyệt</option>
                </select>
               &nbsp;&nbsp;&nbsp; <button type="submit">Tìm kiếm</button>
            </form>
            <c:forEach var="booking" items="${bookings}">
                <div class="booking-details">
                    <div class="left-info">
                        <h3>Thông tin khách hàng</h3>
                        <p><strong>Họ và Tên:</strong> ${booking.customerName}</p>
                        <p><strong>Email:</strong> ${booking.email}</p>
                        <p><strong>Số điện thoại:</strong> ${booking.phoneNumber}</p>
                        <p><strong>Ghi chú:</strong> ${booking.note}</p>
                        <p><strong>Trạng thái:</strong> <c:choose>
                                <c:when test="${booking.status == 1}">Đã duyệt</c:when>
                                <c:otherwise>Chưa duyệt</c:otherwise>
                            </c:choose></p>
                    </div>
                    <div class="right-actions">
                        <button class="btn-edit" onclick="window.location.href = 'editBooking?bookingID=${booking.bookingID}'">Sửa thông tin</button>
                        <button class="btn-details" onclick="window.location.href = 'viewBookingDetails?bookingID=${booking.bookingID}'">Xem chi tiết</button>
                        <div class="checkbox-group">
                            <label><input type="radio" name="status${booking.bookingID}" value="1" onclick="toggleCheckbox(this)" ${booking.status == 1 ? 'disabled' : ''}> Duyệt</label>
                            <label><input type="radio" name="status${booking.bookingID}" value="0" onclick="toggleCheckbox(this)" ${booking.status == 0 ? 'disabled' : ''}> Không Duyệt</label>
                        </div>
                        <button class="btn-confirm" onclick="confirmApproval(${booking.bookingID}, ${tourID})">Xác nhận</button>
                    </div>
                </div>
            </c:forEach>
            <div class="pagination">
                <c:if test="${currentPage > 1}">
                    <a href="viewBooking?tourID=${tourID}&page=1&search=${search}&sortStatus=${sortStatus}">&laquo; Trang đầu</a>
                    <a href="viewBooking?tourID=${tourID}&page=${currentPage - 1}&search=${search}&sortStatus=${sortStatus}">&lsaquo; Trang trước</a>
                </c:if>
                <c:forEach begin="1" end="${totalPages}" var="i">
                    <a href="viewBooking?tourID=${tourID}&page=${i}&search=${search}&sortStatus=${sortStatus}" class="${i == currentPage ? 'active' : ''}">${i}</a>
                </c:forEach>
                <c:if test="${currentPage < totalPages}">
                    <a href="viewBooking?tourID=${tourID}&page=${currentPage + 1}&search=${search}&sortStatus=${sortStatus}">Trang sau &rsaquo;</a>
                    <a href="viewBooking?tourID=${tourID}&page=${totalPages}&search=${search}&sortStatus=${sortStatus}">Trang cuối &raquo;</a>
                </c:if>
            </div>
            <form id="approvalForm" method="post" action="viewBooking" style="display:none;">
                <input type="hidden" name="bookingID" id="bookingID">
                <input type="hidden" name="status" id="status">
                <input type="hidden" name="tourID" id="tourID">
                <input type="hidden" name="search" value="${search}">
                <input type="hidden" name="sortStatus" value="${sortStatus}">
            </form>
        </div>
    </body>
</html>

