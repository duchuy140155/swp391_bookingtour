<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit Ticket</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                max-width: 600px;
                margin: 50px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h1 {
                text-align: center;
                color: #333;
            }
            form {
                display: flex;
                flex-direction: column;
            }
            label {
                margin: 10px 0 5px;
                font-weight: bold;
            }
            input, select {
                padding: 8px;
                margin-bottom: 10px;
                border: 1px solid #ddd;
                border-radius: 4px;
                font-size: 16px;
            }
            .btn {
                display: inline-block;
                padding: 10px;
                margin: 10px 0;
                font-size: 16px;
                text-align: center;
                cursor: pointer;
                border: 1px solid transparent;
                border-radius: 4px;
                text-decoration: none;
            }
            .btn-save {
                color: #fff;
                background-color: #5cb85c;
                border-color: #4cae4c;
            }
            .btn-cancel {
                color: #fff;
                background-color: #d9534f;
                border-color: #d43f3a;
            }
            .btn-save:hover {
                background-color: #4cae4c;
                border-color: #398439;
            }
            .btn-cancel:hover {
                background-color: #c9302c;
                border-color: #ac2925;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <button type="button" name="back" onclick="history.back()" class="btn btn-secondary">Trở về</button>
            <h1>Edit Ticket</h1>
            <form action="listTicket" method="post">
                <input type="hidden" name="action" value="updateTicket">
                <input type="hidden" name="typeID" value="${ticket.typeID}">

                <label for="typeName">Loại vé</label>
                <input type="text" id="typeName" name="typeName" value="${ticket.typeName}" required>

                <label for="typeCode">Mã vé</label>
                <input type="text" id="typeCode" name="typeCode" value="${ticket.typeCode}" required>

                <label for="ageMin">Độ tuổi tối thiểu</label>
                <input type="number" id="ageMin" name="ageMin" value="${ticket.ageMin}" required>

                <label for="ageMax">Độ tuổi tối đa</label>
                <input type="number" id="ageMax" name="ageMax" value="${ticket.ageMax}" required>

                <label for="isDefault">Trạng thái</label>
                <select id="isDefault" name="isDefault" required>
                    <option value="true" ${ticket.isDefault == 'true' ? 'selected' : ''}>Hiển thị lên trang</option>
                    <option value="false" ${ticket.isDefault == 'false' ? 'selected' : ''}>Không hiển thị lên trang</option>
                </select>

                <button type="submit" class="btn btn-save">Lưu</button>
            </form>
        </div>
    </body>
</html>
