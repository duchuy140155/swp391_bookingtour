<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Đăng Ký</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            background: #f3f4f6;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .form-signup {
            background: #fff;
            padding: 2rem;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0,0,0,0.1);
            width: 100%;
            max-width: 400px;
        }

        .form-signup p {
            color: red;
        }

        .form-signup label {
            margin-top: 1rem;
            display: block;
            font-weight: bold;
            color: #333;
        }

        .form-styling {
            width: 100%;
            padding: 0.8rem;
            margin-top: 0.5rem;
            border: 1px solid #ddd;
            border-radius: 5px;
            transition: border-color 0.3s;
        }

        .form-styling:focus {
            border-color: #007BFF;
            outline: none;
        }

        .btn-signup {
            width: 100%;
            padding: 0.8rem;
            margin-top: 1rem;
            background: #007BFF;
            color: #fff;
            border: none;
            border-radius: 5px;
            font-size: 1rem;
            cursor: pointer;
            transition: background 0.3s;
        }

        .btn-signup:hover {
            background: #0056b3;
        }

        .success {
            display: none;
            justify-content: center;
            align-items: center;
            margin-top: 2rem;
        }

        .success svg {
            fill: #28a745;
        }
    </style>
</head>
<body>

<form class="form-signup" action="register" method="post" name="form" onsubmit="return validateForm()">
    <p class="text-danger">${abc}</p>
    <label for="userName">Họ và tên</label>
    <input class="form-styling" type="text" name="userName" placeholder="" required minlength="10" maxlength="30" />

    <label for="email">Email</label>
    <input class="form-styling" type="email" name="email" placeholder="" required minlength="10" maxlength="30" />

    <label for="password">Mật khẩu</label>
    <input class="form-styling" type="password" name="password" placeholder="" required />

    <label for="userDOB">Ngày sinh</label>
    <input class="form-styling" type="date" name="userDOB" placeholder="" required />

    <label for="phoneNumber">Số điện thoại</label>
    <input class="form-styling" type="text" name="phoneNumber" placeholder="" required />

    <label for="address">Địa chỉ</label>
    <input class="form-styling" type="text" name="address" placeholder="" required minlength="10" maxlength="30" />

    <button type="submit" class="btn-signup">Đăng kí</button>
</form>

<!--<div class="success">
    <svg width="270" height="270" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 60 60" id="check" ng-class="checked ? 'checked' : ''">
    <path fill="#ffffff" d="M40.61,23.03L26.67,36.97L13.495,23.788c-1.146-1.147-1.359-2.936-0.504-4.314
          c3.894-6.28,11.169-10.243,19.283-9.348c9.258,1.021,16.694,8.542,17.622,17.81c1.232,12.295-8.683,22.607-20.849,22.042
          c-9.9-0.46-18.128-8.344-18.972-18.218c-0.292-3.416,0.276-6.673,1.51-9.578" />
</div>-->

<script>
    function validateForm() {
        const email = document.forms["form"]["email"].value;
        if (!email.endsWith("@gmail.com")) {
            alert("Email must be in the form @gmail.com");
            return false;
        }

        const userName = document.forms["form"]["userName"].value;
        if (userName.length < 10 || userName.length > 30) {
            alert("Họ và tên must be between 10 and 30 characters.");
            return false;
        }

        const address = document.forms["form"]["address"].value;
        if (address.length < 10 || address.length > 30) {
            alert("Địa chỉ must be between 10 and 30 characters.");
            return false;
        }

        const phoneNumber = document.forms["form"]["phoneNumber"].value;
        const phonePattern = /^\d{10}$/;
        if (!phonePattern.test(phoneNumber)) {
            alert("Phone number must be exactly 10 digits long and contain only numbers.");
            return false;
        }

        const password = document.forms["form"]["password"].value;
        const passwordPattern = /^(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
        if (!passwordPattern.test(password)) {
            alert("Mật khẩu must be at least 8 characters long and contain at least one special character, one digit, and one lowercase letter.");
            return false;
        }

        return validateDOB();
    }

    function validateDOB() {
        const userDOB = new Date(document.forms["form"]["userDOB"].value);
        const today = new Date();
        if (userDOB > today) {
            alert("Ngày sinh cannot be in the future.");
            return false;
        }

        let age = today.getFullYear() - userDOB.getFullYear();
        const monthDiff = today.getMonth() - userDOB.getMonth();
        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < userDOB.getDate())) {
            age--;
        }

        if (age < 18) {
            alert("You must be at least 18 years old.");
            return false;
        }

        if (age > 100) {
            alert("You cannot be older than 100 years.");
            return false;
        }

        return true;
    }
</script>

</body>
</html>
