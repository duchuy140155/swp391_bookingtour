document.addEventListener('DOMContentLoaded', function () {
    var customerListCheckbox = document.getElementById('customerListCheckbox');
    if (customerListCheckbox && customerListCheckbox.checked) {
        listTickets.forEach(ticket => {
            var initialCount = ticket.typeID === '1' ? 1 : 0;
            for (var i = 1; i <= initialCount; i++) {
                addPassengerForm(ticket.typeID, ticket.typeName, i);
            }
        });
        updateTotalPeople();
    }

    var bookingForm = document.getElementById('bookingForm');
    if (bookingForm) {
        bookingForm.addEventListener('submit', function (event) {
            updateTotalPeople();
        });
    }

    // Close modal
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});

function addPassengerForm(typeID, typeName, number) {
    var container = document.getElementById('infantDetails');
    var formGroup = passengerForms[typeID];

    if (!formGroup) {
        formGroup = document.createElement('div');
        formGroup.setAttribute('id', typeID + 'Forms');
        formGroup.classList.add('form-group');
        formGroup.innerHTML = `<h4>${typeName}</h4>`;
        container.appendChild(formGroup);
        passengerForms[typeID] = formGroup;
    }

    var form = document.createElement('div');
    form.setAttribute('id', typeID + 'Form' + number);
    form.classList.add('passenger-form');
    form.innerHTML = `
        <div class="passenger-info form-group-horizontal">
            <div class="form-group">
                <label>Họ và tên:</label>
                <input type="text" name="passenger[${typeID}][${number}][name]" required placeholder="Nhập họ tên" pattern="[A-Za-zÀ-ỹà-ỹ\\s]+" title="Please enter a valid name">
            </div>
            <div class="form-group">
                <label>Giới tính:</label>
                <select name="passenger[${typeID}][${number}][gender]" required>
                    <option value="male">Nam</option>
                    <option value="female">Nữ</option>
                </select>
            </div>
            <div class="form-group">
                <label>Ngày sinh:</label>
                <div style="display: flex;">
                    <select id="dob-day-${typeID}-${number}" name="passenger[${typeID}][${number}][dob-day]" required>
                        <option value="">Ngày</option>
                    </select>
                    <select id="dob-month-${typeID}-${number}" name="passenger[${typeID}][${number}][dob-month]" required>
                        <option value="">Tháng</option>
                    </select>
                    <select id="dob-year-${typeID}-${number}" name="passenger[${typeID}][${number}][dob-year]" required>
                        <option value="">Năm</option>
                    </select>
                </div>
            </div>
        </div>
    `;
    formGroup.appendChild(form);

    // Populate day, month, and year options for new form
    populateDayMonthYearOptions(typeID, number);
}

function populateDayMonthYearOptions(typeID, number) {
    populateOptions(`dob-day-${typeID}-${number}`, 1, 31);
    populateOptions(`dob-month-${typeID}-${number}`, 1, 12);
    populateYearOptions(`dob-year-${typeID}-${number}`, typeID);
}

function populateOptions(elementId, start, end) {
    var select = document.getElementById(elementId);
    for (var i = start; i <= end; i++) {
        var option = document.createElement('option');
        option.value = i;
        option.text = i < 10 ? '0' + i : i;
        select.appendChild(option);
    }
}

function populateYearOptions(elementId, typeID) {
    var yearSelect = document.getElementById(elementId);
    var currentYear = new Date().getFullYear();

    var ticket = listTickets.find(t => t.typeID == typeID);
    if (ticket) {
        var minAge = ticket.ageMin;
        var maxAge = ticket.ageMax || currentYear;

        var minYear = currentYear - maxAge;
        var maxYear = currentYear - minAge;

        populateOptions(elementId, minYear, maxYear);
    }
}

function removePassengerForm(typeID, number) {
    var form = document.getElementById(typeID + 'Form' + number);
    if (form) {
        form.remove();
        var formGroup = passengerForms[typeID];
        if (formGroup && formGroup.children.length === 1) {
            formGroup.remove();
            delete passengerForms[typeID];
        }
    }
}

function increment(typeID, typeName) {
    var totalCount = getTotalCount();
    if (totalCount >= remainsTicket) {
        showModal("Số khách tối đa :" + remainsTicket);
        return;
    }

    var count = document.getElementById(typeID + 'Count');
    var value = parseInt(count.value);
    count.value = value + 1;
    updateTotalPeople();
    addPassengerForm(typeID, typeName, value + 1);
}

function decrement(typeID, typeName) {
    var count = document.getElementById(typeID + 'Count');
    var value = parseInt(count.value);
    if (typeID == 1 && value <= 1) {
        return;
    }
    if (value > 1 || (typeID != 1 && value > 0)) {
        count.value = value - 1;
        updateTotalPeople();
        removePassengerForm(typeID, value);
    }
}

function updateTotalPeople() {
    var totalPeople = '';
    var totalCount = 0;
    var totalPrice = 0;
    var priceDetails = '';
    var typeIDs = [];

    listTickets.forEach(ticket => {
        var countElement = document.getElementById(ticket.typeID + 'Count');
        if (countElement) {
            var count = parseInt(countElement.value);
            if (count > 0) {
                totalPeople += count + ' ' + ticket.typeName + '<br>';
                totalCount += count;
                var price = parseFloat(ticket.priceTicket);
                totalPrice += count * price;
                priceDetails += `${count} x ${price.toLocaleString()}, <br>`;
                typeIDs.push(ticket.typeID);
            }
        }
    });

    var totalPeopleElement = document.getElementById('totalPeople');
    var totalPriceDisplayElement = document.getElementById('totalPriceDisplay');
    var hiddenTotalPeopleElement = document.getElementById('hiddenTotalPeople');
    var hiddenTotalPriceElement = document.getElementById('hiddenTotalPrice');
    var hiddenTypeIDsElement = document.getElementById('hiddenTypeIDs');

    if (totalPeopleElement) totalPeopleElement.innerHTML = `Tổng số người: ${totalCount} gồm có:<br> ${totalPeople}`;
    if (totalPriceDisplayElement) totalPriceDisplayElement.innerHTML = `${priceDetails.slice(0, -6)}<br> Tổng: ${totalPrice.toLocaleString()}`;

    if (hiddenTotalPeopleElement) hiddenTotalPeopleElement.value = totalCount;
    if (hiddenTotalPriceElement) hiddenTotalPriceElement.value = totalPrice;
    if (hiddenTypeIDsElement) hiddenTypeIDsElement.value = typeIDs.join(',');
}

function toggleForm() {
    var consultationCheckbox = document.getElementById('consultationCheckbox');
    var priceDisplay = document.getElementById('priceDisplay');

    if (consultationCheckbox && consultationCheckbox.checked) {
        if (priceDisplay) {
            priceDisplay.style.display = 'none';
        }
        document.getElementById('totalPeople').style.display = 'none';
        document.getElementById('totalPriceDisplay').style.display = 'none';
    } else {
        updateTotalPeople();
        document.getElementById('totalPeople').style.display = 'block';
        document.getElementById('totalPriceDisplay').style.display = 'block';
        if (priceDisplay) {
            priceDisplay.style.display = 'block';
        }
    }

    var passengerSection = document.getElementById('passengerSection');
    var consultationNote = document.getElementById('consultationNote');
    if (consultationCheckbox && consultationCheckbox.checked) {
        if (passengerSection) {
            passengerSection.style.display = 'none';
        }
        if (consultationNote) {
            consultationNote.style.display = 'block';
        }

        // Remove required attribute from passenger fields
        document.querySelectorAll('#passengerSection input, #passengerSection select').forEach(function (element) {
            element.removeAttribute('required');
        });
    } else {
        if (passengerSection) {
            passengerSection.style.display = 'block';
        }
        if (consultationNote) {
            consultationNote.style.display = 'none';
        }

        // Add required attribute to passenger fields
        document.querySelectorAll('#passengerSection input, #passengerSection select').forEach(function (element) {
            element.setAttribute('required', 'required');
        });
    }
}

function getTotalCount() {
    var totalCount = 0;
    listTickets.forEach(ticket => {
        var countElement = document.getElementById(ticket.typeID + 'Count');
        if (countElement) {
            var count = parseInt(countElement.value);
            totalCount += count;
        }
    });
    return totalCount;
}

function showModal(message) {
    var modal = document.getElementById("myModal");
    var modalText = document.getElementById("modalText");
    if (modalText) modalText.innerHTML = message;
    if (modal) modal.style.display = "block";
}

var passengerForms = {};
