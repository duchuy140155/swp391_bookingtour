/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function toggleCheckbox(checkbox) {
    const form = checkbox.form;
    const checkboxes = form.querySelectorAll('input[type="radio"]');
    checkboxes.forEach(cb => {
        if (cb !== checkbox) {
            cb.checked = false;
        }
    });
}

function confirmApproval(bookingID, tourID) {
    const approvedRadio = document.querySelector(`input[name="status${bookingID}"][value="1"]`);
    const rejectedRadio = document.querySelector(`input[name="status${bookingID}"][value="0"]`);

    if (approvedRadio && approvedRadio.checked) {
        if (confirm("Bạn có chắc chắn duyệt tour này không?")) {
            document.getElementById('bookingID').value = bookingID;
            document.getElementById('status').value = 1;
            document.getElementById('tourID').value = tourID;
            document.getElementById('approvalForm').submit();
        }
    } else if (rejectedRadio && rejectedRadio.checked) {
        if (confirm("Bạn có chắc chắn không duyệt tour này không?")) {
            document.getElementById('bookingID').value = bookingID;
            document.getElementById('status').value = 0;
            document.getElementById('tourID').value = tourID;
            document.getElementById('approvalForm').submit();
        }
    }
}

// Function to send email when the tour is about to start
async function sendTourStartNotification(tourID) {
    try {
        const response = await fetch('sendTourStartNotification', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: `tourID=${tourID}`
        });
        if (response.ok) {
            alert("Email thông báo đã được gửi thành công!");
        } else {
            console.error('Failed to send notification email');
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

// Function to check for tours about to start and send notifications
async function checkAndNotifyUpcomingTours() {
    try {
        const response = await fetch('getUpcomingTours');
        if (response.ok) {
            const upcomingTours = await response.json();
            upcomingTours.forEach(tour => {
                sendTourStartNotification(tour.tourID);
            });
        } else {
            console.error('Failed to fetch upcoming tours');
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

// Schedule the checkAndNotifyUpcomingTours function to run periodically
setInterval(checkAndNotifyUpcomingTours, 86400000); // Check every 24 hours


