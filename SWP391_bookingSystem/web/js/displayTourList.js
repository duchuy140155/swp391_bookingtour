/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function initLocations() {
    const locations = [
        "Hà Nội", "TP Hồ Chí Minh", "An Giang", "Bà Rịa – Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh",
        "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau",
        "Cần Thơ", "Cao Bằng", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai",
        "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương",
        "Hải Phòng", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang",
        "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định",
        "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên", "Quảng Bình",
        "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La",
        "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang",
        "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái"
    ];

    const startLocationSelect = document.getElementById('start-location');
    const endLocationSelect = document.getElementById('end-location');

    // Thêm tùy chọn "Không rõ"
    const unknownOption1 = document.createElement('option');
    unknownOption1.value = "";
    unknownOption1.text = "Không rõ";
    startLocationSelect.add(unknownOption1, startLocationSelect.options[0]);

    const unknownOption2 = document.createElement('option');
    unknownOption2.value = "";
    unknownOption2.text = "Không rõ";
    endLocationSelect.add(unknownOption2, endLocationSelect.options[0]);

    locations.forEach(location => {
        const option1 = document.createElement('option');
        option1.value = location;
        option1.text = location;
        startLocationSelect.add(option1);

        const option2 = document.createElement('option');
        option2.value = location;
        option2.text = location;
        endLocationSelect.add(option2);
    });
}

// Gọi hàm khởi tạo khi tài liệu đã sẵn sàng
document.addEventListener('DOMContentLoaded', initLocations);

// Cập nhật ngân sách
document.getElementById('budget').addEventListener('input', function () {
    document.getElementById('budget-amount').innerText = parseInt(this.value).toLocaleString('vi-VN') + ' VND';
});

// Lưu trạng thái của các bộ lọc sau khi tìm kiếm
document.addEventListener('DOMContentLoaded', function () {
    const urlParams = new URLSearchParams(window.location.search);
    const daysFilter = urlParams.get('daysFilter');
    if (daysFilter) {
        document.getElementById('days-filter').value = daysFilter;
    }
});
