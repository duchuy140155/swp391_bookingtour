/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function showCashMessage() {
    var paymentMethods = document.getElementsByName('paymentMethod');
    var messageDiv = document.getElementById('cashMessage');
    for (var i = 0; i < paymentMethods.length; i++) {
        if (paymentMethods[i].checked && paymentMethods[i].value === 'cash') {
            messageDiv.style.display = 'block';
        } else {
            messageDiv.style.display = 'none';
        }
    }
}
