<%-- 
    Document   : editProfile
    Created on : May 27, 2024, 9:00:07 PM
    Author     : Admin
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Profile</title>
        <link href="css/editProfile.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="back-button-container">
            <button onclick="window.location.href = 'viewProfile'" class="back-btn">Back to profile</button>
        </div>
        <div class="container">
            <div class="sidebar">
                <nav>
                    <ul>
                        <li><a href="viewProfile">Thông tin cá nhân</a></li>
                        <li><a href="#">Đổi mật khẩu</a></li>
                        <li><a href="#">Thông tin thanh toán</a></li>
                        <li><a href="#">Yêu cầu xóa tài khoản</a></li>
                    </ul>
                </nav>
            </div>
            <div class="main-content">
                <h2>Edit Profile</h2>
                <div class="profile-header">
                    <div class="profile-picture">
                        <img src="${user.userPicture}" alt="User Picture">
                    </div>
                    <h1>${user.userName}</h1>
                    <p>${user.email}</p>
                </div>
                <div class="profile-content">
                    <form action="editProfile" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="field" value="${param.field}">
                        <table>
                            <c:choose>
                                <c:when test="${param.field == 'userName'}">
                                    <tr>
                                        <th>Họ và Tên</th>
                                        <td><input type="text" name="userName" value="${user.userName}"></td>
                                    </tr>
                                </c:when>
                                <c:when test="${param.field == 'userDOB'}">
                                    <tr>
                                        <th>DOB</th>
                                        <td><input type="date" name="userDOB" value="${user.userDOB}"></td>
                                    </tr>
                                </c:when>
                                <c:when test="${param.field == 'phoneNumber'}">
                                    <tr>
                                        <th>Số điện thoại</th>
                                        <td><input type="text" name="phoneNumber" value="${user.phoneNumber}"></td>
                                    </tr>
                                </c:when>
                                <c:when test="${param.field == 'address'}">
                                    <tr>
                                        <th>Địa chỉ</th>
                                        <td><input type="text" name="address" value="${user.address}"></td>
                                    </tr>
                                </c:when>
                                <c:when test="${param.field == 'userPicture'}">
                                    <tr>
                                        <th>Ảnh</th>
                                        <td><input type="file" name="userPicture"></td>
                                    </tr>
                                </c:when>
                            </c:choose>
                        </table>
                        <button type="submit" class="save-btn">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
