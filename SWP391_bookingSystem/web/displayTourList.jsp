<%-- 
    Document   : displayTourList
    Created on : Jul 12, 2024, 11:18:36 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tour List</title>
        <link href="css/displayTourList.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <h1>Danh sách các tour có sẵn</h1>
            <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
            <div class="content">
                <!-- Form bên trái: Bộ lọc -->
                <div class="filter-form">
                    <h3>Bộ lọc</h3>
                    <form action="displayTour" method="GET">
                        <input type="hidden" name="action" value="filter">
                        <!-- Điểm đi -->
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                            </div>
                            <select id="start-location" name="startLocation" class="form-control">
                                <option value="" ${startLocation == null ? "selected" : ""}>Không rõ</option>
                                <!-- Các tỉnh/thành phố sẽ được thêm vào đây -->
                                <c:forEach var="location" items="${locations}">
                                    <option value="${location}" ${location == startLocation ? "selected" : ""}>${location}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <!-- Điểm đến -->
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                            </div>
                            <select id="end-location" name="endLocation" class="form-control">
                                <option value="" ${endLocation == null ? "selected" : ""}>Không rõ</option>
                                <!-- Các tỉnh/thành phố sẽ được thêm vào đây -->
                                <c:forEach var="location" items="${locations}">
                                    <option value="${location}" ${location == endLocation ? "selected" : ""}>${location}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <!-- Số ngày -->
                        <div class="input-group">
                            <h4>Số ngày</h4>
                            <select id="days-filter" name="daysFilter" class="form-control">
                                <option value="" disabled ${daysFilter == null ? "selected" : ""}>Chọn số ngày</option>
                                <option value="1-3" ${daysFilter == "1-3" ? "selected" : ""}>1-3 ngày</option>
                                <option value="4-7" ${daysFilter == "4-7" ? "selected" : ""}>4-7 ngày</option>
                                <option value="8-15" ${daysFilter == "8-15" ? "selected" : ""}>8-15 ngày</option>
                                <option value=">15" ${daysFilter == ">15" ? "selected" : ""}>> 15 ngày</option>
                            </select>
                        </div>

                        <!-- Ngày đi -->
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-calendar-alt"></i>
                                    <span id="current-date-text">${currentDate}</span>
                                </span>
                            </div>
                            <input type="date" name="startDate" class="form-control" value="${startDate}">
                        </div>

                        <!-- Ngân sách -->
                        <div class="input-group">
                            <h4>Ngân sách của quý khách</h4>
                            <input type="range" id="budget" name="budget" min="0" max="100000000" value="${budget}" class="slider">
                            <p>Ngân sách: <span id="budget-amount"><fmt:formatNumber value="${budget}" type="number" minFractionDigits="0" maxFractionDigits="0"/> VND</span></p>
                        </div>

                        <!-- Nút tìm kiếm -->
                        <button type="submit" class="btn btn-search"><i class="fas fa-search"></i> Tìm kiếm</button>
                    </form>
                </div>

                <!-- Form bên phải: Danh sách tour -->
                <div class="tour-list">
                    <c:if test="${not empty message}">
                        <div class="message" style="text-align: center; font-size: 20px; color: red;">
                            ${message}
                        </div>
                    </c:if>
                    <c:forEach var="tour" items="${tourList}">
                        <div class="tour-card">
                            <img src="${tour.thumbnails}" alt="${tour.tourName}">
                            <div class="tour-info">
                                <h2>${tour.tourName}</h2>
                                <p>${tour.tourDescription}</p>
                                <p><i class="fas fa-map-marker-alt"></i> Điểm đi: ${tour.startLocation}</p>
                                <p><i class="fas fa-map-marker-alt"></i> Điểm đến: ${tour.endLocation}</p>
                                <p><i class="fas fa-calendar-alt"></i> Ngày bắt đầu: ${tour.startDate}</p>
                                <p><i class="fas fa-calendar-alt"></i> Ngày kết thúc: ${tour.endDate}</p>
                                <p>Giá: <fmt:formatNumber value="${tour.price}" type="number" minFractionDigits="0" maxFractionDigits="0"/> VND</p>
                            </div>
                            <div class="tour-buttons">
                                <button class="btn btn-read-more">Read More</button>
                                <a href="tourDetails?tourID=${tour.tourID}" class="btn btn-book-now">Xem chi tiết</a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <script src="js/displayTourList.js" type="text/javascript"></script>
    </body>
</html>


