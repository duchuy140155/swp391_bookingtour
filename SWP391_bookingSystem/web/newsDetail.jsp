<%-- 
    Document   : newsDetail
    Created on : May 21, 2024, 11:54:31 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>News Detail</title>
        <link rel="stylesheet" type="text/css" href="css/newDetail.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="back-button">
                <button onclick="window.location.href = 'newsList'">Trở về tin tức</button>
            </div>
            <div class="container">
                <h1>${news.title}</h1>
                <div class="image-section">
                    <img src="uploads/${news.imageTheme}" alt="${news.title}">
                </div>
                <div class="text-section">
                    <p>${news.shortDescription}</p>
                    <p>${news.longDescription}</p>
                    <p>Publish Date: ${news.publishDate}</p>
                </div>
                <h2>Ảnh sưu tầm</h2>
                <div class="small-images">
                    <c:if test="${not empty images}">
                        <ul>
                            <c:forEach var="image" items="${images}">
                                <li>
                                    <img src="uploads/${image.newsImageURL}" alt="${news.title}">
                                </li>
                            </c:forEach>
                        </ul>
                    </c:if>
                </div>
            </div>
        </div>
    </body>
</html>