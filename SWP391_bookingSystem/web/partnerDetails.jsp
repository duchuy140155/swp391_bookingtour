<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Profile</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
            body {
                background: rgb(99, 39, 120)
            }
            .form-control:focus {
                box-shadow: none;
                border-color: #BA68C8
            }
            .profile-button {
                background: rgb(99, 39, 120);
                box-shadow: none;
                border: none
            }
            .profile-button:hover {
                background: #682773
            }

            .profile-button:focus {
                background: #682773;
                box-shadow: none
            }
            .profile-button:active {
                background: #682773;
                box-shadow: none
            }
            .back:hover {
                color: #682773;
                cursor: pointer
            }
            .labels {
                font-size: 11px
            }
            .add-experience:hover {
                background: #BA68C8;
                color: #fff;
                cursor: pointer;
                border: solid 1px #BA68C8
            }
            #overlay {
                display: none;
                position: fixed;
                z-index: 999;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.7);
                overflow: auto;
            }
            #overlay-img {
                display: block;
                margin: auto;
                max-width: 90%;
                max-height: 90%;
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
        </style>
    </head>
    <body>
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-3 border-right">
                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                        <!-- Thêm ID cho ảnh để bắt sự kiện click -->
                        <img id="partnerImage" class="rounded-circle mt-5" width="150px" src="${partner.certificate}">
                        <span class="font-weight-bold">${partner.partnerName}</span>
                        <span class="text-black-50">${partner.email}</span>
                        <span> </span>
                    </div>
                </div>
                <div class="col-md-5 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Thông tin đối tác</h4>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6"><label class="labels">Tên</label><input class="form-control" placeholder="${partner.partnerName}" value=""></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Email</label><input type="text" class="form-control" placeholder=" ${partner.email}" value=""></div>
                            <div class="col-md-12"><label class="labels">Điện thoại</label><input type="text" class="form-control" placeholder="${partner.phoneNumber}" value=""></div>
                            <div class="col-md-12"><label class="labels">Địa chỉ</label><input type="text" class="form-control" placeholder="${partner.address}" value=""></div>
                        </div>
                        <form id="activatePartnerForm" action="activatePartner" method="post">
                            <input type="hidden" name="partnerID" value="${partner.partnerID}">
                            <div class="mt-5 text-center"><button id="activateButton" class="btn btn-primary profile-button" type="button">Kích Hoạt</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="overlay">
            <img id="overlay-img">
        </div>
        <script>
            $(document).ready(function () {
                $('#partnerImage').click(function () {
                    $('#overlay').fadeIn();
                    $('#overlay-img').attr('src', $(this).attr('src'));
                });
                $('#overlay').click(function () {
                    $(this).fadeOut();
                });
                $('#activateButton').click(function () {
                    $('#activatePartnerForm').submit();
                    alert("Bạn đã kích hoạt tài khoản đối tác thành công!");
                });
            });
        </script>

    </body>
</html>
