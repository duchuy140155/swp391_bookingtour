<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Chi Tiết Bài Đăng</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container {
            margin-top: 50px;
            max-width: 1200px; /* Tăng chiều rộng để chứa bảng bên phải */
            background-color: #fff; /* Thêm nền trắng cho container */
            padding: 20px; /* Thêm khoảng cách bên trong container */
            border-radius: 10px; /* Bo tròn các góc */
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Thêm đổ bóng */
            display: flex;
            justify-content: space-between;
        }
        .post-content-container {
            width: 70%;
        }
        .related-posts-container {
            width: 25%;
            padding-left: 20px;
        }
        .post-title {
            color: #b71c1c;
            font-size: 28px; /* Tăng kích thước chữ */
            font-weight: bold;
            margin-bottom: 20px; /* Thêm khoảng cách dưới tiêu đề */
            text-align: center; /* Căn giữa tiêu đề */
        }
        .post-content {
            font-size: 18px;
            line-height: 1.8; /* Tăng khoảng cách dòng */
            text-align: justify; /* Chia đều chữ ra */
            word-spacing: 1px; /* Khoảng cách giữa các từ */
            margin-bottom: 20px; /* Khoảng cách dưới đoạn văn */
        }
        .post-section {
            margin-bottom: 40px;
        }
        .post-section img {
            width: 100%; /* Đảm bảo hình ảnh chiếm toàn bộ chiều rộng của container */
            height: auto; /* Tự động điều chỉnh chiều cao theo tỷ lệ */
            max-height: 400px; /* Giới hạn chiều cao của hình ảnh nếu cần */
            border-radius: 5px;
            margin-bottom: 20px; /* Thêm khoảng cách dưới hình ảnh */
            object-fit: cover; /* Đảm bảo hình ảnh được cắt và phủ đầy khung hình */
        }
        .related-posts {
            list-style: none;
            padding: 0;
        }
          img {
            max-width: 100%;
            height: auto;
            max-height: 100px; /* Giới hạn chiều cao của ảnh */
            display: block;
            margin-bottom: 15px;
            border-radius: 10px;
            object-fit: cover; /* Đảm bảo ảnh không bị méo */
        }
        .related-posts li {
            margin-bottom: 10px;
            display: flex; /* Để hình ảnh và nội dung hiển thị theo hàng ngang */
            align-items: center;
        }
        .related-posts img {
            width: 60px; /* Đặt chiều rộng cố định cho hình ảnh nhỏ */
            height: 60px; /* Đặt chiều cao cố định cho hình ảnh nhỏ */
            object-fit: cover; /* Đảm bảo hình ảnh được cắt và phủ đầy khung hình */
            border-radius: 5px;
            margin-right: 10px; /* Thêm khoảng cách giữa hình ảnh và nội dung */
        }
        .related-posts h6 {
            margin: 0;
            font-size: 14px;
            font-weight: bold;
        }
        .related-posts p {
            margin: 5px 0 0;
            font-size: 12px;
        }
        .related-posts a {
            text-decoration: none;
            color: #007bff;
        }
        .related-posts a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="post-content-container">
            <div class="d-flex justify-content-end">
                <a href="javascript:history.back()" class="btn btn-primary mb-3">Quay lại</a>
            </div>
            <c:set var="p" value="${sessionScope.post}" />
            <div class="post-section">
                <img src="${p.image}" alt="Post Image 1" width="100px">
                <div class="post-title"><b>${p.postTitle}</b></div>
                <div class="post-content">
                    <c:out value="${p.postContent}" escapeXml="false"/>
                </div>
            </div>
        </div>
        <div class="related-posts-container">
            <h4>Bài viết liên quan</h4>
            <ul class="related-posts">
                <c:forEach items="${sessionScope.relatedpost}" var="p">
                    
                    
               
                <li>
                    <img src="${p.image}" alt="Related Post Image">
                    <div>
                        <h6><a href="postdetail?id=${p.postId}">${p.postTitle}</a></h6>
                        <p>${p.createAt}</p>
                    </div>
                </li>
                 </c:forEach>
                <!-- Thêm nhiều bài viết liên quan hơn ở đây -->
            </ul>
        </div>
    </div>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
