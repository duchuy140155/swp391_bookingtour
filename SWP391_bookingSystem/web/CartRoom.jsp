<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping Cart</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- QRious JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>
    <style>
        body {
            font-family: 'Helvetica Neue', Arial, sans-serif;
            margin: 0;
            padding: 0;
            background: linear-gradient(to right, #ffecd2, #fcb69f);
            color: #343a40;
        }
        .container {
            margin-top: 50px;
        }
        .header, .footer {
            background-color: #343a40;
            color: white;
            padding: 10px 0;
            text-align: center;
        }
        .footer {
            position: fixed;
            width: 100%;
            bottom: 0;
        }
        .cart-item {
            padding: 20px;
            background-color: #f0f8ff;
            border: 1px solid #b0c4de;
            border-radius: 10px;
            margin-bottom: 20px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            transition: transform 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
        }
        .cart-item:hover {
            transform: scale(1.02);
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        }
        .cart-item-info {
            flex-grow: 1;
        }
        .cart-item-info p {
            margin: 0;
            font-size: 1.1em;
            color: #495057;
        }
        .cart-item-controls {
            display: flex;
            align-items: center;
            gap: 10px;
        }
        .quantity-input {
            width: 60px;
            text-align: center;
            border: 1px solid #ced4da;
            border-radius: 5px;
            pointer-events: none; /* Ngăn chặn chỉnh sửa trực tiếp */
        }
        .cart-form {
            padding: 30px;
            background-color: rgba(255, 255, 255, 0.9);
            border-radius: 15px;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        }
        .total-price-form {
            max-width: 400px;
            margin: 30px auto;
            padding: 20px;
            background-color: #e0f7fa;
            border: 1px solid #00acc1;
            border-radius: 10px;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
            text-align: center;
        }
        .total-price-form h2 {
            font-size: 1.5em;
            margin-bottom: 20px;
            color: #00796b;
        }
        .total-price-form p {
            font-size: 1.2em;
            margin-bottom: 10px;
            color: #00796b;
        }
        .empty-cart-message {
            text-align: center;
            font-size: 1.2em;
            color: #6c757d;
            margin-top: 20px;
        }
        .back-button, .booking-button {
            position: absolute;
            top: 10px;
            right: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1em;
            transition: background-color 0.3s ease;
        }
        .booking-button {
            right: 140px;
        }
        .back-button:hover, .booking-button:hover {
            background-color: #0056b3;
        }
        .btn-primary {
            background-color: #007bff;
            border: none;
        }
        .btn-primary:hover {
            background-color: #0056b3;
        }
        .qr-code-container {
            text-align: center;
            margin-top: 20px;
        }
        .qr-code-container canvas {
            border: 1px solid #00796b;
            padding: 10px;
            background-color: white;
            border-radius: 10px;
        }
        .service-card {
            padding: 15px;
            background-color: white;
            border: 1px solid #dee2e6;
            border-radius: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            transition: background-color 0.3s ease, transform 0.3s ease;
        }
        .service-card:hover {
            background-color: #f8f9fa;
            transform: scale(1.02);
        }
        .service-form {
            padding: 10px;
            background-color: #f8f9fa;
            border: 1px solid #dee2e6;
            border-radius: 5px;
            margin-top: 10px;
        }
        .thank-you-message {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 30px;
            background-color: #fff;
            border: 2px solid #007bff;
            border-radius: 10px;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
            text-align: center;
            z-index: 1000;
            animation: fadeIn 0.5s ease-in-out;
        }
        .thank-you-message h3 {
            margin-bottom: 20px;
            color: #007bff;
        }
        .thank-you-message p {
            margin-bottom: 10px;
            color: #343a40;
        }
        .thank-you-message button {
            background-color: #007bff;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1em;
            transition: background-color 0.3s ease;
        }
        .thank-you-message button:hover {
            background-color: #0056b3;
        }
        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    </style>
</head>
<body>
   <c:if test="${empty sessionScope.account || sessionScope.account.roleID != 2}">
    <script type="text/javascript">
        window.location.href = 'login.jsp';
    </script>
</c:if>

    <div class="header">
        <h1>Welcome to Our Hotel Booking</h1>
        <button class="back-button" onclick="goBack()">Back</button>
        <button class="booking-button" onclick="goToBooking()">Đặt Phòng</button>
    </div>

    <div class="container">
        <form class="cart-form" id="cartForm" onsubmit="handleSubmit(event)">
            <input type="hidden" id="accountNumber" value="123456789"> <!-- Replace with your actual account number -->
            <input type="hidden" id="totalHidden" name="total" value="0"> <!-- Hidden input for total -->
            <div class="cart-items">
                <c:forEach items="${sessionScope.cartItems}" var="item">
                    <div class="cart-item">
                        <div class="cart-item-info">
                            <p><strong>Tên Phòng:</strong> ${item.room.name}</p>
                            <p><strong>Giá:</strong> ${item.room.price} đ</p>
                            <p><strong>Số Lượng Giường:</strong> ${item.room.number_beds}</p>
                            <p><strong>Số Lượng Người Tối Đa:</strong> ${item.room.max_guests}</p>
                            <p><strong>Miêu Tả Phòng:</strong> ${item.room.description}</p>
                        </div>
                        <div class="cart-item-controls">
                            <input type="number" class="quantity-input" value="${item.quantity}" min="1" readonly>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <c:if test="${empty sessionScope.cartItems}">
                <p class="empty-cart-message">Your cart is empty.</p>
            </c:if>
            <button type="button" id="show-services-btn" class="btn btn-secondary mt-3">Show Services</button>
            <div id="services-section" style="display:none;">
                <h3>Services</h3>
                <div class="service-card">
                    <input type="checkbox" id="service1" name="services" value="Thêm Đồ Ăn" onchange="toggleServiceForm('foodServiceForm')">
                    <label for="service1">Thêm Đồ Ăn</label>
                    <div id="foodServiceForm" class="service-form" style="display:none;">
                        <input type="checkbox" id="breakfast" name="foodService" value="Bữa sáng">
                        <label for="breakfast">Bữa sáng</label><br>
                        <input type="checkbox" id="lunch" name="foodService" value="Bữa trưa">
                        <label for="lunch">Bữa trưa</label><br>
                        <input type="checkbox" id="dinner" name="foodService" value="Bữa tối">
                        <label for="dinner">Bữa tối</label><br>
                        <input type="checkbox" id="snacks" name="foodService" value="Đồ ăn nhẹ">
                        <label for="snacks">Đồ ăn nhẹ</label><br>
                    </div>
                </div>
                <div class="service-card">
                    <input type="checkbox" id="service2" name="services" value="Chuẩn bị đồ tắm biển" onchange="toggleServiceForm('beachServiceForm')">
                    <label for="service2">Chuẩn bị đồ tắm biển</label>
                    <div id="beachServiceForm" class="service-form" style="display:none;">
                        <input type="checkbox" id="lifeJacket" name="beachService" value="Thuê áo phao">
                        <label for="lifeJacket">Thuê áo phao</label><br>
                        <input type="checkbox" id="float" name="beachService" value="Thuê phao">
                        <label for="float">Thuê phao</label><br>
                        <input type="checkbox" id="sunglasses" name="beachService" value="Thuê kính râm">
                        <label for="sunglasses">Thuê kính râm</label><br>
                        <input type="checkbox" id="hat" name="beachService" value="Thuê mũ">
                        <label for="hat">Thuê mũ</label><br>
                    </div>
                </div>
                <div class="service-card">
                    <input type="checkbox" id="service3" name="services" value="Vận chuyển hành lý">
                    <label for="service3">Vận chuyển hành lý</label>
                </div>
            </div>
            <div class="total-price-form">
                <h2>Order Summary</h2>
                <p>Total: <span id="total-price">0</span> đ</p>
                <button type="submit" class="btn btn-primary w-100 mt-3">Proceed to Checkout</button>
            </div>
        </form>
    </div>

    <div class="footer">
        <p>&copy; 2024 Our Hotel Booking. All rights reserved.</p>
    </div>

    <div class="thank-you-message" id="thankYouMessage">
        <h3>Cảm ơn bạn đã tin tưởng dịch vụ của chúng tôi</h3>
        <p>Chúng tôi sẽ nhờ nhân viên tư vấn xác minh lại những lựa chọn này của bạn sau khi bạn thanh toán.</p>
        <p>Chúc bạn ngày mới vui vẻ!</p>
        <p>Travela chúng tôi xin chân thành cảm ơn!</p>
        <button id="closeButton" onclick="sendDataAndClose()">Close</button>
    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script>
        function goBack() {
            window.history.back();
        }

        function goToBooking() {
            window.location.href = 'ViewListRoom.jsp';
        }

        function updateTotal() {
            var total = 0;
            var cartItems = document.querySelectorAll('.cart-item');
            cartItems.forEach(function(item) {
                var price = parseFloat(item.querySelector('.cart-item-info p:nth-child(2)').textContent.replace('Giá:', '').replace(' đ', ''));
                var quantity = parseInt(item.querySelector('.quantity-input').value);
                total += price * quantity;
            });
            document.getElementById('total-price').textContent = total.toFixed(2);
            document.getElementById('totalHidden').value = total.toFixed(2); // Update the hidden input with total value

            // Ẩn/hiện nút "Show Services" dựa trên giá trị tổng
            var showServicesBtn = document.getElementById('show-services-btn');
            if (total === 0) {
                showServicesBtn.style.display = 'none';
            } else {
                showServicesBtn.style.display = 'block';
            }
        }

        document.getElementById('show-services-btn').addEventListener('click', function() {
            var servicesSection = document.getElementById('services-section');
            if (servicesSection.style.display === 'none') {
                servicesSection.style.display = 'block';
            } else {
                servicesSection.style.display = 'none';
            }
        });

        function toggleServiceForm(formId) {
            var form = document.getElementById(formId);
            if (form.style.display === 'none') {
                form.style.display = 'block';
            } else {
                form.style.display = 'none';
            }
        }

        function handleSubmit(event) {
            event.preventDefault();
            var total = parseFloat(document.getElementById('total-price').textContent);
            if (total === 0) {
                alert('Bạn chưa có đơn đặt phòng nào. Vui lòng quay lại trang đặt phòng để lựa chọn.');
            } else {
                saveServicesAndSendEmail();
            }
        }

        function saveServicesAndSendEmail() {
            var formData = new FormData(document.getElementById('cartForm'));

            fetch('saveServices', {
                method: 'POST',
                body: formData
            })
            .then(response => response.text())
            .then(data => {
                console.log('Success:', data);
                showThankYouMessage();

                // Gửi email sau khi lưu dịch vụ thành công
                var customerEmail = 'duycaduoc0704@gmail.com'; // Thay thế bằng email thực của khách hàng
                var subject = 'Thông tin đơn hàng của bạn';
                var content = 'Cảm ơn bạn đã đặt hàng tại khách sạn của chúng tôi. Đơn hàng của bạn đã được xử lý.';
                fetch('sendemail', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        customerEmail: customerEmail,
                        subject: subject,
                        content: content
                    })
                })
                .then(response => response.text())
                .then(data => {
                    console.log('Email sent:', data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
            })
            .catch((error) => {
                console.error('Error:', error);
            });
        }

        function showThankYouMessage() {
            var thankYouMessage = document.getElementById('thankYouMessage');
            thankYouMessage.style.display = 'block';
        }

        function closeThankYouMessage() {
            var thankYouMessage = document.getElementById('thankYouMessage');
            thankYouMessage.style.display = 'none';

            // Xóa tất cả các đơn đặt phòng
            var cartItems = document.querySelectorAll('.cart-item');
            cartItems.forEach(function(item) {
                item.remove();
            });
            updateTotal();
            window.location.href = 'bookroom'; // Điều hướng tới trang HistoryBookRoom sau khi đóng thông báo
        }

        function sendDataAndClose() {
            var formData = new FormData(document.getElementById('cartForm'));

            fetch('saveServices', {
                method: 'POST',
                body: formData
            })
            .then(response => response.text())
            .then(data => {
                console.log('Success:', data);
                // Thêm giá trị tổng vào URL khi điều hướng
                var total = document.getElementById('totalHidden').value;
                window.location.href = 'bookroom?total=' + total; // Điều hướng tới trang bookroom với giá trị tổng
            })
            .catch((error) => {
                console.error('Error:', error);
            });
        }

        function saveServices() {
            var formData = new FormData(document.getElementById('cartForm'));

            fetch('saveServices', {
                method: 'POST',
                body: formData
            })
            .then(response => response.text())
            .then(data => {
                console.log('Success:', data);
                showThankYouMessage();
            })
            .catch((error) => {
                console.error('Error:', error);
            });
        }

        document.getElementById('closeButton').addEventListener('click', sendDataAndClose);

        updateTotal();
    </script>
</body>
</html>
