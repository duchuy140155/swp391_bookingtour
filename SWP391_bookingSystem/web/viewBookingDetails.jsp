<%-- 
    Document   : viewBookingDetails
    Created on : Jun 13, 2024, 8:02:30 PM
    Author     : Admin
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết Đặt Tour</title>
        <link href="css/viewBookingDetails.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <h2 class="centered">Thông tin chi tiết đặt tour</h2>
            <!-- Nút Back -->
            <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
            
            <h2>Thông tin khách hàng</h2>
            <p><strong>Họ và Tên:</strong> ${bookingDetails.customerName}</p>
            <p><strong>Email:</strong> ${bookingDetails.email}</p>
            <p><strong>Số điện thoại:</strong> ${bookingDetails.phoneNumber}</p>
            <p><strong>Địa chỉ:</strong> ${bookingDetails.customerAddress}</p>
            <p><strong>Ghi chú:</strong> ${bookingDetails.note}</p>

            <h2>Thông tin Tour</h2>
            <p><strong>Tên Tour:</strong> ${bookingDetails.tour.tourName}</p>
            <p><strong>Điểm đi:</strong> ${bookingDetails.tour.startLocation}</p>
            <p><strong>Điểm đến:</strong> ${bookingDetails.tour.endLocation}</p>
            <p><strong>Ngày đi:</strong> <fmt:formatDate value="${bookingDetails.tour.startDate}" pattern="dd-MM-yyyy" /></p>
            <p><strong>Ngày về:</strong> <fmt:formatDate value="${bookingDetails.tour.endDate}" pattern="dd-MM-yyyy" /></p>

            <h2>Thông tin hành khách</h2>
            <c:forEach var="ticket" items="${bookingDetails.tickets}">
                <div class="passenger-info">
                    <p><strong>Họ và tên:</strong> ${ticket.name}</p>
                    <p><strong>Giới tính:</strong> <c:choose>
                            <c:when test="${ticket.gender}">Nam</c:when>
                            <c:otherwise>Nữ</c:otherwise>
                        </c:choose></p>
                    <p><strong>Ngày sinh:</strong> <fmt:formatDate value="${ticket.dob}" pattern="dd-MM-yyyy" /></p>
                    <p><strong>Loại vé:</strong> ${ticket.tickettype.typeName}</p>
                </div>
            </c:forEach>

            <h2>Thông tin thanh toán</h2>
            <p><strong>Phương thức thanh toán:</strong> ${bookingDetails.paymentMethod}</p>
            <p><strong>Giá đầu tiên (Chưa có):</strong> </p>
            <p><strong>Giá cuối cùng:</strong> <fmt:formatNumber value="${bookingDetails.finalPrice}" type="number" groupingUsed="true" /> vnđ</p>
            <p><strong>Mã discount:</strong> ${bookingDetails.discountID.discountName}</p>

            <h2>Trạng thái:</h2>
            <p><c:choose>
                    <c:when test="${bookingDetails.status == 0}">
                        Đang duyệt
                    </c:when>
                    <c:when test="${bookingDetails.status == 1}">
                        Đã được chấp nhận
                    </c:when>
                    <c:otherwise>
                        Trạng thái không xác định
                    </c:otherwise>
                </c:choose></p>
        </div>
    </body>
</html>


