<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Xác nhận đặt tour</title>
        <link href="css/confirm.css" rel="stylesheet" type="text/css"/>
        <style>
            .container {
                width: 80%;
                margin: auto;
            }
            table {
                width: 80%;
                border-collapse: collapse;
                margin: 20px 0;
            }

            table, th, td {
                border: 1px solid black;
            }

            th, td {
                padding: 10px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            .btn-back {
                padding: 10px 20px;
                border: none;
                border-radius: 4px;
                background-color: #6c757d;
                color: #fff;
                cursor: pointer;
                font-size: 16px;
                text-decoration: none;
                display: inline-block;
                text-align: center;
                margin: 20px 0; /* Add margin for spacing */
            }

            .btn-back:hover {
                background-color: #5a6268;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <button onclick="window.location.href = 'homepage'" class="btn-back">Trở về trang chủ</button>
            <h1>Thông tin xác nhận đặt tour</h1>
            <h2>Thông tin Tour</h2>
            <p><strong>Tên Tour:</strong> ${tour.tourName}</p>
            <p><strong>Ngày đi:</strong> <fmt:formatDate value="${tour.startDate}" pattern="dd-MM-yyyy" /></p>
            <p><strong>Ngày về:</strong> <fmt:formatDate value="${tour.endDate}" pattern="dd-MM-yyyy" /></p>

            <h2>Thông tin khách hàng</h2>
            <h3>Người đặt Tour: </h3>
            <p><strong>Họ và Tên:</strong> ${bookingDetails.customerName}</p>
            <p><strong>Email:</strong> ${bookingDetails.email}</p>
            <p><strong>Số điện thoại:</strong> 0${bookingDetails.phoneNumber}</p> 
            <p><strong>Địa chỉ:</strong> ${bookingDetails.customerAddress}</p>
            <p><strong>Ghi chú:</strong> 
                <c:choose>
                    <c:when test="${not empty bookingDetails.note}">
                        ${bookingDetails.note}
                    </c:when>
                    <c:otherwise>
                        Không
                    </c:otherwise>
                </c:choose>
            </p>
            <c:if test="${not empty tickets}">
                <div class="tickets-section">
                    <h2>Các hành khách bao gồm:</h2> 
                    <table>
                        <thead>
                            <tr>
                                <th>Họ và tên</th>
                                <th>Giới tính</th>
                                <th>Ngày sinh</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="ticket" items="${tickets}">
                                <tr>
                                    <td>${ticket.name}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${ticket.gender}">Nam</c:when>
                                            <c:otherwise>Nữ</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><fmt:formatDate value="${ticket.dob}" pattern="dd-MM-yyyy" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
            <h2>Thông tin thanh toán</h2>
            <!--<p><strong>Phương thức thanh toán:</strong> ${bookingDetails.paymentMethod}</p>-->
            <p><strong>Giá Cuối cùng:</strong> <fmt:formatNumber value="${bookingDetails.finalPrice}" type="number" minFractionDigits="0" maxFractionDigits="0"/> VND</p>

            <h2>Trạng thái:</h2>
            <c:choose>
                <c:when test="${bookingDetails.status == 0}">
                    Đang duyệt
                </c:when>
                <c:when test="${bookingDetails.status == 1}">
                    Đã được chấp nhận
                </c:when>
                <c:otherwise>
                    Trạng thái không xác định
                </c:otherwise>
            </c:choose>
        </div>
    </body>
</html>
