<%-- 
    Document   : createNews
    Created on : May 22, 2024, 8:43:31 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Create News</title>
        <link rel="stylesheet" type="text/css" href="css/newsManagement.css">
    </head>
    <body>
        <div class="container">
            <h1>Tạo một tin mới</h1>
            <button onclick="window.location.href = 'manageNews'" class="btn forward">Trở về quản lý tin</button>
            <form action="manageNews" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="create"><br>
                <label for="title">Title:</label>
                <input type="text" id="title" name="title" required><br><br>
                <label for="shortDescription">Short Description:</label>
                <textarea id="shortDescription" name="shortDescription" required></textarea><br><br>
                <label for="longDescription">Long Description:</label>
                <textarea id="longDescription" name="longDescription" required></textarea><br><br>
                <label for="imageTheme">Image Theme:</label>
                <input type="file" id="imageTheme" name="imageTheme" required><br><br>
                <label for="newsImages">Additional Images:</label>
                <input type="file" id="newsImages" name="newsImages" multiple><br><br>
                <button class="btn create" type="submit">Tạo news mới</button>
            </form>
        </div>
    </body>
</html>
