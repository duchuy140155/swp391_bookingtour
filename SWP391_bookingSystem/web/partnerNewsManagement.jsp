<%-- 
    Document   : partnerNewsManagement
    Created on : May 22, 2024, 11:08:59 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Quản lý tin tức</title>
        <link rel="stylesheet" type="text/css" href="css/newsManagement.css">
    </head>
    <body>
        <div class="container">
            <h1>Quản lý tin tức</h1>
            <button onclick="window.location.href = 'partner.jsp'" class="btn forward">Trở về trang quản lý</button>
            <div class="search-create-container">
                <div class="search-bar">
                    <input type="text" id="searchQuery" placeholder="Search by title" value="<%= request.getParameter("query") != null ? request.getParameter("query") : "" %>">
                    <button onclick="window.location.href = 'manageNews?query=' + encodeURIComponent(document.getElementById('searchQuery').value)" class="btn search">Tìm kiếm</button>
                </div>
            </div>
            <div class="back-button">
                <a href="createNews.jsp" class="btn create-btn">Tạo một tin mới</a>
            </div>
            <ul>
                <c:forEach var="news" items="${newsList}">
                    <li>
                        <h2>${news.title}</h2>
                        <p>${news.shortDescription}</p>
                        <p>Publish Date: ${news.publishDate}</p>
                        <a href="manageNews?action=edit&idNew=${news.idNew}" class="btn edit">Sửa</a>
                        <form action="manageNews" method="post" style="display:inline;">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" name="id" value="${news.idNew}">
                            <button type="submit" class="btn delete">Xóa</button>
                        </form>
                    </li>
                </c:forEach>
            </ul>
            <div class="pagination">
                <c:if test="${currentPage > 1}">
                    <a href="manageNews?page=${currentPage - 1}&query=${query}" class="btn">Previous</a>
                </c:if>
                <c:forEach begin="1" end="${totalPages}" var="i">
                    <a href="manageNews?page=${i}&query=${query}" class="btn <c:if test='${i == currentPage}'>active</c:if>">${i}</a>
                </c:forEach>
                <c:if test="${currentPage < totalPages}">
                    <a href="manageNews?page=${currentPage + 1}&query=${query}" class="btn">Next</a>
                </c:if>
            </div>
        </div>
    </body>
</html>



