<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tạo mới Tour</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                max-width: 800px;
                margin: 50px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h1 {
                text-align: center;
                color: #333;
            }
            form {
                display: flex;
                flex-direction: column;
            }
            label {
                margin: 10px 0 5px;
                color: #555;
            }
            input[type="text"],
            input[type="date"],
            input[type="number"],
            textarea,
            select {
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 4px;
                width: 100%;
                box-sizing: border-box;
                margin-bottom: 15px;
            }
            input[type="file"] {
                margin-bottom: 15px;
            }
            input[type="submit"] {
                padding: 10px 20px;
                background-color: #28a745;
                border: none;
                color: #fff;
                border-radius: 4px;
                cursor: pointer;
                font-size: 16px;
            }
            input[type="submit"]:hover {
                background-color: #218838;
            }
            #thumbnailsPreview,
            #imagePreviewContainer {
                display: flex;
                flex-wrap: wrap;
            }
            .imagePreview {
                max-width: 100px;
                max-height: 100px;
                margin: 10px;
            }
            .message {
                text-align: center;
                padding: 10px;
                margin-top: 10px;
                border-radius: 4px;
            }
            .error {
                background-color: #f8d7da;
                color: #721c24;
            }
            .success {
                background-color: #d4edda;
                color: #155724;
            }
            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .form-column {
                flex: 1;
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="back-button">
                <button onclick="window.location.href = 'listTour?action=list'" class="btn">Trở về quản lý tin</button>
            </div>
            <h1>Tạo mới Tour</h1>
            <c:if test="${not empty message}">
                <p class="message ${message.startsWith('Failed') ? 'error' : 'success'}">${message}</p>
            </c:if>
            <form id="tourForm" action="createTour" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                <div class="form-row">
                    <div class="form-column">
                        <label for="tourName">Tên của tour: </label>
                        <input type="text" id="tourName" name="tourName" required minlength="10" maxlength="70"
                               pattern="[a-zA-Z0-9\s-]+" title="Tour name must be between 10 and 70 characters and cannot contain special characters except hyphens.">

                        <label for="tourDescription">Mô tả: </label>
                        <textarea id="tourDescription" name="tourDescription" required maxlength="100"
                                  pattern="[a-zA-Z0-9\s]+" title="Tour description cannot contain special characters and must be less than 100 characters."></textarea>

                        <label for="startLocation">Điểm đi: </label>
                        <select id="startLocation" name="startLocation" required>
                            <c:forEach var="location" items="${locations}">
                                <option value="${location.locationName}">${location.locationName}</option>
                            </c:forEach>
                        </select>

                        <label for="endLocation">Điểm đến:</label>
                        <select id="endLocation" name="endLocation" required>
                            <c:forEach var="location" items="${locations}">
                                <option value="${location.locationName}">${location.locationName}</option>
                            </c:forEach>
                        </select>

                        <label for="startDate">Ngày đi:</label>
                        <input type="date" id="startDate" name="startDate" required min="<%= new java.sql.Date(System.currentTimeMillis()).toString() %>">

                        <label for="endDate">Ngày về:</label>
                        <input type="date" id="endDate" name="endDate" required oninput="validateDates()">

                        <label for="price">Giá:</label>
                        <input type="number" step="0.01" id="price" name="price" required min="1" title="Giá phải lớn hơn 0">

                        <label for="numberOfPeople">Số lượng thành viên:</label>
                        <input type="number" id="numberOfPeople" name="numberOfPeople" required min="1" title="Số lượng thành viên phải lớn hơn 0">
                    </div>
                    <div class="form-column">
                        <label for="thumbnail">Ảnh nền:</label>
                        <input type="file" id="thumbnail" name="thumbnail" accept="image/*" onchange="validateThumbnail(event)">
                        <div id="thumbnailsPreview"></div>

                        <label for="tourImages">Ảnh về tour:</label>
                        <input type="file" id="tourImages" name="images" accept="image/*" multiple onchange="validateImages(event)">
                        <div id="imagePreviewContainer"></div>
                        <p id="imageError" style="color: red; display: none;">Bạn chỉ có thể tải lên tối đa 5 hình ảnh.</p>
                    </div>
                </div>
                <input type="submit" value="Tạo Tour">
            </form>
        </div>

        <script>
            function previewThumbnail(event) {
                var file = event.target.files[0];
                var container = document.getElementById('thumbnailsPreview');
                container.innerHTML = ''; // Clear previous image
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = document.createElement('img');
                    img.src = e.target.result;
                    img.className = 'imagePreview';
                    container.appendChild(img);
                }
                reader.readAsDataURL(file);
            }

            function validateThumbnail(event) {
                var files = event.target.files;
                var container = document.getElementById('thumbnailsPreview');
                if (files.length > 1) {
                    alert('Chỉ được tải lên một ảnh nền.');
                    event.target.value = ""; // Clear the file input
                    container.innerHTML = ''; // Clear the preview
                } else {
                    previewThumbnail(event);
                }
            }

            function previewImages(files, container) {
                for (var i = 0; i < files.length; i++) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement('img');
                        img.src = e.target.result;
                        img.className = 'imagePreview';
                        container.appendChild(img);
                    }
                    reader.readAsDataURL(files[i]);
                }
            }

            function validateImages(event) {
                var files = event.target.files;
                var container = document.getElementById('imagePreviewContainer');
                var error = document.getElementById('imageError');

                // Clear previous images and error message
                container.innerHTML = '';
                error.style.display = 'none';

                if (files.length > 5) {
                    error.style.display = 'block';
                    event.target.value = ""; // Clear the file input
                    return;
                }

                previewImages(files, container);
            }

            function validateDates() {
                const startDate = document.getElementById('startDate');
                const endDate = document.getElementById('endDate');
                if (startDate.value && endDate.value) {
                    if (startDate.value >= endDate.value) {
                        endDate.setCustomValidity('Ngày về phải sau ngày đi.');
                    } else {
                        endDate.setCustomValidity('');
                    }
                }
            }

            function validateForm() {
                const startLocation = document.getElementById('startLocation').value;
                const endLocation = document.getElementById('endLocation').value;
                const price = document.getElementById('price').value;
                const numberOfPeople = document.getElementById('numberOfPeople').value;
                
                if (startLocation === endLocation) {
                    alert('Điểm đi không được trùng với điểm đến.');
                    return false;
                }

                if (price <= 0) {
                    alert('Giá phải lớn hơn 0.');
                    return false;
                }

                if (numberOfPeople <= 0) {
                    alert('Số lượng thành viên phải lớn hơn 0.');
                    return false;
                }
                
                return true;
            }

            // Set the minimum date for start date
            document.getElementById('startDate').min = new Date().toISOString().split('T')[0];
        </script>
    </body>
</html>
