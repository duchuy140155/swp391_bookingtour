<%-- 
    Document   : unresolvedBookings
    Created on : Jul 18, 2024, 11:33:51 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Unresolved Bookings</title>
</head>
<body>
    <h1>Danh sách các booking chưa giải quyết</h1>
    <table border="1">
        <thead>
            <tr>
                <th>Booking ID</th>
                <th>Trạng thái</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="booking" items="${unresolvedBookings}">
                <tr>
                    <td>${booking.bookingID}</td>
                    <td>${booking.resolutionStatus}</td>
                    <td>
                        <form action="unresolvedBookings" method="POST">
                            <input type="hidden" name="bookingID" value="${booking.bookingID}"/>
                            <select name="resolutionStatus">
                                <option value="Chưa giải quyết" ${booking.resolutionStatus == 'Chưa giải quyết' ? 'selected' : ''}>Chưa giải quyết</option>
                                <option value="Đã giải quyết" ${booking.resolutionStatus == 'Đã giải quyết' ? 'selected' : ''}>Đã giải quyết</option>
                            </select>
                            <input type="submit" value="Cập nhật"/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
