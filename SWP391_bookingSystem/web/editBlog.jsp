<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <title>Edit Blog</title>
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f7f7f7;
        }

        .container {
            width: 80%;
            margin: 50px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            text-align: center;
            color: #333;
        }

        form {
            display: flex;
            flex-direction: column;
        }

        form div {
            margin-bottom: 20px;
        }

        label {
            font-weight: bold;
            margin-bottom: 5px;
            display: inline-block;
            color: #555;
        }

        input[type="text"],
        textarea {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
            font-size: 16px;
        }

        input[type="file"] {
            margin-top: 10px;
        }

        .existing-pictures img {
            max-width: 100px;
            height: auto;
            margin-right: 10px;
            margin-bottom: 10px;
            border-radius: 5px;
        }

        button[type="submit"] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
        }

        button[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Edit Blog</h1>
        <form id="editBlogForm" action="editBlog" method="post" enctype="multipart/form-data">
            <input type="hidden" name="blogID" value="${blog.blogID}">
            <div>
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" value="${blog.title}" required>
            </div>
            <div>
                <label for="content">Content:</label>
                <textarea name="content" id="editor" rows="10" required>${fn:escapeXml(blog.content)}</textarea>
            </div>
            
            <div class="existing-pictures">
                <c:forEach var="picture" items="${blog.pictures}">
                    <img src="${picture.pictureURL}" alt="Blog Image">
                </c:forEach>
            </div>
            <button type="submit">Save Changes</button>
        </form>
    </div>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: 'uploadImage'
                }
            })
            .catch(error => {
                console.error(error);
            });
    </script>
</body>
</html>
