<%-- 
    Document   : editHandbook
    Created on : May 25, 2024, 4:38:42 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>gggfsdfEdit Handbook</title>
    </head>
    <body>
        <h2>Edit Handbook</h2>
        <form action="${pageContext.request.contextPath}/manageHandbook?action=edit" method="post" enctype="multipart/form-data">
            <input type="hidden" name="handbookID" value="${handbook.handbookID}">
            <input type="hidden" name="existingImageTheme" value="${handbook.imageTheme}">

            <label for="partnerID">Partner ID:</label>
            <input type="text" id="partnerID" name="partnerID" value="${handbook.partnerID}" readonly><br>

            <label for="title">Title:</label>
            <input type="text" id="title" name="title" value="${handbook.title}" required><br>

            <label for="shortContent">Short Content:</label>
            <textarea id="shortContent" name="shortContent" required>${handbook.shortContent}</textarea><br>

            <label for="imageTheme">Image Theme:</label>
            <input type="file" id="imageTheme" name="imageTheme"><br>
            <img src="${pageContext.request.contextPath}/${handbook.imageTheme}" alt="Current Image" width="100"><br>

            <button type="submit">Update</button>
        </form>
        <a href="${pageContext.request.contextPath}/manageHandbook?action=list">Back to List</a>
    </body>
</html>
