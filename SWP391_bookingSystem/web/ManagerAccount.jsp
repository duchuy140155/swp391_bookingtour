<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quản lý tài khoản</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #e0f7fa;
            font-family: 'Candara', sans-serif;
        }
        .side-nav {
            background: linear-gradient(to bottom, #0077b6, #00aaff);
            color: white;
            padding: 20px;
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            position: fixed;
            width: 270px;
            border-radius: 0 15px 15px 0;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .user-info {
            text-align: center;
            margin-bottom: 20px;
        }
        .user-image {
            border-radius: 50%;
            width: 60px;
            height: 60px;
            object-fit: cover;
            margin-bottom: 10px;
            border: 2px solid #ffffff;
        }
        .user-info p {
            font-size: 18px;
            font-weight: bold;
            margin: 0;
        }
        .side-nav a {
            color: white;
            text-decoration: none;
            padding: 10px 20px;
            transition: background-color 0.3s;
            position: relative;
            display: block;
            margin-bottom: 10px;
            border-radius: 25px;
            text-align: center;
        }
        .side-nav a:hover {
            background-color: #005f99;
        }
        .side-nav .active {
            background-color: #004080;
            font-weight: bold;
        }
        .notification {
            background-color: red;
            color: white;
            border-radius: 50%;
            padding: 2px 8px;
            font-size: 12px;
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .main-content {
            margin-left: 290px;
            padding: 20px;
        }
        .container {
            margin-top: 30px;
        }
        h1 {
            text-align: center;
            margin-bottom: 30px;
            font-size: 28px;
            color: #004080;
            font-weight: 700;
        }
        .table-responsive {
            background-color: #ffffff;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            margin-top: 20px;
        }
        .table {
            border-radius: 15px;
            overflow: hidden;
        }
        .table thead {
            background-color: #004080;
            color: white;
            border-radius: 15px 15px 0 0;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
            border: none;
        }
        .btn {
            border: none;
            border-radius: 25px;
            font-size: 14px;
            padding: 10px 20px;
            transition: all 0.3s ease;
        }
        .btn-primary {
            background-color: #0077b6;
            border-color: #0077b6;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-primary:hover {
            background-color: #005f99;
            border-color: #005f99;
        }
        .btn-success {
            background-color: #004080;
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-success:hover {
            background-color: #004080;
            border-color: #1e7e34;
        }
        .btn-danger {
            background-color: #004080;
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-danger:hover {
            background-color: #004080;
            border-color: #004080;
        }
        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .pagination a {
            color: #0077b6;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #dee2e6;
            margin: 0 4px;
            border-radius: 25px;
            transition: background-color 0.3s;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .pagination a.active {
            background-color: #0077b6;
            color: white;
            border: 1px solid #0077b6;
        }
        .pagination a:hover:not(.active) {
            background-color: #dee2e6;
        }
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.5);
            padding-top: 60px;
        }
        .modal-content {
            background-color: #fff;
            margin: 5% auto;
            padding: 20px;
            border: 1px solid #888;
            border-radius: 15px;
            width: 80%;
            transition: all 0.3s ease;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }
        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
        .search-form .input-group {
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            border-radius: 25px;
        }
        .input-group .form-control {
            border-radius: 25px 0 0 25px;
        }
        .input-group .btn {
            border-radius: 0 25px 25px 0;
        }
    </style>
</head>
<body>
<div class="side-nav">
    <div>
        <div class="user-info">
            <img src="${sessionScope.account.userPicture}" alt="User Image" class="user-image">
            <p>Xin chào ${sessionScope.account.userName}!</p>
        </div>
        <a href="#" class="active"><i class="fas fa-user"></i> Quản lí tài khoản</a>
        <a href="#"><i class="fas fa-envelope"></i> Tin nhắn</a>
        <a href="${pageContext.request.contextPath}/pendingPartners"><i class="fas fa-user-friends"></i> Tài khoản đối tác</a>
    </div>
    <div>
        <a href="logout" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
    </div>
</div>
<div class="main-content">
    <div class="container">
        <h1>Quản lí tài khoản</h1>
        <c:if test="${not empty error}">
            <div class="alert alert-danger" role="alert">
                ${error}
            </div>
        </c:if>
        <div class="search-form mb-4">
            <form action="${pageContext.request.contextPath}/manageRoles" method="get">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Tìm kiếm tài khoản..." value="${search}">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Tìm kiếm</button>
                    </div>
                </div>
            </form>
        </div>
        <c:if test="${currentRoleID == 4}">
            <div class="add-member-btn mb-4">
                <button type="button" class="btn btn-success" onclick="history.back()">Quay lại</button>
                <button class="btn btn-success" onclick="showPromoteModal()"><i class="fas fa-user-shield"></i> Thêm người quản lý</button>
                <button class="btn btn-success" onclick="showPromoteModal1()"><i class="fas fa-user-shield"></i> Thêm hướng dẫn viên</button>
            </div>
        </c:if>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th style="background:#004080;">Tên tài khoản</th>
                        <th style="background:#004080;">Email</th>
                        <th style="background:#004080;">Quyền hạn</th>
                        <th style="background:#004080;">Trạng thái hiện tại</th>
                        <th style="background:#004080;">Thời gian truy cập cuối</th>
                        <th style="background:#004080;">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${userList}">
                        <tr>
                            <td>${user.userName}</td>
                            <td>${user.email}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${user.roleID == 5}">
                                        Hướng dẫn viên
                                    </c:when>
                                    <c:when test="${user.roleID == 4}">
                                        Quản trị viên
                                    </c:when>
                                    <c:when test="${user.roleID == 1}">
                                        Quản lý
                                    </c:when>
                                    <c:when test="${user.roleID == 2}">
                                        Người dùng
                                    </c:when>
                                    <c:when test="${user.roleID == 3}">
                                        Đối tác
                                    </c:when>
                                    <c:otherwise>
                                        Tin tức
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <form method="post" action="manageRoles" class="d-inline-block">
                                    <input type="hidden" name="userID" value="${user.userID}">
                                    <input type="hidden" name="action" value="toggleStatus">
                                    <c:if test="${(currentRoleID > 1 && user.roleID <= 5) || (currentRoleID == 1 && user.roleID > 1 && user.roleID < 4)}">
                                        <c:if test="${currentUser.userID != user.userID}">
                                            <label class="toggle-switch">
                                                <input type="checkbox" ${user.status ? 'checked' : ''} onclick="this.form.submit()">
                                                <span class="slider round"></span>
                                            </label>
                                        </c:if>
                                    </c:if>
                                </form>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${not empty lastAccessTimes[user.userID]}">
                                        <fmt:formatDate value="${lastAccessTimes[user.userID]}" pattern="dd-MM-yyyy HH:mm:ss" />
                                    </c:when>
                                    <c:otherwise>
                                        N/A
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${(currentRoleID == 4 && user.roleID != 4) || (currentRoleID == 1 && (user.roleID == 2 || user.roleID == 3 || user.roleID == 5))}">
                                        <c:if test="${currentUser.userID != user.userID}">
                                            <form method="post" action="manageRoles" class="d-inline-block" onsubmit="return confirmDelete()">
                                                <input type="hidden" name="userID" value="${user.userID}">
                                                <input type="hidden" name="action" value="delete">
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fas fa-trash-alt"></i> Xóa
                                                </button>
                                            </form>
                                        </c:if>
                                    </c:when>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="pagination">
            <c:forEach var="i" begin="1" end="${totalPages}">
                <a href="${pageContext.request.contextPath}/manageRoles?page=${i}&search=${search}" class="${i == currentPage ? 'active' : ''}">${i}</a>
            </c:forEach>
        </div>

        <!-- Modal Promote to Admin -->
        <div id="promoteModal" class="modal">
            <div class="modal-content">
                <span class="close" onclick="closePromoteModal()">&times;</span>
                <h2>Thêm người quản lý</h2>
                <form id="promoteForm" method="post" action="manageRoles">
                    <input type="hidden" name="action" value="promoteToAdmin">
                    <div class="form-group">
                        <label for="promoteUserID">Chọn người dùng</label>
                        <select class="form-control" id="promoteUserID" name="promoteUserID" required>
                            <c:forEach var="user" items="${userListWithRole2}">
                                <option value="${user.userID}">
                                    ${user.userName} - ${user.email} - ${user.phoneNumber} - ${user.address}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Thăng chức</button>
                </form>
            </div>
        </div>

        <!-- Modal Promote to Guide -->
        <div id="promoteModal1" class="modal">
            <div class="modal-content">
                <span class="close" onclick="closePromoteModal1()">&times;</span>
                <h2>Thêm hướng dẫn viên</h2>
                <form id="promoteForm1" method="post" action="UpdateRoleServlet">
                    <div class="form-group">
                        <label for="promoteUserID1">Chọn người dùng</label>
                        <select class="form-control" id="promoteUserID1" name="promoteUserID" required>
                            <c:forEach var="user" items="${userListWithRole2}">
                                <option value="${user.userID}">
                                    ${user.userName} - ${user.email} - ${user.phoneNumber} - ${user.address}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Thăng chức</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function showPromoteModal() {
        document.getElementById('promoteModal').style.display = 'block';
    }
    function showPromoteModal1() {
        document.getElementById('promoteModal1').style.display = 'block';
    }
    function closePromoteModal() {
        document.getElementById('promoteModal').style.display = 'none';
    }
    function closePromoteModal1() {
        document.getElementById('promoteModal1').style.display = 'none';
    }
    function confirmDelete() {
        return confirm('Bạn có chắc chắn muốn xóa tài khoản này không?');
    }
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
