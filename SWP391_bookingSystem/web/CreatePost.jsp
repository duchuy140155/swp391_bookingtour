<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tạo mới Bài Đăng</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            max-width: 800px;
            margin: 50px auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin: 10px 0 5px;
            color: #555;
        }
        input[type="text"],
        textarea {
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 4px;
            width: 100%;
            box-sizing: border-box;
            margin-bottom: 15px;
        }
        input[type="submit"] {
            padding: 10px 20px;
            background-color: #28a745;
            border: none;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
        }
        input[type="submit"]:hover {
            background-color: #218838;
        }
        .message {
            text-align: center;
            padding: 10px;
            margin-top: 10px;
            border-radius: 4px;
        }
        .error {
            background-color: #f8d7da;
            color: #721c24;
        }
        .success {
            background-color: #d4edda;
            color: #155724;
        }
        .section {
            border: 1px solid #ddd;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 10px;
            position: relative;
        }
        .add-section,
        .remove-section-common {
            padding: 10px;
            border: none;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
            margin-bottom: 15px;
        }
        .add-section {
            background-color: #007bff;
        }
        .add-section:hover {
            background-color: #0056b3;
        }
        .remove-section-common {
            background-color: #dc3545;
        }
        .remove-section-common:hover {
            background-color: #c82333;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="back-button">
            <button onclick="window.location.href = 'listPost'" class="btn">Trở về quản lý bài đăng</button>
        </div>
        <h1>Tạo mới Bài Đăng</h1>
        <c:if test="${not empty message}">
            <p class="message ${message.startsWith('Failed') ? 'error' : 'success'}">${message}</p>
        </c:if>
        <form action="createpost" method="post" enctype="multipart/form-data">
            <label for="postTitle">Tiêu đề:</label>
            <input type="text" id="postTitle" name="postTitle" required minlength="10" maxlength="70"
                   pattern="[a-zA-Z0-9\s-]+" title="Tiêu đề bài đăng phải từ 10 đến 70 ký tự và không được chứa ký tự đặc biệt ngoại trừ dấu gạch ngang.">

            <label for="imageDescription">Mô tả Ảnh:</label>
            <input type="text" id="imageDescription" name="imageDescription" required>

            <div id="sectionsContainer">
                <label>Nội dung:</label>
                <div class="section" id="section-1">
                    <label for="sectionTitle1">Tiêu đề section 1:</label>
                    <input type="text" id="sectionTitle1" name="sectionTitle1" required>
                    <label for="sectionContent1">Nội dung section 1:</label>
                    <textarea id="sectionContent1" name="sectionContent1" required></textarea>
                </div>
            </div>
            <button type="button" class="add-section" onclick="addSection()">Thêm Section</button>
            <button type="button" class="remove-section-common" onclick="removeLastSection()">Xóa Section Cuối</button>

            <input type="submit" value="Tạo Bài Đăng">
        </form>
    </div>

    <script>
        let sectionCount = 1;

        function addSection() {
            if (sectionCount < 3) {
                sectionCount++;
                const sectionsContainer = document.getElementById('sectionsContainer');
                const newSection = document.createElement('div');
                newSection.className = 'section';
                newSection.id = 'section-' + sectionCount;
                newSection.innerHTML = `
                    <label for="sectionTitle${sectionCount}">Tiêu đề section ${sectionCount}:</label>
                    <input type="text" id="sectionTitle${sectionCount}" name="sectionTitle${sectionCount}" required>
                    <label for="sectionContent${sectionCount}">Nội dung section ${sectionCount}:</label>
                    <textarea id="sectionContent${sectionCount}" name="sectionContent${sectionCount}" required></textarea>
                `;
                sectionsContainer.appendChild(newSection);
            } else {
                alert('Tối đa 3 section');
            }
        }

        function removeLastSection() {
            if (sectionCount > 1) {
                const sectionsContainer = document.getElementById('sectionsContainer');
                sectionsContainer.removeChild(sectionsContainer.lastChild);
                sectionCount--;
            } else {
                alert('Phải có ít nhất một section');
            }
        }
    </script>
</body>
</html>
