<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Manager</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link href="../css/manager.css" rel="stylesheet" type="text/css">
</head>
<style>
    body {
        background: #f5f5f5;
        font-family: 'Varela Round', sans-serif;
        font-size: 18px;
    }

    .header__top__left {
        display: flex;
        justify-content: flex-start;
        padding: 10px 0 13px;
    }

    .header__top__left__back-button {
        text-align: left;
        text-decoration: none;
        color: #000;
        padding: 10px;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 5px;
    }

    .header__top__left__back-button:hover {
        background-color: #f0f0f0;
        text-decoration: none;
    }

    .header__top__right {
        display: flex;
        justify-content: flex-end;
        padding: 10px 0 13px;
        text-align: right;
    }

    .header__top__right__auth {
        display: inline-block;
        padding: 5px;
        position: relative;
        text-decoration: none;
    }

    .header__top__right__auth a {
        display: block;
        font-size: 22px;
        color: #1c1c1c;
    }

    .header__top__right__auth a i {
        margin-right: 6px;
    }

    .header__top__right__auth .dropdown-menu {
        position: absolute;
        top: 100%;
        left: -50px;
        display: none;
        background-color: #fff;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

    .header__top__right__auth:hover .dropdown-menu {
        display: block;
    }

    .header__top__right__auth .dropdown-menu a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .header__top__right__auth .dropdown-menu a:hover {
        background-color: #ddd;
    }

    .table-responsive {
        margin: 20px 0;
    }

    .table-wrapper {
        background: #fff;
        padding: 20px 25px;
        border-radius: 3px;
        width: 100%;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
    }

    .table-scroll {
        max-width: 100%;
        overflow-x: auto;
    }

    .table-title {
        padding-bottom: 15px;
        color: black;
        padding: 16px 30px;
        min-width: 100%;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }

    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }

    .table-title .btn-group {
        float: right;
        background: #7fad39;
    }

    .table-title .btn-info {
        background-color: #7fad39;
    }

    .table-title .btn {
        color: #fff;
        float: right;
        font-size: 13px;
        border: none;
        min-width: 50px;
        border-radius: 2px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }

    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }

    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }

    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }

    table.table tr th:first-child {
        width: 60px;
    }

    table.table tr th:last-child {
        width: 100px;
    }

    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }

    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }

    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }

    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }

    table.table td a {
        font-weight: bold;
        color: #7fad39;
        display: inline-block;
        text-decoration: none;
        outline: none !important;
    }

    table.table td a:hover {
        color: #2196F3;
    }

    table.table td a.edit {
        color: #FFC107;
    }

    table.table td a.delete {
        color: #F44336;
    }

    table.table td i {
        font-size: 19px;
    }

    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }

    /* Modal styles */
    .modal .modal-dialog {
        max-width: 500px;
        margin: 0 auto;
    }

    .modal .modal-header, .modal .modal-body, .modal .modal-footer {
        padding: 20px;
        color: black;
    }

    .modal .modal-content {
        border-radius: 3px;
        font-size: 14px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    }

    .modal .modal-footer {
        background: #f5f5f5;
        padding: 10px;
        border-radius: 0 0 3px 3px;
    }

    .modal-footer .btn-info {
        width: 100%;
        background: #7fad39;
    }

    .modal .modal-title {
        font-size: 18px;
        display: inline-block;
    }

    .modal .form-control {
        border-radius: 5px;
        box-shadow: none;
        border-color: #dddddd;
    }

    .modal textarea.form-control {
        resize: vertical;
    }

    .modal .btn {
        border-radius: 5px;
        min-width: 100px;
    }

    .modal form label {
        font-weight: bold;
    }

    table.modal-body {
        width: 100%;
        border-collapse: collapse;
    }

    table.modal-body tr {
        border-bottom: 1px solid #ddd;
    }

    table.modal-body td {
        padding: 10px;
    }

    .button {
        text-align: center;
        display: inline-block;
        padding: 5px;
        border: 1px solid #ccc;
        margin: 5px;
        background-color: #f5f5f5;
        transition: background-color 0.3s, border-color 0.3s, color 0.3s;
    }

    .button a {
        text-decoration: none;
        color: black;
    }

    .button:hover {
        background-color: #D9FAF4;
        border-color: #0077b6;
        color: #fff;
    }

    .active {
        background-color: #D9FAF4;
        border-color: #0077b6;
        color: white;
    }

    .button-paging {
        display: flex;
        justify-content: space-around;
        align-items: center;
    }

    .small-button {
        padding: 2px 5px;
        font-size: 12px;
    }
</style>

<body>

   

    <div class="header__top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <a href="javascript:history.back()" class="header__top__left__back-button">
                            <i class="fa fa-home"></i> Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
        <div class="container-fluid">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2>Manager <b>Hotels</b></h2>
                                <h6 style="color: red;">${mess1}</h6>
                                <h6 style="color: red;">${mess2}</h6>
                                <h6 style="color: red;">${mess3}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <c:set value="${sessionScope.hotelbyid}" var="p"/>

        <!-- Edit Modal HTML -->
        <div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="edithotel" method="post" onsubmit="return validateInput()">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Hotels</h4>
                        </div>
                        <table class="modal-body">
                            <tr>
                                <td>ID Khách Sạn</td>
                                <td><input value='${p.hotel_id}' name="id" type="text" class="form-control" readonly></td>
                            </tr>
                            <tr>
                                <td>Tên Khách Sạn</td>
                                <td><input value='${p.name}' name="name" type="text" class="form-control" required></td>
                            </tr>
                            <tr>
                                <td>Địa Chỉ Khách Sạn</td>
                                <td><input value='${p.address}' name="address" type="text" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Vị Trí</td>
                                <td><input value='${p.city}' name="city" type="text" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Liên Hệ</td>
                                <td><input value='${p.phone}' name="phone" type="text" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Email Khách Sạn</td>
                                <td><input value='${p.email}' name="email" type="text" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Website Khách Sạn</td>
                                <td>
                                    <input id="website" value='${p.website}' name="website" type="text" class="form-control" readonly>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-secondary small-button" onclick="enableWebsiteEdit()">Edit Website</button>
                                </td>
                            </tr>
                            <tr>
                                <td>URL Ảnh Khách Sạn</td>
                                <td>
                                    <input id="image" value='${p.image}' name="image" type="text" class="form-control" readonly>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-secondary small-button" onclick="enableImageEdit()">Edit Image</button>
                                </td>
                            </tr>
                        </table>
                        <div class="modal-footer">
                            <input type="submit" name="save" class="btn btn-info" value="Save" style="background-color: blue">
                        </div>
                    </form>
                </div>
            </div>
        </div>
   

    <script type="text/javascript">
        function enableWebsiteEdit() {
            document.getElementById('website').removeAttribute('readonly');
        }

        function enableImageEdit() {
            document.getElementById('image').removeAttribute('readonly');
        }

        function validateInput() {
            // Add your validation logic here
            return true;
        }
    </script>
</body>
</html>
