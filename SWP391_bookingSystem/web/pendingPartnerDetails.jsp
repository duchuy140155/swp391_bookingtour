<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quản lý tài khoản</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #e0f7fa; /* Light cyan background */
            font-family: 'Candara', sans-serif;
        }
        .side-nav {
            background: linear-gradient(to bottom, #0066cc, #0099ff); /* Gradient blue */
            color: white;
            padding: 20px;
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            position: fixed;
            width: 270px;
            border-radius: 0 15px 15px 0;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .user-info {
            text-align: center;
            margin-bottom: 20px;
        }
        .user-image {
            border-radius: 50%;
            width: 60px;
            height: 60px;
            object-fit: cover;
            margin-bottom: 10px;
            border: 2px solid #ffffff;
        }
        .user-info p {
            font-size: 18px;
            font-weight: bold;
            margin: 0;
        }
        .side-nav a {
            color: white;
            text-decoration: none;
            padding: 10px 20px;
            transition: background-color 0.3s;
            position: relative;
            display: block;
            margin-bottom: 10px;
            border-radius: 25px;
            text-align: center;
        }
        .side-nav a:hover {
            background-color: #0059b3; /* Darker blue */
        }
        .side-nav .active {
            background-color: #004080; /* Darkest blue */
            font-weight: bold;
        }
        .notification {
            background-color: red;
            color: white;
            border-radius: 50%;
            padding: 2px 8px;
            font-size: 12px;
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .main-content {
            margin-left: 290px;
            padding: 20px;
        }
        .container {
            margin-top: 30px;
        }
        h1 {
            text-align: center;
            margin-bottom: 30px;
            font-size: 28px;
            color: #004080; /* Navy blue */
            font-weight: 700;
        }
        .table-responsive {
            background-color: #ffffff;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            margin-top: 20px;
        }
        .table {
            border-radius: 15px;
            overflow: hidden;
        }
        .table thead {
            background-color: #004080; /* Navy blue */
            color: white;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
            border: none;
        }
        .btn {
            border: none;
            border-radius: 25px;
            font-size: 14px;
            padding: 10px 20px;
            transition: all 0.3s ease;
        }
        .btn-primary {
            background-color: #004080; /* Navy blue */
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-primary:hover {
            background-color: #003366; /* Darker blue */
            border-color: #003366;
        }
        .btn-success {
            background-color: #004080; /* Navy blue */
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-success:hover {
            background-color: #003366; /* Darker blue */
            border-color: #003366;
        }
        .btn-danger {
            background-color: #004080; /* Navy blue */
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-danger:hover {
            background-color: #003366; /* Darker blue */
            border-color: #003366;
        }
        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .pagination a {
            color: #004080; /* Navy blue */
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #dee2e6;
            margin: 0 4px;
            border-radius: 25px;
            transition: background-color 0.3s;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .pagination a.active {
            background-color: #004080; /* Navy blue */
            color: white;
            border: 1px solid #004080;
        }
        .pagination a:hover:not(.active) {
            background-color: #dee2e6;
        }
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.5);
            padding-top: 60px;
        }
        .modal-content {
            background-color: #fff;
            margin: 5% auto;
            padding: 20px;
            border: 1px solid #888;
            border-radius: 15px;
            width: 80%;
            transition: all 0.3s ease;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }
        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
        .search-form .input-group {
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            border-radius: 25px;
        }
        .input-group .form-control {
            border-radius: 25px 0 0 25px;
        }
        .input-group .btn {
            border-radius: 0 25px 25px 0;
        }
    </style>
</head>
<body>
    <div class="side-nav">
        <div>
            <div class="user-info">
                <img src="${sessionScope.account.userPicture}" alt="User Image" class="user-image">
                <p>Xin chào ${sessionScope.account.userName}!</p>
            </div>
            <a href="manageRoles" class="active"><i class="fas fa-user"></i> Quản lí tài khoản</a>
            <a href="#"><i class="fas fa-envelope"></i> Tin nhắn</a>
            <a href="pendingPartners"><i class="fas fa-user-friends"></i> Tài khoản đối tác</a>
        </div>
        <div>
            <a href="logout" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
        </div>
    </div>
    <div class="main-content">
        <div class="container">
            <h1>Danh sách đối tác chưa kích hoạt tài khoản</h1>
            <c:if test="${not empty error}">
                <div class="alert alert-danger" role="alert">
                    ${error}
                </div>
            </c:if>
            <div class="search-form mb-4">
                <form action="${pageContext.request.contextPath}/pendingPartners" method="get">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm tài khoản..." value="${search}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Tìm kiếm</button>
                        </div>
                        
                    </div>
                         <a href="manageRoles" class="btn btn-danger">
                <i class="fas fa-sign-out-alt"></i> Quay lại
            </a>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th style="background:#004080;">Tên tài khoản</th>
                            <th style="background:#004080;">Email</th>
                            <th style="background:#004080;">Trạng Thái</th>
                            <th style="background:#004080;">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="partner" items="${pendingPartners}">
                            <tr>
                                <td>${partner.partnerName}</td>
                                <td>${partner.email}</td>
                                <td>Chưa kích hoạt</td>
                                <td>
                                    <a href="pendingPartnerDetails?partnerID=${partner.partnerID}" class="btn btn-primary">Xem chi tiết</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="pagination">
                <c:forEach var="i" begin="1" end="${totalPages}">
                    <a href="${pageContext.request.contextPath}/pendingPartners?page=${i}&search=${search}" class="${i == currentPage ? 'active' : ''}">${i}</a>
                </c:forEach>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
