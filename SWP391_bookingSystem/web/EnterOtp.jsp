<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Enter OTP</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <style type="text/css">
            .form-gap {
                padding-top: 70px;
            }
            .modal-centered {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100%;
            }
            .custom-modal {
                width: 600px;
            }
            .modal-body {
                height: 150px;
                font-size: 18px; /* Tăng kích thước phông chữ */
            }
            .modal-title {
                font-size: 24px; /* Tăng kích thước tiêu đề */
            }
            .modal-footer .btn-primary {
                font-size: 18px; /* Tăng kích thước nút */
            }
        </style>
    </head>
    <body>
        <div class="form-gap"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <h3>
                                    <i class="fa fa-lock fa-4x"></i>
                                </h3>
                                <h2 class="text-center">Nhập mã OTP</h2>
                                <%
                                    String message = (String) request.getAttribute("message");
                                    if (message != null && !"OTP has expired".equals(message)) {
                                        out.print("<p class='text-danger ml-1'>" + message + "</p>");
                                    }
                                %>
                                <div class="panel-body">
                                    <form id="register-form" action="validateOtp" role="form" autocomplete="off"
                                          class="form" method="post">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <input id="otp" name="otp" placeholder="Enter OTP" class="form-control" type="text" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Kiểm tra OTP" type="submit">
                                        </div>
                                        <input type="hidden" name="action" value="validate">
                                        <input type="hidden" class="hide" name="token" id="token" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="otpModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-centered custom-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Notification</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="modalMessage"><%= message %></p>
                    </div>
                    <div class="modal-footer">
                        <a href="forgotPassword.jsp" class="btn btn-primary">OK</a>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                var message = '<%= message %>';
                if (message === "OTP has expired") {
                    $('#otpModal').modal('show');
                }
            });
        </script>
    </body>
</html>
