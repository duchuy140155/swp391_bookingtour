<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thanh toán</title>
        <link href="css/payment.css" rel="stylesheet" type="text/css"/>
        <script src="js/payment.js" type="text/javascript"></script>
        <script>
            function setPaymentMethodAction() {
                const paymentMethod = document.querySelector('input[name="paymentMethod"]:checked').value;
                const form = document.getElementById('paymentForm');
                if (paymentMethod === 'vnpay') {
                    form.action = '${pageContext.request.contextPath}/vnpay';
                } else {
                    form.action = '${pageContext.request.contextPath}/confirm';
                }
            }

            function showCashMessage() {
                const paymentMethod = document.querySelector('input[name="paymentMethod"]:checked').value;
                const cashMessage = document.getElementById('cashMessage');
                if (paymentMethod === 'cash') {
                    cashMessage.style.display = 'block';
                } else {
                    cashMessage.style.display = 'none';
                }
            }

            function handlePaymentMethodChange() {
                showCashMessage();
                setPaymentMethodAction();
            }
        </script>
    </head>
    <body>
        <div class="container">
            <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
            <div class="payment-section">
                <form method="post" id="paymentForm">
                    <input type="hidden" name="tourID" value="${tour.tourID}">
                    <input type="hidden" name="bookingID" value="${bookingID}">
                    <input type="hidden" name="customerName" value="${bookingDetails.customerName}">
                    <input type="hidden" name="customerEmail" value="${bookingDetails.email}">
                    <input type="hidden" name="customerPhone" value="${bookingDetails.phoneNumber}">
                    <input type="hidden" name="customerAddress" value="${bookingDetails.customerAddress}">
                    <input type="hidden" name="totalPeople" value="${bookingDetails.totalPeople}">
                    <input type="hidden" name="totalPrice" value="${bookingDetails.finalPrice}">
                    <div class="payment-form">
                        <h2>Thông tin Thanh toán</h2>
                        <div>
                            <h3>Các hình thức thanh toán</h3>
                            <label><input type="radio" name="paymentMethod" value="cash" onchange="handlePaymentMethodChange()"> Tiền mặt</label><br>
                            <label><input type="radio" name="paymentMethod" value="vnpay" onchange="handlePaymentMethodChange()"> Thanh toán VNPAY</label><br>
                            <label><input type="radio" name="paymentMethod" value="momo" onchange="handlePaymentMethodChange()"> Thanh toán bằng Momo</label><br>
                            <label><input type="radio" name="paymentMethod" value="bankTransfer" onchange="handlePaymentMethodChange()"> Chuyển khoản</label><br>
                            <label><input type="radio" name="paymentMethod" value="mbbank" onchange="handlePaymentMethodChange()"> Thanh toán bằng MBBank</label><br>
                            <label><input type="radio" name="paymentMethod" value="paypal" onchange="handlePaymentMethodChange()"> Thanh toán bằng PayPal</label><br>
                            <label><input type="radio" name="paymentMethod" value="creditCard" onchange="handlePaymentMethodChange()"> Thẻ tín dụng</label><br>
                        </div>
                        <div id="cashMessage" style="display: none;">
                            <p>Vui lòng đến các quầy của Booking Tour để nộp tiền</p>
                        </div>
                        <div>
                            <h3>Điều khoản bắt buộc khi đăng ký online</h3>
                            <div class="terms-container">
                                <h2>ĐIỀU KIỆN BÁN VÉ CÁC CHƯƠNG TRÌNH DU LỊCH TRONG NƯỚC</h2>
                                <div class="terms-content">
                                    <pre>
ĐIỀU KIỆN BÁN VÉ CÁC CHƯƠNG TRÌNH DU LỊCH TRONG NƯỚC
                                    (nội dung điều khoản...)
                                    </pre>
                                </div>
                            </div>
                            <label><input type="checkbox" name="terms" required> Tôi đồng ý với các điều kiện trên</label><br>
                        </div>
                    </div>
                    <div class="summary-section">
                        <p><strong>Tên chuyến đi:</strong> ${tour.tourName}</p>
                        <p><strong>Ngày đi:</strong> <fmt:formatDate value="${tour.startDate}" pattern="dd-MM-yyyy" /></p>
                        <p><strong>Ngày về:</strong> <fmt:formatDate value="${tour.endDate}" pattern="dd-MM-yyyy" /></p>
                        <p><strong>Số lượng người:</strong> ${bookingDetails.totalPeople} người</p>
                        <p><strong>Giá:</strong> <fmt:formatNumber value="${bookingDetails.finalPrice}" type="number" minFractionDigits="0" maxFractionDigits="0"/> VND</p>
                        <button type="submit" onclick="setPaymentMethodAction()">Đặt Tour</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
