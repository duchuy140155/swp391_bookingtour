<%-- 
    Document   : editLocation
    Created on : May 21, 2024, 6:26:05 PM
    Author     : Admin
--%>

<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Chỉnh sửa Địa điểm</title>
        <link rel="stylesheet" type="text/css" href="css/editLocation.css">
    </head>
    <body>
        <div class="container">
            <div class="header-container">
                <button class="back-button" onclick="window.location.href = 'locations'">Back to Locations</button>
                <h1>Chỉnh sửa Địa điểm</h1>
            </div>
            <form action="locations" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="update">
                <input type="hidden" name="locationId" value="${location.locationId}">

                <label for="locationName">Tên Địa điểm:</label>
                <input type="text" name="locationName" id="locationName" value="${location.locationName}" required>

                <label for="locationDescription">Mô tả:</label>
                <textarea name="locationDescription" id="locationDescription" required>${location.locationDescription}</textarea>

                <label for="locationImage">Hình ảnh:</label>
                <input type="file" name="locationImage" id="locationImage">
                <img src="uploads/${location.locationImage}" alt="${location.locationName}">

                <button type="submit">Cập nhật</button>
            </form>
        </div>
    </body>
</html>


