<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Đặt Tour</title>
        <link href="css/booking.css" rel="stylesheet" type="text/css"/>
        <script>
            // Chuyển danh sách vé từ JSP vào JavaScript
            var listTickets = [];
            <c:forEach items="${listTicket}" var="ticket">
            listTickets.push({
                typeID: '${ticket.tickettype.typeID}',
                typeName: '${ticket.tickettype.typeName}',
                priceTicket: '${ticket.priceTicket}',
                ageMin: '${ticket.tickettype.ageMin}',
                ageMax: '${ticket.tickettype.ageMax}'
            });
            </c:forEach>
            // Chuyển giá tour từ JSP vào JavaScript
            var tourPrice = ${tour.price};
            var remainsTicket = ${remainsTicket};
        </script>
        <script src="js/booking.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
            <c:if test="${not empty errorMessage}">
                <div class="error-message" style="color: red;">
                    ${errorMessage}
                </div>
            </c:if>
            <div class="tour-details">
                <img src="${tour.thumbnails}" alt="Tour Image" class="tour-image">
                <div class="tour-info">
                    <h2>Thông tin Tour du lịch</h2>
                    <p><strong>Tên Tour:</strong> ${tour.tourName}</p>
                    <p><strong>Mô tả:</strong> ${tour.tourDescription}</p>
                    <p><strong>Điểm đi:</strong> ${tour.startLocation}</p>
                    <p><strong>Điểm đến:</strong> ${tour.endLocation}</p>
                    <p><strong>Ngày đi:</strong> <fmt:formatDate value="${tour.startDate}" pattern="dd-MM-yyyy" /></p>
                    <p><strong>Ngày về:</strong> <fmt:formatDate value="${tour.endDate}" pattern="dd-MM-yyyy" /></p>
                    <p><strong>Giá:</strong> <fmt:formatNumber value="${tour.price}" type="number" minFractionDigits="0" maxFractionDigits="0"/> VND</p>
                    <p><strong>Số chỗ còn nhận: </strong>${remainsTicket}</p>
                </div>
            </div>
            <div class="booking-section">
                <form id="bookingForm" method="post" action="booking">
                    <div class="booking-form">
                        <h2>Tổng quan về chuyến đi</h2>
                        <input type="hidden" name="tourID" value="${tour.tourID}">
                        <div id="customerForm">
                            <h3>Thông tin khách hàng</h3>
                            <label>Họ và Tên:</label>
                            <input type="text" id="customerName" name="customerName" required><br><br>

                            <label>Email:</label>
                            <input type="email" id="customerEmail" name="customerEmail" required><br><br>

                            <label>Số điện thoại:</label>
                            <input type="text" id="customerPhone" name="customerPhone" required><br><br>

                            <label>Địa chỉ:</label>
                            <input type="text" id="customerAddress" name="customerAddress" required><br><br>
                        </div>
                        <div class="checkbox-group">
                            <div class="checkbox-item">
                                <input type="radio" id="customerListCheckbox" name="option" value="customerList" onclick="toggleForm()" checked>
                                <label for="customerListCheckbox">Nhập danh sách khách hàng</label>
                            </div>
                            <div class="checkbox-item">
                                <input type="radio" id="consultationCheckbox" name="option" value="consultation" onclick="toggleForm()">
                                <label for="consultationCheckbox">Tôi cần được nhân viên tư vấn</label>
                            </div>
                        </div>
                        <div id="passengerSection">
                            <h3>Hành khách</h3>
                            <c:forEach items="${listTicket}" var="listTicket">
                                <div class="passenger">
                                    <label>${listTicket.tickettype.typeName}:</label>
                                    <button type="button" onclick="decrement('${listTicket.typeID}', '${listTicket.tickettype.typeName}')">-</button>
                                    <input type="text" id="${listTicket.typeID}Count" value="${listTicket.typeID == 1 ? 1 : 0}" readonly>
                                    <button type="button" onclick="increment('${listTicket.typeID}', '${listTicket.tickettype.typeName}')">+</button>
                                </div>
                            </c:forEach>
                            <div id="infantDetails"></div>
                        </div>

                        <div id="consultationNote" style="display: none;">
                            <h3>Quý khách có ghi chú lưu ý gì, hãy nói với chúng tôi!</h3>
                            <textarea id="note" name="note" placeholder="Vui lòng nhập nội dung bằng tiếng Anh hoặc tiếng Việt"></textarea>
                        </div>
                    </div>
                    <div class="summary-section">
                        <h2>Tóm tắt chuyến đi</h2>
                        <p><strong>Tên chuyến đi:</strong> ${tour.tourName}</p>
                        <p><strong>Ngày đi:</strong> <fmt:formatDate value="${tour.startDate}" pattern="dd-MM-yyyy" /></p>
                        <p><strong>Ngày về:</strong> <fmt:formatDate value="${tour.endDate}" pattern="dd-MM-yyyy" /></p>
                        <p><strong>Số lượng người:</strong> <span id="totalPeople"></span></p>
                        <p><strong>Giá:<br></strong> <span id="totalPriceDisplay"></span></p>
                        Kho Mã Giảm Giá : <a href="discountcontroller">Ấn để lấy mã</a><br>
                        Kho Lucky Discount : <a href="getluckydiscountforuser">Ấn để lấy mã</a>

                        <div class="discount-section">

                            <label for="discountCode">Mã giảm giá</label>
                            <input type="text" id="discountCode" name="discountCode">
                        </div>
                            ${sessionScope.err}<br>
                        <c:if test="${sessionScope.price != null}">
                            Price : ${sessionScope.price}
                        </c:if><br>

                        <div id="hiddenFieldsContainer">
                            <input type="hidden" name="totalPeople" id="hiddenTotalPeople">
                            <input type="hidden" name="totalPrice" id="hiddenTotalPrice">
                            <input type="hidden" name="typeIDs" id="hiddenTypeIDs">
                        </div>
                        <button type="button" onclick="checkDiscount()" >Check Discount</button>
                        <button type="submit">Đặt Tour</button>
                    </div>
                </form>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <p id="modalText"></p>
                </div>
            </div>
        </div>
                        <script>
    function applyDiscount() {
        var reduce = "${sessionScope.loadDiscountById.discountReduce}";
        var percent = "${sessionScope.loadDiscountById.discountPercentage}";
        window.location.href = "calculatemoneydiscount?reduce=" + reduce + "&percent=" + percent;
    }
    
     function checkDiscount() {
                var discountCode = document.getElementById("discountCode").value;
                  var url = "checkdiscount?discountCode=" + discountCode;
                window.location.href = url;
                }
</script>
    </body>
</html>
