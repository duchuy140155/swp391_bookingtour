<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách hủy tour</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <style>
        body {
            background-color: #e0f7fa;
            font-family: 'Roboto', sans-serif;
            color: #004d40;
            margin: 0;
            padding: 0;
        }
        .container {
            margin-top: 30px;
            padding: 20px;
        }
        .custom-header {
            background-color: #00796b;
            color: white;
            padding: 20px;
            border-radius: 12px;
            text-align: center;
            margin-bottom: 20px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        .back-button, .history-button {
            background-color: #00796b;
            color: white;
            border: none;
                       padding: 10px 20px;
            border-radius: 8px;
            cursor: pointer;
            transition: background-color 0.3s, box-shadow 0.3s;
            margin-bottom: 20px;
            display: inline-block;
            text-decoration: none;
            font-size: 16px;
        }
        .back-button:hover, .history-button:hover {
            background-color: #004d40;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            text-decoration: none;
            color: white;
        }
        .table-container {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 12px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        .table {
            margin: 0;
            padding: 0;
            background-color: #fff;
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
            padding: 12px;
            border: none;
            font-size: 14px;
        }
        .table th {
            background-color: #00796b;
            color: white;
            border-radius: 12px 12px 0 0;
        }
        .table td {
            background-color: #ffffff;
            border-bottom: 1px solid #dee2e6;
            border-radius: 0 0 12px 12px;
        }
        .btn-confirm {
            background-color: #dc3545;
            color: white;
            border: none;
            padding: 5px 10px;
            cursor: pointer;
            border-radius: 5px;
            transition: background-color 0.3s, box-shadow 0.3s;
            font-size: 14px;
        }
        .btn-confirm:hover {
            background-color: #c82333;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
        .icon {
            margin-right: 8px;
        }
    </style>
</head>
<body>
    <div class="container">
        <a href="PartnerInfo" class="back-button"><i class="fas fa-arrow-left icon"></i>Quay lại</a>
        <a href="PartnerCancelHistory" class="history-button"><i class="fas fa-history icon"></i>Lịch sử hủy tour</a>
        <div class="custom-header">
            <h2><i class="fas fa-list-alt icon"></i>Danh sách hủy tour</h2>
        </div>
        <div class="table-container">
            <table class="table">
                <thead>
                    <tr>
                        <th><i class="fas fa-tag icon"></i>Tên tour</th>
                        <th><i class="fas fa-calendar-alt icon"></i>Ngày đi</th>
                        <th><i class="fas fa-calendar-alt icon"></i>Ngày đến</th>
                        <th><i class="fas fa-calendar-times icon"></i>Ngày hủy</th>
                        <th><i class="fas fa-exclamation-circle icon"></i>Lý do hủy</th>
                        <th><i class="fas fa-percentage icon"></i>Tỷ lệ phạt</th>
                        <th><i class="fas fa-money-bill-wave icon"></i>Số tiền hoàn lại</th>
                        <th><i class="fas fa-info-circle icon"></i>Trạng thái</th>
                        <th><i class="fas fa-cogs icon"></i>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="cancel" items="${cancelledBookings}">
                        <tr>
                            <td>${cancel.tourName}</td>
                            <td><fmt:formatDate value="${cancel.startDate}" pattern="yyyy-MM-dd"/></td>
                            <td><fmt:formatDate value="${cancel.endDate}" pattern="yyyy-MM-dd"/></td>
                            <td><fmt:formatDate value="${cancel.cancelDate}" pattern="yyyy-MM-dd"/></td>
                            <td>${cancel.cancelReason}</td>
                            <td><fmt:formatNumber value="${cancel.penaltyPercentage * 100}" pattern="#0.##"/>%</td>
                            <td><fmt:formatNumber value="${cancel.refundAmount}" type="currency" pattern="#,### đ"/></td>
                            <td>${cancel.status == 0 ? 'Đang xử lý' : 'Đã hủy'}</td>
                            <td>
                                <c:if test="${cancel.status == 0}">
                                    <form action="ConfirmCancelServlet" method="post">
                                        <input type="hidden" name="cancelID" value="${cancel.cancelID}"/>
                                        <input type="hidden" name="bookingID" value="${cancel.bookingID}"/>
                                        <button type="submit" class="btn-confirm"><i class="fas fa-check icon"></i>Xác nhận hủy</button>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

