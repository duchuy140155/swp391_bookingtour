<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Xác nhận hủy tour</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="custom-header">
                <h2>Xác nhận hủy tour</h2>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmCancelModalLabel">Xác nhận hủy</h5>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn hủy tour này?</p>
                    <p>Lý do hủy: ${cancelReason}</p>
                    <p>Tỷ lệ phạt: ${penaltyPercentage}%</p>
                    <p>Số tiền hoàn lại: ${refundAmount}</p>
                    <p>Bạn hủy tour trước ${daysUntilStart} ngày, nên bạn sẽ chịu phạt ${penaltyPercentage}% và chúng tôi sẽ hoàn lại ${refundAmount} cho bạn.</p>
                </div>
                <div class="modal-footer">
                    <form id="confirmCancelForm" action="ProcessCancelBookingServlet" method="post">
                        <input type="hidden" name="bookingID" value="${bookingID}">
                        <input type="hidden" name="penaltyPercentage" value="${penaltyPercentage}">
                        <input type="hidden" name="refundAmount" value="${refundAmount}">
                        <button type="submit" class="btn btn-danger">Xác nhận hủy</button>
                    </form>
                    <button type="button" class="btn btn-secondary" onclick="window.history.back()">Đóng</button>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
</html>
