<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Travela - Contact Us</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0"><i class="fa fa-map-marker-alt me-3"></i>Travela</h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="index.html" class="nav-item nav-link">Home</a>
                        <a href="about.html" class="nav-item nav-link">About</a>
                        <a href="services.html" class="nav-item nav-link">Services</a>
                        <a href="packages.html" class="nav-item nav-link">Packages</a>
                        <a href="blog.html" class="nav-item nav-link">Blog</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                            <div class="dropdown-menu m-0">
                                <a href="destination.html" class="dropdown-item">Destination</a>
                                <a href="tour.html" class="dropdown-item">Explore Tour</a>
                                <a href="booking.html" class="dropdown-item">Travel Booking</a>
                                <a href="gallery.html" class="dropdown-item">Our Gallery</a>
                                <a href="guides.html" class="dropdown-item">Travel Guides</a>
                                <a href="testimonial.html" class="dropdown-item">Testimonial</a>
                                <a href="404.html" class="dropdown-item">404 Page</a>
                            </div>
                        </div>
                        <a href="contact.html" class="nav-item nav-link active">Contact</a>
                    </div>
                    <a href="" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4">Book Now</a>
                </div>
            </nav>
        </div>

        <div class="container-fluid bg-breadcrumb">
            <div class="container text-center py-5" style="max-width: 900px;">
                <h3 class="text-white display-3 mb-4">Liên Hệ Với Chúng Tôi</h1>

            </div>
        </div>

        <div class="container-fluid contact bg-light py-5">
            <div class="container py-5">
                <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                    <h5 class="section-title px-3">Contact Us</h5>
                    <h1 class="mb-0">Liên hệ với chúng tôi</h1>
                </div>
                <div class="row g-5 align-items-center">
                    <div class="col-lg-4">
                        <div class="bg-white rounded p-4">
                            <div class="text-center mb-4">
                                <i class="fa fa-map-marker-alt fa-3x text-primary"></i>
                                <h4 class="text-primary">Địa chỉ</h4>
                                <p class="mb-0">Hà Nội, Việt Nam</p>
                            </div>
                            <div class="text-center mb-4">
                                <i class="fa fa-phone-alt fa-3x text-primary mb-3"></i>
                                <h4 class="text-primary">Điện thoại</h4>
                                <p class="mb-0">+84 123 456 789</p>
                            </div>
                            <div class="text-center">
                                <i class="fa fa-envelope-open fa-3x text-primary mb-3"></i>
                                <h4 class="text-primary">Email</h4>
                                <p class="mb-0">info@example.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <h3 class="mb-2">Gửi tin nhắn cho chúng tôi</h3>
                        <form action="sendEmail" method="post" onsubmit="return validateForm();">
                            <input type="hidden" name="tourID" id="tourID" value="${param.tourID}">
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control border-0" id="name" name="name" placeholder="Tên của bạn">
                                        <label for="name">Tên của bạn</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="email" class="form-control border-0" id="email" name="email" placeholder="Email của bạn">
                                        <label for="email">Email của bạn</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control border-0" id="phone" name="phone" placeholder="Số điện thoại">
                                        <label for="phone">Số điện thoại</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control border-0" id="subject" name="subject" placeholder="Chủ đề">
                                        <label for="subject">Chủ đề</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <textarea class="form-control border-0" placeholder="Nội dung tin nhắn" id="message" name="message" style="height: 160px"></textarea>
                                        <label for="message">Nội dung tin nhắn</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-primary w-100 py-3" type="submit">Gửi tin nhắn</button>
                                </div>
                            </div>
                            <div class="chatbot-icon">
                                <img width="60px" style="border-radius: 50%;" src="https://img.pikbest.com/png-images/qiantu/cartoon-consulting-service-staff-banner-vector-download_2518761.png!w700wp" alt="ChatBot" onclick="openChat()">
                            </div>

                            <div id="chatbox" class="chatbox">
                                <div class="chatbox-header">
                                    <h2>Tư vấn viên cà rốt</h2>
                                    <span class="close-btn" onclick="closeChat()">×</span>
                                </div>
                                <div class="chatbox-content">
                                    <ul class="question-list" id="question-list">
                                        <li onclick="selectQuestion('xin chào')">Xin chào</li>
                                        <li onclick="selectQuestion('bạn khỏe không')">Bạn khỏe không</li>
                                        <li onclick="selectQuestion('bạn tên là gì')">Bạn tên là gì</li>
                                        <li onclick="selectQuestion('tạm biệt')">Tạm biệt</li>
                                        <li onclick="selectQuestion('làm thế nào để đặt tour')">Làm thế nào để đặt tour</li>
                                        <li onclick="selectQuestion('chính sách hủy tour là gì')">Chính sách hủy tour là gì</li>
                                        <li onclick="selectQuestion('tour có bảo hiểm không')">Tour có bảo hiểm không?</li>
                                        <li onclick="selectQuestion('thanh toán như thế nào')">Thanh toán như thế nào?</li>
                                        <li onclick="selectQuestion('có khuyến mãi gì không')">Có khuyến mãi gì không?</li>
                                        <li onclick="selectQuestion('thời gian khởi hành')">Thời gian khởi hành là khi nào?</li>
                                        <li onclick="selectQuestion('cần chuẩn bị gì cho tour')">Cần chuẩn bị gì cho tour?</li>

                                    </ul>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="col-12">
                        <div class="rounded">
                            <iframe class="rounded w-100" style="height: 450px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9151137396325!2d105.78412831506288!3d21.02851179303965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab70c8e6d8f5%3A0x1d42a9bbf118ac1!2zSOG6o2kgS2jDoW5nLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sen!2sbd!4v1694259649153!5m2!1sen!2sbd" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="footer.jsp" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/lightbox/js/lightbox.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script src="js/main.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script>
                                            function openChat() {
                                                document.getElementById("chatbox").style.display = "flex";
                                                setTimeout(() => {
                                                    addBotMessage("Chào bạn! Tôi có thể giúp gì cho bạn hôm nay?");
                                                }, 500);
                                            }

                                            function closeChat() {
                                                document.getElementById("chatbox").style.display = "none";
                                            }

                                            function selectQuestion(question) {
                                                addUserMessage(question);
                                                sendQuestion(question);
                                            }

                                            function addUserMessage(message) {
                                                const chatContent = document.querySelector(".chatbox-content");
                                                const userMessage = document.createElement("div");
                                                userMessage.classList.add("chat-message", "user-message");
                                                userMessage.innerText = message;
                                                chatContent.appendChild(userMessage);
                                                chatContent.scrollTop = chatContent.scrollHeight;
                                            }

                                            function addBotMessage(message) {
                                                const chatContent = document.querySelector(".chatbox-content");
                                                const botMessage = document.createElement("div");
                                                botMessage.classList.add("chat-message", "bot-message");
                                                botMessage.innerText = message;
                                                chatContent.appendChild(botMessage);
                                                chatContent.scrollTop = chatContent.scrollHeight;
                                            }

                                            function sendQuestion(question) {
                                                var xhr = new XMLHttpRequest();
                                                xhr.open("POST", "chat", true);
                                                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                                                xhr.onreadystatechange = function () {
                                                    if (xhr.readyState == 4 && xhr.status == 200) {
                                                        var jsonResponse = JSON.parse(xhr.responseText);
                                                        addBotMessage(jsonResponse.responseMessage);
                                                    } else if (xhr.readyState == 4 && xhr.status != 200) {
                                                        addBotMessage("Có lỗi xảy ra. Vui lòng thử lại.");
                                                    }
                                                };
                                                xhr.send("question=" + encodeURIComponent(question));
                                            }

                                            function resetChat() {
                                                const chatContent = document.querySelector(".chatbox-content");
                                                chatContent.innerHTML = '<ul class="question-list" id="question-list">' +
                                                        '<li onclick="selectQuestion(\'xin chào\')">Xin chào</li>' +
                                                        '<li onclick="selectQuestion(\'bạn khỏe không\')">Bạn khỏe không</li>' +
                                                        '<li onclick="selectQuestion(\'bạn tên là gì\')">Bạn tên là gì</li>' +
                                                        '<li onclick="selectQuestion(\'tạm biệt\')">Tạm biệt</li>' +
                                                        '<li onclick="selectQuestion(\'làm thế nào để đặt tour\')">Làm thế nào để đặt tour</li>' +
                                                        '<li onclick="selectQuestion(\'chính sách hủy tour là gì\')">Chính sách hủy tour là gì</li>' +
                                                        '</ul>';
                                                addBotMessage("Chào bạn! Tôi có thể giúp gì cho bạn hôm nay?");
                                            }
            <c:if test="${param.status == 'success'}">
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Thành công',
                                                text: 'Gửi thông tin thành công. Chúng tôi sẽ liên hệ với quý khách sớm nhất',
                                                confirmButtonText: 'OK'
                                            });
            </c:if>
            <c:if test="${param.status == 'error'}">
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Lỗi',
                                                text: 'Có lỗi xảy ra khi gửi tin nhắn. Vui lòng thử lại sau.',
                                                confirmButtonText: 'OK'
                                            });
            </c:if>

                                            function validateForm() {
                                                let name = document.getElementById('name').value.trim();
                                                let email = document.getElementById('email').value.trim();
                                                let phone = document.getElementById('phone').value.trim();
                                                let subject = document.getElementById('subject').value.trim();
                                                let message = document.getElementById('message').value.trim();

                                                if (name === "" || email === "" || phone === "" || subject === "" || message === "") {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: 'Lỗi',
                                                        text: 'Vui lòng điền đầy đủ tất cả các trường!',
                                                        confirmButtonText: 'OK'
                                                    });
                                                    return false;
                                                }

                                                if (!validateEmail(email)) {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: 'Lỗi',
                                                        text: 'Vui lòng nhập đúng định dạng email!',
                                                        confirmButtonText: 'OK'
                                                    });
                                                    return false;
                                                }

                                                if (isOnlySpaces(name) || isOnlySpaces(subject) || isOnlySpaces(message)) {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: 'Lỗi',
                                                        text: 'Các trường không được chứa chỉ dấu cách!',
                                                        confirmButtonText: 'OK'
                                                    });
                                                    return false;
                                                }

                                                return true;
                                            }

                                            function validateEmail(email) {
                                                const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(([^<>()[\]\.,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,})$/i;
                                                return re.test(String(email).toLowerCase());
                                            }

                                            function isOnlySpaces(str) {
                                                return str.replace(/\s/g, '').length === 0;
                                            }
        </script>
    </body>
</html>
