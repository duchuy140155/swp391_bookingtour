<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">
    <style>
        body {
            font-family: 'Candara', sans-serif;
            display: flex;
            min-height: 100vh;
            margin: 0;
            background-color: #004080;
        }
        .grid-container {
            display: grid;
            grid-template-columns: 260px 1fr;
            grid-template-rows: 70px 1fr;
            grid-template-areas: 
                "header header"
                "sidebar main";
            height: 100vh;
        }
        header {
            grid-area: header;
            background-color: #004080;
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0 20px;
            color: white;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .header-left, .header-right {
            display: flex;
            align-items: center;
        }
        .header-right span, .header-right a {
            margin-left: 20px;
            cursor: pointer;
            color: white;
            text-decoration: none;
        }
        .header-right a:hover {
            color: #ff5722;
        }
        #sidebar {
            grid-area: sidebar;
            background-color: #004080;
            color: white;
            display: flex;
            flex-direction: column;
            padding: 20px;
            box-shadow: 2px 0 5px rgba(0, 0, 0, 0.1);
        }
        #sidebar .sidebar-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 30px;
        }
        #sidebar .sidebar-title .sidebar-brand {
            font-size: 1.8em;
            font-weight: bold;
        }
        #sidebar ul {
            list-style-type: none;
            padding: 0;
        }
        #sidebar ul li {
            padding: 15px 0;
        }
        #sidebar ul li a {
            color: white;
            text-decoration: none;
            display: flex;
            align-items: center;
            padding: 10px;
            border-radius: 6px;
            transition: background 0.3s;
            font-family: 'Montserrat', sans-serif;
            font-weight: 600;
        }
        #sidebar ul li a:hover {
            background-color: #3949ab;
        }
        #sidebar ul li a .material-icons-outlined {
            margin-right: 10px;
        }
        main {
            grid-area: main;
            padding: 20px;
            background-color: #fff;
            display: flex;
            flex-direction: column;
            overflow-y: auto;
            border-radius: 8px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.05);
        }
        .main-title h2 {
            margin: 0;
            color: #3f51b5;
            font-weight: 600;
            font-size: 24px;
            font-family: 'Montserrat', sans-serif;
        }
        .main-cards {
            display: flex;
            justify-content: space-between;
            margin: 20px 0;
            gap: 20px;
        }
        .card {
            background: white;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            padding: 20px;
            flex: 1;
            text-align: center;
            transition: transform 0.3s, box-shadow 0.3s;
        }
        .card:hover {
            transform: translateY(-5px);
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.1);
        }
        .card .card-inner {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
        }
        .card .card-inner h3 {
            margin: 0;
            color: #3f51b5;
            font-weight: 500;
        }
        .card .card-inner span {
            font-size: 2rem;
            color: #ff5722;
        }
        .card h1 {
            font-size: 2.5rem;
            color: #283593;
            margin: 0;
        }
        .charts {
            display: grid;
            grid-template-columns: 1fr 1fr;
            gap: 20px;
        }
        .charts-card {
            background: white;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            padding: 20px;
            text-align: center;
            transition: transform 0.3s, box-shadow 0.3s;
        }
        .charts-card:hover {
            transform: translateY(-5px);
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.1);
        }
        .charts-card h2 {
            margin: 0 0 20px 0;
            color: #3f51b5;
            font-weight: 500;
            font-size: 18px;
        }
        .charts-card img {
            max-width: 100%;
            border-radius: 8px;
        }
        .dropdown-btn {
            cursor: pointer;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .dropdown-container {
            display: none;
            background-color: #3949ab;
            padding-left: 20px;
        }
        .dropdown-container a {
            padding: 10px 0;
            display: block;
        }
        .sidebar-list-item.active .dropdown-container {
            display: block;
        }
        /* Responsive Design */
        @media (max-width: 768px) {
            .main-cards {
                flex-direction: column;
            }
            .charts {
                grid-template-columns: 1fr;
            }
        }
    </style>
</head>
<body>
<div class="grid-container">

    <header class="header">
        <div class="menu-icon" onclick="openSidebar()">
            <span class="material-icons-outlined">menu</span>
        </div>
        
        <div class="header-right">
            <span class="material-icons-outlined">notifications</span>
            <span class="material-icons-outlined">email</span>
            <span class="material-icons-outlined">account_circle</span>
            <a href="logout" title="Logout">
                <span class="material-icons-outlined">logout</span>
            </a>
        </div>
    </header>

    <aside id="sidebar">
        <div class="sidebar-title">
            <div class="sidebar-brand">
                QUẢN LÍ
            </div>
            <span class="material-icons-outlined" onclick="closeSidebar()">close</span>
        </div>

        <ul class="sidebar-list">
            <li class="sidebar-list-item">
                <a href="#" target="_blank">
                    <span class="material-icons-outlined">dashboard</span> QUẢN LÍ
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">description</span> Quản lí news
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="manageNews">News</a>
                </div>
            </li>
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">card_travel</span> Quản lí tour
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="createTour">Tạo tour</a>
                    <a href="listTour">Danh sách Tour</a>
                </div>
            </li>
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">hotel</span> Quản lí khách sạn
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="createhotel">Tạo khách sạn</a>
                    <a href="listhotel">Danh sách khách sạn</a>
                    
                </div>
            </li>
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">meeting_room</span> Quản lí phòng
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="createroom">Tạo phòng</a>
                    <a href="listroom">Danh sách phòng</a>
                    <a href="listbookroom">Danh sách phòng đã đặt</a>
                </div>
            </li>
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">local_offer</span> Quản lý mã giảm giá
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="showdiscount">Danh sách mã giảm giá</a>
                </div>
            </li>
            <!-- New Post Management Section -->
            <li class="sidebar-list-item">
                <a href="javascript:void(0)" class="dropdown-btn">
                    <span class="material-icons-outlined">post_add</span> Quản lý bài đăng
                    <span class="material-icons-outlined">expand_more</span>
                </a>
                <div class="dropdown-container">
                    <a href="createpost">Tạo bài đăng</a>
                    <a href="listPost">Danh sách bài đăng</a>
                </div>
            </li>
            <li class="sidebar-list-item">
                <a href="PartnerProcessCancellationServlet" target="_blank">
                    <span class="material-icons-outlined">cancel</span> Quản lí hủy tour
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="${pageContext.request.contextPath}/viewContacts" target="_blank">
                    <span class="material-icons-outlined">contacts</span>  Liên hệ
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="tourList" target="_blank">
                    <span class="material-icons-outlined">group</span> Hướng dẫn viên
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="#" target="_blank">
                    <span class="material-icons-outlined">settings</span> Cài Đặt
                </a>
            </li>
        </ul>
    </aside>

    <main class="main-container">
        <div class="main-cards">
            <div class="card">
                <div class="card-inner">
                    <h3>TỔNG TOUR ĐÃ BÁN</h3>
                    <span class="material-icons-outlined">inventory</span>
                </div>
                <h1>${totalToursSold}</h1>
            </div>

            <div class="card">
                <div class="card-inner">
                    <h3>TỔNG DOANH THU</h3>
                    <span class="material-icons-outlined">attach_money</span>
                </div>
                <h1 id="originalPrice"><fmt:formatNumber value="${totalRevenue}" type="currency" currencySymbol="VNĐ"/></h1>
            </div>

            <div class="card">
                <div class="card-inner">
                    <h3>TỔNG KHÁCH HÀNG</h3>
                    <span class="material-icons-outlined">people</span>
                </div>
                <h1>${totalCustomers}</h1>
            </div>
        </div>

        <div class="charts">
            <div class="charts-card">
                <h2 class="chart-title">Doanh thu hàng tháng</h2>
                <img src="PartnerDashboardServlet?action=monthlyRevenue" alt="Doanh thu hàng tháng">
            </div>

            <div class="charts-card">
                <h2 class="chart-title">Doanh thu hàng năm</h2>
                <img src="PartnerDashboardServlet?action=yearlyRevenue" alt="Doanh thu hàng năm">
            </div>

            <div class="charts-card">
                <h2 class="chart-title">Số lượng tour đã bán hàng tháng</h2>
                <img src="PartnerDashboardServlet?action=monthlyTourCount" alt="Số lượng tour đã bán hàng tháng">
            </div>

            <div class="charts-card">
                <h2 class="chart-title">Số lượng tour đã bán hàng năm</h2>
                <img src="PartnerDashboardServlet?action=yearlyTourCount" alt="Số lượng tour đã bán hàng năm">
            </div>

            <div class="charts-card">
                <h2 class="chart-title">Số lượng khách của từng tour</h2>
                <img src="PartnerDashboardServlet?action=customerCountByTour" alt="Số lượng khách của từng tour">
            </div>

            <div class="charts-card">
                <h2 class="chart-title">Doanh thu của từng tour</h2>
                <img src="PartnerDashboardServlet?action=generateRevenueByTourChart" alt="Doanh thu của từng tour">
            </div>
        </div>
    </main>
</div>

<script>
    function openSidebar() {
        document.getElementById('sidebar').classList.add('active');
    }
    function closeSidebar() {
        document.getElementById('sidebar').classList.remove('active');
    }
    const dropdownBtns = document.querySelectorAll('.dropdown-btn');
    dropdownBtns.forEach(btn => {
        btn.addEventListener('click', function() {
            this.parentElement.classList.toggle('active');
        });
    });
</script>
</body>
</html>
