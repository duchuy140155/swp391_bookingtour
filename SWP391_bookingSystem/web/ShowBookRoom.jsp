<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Booking History</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Helvetica Neue', Arial, sans-serif;
            background: linear-gradient(to right, #ffecd2, #fcb69f);
            color: #343a40;
            margin: 0;
            padding: 0;
        }
        .container {
            margin-top: 50px;
        }
        .header, .footer {
            background-color: #343a40;
            color: white;
            padding: 10px 0;
            text-align: center;
        }
        .footer {
            position: fixed;
            width: 100%;
            bottom: 0;
        }
        .booking-item {
            padding: 20px;
            background-color: #f0f8ff;
            border: 1px solid #b0c4de;
            border-radius: 10px;
            margin-bottom: 20px;
            transition: transform 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
        }
        .booking-item:hover {
            transform: scale(1.02);
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        }
        .booking-item h5 {
            margin-bottom: 10px;
            color: #007bff;
        }
        .booking-item p {
            margin: 0;
            font-size: 1.1em;
            color: #495057;
        }
        .back-button {
            position: absolute;
            top: 10px;
            right: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1em;
            transition: background-color 0.3s ease;
        }
        .back-button:hover {
            background-color: #0056b3;
        }
        .total-booked-rooms {
            text-align: center;
            margin: 20px 0;
            padding: 20px;
            background: #f7f7f7;
            border-radius: 10px;
            border: 1px solid #ddd;
            font-size: 1.5em;
            color: #333;
            transition: all 0.3s ease-in-out;
        }
        .total-booked-rooms:hover {
            background: #e9e9e9;
            transform: scale(1.05);
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        }
        .highlight {
            color: #007bff;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>Các đơn đặt phòng</h1>
        <button class="back-button" onclick="goBack()">Back</button>
    </div>

    <div class="container">
        <div class="total-booked-rooms">
            <c:choose>
                <c:when test="${not empty sessionScope.totalBookedRooms}">
                    <span class="highlight">Tổng số lượng phòng đã được đặt:</span> ${sessionScope.totalBookedRooms}
                </c:when>
                <c:otherwise>
                    <span class="highlight">Chưa có phòng nào được đặt</span>
                </c:otherwise>
            </c:choose>
        </div>

        <c:if test="${not empty listbookroom}">
            <c:forEach items="${sessionScope.listbookroom}" var="booking">
                <div class="booking-item">
                    <h5>ID Người dùng: ${booking.userId}</h5>
                    <p><strong>Tên Phòng:</strong> ${booking.name}</p>
                    <p><strong>Giá Tiền:</strong> ${booking.price} đ</p>
                    <p><strong>Số lượng giường:</strong> ${booking.numberBeds}</p>
                    <p><strong>Số lượng khách tối đa:</strong> ${booking.maxGuests}</p>
                    <p><strong>Mô tả:</strong> ${booking.description}</p>
                    <p><strong>Ngày đặt phòng:</strong> ${booking.date_booking}</p>
                </div>
            </c:forEach>
        </c:if>
        <c:if test="${empty listbookroom}">
            <p class="text-center">You have no booking history.</p>
        </c:if>
    </div>

   

    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>
</html>
