<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Quản Lý Phòng</title>
    <script type="text/javascript">
        function showAlert(message) {
            alert(message);
        }

        function confirmDelete(roomId) {
            if (confirm('Bạn có chắc chắn muốn xóa phòng này không?')) {
                window.location.href = 'deleteroom?id=' + roomId;
            }
        }

        window.onload = function() {
            var urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('deleteStatus')) {
                var deleteStatus = urlParams.get('deleteStatus');
                if (deleteStatus === 'success') {
                    showAlert('Xóa thành công!');
                } else if (deleteStatus === 'fail') {
                    showAlert('Xóa không thành công. Vui lòng thử lại.');
                }
            }
        };

        function filterRooms() {
            var hotelId = document.getElementById('hotelFilter').value;
            window.location.href = 'manageroom?hotel_id=' + hotelId;
        }
    </script>
    <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <style>
        ::-webkit-scrollbar {
            width: 8px;
        }
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
        ::-webkit-scrollbar-thumb {
            background: #888; 
        }
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        } 
        body {
            background: linear-gradient(to right, #c04848, #480048);
            min-height: 100vh;
        }
        .text-gray {
            color: #aaa;
        }
        img {
            height: 170px;
            width: 140px;
        }
        .back-button {
            position: absolute;
            top: 20px;
            right: 20px;
            display: flex;
            flex-direction: column;
            align-items: flex-end;
            gap: 10px;
        }
        .btn-group {
            display: flex;
            gap: 5px;
        }
        .filter-select {
            width: 150px;
        }
    </style>
</head>
<body className='snippet-body'>
    <div class="container py-5">
        <div class="back-button">
            <button onclick="history.back()" class="btn btn-secondary">Quay Lại</button>
            <a href="partner.jsp" class="btn btn-primary">Trang Chủ</a>
            <select id="hotelFilter" class="filter-select form-control" onchange="filterRooms()">
                <option value="0">Tất cả các loại phòng</option>
                <c:forEach items="${sessionScope.listhotel}" var="p">
                    <option value="${p.hotel_id}" ${p.hotel_id == param.hotel_id ? 'selected' : ''}>${p.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="row text-center text-white mb-5">
            <div class="col-lg-7 mx-auto">
                <h1 class="display-4">Quản Lý Phòng</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <!-- List group-->
                <ul class="list-group shadow" id="room-list">
                    <!-- list group item-->
                    <c:forEach items="${sessionScope.listroom}" var="r">
                        <c:if test="${param.hotel_id == null || param.hotel_id == r.hotel_id}">
                        <li class="list-group-item">
                            <!-- Custom content-->
                            <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                                <div class="media-body order-2 order-lg-1">
                                    <a href="detailroom?id=${r.room_id}">
                                        <h5 class="mt-0 font-weight-bold mb-2">Phòng ${r.name}</h5>
                                    </a>
                                    <p class="font-italic text-muted mb-0 small"><b>Loại Phòng: </b>
                                        <c:choose>
                                            <c:when test="${r.room_type == 1}">Vip</c:when>
                                            <c:when test="${r.room_type == 2}">Medium</c:when>
                                            <c:when test="${r.room_type == 3}">Normal</c:when>
                                            <c:otherwise>Unknown</c:otherwise>
                                        </c:choose>
                                    </p>
                                    <p class="font-italic text-muted mb-0 small"><b>Giá: </b>${r.price} VND</p>
                                    <p class="font-italic text-muted mb-0 small"><b>Số lượng giường: </b>${r.number_beds}</p>
                                    <p class="font-italic text-muted mb-0 small"><b>Số lượng khách tối đa: </b>${r.max_guests}</p>
                                    <div class="d-flex align-items-center justify-content-between mt-1">
                                        <div class="btn-group">
                                            <a href="editroom?id=${r.room_id}" class="btn btn-primary btn-sm">Chỉnh sửa</a>
                                            <button onclick="confirmDelete(${r.room_id})" class="btn btn-danger btn-sm">Xóa</button>
                                        </div>
                                    </div>
                                </div>
                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExIWFRUWFxoXGBgYFxodGBoaGBgYHRcfFx0YHSggGBolHRgYITEiJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGi8mICUtLS0vLS0tLS8tLS8wNy0tLy0tKzUtMi8tLS0vLS8tNS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAK4BIgMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAECBwj/xABIEAACAQIEAwYDBgMECQEJAAABAhEDIQAEEjEFQVEGEyJhcYEykaEjQrHB0fAUUmIHM3LhFRZTgpKissLxNBckQ2Nzg6PS0//EABkBAAMBAQEAAAAAAAAAAAAAAAECAwQABf/EADIRAAICAQMCBAMGBwEAAAAAAAECABEDEiExBFETIkHwFGGhBUKBkdHhMlJTcbHB8SP/2gAMAwEAAhEDEQA/AD9OMjEeTz9Gr/dVUfyVgT7jce4wTox7gnjHaRgY6VcdKuOwMdOuaAxmnEgGMjAnXOAMdAY6042Bjp1zQGO1xgGOwuOgM6XEi44QYkGOizoJjYXHdI4mWlgEzhICuN6MEomJDTwuqNpgDLiBsMKlO2ITlsEGCoC2IXwXXpRgdqeGBggxGNg4n7jGVcuV3wYbnCtjsNiAnGw+OnQkNiRcDK2Jle0nbAJAFmcFLGhJibHHCcRZaZorszaiRvsBE8hbERqE+Q+p9PL9+WOadAk6QsnePugAgSx8if0GPOz5Q/HHeer02A4x5ue041W5R1O3t1/D1xtVjrfrufnsMHUMr4qgYBiiPHSRFwPnB5Y44oZquZkTva9h039sRRgGr6/l+s0vZE44cwFRSTAE+Q+FueMzFYMtMRGhbmBfbb5cz8sDE+w6nEFHMq+rQZj73IzMR12+uOIttQ98/rOFBaMzNtqu5hfM7/v9MPuxwIrEAWK39iN/3zxUqkWZzqOqB/kPriycDr1EqMaVPW0ab7CSDJOw25nAfH5TOD7xv29MIu9mn/lfFMziNVqK6+EKdRFyQJEC9gLb8vpi35nhrN9pmams/wAosojz3PoIPrhTnD/KsKNuQ9vPz54irKNh+0oAa93FWrz+h/TG8bk9PrjMav8A07/SS8k8WzCFDpdSp3hgQfkb4vPYzK58VFaoX/hyDIqPMgqdJQEkgTB5SDzGHeV7Y5KpGqpoO4FRCIPrBUH3xYcjnKdQTTqJUHVGVh/yk42qgu9U8t8hqtMiCYzTgxlB5Y4eiCIOxEYvczD5yFcdxhZRdpZfFK2OxHPaYgWPXBC5kjePqP8Aq39sZV6oHkTa3RH7phenHQXGUqk8iPUYlVhiq5kbgyD9PkXkTkJjenEyjGyuKSMjAx3px2ExIqY64JEi4Lpm2OFXHSocKTCJJiQYjRcdYUygmVBOIbjE04yMdFMBzBsfQ/hgaoRhPxjtfQUlKYNUmQWBhLyLGCTHpHngXJdqKVQwwNMnaTK/OBHuMOpE4o3NSwrjWaMjHKtyxxXaIm07YNi4oBIsCCPjS4mWkWNvc8hglKWnbfrzPp0GEy5lx/3lsPTtk447yAJG9z0/XpjNP3mNgN4sP8I5/v4sTLTHkfqJ/MziDJZV3ZjczqAJ5AkRA2ix9cY2Yv5mO09FEXH5UG83lswGmAQLXO5kAyfntg7I1QpJP8sf8yn22OOq3D0o0XPxOAB5A2USevy9MKwTaxZt+ijab9NtvriJdWFDiVVGG5h9atJZhbUSSJtc7Haf8sDLBvMjry9hjfcg2e8KxifDMcuvkDvgjNiHPKP0G3TAStVHt+kZjS2Io40BoE/zLv8A4gPlfEHBWLB4FoQC3kefy64Y16aPYjVcGOUgyPWMbdoEbADYcgPTbDlt/LFC7bxdUywWQ3iOrUAPCB0k8/3bFh7NZ6qWKMRAEqAokcj0nlOrCBq5PwrA6tv7DD7sxTHfkRuu/P4hzxLItqSZRTTR5xcHwDUQWZQSIZoPTUCPpOB6CBWr6RYBQGYkkAqZuep5SPTlibtDnqY0KHUwQx0329LYTVuMQ1RkWdYADPuAFgwB+R6WxmGNiOPdiOXEUR5H5/54zEfeno30xmPQ1DvM+lu081y3Y3M1VJomnUi+kPpaOvjAWP8Ae/ESFmuzWcpEFsrVBH3lUsB/vJIHzxt0z2UKuozFEzAswVuun7tQek4ZZL+0PPJZqiVPKpTX/s0n5nBxmxRO8hkBBtRtLV2K4Tn6bCpmKzd0VP2VR2ZwTsSG/uyLWmb3giMXSced5b+1F7d7lUbqUqFfkGVvxxdOznGEzlHvkR0XUV8QF4AMqQSCt4nqDjXjZRtcw5Va9RE7ajFRyPvBem41A/kffHQPX9/PHOcy51SCRI5Hp5c+XyxGC45g+o5e1pxmfZjtPQwnUgNydUjYbnlbfryxuoh5qR02Nr7aCeh+WIkrEEHTNwbGdsGLmlZ1a4iZ63n06jGZ+eJqW6gVGfutPoQ34xGJ0zLDePw/6rnHZOtjo8dgbX2UDz54Wd5VGY0+IIWAuPDHdEmOniHz9cWTVVg1IOFJ3FxymcX7wI/f754KpVlOxGK9wvOmpqBC2H3ZE+N1uP8Ac+pxNUqIrhSYYxFjzJAHhiNueKDLkHO8genxNuNpYMSqMJKNY30uDHKQfnscEJn2HxAH6fIGMMOoX1Em3RsP4TcZNUC7nfGnf+lvkcKzVLOSW8gDIgct8Mf4lrQymY+4vQ9AR03jCPnI/hqOnTCvMDMFQHCftPQrVqXcUGVS86yxIGkfdsCfFPpCkHe5HFeMPSf7bLlqcT3iAyIpoxn7p8RYAArsMTZU03+1pvqUkj3Bg77EYRer1CiKMb4OmsHaVfKf2cEx3mZAPMKkjlzLDp0wJU7BzVFOnWabatSgzYExBAHoZw5rcXzzORRpuq6LE0F+OQDHeVBI3IttF+WLlw6ESpWtqLaQSei2wrZnSyZbw0PEzhnB6dBFSC7KvO+w5yLn2/4dsIeM0RUqSbD+UGdwLT0sb4Y8Q4mWlacqvM8z+gwnzNQhJW5JEc9yL+eI4jkLar3lGVQtHidokAAQANhy/wA/U/WcTd0NOomxaI6iLkk78h0HyiThfA6js1RjpU8zJJELsOWx+eOM7TAICCbkCTzAJvHO23nhmZboGzCtn5CQ5uvqJKiFAAk2AA2t+X44kyFSdjadzInYW6i/LEdVbEbmCP8Ax0xJlqDOSJAE7npY+u84DjbeMh32jHPickWmZIA2/nGwFoscJKanfy/MYsPE+HpSyzMCXYAAE/CJYAwNvvE88VZwRACl2IJuYURvJO2/Ie2FxnykTm5uF1aksSBvPyOISs738ht7nEeYpEiCTZGJCmFJEc9yN/WcEKgAgKFA2AsB6devnii0W0/L9IGsLfv1mqNDU6oXKKTcrvYHmZ9JOI6qKFhV0nTc3JbeJJ9/njdcW+KP3yxCpAJiTq3n8h0vgts/y/7+0C7p7+UHp0dQF/P99MO+E8I72WFRQNjpOphzhoPh5bm3TCbMtbxsFHSY9vOfXDbstTAqssWK897G0AW5nfCNqVSQY2zECH5jh9KnZQXb/iPvFl9RhFnK6zBZZJ2mTPnHPDftrRPckHafbZthyxUeGj7NLzd7n/H64GFC41EwZcnh7ARh3fkMZjuPLGY1aRI6jEvarKVq2X/93qEaSH7u0MVvYkSjjkVImSDvIpWU7ZVaUipRoV0NmStTmGHv4W35e1hHoKOUOpduY/f44V9r8nWqrTrUWNQU5BpEBjDAA6JBv1TYi+4E5gd7jkWK78xJle1PDWZWrcJVSpBBo1LT5pCKR5GcXrhv9oeRrMiaqlNnYIoqUzdmIAE0ywFyNyMeWPxApKVuH5d1Bu3dVKb9bvSdYPkR5bYddlu22SyjFl4dpLbuK5qMAOSCqvhHWGE85gRpR+0yZMY+c9M4tWUPpNoE7bg+3UEe2BlYHYg/vyxL/pIZmlTrd1VpatQ01l0vYiDEmRcwfXEDUFO4Hy/TBYkm5TEAFAnOcHgb0NwdvzGBadRhTY6iTKxN99M7+pwUKMfDM7AAxPTe31xt6TAQym/IrM3/AKZnb6YQuBsRK6CdwZP2cUmvTNg2phYRbSw64sHHVY6pjw9dz8Pl/UOZ2OK9wNprIAdNzDIbq0E7N7iPPD/irMoYGoHmQSVKtfSdwYPwjljNl06wRKpq07xG+XZblYnYkC9g2672cGP6vXA9TKh3VjIKwRBtYncHlc4OzOfDBQQRHvMKi9P6Z98ao1FIIJUz1E/dYcwYN/LDhiFu952kE7iSZHgFUanUBwehEiWZuZv8X0wJmaFVKl1ZVIO+0wYsR1/DFm4JmATGoQKZ58xpjnvvhd2i4i65lKEArURWDS0jUtcnlEfYgcvi32BUZnJoi4DjUcGoqyjFl8UTMWEchuJ3vjSZtQJJK307TfTNtPl5YjyGa70Ei2kgHxSLqrD0sw3wWvDVchXECQwMRBKwD0+E/XDMV+8KhUN903J2p6rP4hcb3ANjBEEe2JKFBUGkEgEk3PM7xN8EZvLimVAfVqEgwQI5eX1xC3z/AH5Yjt90yo43El8IvP1wPmK6hfLePlsD7X8sV12z3iHdu+qkoB7wJpqQNRAWpTkmSb2sNrgvqigHUYhRsfh3Bv1FthjhdnUYSAB5ROatOQd4gj3PXrbGnqwwncmBz6C55bjDDj9eahQLAVVgLbdZtG2+FlZNTLf4TNvUfphwb5kzdbSycFrhkZ6hChXKST0UH8Tiu1GsogyBt6rH54sXCMrS0EqoZgbkmYYgHbkYjziMUziHEy6l/g+1VLEEwX07wN/bfC4wCfLAxoeaFuTcnlyG/wA8RZXiP2b1FAXSUA1bHWaUH1ipEdRzxKjTTBndRueoxBwThfham9QEOUJgERpSksC8sCaeqbfFHrdgoG8QFidpYFrauGhmYlmPOxMVjyPkD8sJAh1A9FO/W0YsPFOGUaGVY01uBAJvGphMAWFyTtvioZiutPTq1MWkD1AJ9rA4njoggQ8UTD6zAkk3Jn674jdmJuYn54FOYZ0pMsoKgDERcAgGPI8sEZEfZrck8zzNzucWXHW8Q5QTQmJQJZVRCzsYEkATBJJ5xA8z5Y57ttIJZTqWQqfd9TNz7DbnglTpZWjYnf8AwnyxFqOkBrQIF8IbD0OP+/tGABSzz/ySUkpIQVQAmlBZjqYsTcyT+eJuA5padbU8xpj6jb5YXvnguwk6dJIH1JOGHZ9qbkh6JLATMlgR6WH4+uJAaVOr17ShNkaZN2k4mMyNFNWHPqdiNhtvhFSyJpqA0mJud7mT1iMW7iucSlTJIVEHqQPamDHvipZviyNBEsGJi0AxAPnzG4w2LI1Uor6xXRfvGZ3o64zHTD+rGYvTd/oJO17Qek4fazDcfp1GOabFDIFvvLgirlQ/iU35EYj1SdLeFuR5H1xmuVqVDtXwHRNegzLTJ1NokGm384gjwTuORvtMVb/SuYV//UFyrAjvBrIYXBUVQwB5yMeqhihIiQfiXy6id8UXtZ2f7kipSvRb4IUNpP8AIZ2H8vuvITRGrmTyLY2j7sr2krVIOZz1Fy3hWkyqrySADIVReNrzIPli2d83NAfQj8MeM8Kz7UaquHFM7FjRViBzIVhE+hB88ex5Cr3lJH8RkC7poLRudLAQDv0vbF9z6yK0BVSVK4BBKsIIPlY++Cv42mXVp0gTPvq9OowHmkOhiBFjcW/A4BRn7tjrJI0xIBiYnl54VsRbeUGULtHvDq6/xKuCXUEE6ZJjRpO3nhnxXM03NQggSLAwD/8ADjcf4vkcI+zNMmvSNtWphIEW0sOuDO0HE2OaqZYrZAh1ah99ah+GJt3fW8+WIZEOsCuBKIwq75necyqimjKBJgGCf9nTOwMbk/M4V1mKssRfqP6lHl1OBcvV1OyjUpEmdUizFfynBDVACAzX5Ss8xzXzIxQLpFERSb3Bly4SVYaWVWOktdQbCP13wJx/K0A6RTUVAAymDIkOBBO1tfT4j1xzwZ5J01gH0kR4TaRPhkEDbEvGco8rUZ1aFC2UraG9f5jjIux5liN5Wcmirq7sC5BPxbhVA+KY8KrYeu5OGeWzRLC20CxHKBv7YXUBomSbn+U9APywLk6y1O8NJwWZmUQYYsjPMTBMahcfPF2o+sVTXIlo4kDNPUuk6Ao8QM6dyIMxfAbDBPEWDd3FyKagzya874DYeuJKD2lYgqcVzBNSGZVCFkCZY1Cxnw3DWlYMaZv5QXuYoyZMwAQY33Gw9sV//WZvFIooVQtDmqSYYiLKOgMmN/fFnpqDvtbfbe+9vnjgasiEji4TxDKzUsSZsS25hZGwuIjCfMkykTE3jpK7x0vh1xWvpGtiqCfW5AWJ2m2298Jq1cqyiCdRjfb2574OIn+8m3HNR32cWtTp1IpAaqruGYwNLAAWFzty/wAghr5ZKIgsANUy1/EzEjSDzkmIxaezzl6bl4AWoyDfYKN5858sUypTZkvJPeAybWDG9/KMNjssfSKxAHeFFQVLb2LCfSRbpgrKUyoktuJBHh+8BYC4573wMrrGkGbRb5b7Yko1STBBUeW+GcCqEK3e8d551/gNMgMTtsf7ydvmcVDiWVdymkTBM+KABoYeKbkSRYYsfE0pjKu6KCSAAxOr7wUxci1+mK5Sy7GAFZzEklgAACBeTMSRYYOE6VMTItmZlaASlRRiJpoqkJJWQoB0k8pHPE1KowAVFMDmx/GP1x09FxqGpVKo7GFLSUiwMjmfiI5bYmrUFUkSXANmeAT7QAI225e+Khyxq/fsxCgG9e/YglSpYs9UKBc3A/z9pxDRrU2L6ZYpGqZFzPUSdjjOOLNIixuNxb4hjjJpev6j/vw+j1k/E3oTQrvuEVRtcyY62w54FT+3jc6TE/4lwspZYsB6k7YPyjur60QsRaApPOfPpiF6gRNNaaMa9tKWmgQSJIP4Hf8AYxR3HhTbd/PmvO8YtnE1zNcfbCFiPurY72mfphRXyQSBAIExud4nf0x2F0RdJMnlR8hsCdFfL9/LGsQGuf5f38saxbxBB4ZneW8JtafXE1epTZtDEBuX+R2xip4WIF9LQeexxV1pVNyJnzE4wEhALmkDVLFVpFLNJXk3Nf8ALEFWiNLI6h6bjxDkR1Xof0+UOS4hUSA4LL0O49D+R+mDwgYFqVxuyGxHp/KfPbDBwZxWeY9peCNlnkHVTa6v/MPO4AYcxHQ8yFcf2e8Q8Roaom4gwSfOWOrpAWBuTi1ZqgjoyMJpnfYMh63sD9Dt5YqGT7JnV3lPMqArmPs2YeFt/CSCJExO0SQdro97TPkTSbE9AcFQTqMAXsrW97nAx4pRAIaog2BDKVvytF8SZvODuyF8bEadoEkbmZ0j39+eK9V4IxU6hTZuoq3tyhrfTFXNDyyYsniWbgvEk75VWovMjuz4phiI12JMERI3xAeMUqlStULlqqiTJswQMBzgEazI/GMVivwo07iCbeq9YKyCJ2jaPbG6vDmNVXXWKwaKisdzBCsCx5xBHtyjHnP1Th/NtNIQadhAq/aOuarGlKgCTCiQpJMmZG7eYE+WDcpxKsG1tVasARqEHbUIgRaYFwLmehGOM5TFJVqIA6sSQwbS1y4Pw8gymNxNP1jvglMoIIIp6bkT8JZTZSSCCS4A/qN8Ic7tRBkxjA2MsPZntfoqs9aiwVlYLHxeIyoIP+A36YtnFHZ6yVQjqhpIpLKRcd+bz/8AUX5+WKATpc5g6WNoQrAg7iW+6AxmQYE7RAteU7QVK609SoqPTVgFJP3qwPSPgAiDEHnIDo5dpQbSuZriRyyaS0O7iNUsAq0k1GzGPFJ8yG35qqa1KapmASNLl1Lz8TleYIsRSuP67jqbxHI97UL6QzTpACtEACZK22Y2NjtzuxNImnoUeIFWWPhJAbT4QIO3IGRA54lmzEvpE5V9Zb+J5lXFBhUB10wVhtyQSYvc228sBsDNj+/lirdra7CplVosE0ojhVM1AzxrLRMwLXFwzfdmX/DcxUqUkciCRcMLyDHUbxPvi+Mgx9XpKnx/tLpAp0KYQvSkN4QQSw20EAaYeb7jpfFm4fxLvaKNTM6kvJDOGGkQYJE329MIeI5WjX0uinSnhLQ23iA8NU7zPUn8WPA6+lHdd9gFIE6LTKiSSZJN5keQxwyW5Uxdwblk7RU5JIPginBM7gydr/sYBqMGZTpIAMgmAPrhL2j7aFqtSgGCKyroqKZIlUa8byTFuows4b2oWsaZqKoNNp1LFyD4ovaxU8tiOmKJkAXmojMtz1ThvDqJQFZeSZlraufw7n1xUuM8TQjVTpwO9WnDgKBqqFJEEzcT526444B23p0nzAaO6FR3pwpLuXdpI28MaYESbmcVWv2ppXTuqoPe94dlj7U1FF5kHUATA+uFRhfmM4uK2lrQzT1bFkBsYglZtG+FjWytUs0yad2JN4pH1HOw64n4XxdKtId1YIArayAwgCNQtJibg4MyNVBclCgIB0zAMACSOcQPljWHXTfMnpJPMYUK1P8A0aE1DXqa19hXY8/IYDpiJIgysbean8sPs/3TZN6tJFuIDgeL4tJgsJF5GKhmEC6QVZyRN2HLeZvhMfnBAHrGby0T6Q6tWUzLAEqwiRzjYb8sZUzyTYH5H8WjAjkjRpRRq3kzp22HP6Yk4e5emrGxYSQsRuefO3nii4qN/h7/ACinIDt+Pv8AOZVqlxBpkj+q3psCPrjkhwdkWeYH5z+WC6WXDuinVBN4YztyvbEud4etOkhCwXDySSSdMbz0k88A5KfQffP6TgorV797xVVzJFmrgehA/ADDbs5WcOULORGq4mIIFtRnnjnLVsr4AKuXXTQJbxLYjTM+LeAx9jjnsrmqVSuAtQNIIgETMqfIm2M7MrqduJZdiN444/mTSpFyC0clIU7E9CMU+vn3bTppgayZliSIIHIXmfLbFy7dIoolRvBO9xY/vfHm/E8/3RoyupfESWZQbsANIY3IIvyAODgRQmoxcuU3Q7R6UHU/PGYotbjtQsSuaqKCSQv2VhNhe9vPGYp46yc9ForY+h/DzxXTW6Ojb20gTy+6OR6YsDGEc/0ttvsdvPHmPCeIsaiqpCqfCisYAkjcm8cyd4X1xDKyqLYXK6iDQMecQ4lUHhT+HE+EMTUgWJ1NeABbaYjznFgyb6gGXTuY0z+JiTBAPKQeWK3wGsDXAnUNVRCgKyoSoFUrrnwl3UyREgxsQLZlcsU03BHKNrCDpuYBIJgkxqibYzkKSKlkZvWA8Wzbq4TQT4JPhWC3rzlWAIvMgRiPK8ap5eErqwBLfANSros8tr5ROlRIvvOGj8PXvWqaTr1D7t9PdqAywNgRE7ybQJmucS4KrElsvUW5MqzxJgHwvqWD6fLBTIoyGmAivici6lmzlZTQarSMjumqI1zPhJBh/wADGKfxLtBVKMivEBSW0hTdadlIO+pmPoLTBOF3EKTUlTS2pNWnS4cCYMCoARIuwBjnbzGpaVIFlEAtqBazJZRI17221XnUOexsxZdpmKEGjDBmWlXZmqVFYhladC7TcMDF9hzHnaZuNFkAqlxTAKg042kHT/K1lQwLeUYZcEo0C5IcMgVrFfhLNIkgeKJABg8+e2cRyVENSWoCokOzMQGcLMkwZk6okC/XHnuQW3EsoNQrg+XV6akMlRyZZCdOkEqBIHw6pjVYbG7HHOfbwsqNURHZkRwLJDaQpBaAsqJboV5lZi4Y6gu7UAxMVAAYbQLMFJOkrKpG3iM3sBFxqrBy1MoI1ZxCFmYRkpki8z4NQ3kxeb4mFBO0eqkPF82NU0e7lRpbSq33EkraJFheJ88W3gdXTSp0ypDOobULAahVILzEH7Mg+AiWW5kxUuEZPS7KyoWIWFN11E8pE8h7E8t7LmeGVnIZqtOSSsaWMaTeTrEmZ6YtiXQwuMASCYQoY/7SGVQdRUElSYLaBYwQLTsbSBiWsd1GsCL7FLsxk2m0AHncEggEYHXgNXRAde8mQYYLEXBXUTM/enyjniP/AEPmBcVh0sGF9v5sBsOFm2JBnaGAgPFiGrQLskAuzkCGnSqoPFoVtRGx+0AmxwVT421CmQ1B5BnSNcDmRq0ERPUybmwxHmeGZlfEGUx97U2qwkja2/XDCjw/MlJFQTM3do0x/hN8UxUCfNdfKA427RXwEU6i/Zg+EjWYXTfX4WIgltQItzjAvF++CnQ4qUl8LgaZgQXMbstthtG2+H7cPzliTSJEG7mQeUTS8zf1wLXfMpGsqqmRIYSOdjoEcz6nEWxqWDhhv8pxRiCDPOs7WGvXqmFgCJWNR68pDW5X9uKWZYGFGliDKxYGSZ97m3libjeYeqzBqveEOYJYMwmJOoBfDNwNIAi2F5JOmAVChUAPlVmbf0x7A9MVAuYSI6yWYaQZBnlzBLfWLedxg/h2TFVy9mcTKT4vvbzztueS9SMJlzK6qUodI3jnbYkEWO5O+LZkHy7eIJpARwy/EpUgswIYszbT57XBIxIr86lcKi4Zw0LlgwqNoBp3tYsAAB4bTcwovHnIwD/rCKaMiUiQ2gqZIkqKQ8IIIb4N5wTxfPUnqd0alRmnTCpJmN+cGReABE874W53hwA7sh2OoKpW42BvB/q5/XFkzOg0e/8AcqcVm5c8h2syz8PFGWFS+qUbQnj1eJvhA5A9ffFY4z2opKQKYFSAQSfCu3Kbnl68uUhotOnU7n+GrtUVNTA6QdIlp0s6yvPTz6HAdLJCswC0mljKaissSwmYMADUABsAD64uuYoux+cVsV8zfEO0tSppjUiJHw6ZMEQZmQORsRY7kHBPDO01ZABUVdJUBBdQD1PUAbgEXMW5J2XW9QhQx1FhKGLsQwPpIufvHyMw5iS/iy+pnECTUN0lm21EkqpBAkieW+OGVieYnh9hH+dzuYqR9uoTVI0hVIBtc8wNolt+eEqNUi9QBCpEKdOpQxLLAEAg3OoG0AcsWDhtOvmF0rl6WmmF+MsogQBGsLbYfuAj7W5M0qmmoyt4dSmm4YLMsVI6Xnebb4i+S2pWjMjKt1BmCu0F3YAECCPCLkAAmANR2EYP4ZlqOzUnqtJstoXzloYSD88Isrm9JBvBiVvsPEQRPkPecO24xWFLXTIkwJ0hraoAn2PlYbyAOXY+b/EVLJlqpZ1lRUWkQoFgW2HKdKnlhbxnOuSgItMldDNsRDEjSRHQTN5EbS8N4VmK9Q0xmjPI6FXY8oDbyN8Nsx2MzCIzHO1SV3A0bRJj7McuuKv1uJRp3/KW+Gcn95SX7P1SSYpXM2JAv0GmwxmLS/YziAJHe1bGPjofmmMwPicPzg+HbuPzjOvWPd1AB9xv+k48u4nwTMZRadStTNN2LQn3kVIHigQAdxeSLwAZPq60CJMbCbi3v1GI14nkcwVpVnDuDqJJ1HXNt97GwIMTHWZ9VlCFQeN+/wDqEY9W8857PUxSfSzaWqU2mo33HYrpjoVWWPUyDEY9A4dnVrIrJYAwR0bSJE87RfnGFfa3sxSgvRC06qiTTBhaqc9CkwKkbAWJtzBwjyXEa1BTTppUQTJDCmTMAX1AxYbDDPjKuCY2NzpK1LdxjjzUDKBXKlQV8UhTEiALmfFN4DbRgyh2ioZimwB01NJJpn4rR03ANrdcUvMPma6F2psyofGw0DSpF2YLvABO33b4D7Lh++LnSZISQRYvUQLLDqQPYG2JDANRYjaybj+MRSid9sOIqCENh4HU8iGHLAdPKLVpVKlNw7KutgW8UAjXANjAg9bczi5ZTg3eKNVENqVQPDVDDTqOkQoteQCRym+284qd2KfdoopkxtIJN5MyT6zGG5AAishJJMpfAs8is8l1WoqiRGoFXlWty35jcdMccY7QGrUkooVfDA2ABAEWGyheX3Rhtn8gsSBTlmY3XyUgSpmzFxebR0xUs7AcgMp81AImDNj5jY88HwxdmZ8mobSanxFgIVFvIMgQQ6kGbfFfeYOCc7xp60A6V0am0gXl21VD57sfK/WcIg/IGBIMkza3xegvaduuOdai7MImSB8UWJ3FibixxTQLk7PFx7kuO1Vqd4pLMoYgtG8jSY6yBvM4eZTthmBJYKbgrIHhJjWT1nmDsZ2NsX3/ANnXDJ/9LP8A96v/AP0x5vkuCsueOWCatFfRp2Lork7rHxIJkXv1xY4NPM5XPpL12Y7TVczmGpqioFpkMjltUqwBIIWxuAVI98S8a4+MtmadBlvU3IvOowreQkEex6DD/wD0HRGtqVNadR1Yd4shpYbsQZa4BM7xfHnPZ3jj5jO0BnEBEGn4lhVYAldUmJ1AAefncxPRsMuq5dep8tHmXilVqOXKlO6pa+8BJ1sxRWBS0QBAgkXnywXwuqKy1KKsNdNQtQNIINRPDyusSZFpHkcJ+3nCWTLd9k6emorKHWkt6lMk2IXcB2Vr7eI8ziT+zWslbLFnVO/DurGAWZQzOl7yqhtO9ve5bpj5lBq4Rnuou4N2nepVzD1VKRmVy+gXKtIUKbwIMS08jAPM3ifBnr0/EykEMyETEM0qHBHxBTFpFjgfh9DMZfidRa1AvlXJFOr3QIDMQ6SVXcSyFjckAk4M7ZcMzTBXyIVtSwacqqggqwqAEiZC6Cv+AxY4ll6Jmx6VNbj8IB1A4Mox4BSDGmaw194qbbtEkTvYA2w7y3YFhSY1HCnZT/h+I7TtJ57euD8/2YV4zKK9KqzLW7hyqzVAbUF1c5k3I35Wg/L9reHhWWslValMFCD8SwSIs3mb4w9Rg6hCArfSOiY2FiUziPAaVOR3oLamAEk6SPEOXMDfafDeCMMOAcEVwr6tRSJW94IDBrDr5yPljqhw7L5mualMtSp+I62ZRALAwA7TeWMi3iM+dt4BxHK5em1EV6AQOSCtUatp1MSNJeRNjEQLxLUHSZcmOgd69dt4rBEb9J5txbitWnV1rqCCowBYbmbwbAmQxA8jjKnaLPqVTS6VWOpT3JWo0AgBSVBb42ECfiHMYuNPMUTk2yVfMU66WTvYKBQ8mnojXqKkW2AAAPXHHEOIZStl6dDMZtzVoOrJXSg61A6RBAOoEkb8jYwIt6C9OoFSZyMTdyq8L4nmjmw9TL1qtRVIqI1KoWKMjKO8GksF8QNxB9MXDs3QptTavVohGLLpbTaS3Q/EdREc5Ii5xuv2oyz1kqrq71JTUtIK1RfENLsWP2UnVEAg38jrL8fonvnGXzLd8R3lEqr0iSNMwATdRcCx5jCP0wJsGpRcpAo7yv8AaDNURmWp+FiKZaUJEMfEpRrEm8Rptq/pOK3m+Lu9VWBJKkQVYsJ7koSDcwSCZ3/AWbMcByjnUuUrqZM/bVKe5OymmwA5wAOWNZLsxlgWNWjV6pFRiQZEavCNQjy5bYHglQPWEMxMY9j2Z6dUMrkMiqYU2lx05xJ9pwl472X7sk00rEEmHKeEEkTIFxY7EDnaMWjJcYTL0+6p0ajG51NETFtUIp022AJ35nFU4zkszUq1KqtWmoZKjX3Q5AQ1O4AtfEj0dU6tv29J2TKGNMNu8k4D2ZFdYGXzLeOdaqk04YhdWojcEm0235YlzfZkUH8DVSwAaBTZiPEAQyqCxiDywoytDPU/CKtSip3K1GUDpsZj29cXDsnVp5WnUetWq1HeGLupaEUGFB1HYlmNhOsCDGOHSMz+ZiBELJp8om+zOa7qvqrA0pBjX4JJK2E/li50eJ0q9DMaGDkUwfA8wxTrqk3U28tsUrtDxJK7KgXwgqy1ZB031NK/EslVHW2xBOGfAKFPL0yhY6idbeF7SAB8SKYhem84f4RWci9u8s2Y6dRG/aXD/WOlzV5/fnjMV1eJ0CJFamQdiGEYzFvg1/qfQSPij+n9Z5I2bBvqZvZiPqDhvwjPpSqK7F6e4JRAxtuADty8hY3xyMqn8i/8I/MfXE3DuH1Mw1QLVCBHYEkTY2UKOg0nmN8TOTaXXHvLZ/rrw9fgyuYY7ywpAAxYjfTtyAxUuLcUpVK1SoJQOxYKQSRN4OmRI8sHP2YmzZ4D0pCR/wDkxqp2Zy8f+oqT1Cj8I/PC6tXJPv8ACPp08D3+cEymXOZXu0qOFB1OqD4ogKTqHLU3/Hh3w7hncyJdgQAQ0EWmDAUbSfmcQcOyNPLz3dWpLWJOna2wjyGCqHF6dKorVajRyLE92GkRqIEA9Jt7jGlN6X/MgwA8x5lh4LRp3YBA7AW0KBpsQpgTM3+VrYl4lwpKs61IYCZuWt1j+9X/AJrAeWDKdalXAmzcjN/908/Q/LG3LIIqDWnJhuPXp+78sagoAqZ7veUPinBnjUjJpE3BMGN47zbY28h798I7K5ZAy1jTZ2QsvdoGgllgMwSN1bn57Ys1ShqLFDTrKdy6VNY5QxpiHuDuB74kyAqIYfTBJtTJAAm1qpBETeBcRvAwhxhhU4PRuUyj2A/ry49B+lHCTiPA6lMsrUWG4B1DSY5rEEjY+/LHsP8AH0DTKisO8FSN5iLGy2AkHymcD5ikjqVeozA8hH5g4dQGsCKwK7yk5D+0BqahKtMMygDUGid9wRY+5wFxTtLTrEvSy9NazD+9DtqXa8BCpMWxZOIcNoKfDDcoMah+o9sRU6CjZQMcdXBnADkSvcM7V52koUrrHiN6bEgkzYrFrk7YmzuerZnehS1b6/4dlqqQRBWoTIOLEijyxL+9sDeGhK5leIcTQwWd1gR/dSN+dieW841xGln8yVNRiNDBluqsGBNwyDULee9+kWZcdd5H7jHGEKIgytDiAXSap6ajVYn1+D88DZ3JVqLCq9erpYw7JrcidMEzVE2WBy5cxiyPXjmB6/8AkYQ9oeK6VAJDKTuokgjqRYe+F5jUBvK7xarqACO1YwZ715E7DSjSBbzN9umFdLg1Z0dwI0RYFbyYGkA/uMOmz9NgJj/eH64ZcHydJm23U7ExuPP8MA49ojZK9JXsitVfC9IsN4ZtN5BBBBmbYtGXTJ6ZKOGt4SWYmRB2Yi0AbzcecT1FpJyC+ZP6nEeRz+XStTOtAA6EnkAHBJJGwjn0wgAHEXxW+UZLwpLsKCkFdV1O69YUjVf8cd1crGlkpIZsdoAi24m3l8sXPP5OJOmRF3JCIB5sZb5ThBXqIwJDBgN3Pgpjy1vLH2xZKPpMmbqMicQM0NSNDLN1KlY+7abnzE3GC8pkQ5IapF9gOXvOO3pg0RVUhgJGrZY8jUlo9BfChMy8/ZgtcbA6dxN2PToMNoDA1Ebrnxsur13jPifDNLQlcgQdwpO/thYck+qP48DyNIfs41mhUZiTb58/OR+GBXy7k3P4H/twU6fbcyGX7VbVsI9yPCCVM51W6HREWtI23xJl+DEROepkeVNZ+rL+GFfDqQQEXv5nmZPPBlRECk6R73/HCvi3o/4EqvXMy6q+pgPFRWQkK9NhyJRwfeCRiM5V3peKrSSQBGgvJjzEH3GCMrSFUGFUR0GBczTgwRJGwkx8hg+CpIHaBetetfoeJCnAKz6VQUSNMNCgXLPsAogQRb164kfspVXWHy7NI8JSpUAm86gZkfLBGQpNTab4e0eIsWADG5AxHKNL1Qqehgyl8YN00qScCzAAH8NFv/mf/tjMeyoywLDbGYn8Rj/lH1jaM385nz4X/wAvptbHHDG0vVE3LlgAeUnflzHpOEneNvqY87k/mT+GCsm0ulvX/ONhiePHXM1NkuWFqp/Zxz3vp9cRRO30BOOdJ6YtxFkr1fL8MRmoeSjGgp6fljCr8h+eFJnSOlxl8oAQs0tQU052n/Zk/D6fD6Y9B4H2kSqshiQLMCIdJ5OpuPf1EjHk3aEMdNNhdmAJgAAE3gvaSAx9FvuMR5dqVAh/40axcstUmSQJ8KSCDAsZEADDpkI2PEmy77T1XinDKNVy2XzVOjUkK6lRUW4BHhDKyNBBs0EHY2OBh2Xkgvm8wGgiaVMoDP8AjWoPmcURO3B7xNP2+kkkopVtiNmAANwZWBvYc2Wa7duRC5f1NSvTWPaScWDL3kzfaegcIytJACKIYxqFV1oBm1EmfswNJv8AyLv1nBGYyGXqMTUo02neRqB9VI0++PJKPa3NhAEbLIBtqYkgTYXIsBaQDtjZ4/Xf+84hE/dpKix6NuR7YHiIBBpYz13Lpl6dqdJEHREVR8gBgPijUGBMhX5XA1GLAj7xPzx5SKiMaeqvVrEv4lqVdaldLSNPy+WLFw5qS/BRpp5qir8yBg6weIQjese6T+9vxxHVZxyE8vFAPuRjaVsS1GsZJ2979PPb5464SIiznFqqGCsep/DSb47o5iq4kP7QP+7E3GU1UwCvO3yO1ucYCydTSPbDKBqoyOZmXGWTkd5tcrWc3qkeWoj6C2OM1wWnAL1Cb7R+c4jbOkGxxNknNQhTtONp6VFNnieGPtHqMg0jYmLjwegu2v3aP+mMGcL7PKW8NdlGk2InmOkfngvtFwsUmBpudJGx3B9eYwJw/Nsk+mOGNHS1Em+fqMGQrkbePOFdg6NR4NRvVmgf8on64ZZnsCKdxTRvOZ/6sD8DWrV8WrT6YtWVD7FicYchKNS1PTw/+uMF73+cqTZrPUpC6FAsDoQkAbAFgTYRgCrnq9Rh3470jbvGYgeiqyqPlj0N+EasBVuznpgpnUcgXBk6VieTUBDzlYYTA2Fh6CNhivPxwC2kiOQjFzqcKbQVxWsz2cPlgdMUs6u8P2irMF0egqLKnGp5H5D9cRrxIH/xgqvwEjpgccLYb3Hyx6CjFW08JxmvcTpOIAESfpgipxVNDAncW6zhdVybDA1XKt0wTiQmBc2VfSOODcSChgf37Y3Tq95UtcYQU6Z5YecDy8NJxLMgW2E2dG/iacZ4Eb54aQOeAsjmh3qzO+D+JLbCTKsBUEjn74yIupDPaynRlFT0xc3YXH798ZitLn/P6D9MZjzPAM9XxVnmtPswu7Vn9RpH4jDGhkKVJYRSfM3npPL2jDEGNgB+/wDPENS5vf8AfrjQDEqDhOo+UCMclfIe5xmYrwYvgdqx/fzw9RbhQaP5R7T+mIamaA3b5R+YwAXN744/hmbmMcZ1mJuKVQ2aR6ih1AeEILDSAoBINp1MTtyG8YJPESLU6SoPIKo9go/TBGX4UdRZyJOwE2UG2/Mkkn18sHJkFHIYWjBETV6zfePtYfS/yOMXK1Sefrz9zucWnL8P1C0e5/QYMpcOHMj2GDonSoJweo25+pwbQ7PdT8v/ADi1jKKo6xf9xiZqYHLDBBOsyvZfgYkSTYz9OdvPDallAuJ1ry2lRcCTJix6QDiOudKlmMgCSFt+f0mMMF7RS0kDxYXI5Dl7mAPfENfMhbsQTyUflJv6mPacKsxxUkRTAQeW/wCg+XviLhyktJM/j/5w3AuLdmjG2fmBbleDaeg8vPnE2sBymVIpO56W9zv+P1xYKeSXSrEA6hI6R5/p9cL+Lv4YxXpMbEgtPM+1erVVKY+JWCk4Y8JXxYBNsEZZiLg49XJuDPAxNTgwri+Z1EDpjeSyErqOAal2nD/JNC26YzZLRAFmvEVzZmZ95JwrOFPDizcPzgJnFSFOGw5yFWBjHmS95tw568vaW6jnOpxIMwDisrm4O2GFCrOMxx1Na9QTtG1StgKow6Y4argSpUwEEbK8kraTywJUoqeWJdWOSMXXaYmIMAq5UYDrZQYbOMQuuLKxkGWJxkxgilSjlgkoMSouOZrj4RXEGqGRGAmywmR+OHFWmIwBUEYVD2mjI3eQ38/pjWJbdMZg6YvjfOf/2Q==" alt="Generic placeholder image" width="200" class="ml-lg-5 order-1 order-lg-2">
                            </div>
                        </li>
                        </c:if>
                    </c:forEach>
                </ul> 
            </div>
        </div>
    </div>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript'>#</script>
</body>
</html>
