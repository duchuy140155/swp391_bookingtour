<%-- 
    Document   : viewLocacation
    Created on : May 21, 2024, 7:01:36 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Danh sách Địa điểm</title>
        <link rel="stylesheet" type="text/css" href="css/viewLocation.css">
    </head>
    <body>
        <h1>Danh sách Địa điểm</h1>
        <div class="location-container">
            <c:forEach var="location" items="${locations}">
                <div class="location-item">
                    <img src="uploads/${location.locationImage}" alt="Location Image" class="location-image">
                    <div class="location-info">
                        <h2 class="location-name">${location.locationName}</h2>
                        <p class="location-description">${location.locationDescription}</p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </body>
</html>

