<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Spin Wheels</title>
        <style>
            @keyframes fireworks {
                0% {
                    opacity: 1;
                    transform: translateY(0);
                }
                100% {
                    opacity: 0;
                    transform: translateY(-100vh);
                }
            }

            .firework {
                position: absolute;
                width: 10px;
                height: 10px;
                border-radius: 50%;
                background-color: #ffcc00;
                animation: fireworks 1s ease-out;
            }

            body {
                margin: 0;
                padding: 0;
                background-color: aquamarine;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                height: 100vh;
                overflow: hidden;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            .wrapper {
                width: 500px;
                height: 500px;
                background-color: #ccc;
                border-radius: 50%;
                border: 15px solid #dde;
                position: relative;
                transition: transform 5s ease-out;
            }
            .wrapper div {
                height: 50%;
                width: 200px;
                position: absolute;
                clip-path: polygon(100% 0, 50% 100%, 0 0);
                transform: translate(-50%);
                transform-origin: bottom;
                text-align: center;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 20px;
                font-weight: bold;
                font-family: sans-serif;
                color: #fff;
                left: 135px;
            }
            .wrapper .one {
                background-color: brown;
                left: 50%;
            }
            .wrapper .two {
                background-color: green;
                transform: rotate(45deg);
            }
            .wrapper .three {
                background-color: beige;
                transform: rotate(90deg);
                color: #000;
            }
            .wrapper .four {
                background-color: rosybrown;
                transform: rotate(135deg);
            }
            .wrapper .five {
                background-color: rebeccapurple;
                transform: rotate(180deg);
            }
            .wrapper .six {
                background-color: blue;
                transform: rotate(225deg);
            }
            .wrapper .seven {
                background-color: red;
                transform: rotate(270deg);
            }
            .wrapper .eight {
                background-color: orange;
                transform: rotate(315deg);
            }
            .arrow {
                position: absolute;
                top: 10px;
                left: 50%;
                transform: translateX(-50%) rotate(180deg);
                color: #6a7dfe;
                font-size: 50px;
            }
            #spin {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                z-index: 99;
                background-color: #e2e2e2;
                text-transform: uppercase;
                border: solid 8px #fff;
                font-weight: bold;
                font-size: 20px;
                color: #a2a2a2;
                width: 120px;
                height: 120px;
                font-family: sans-serif;
                border-radius: 50%;
                cursor: pointer;
                outline: none;
                letter-spacing: 1px;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
            }
            #belowButton {
                margin-top: 20px;
                background-color: #4CAF50;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

        </style>
    </head>
    <body>
        
        <c:set value="${sessionScope.dateSpin}" var="t"/>
        <c:if test="${empty sessionScope.account}">
            <script type="text/javascript">
                window.location.href = 'login.jsp';
            </script>
        </c:if>

        <c:set value="${sessionScope.luckyDiscountTop1}" var="top1"/>
        <c:set value="${sessionScope.luckyDiscountTop2}" var="top2"/>
        <c:set value="${sessionScope.luckyDiscountTop3}" var="top3"/>
        <c:set value="${sessionScope.luckyDiscountTop4}" var="top4"/>
        <c:set value="${sessionScope.account.userID}" var="userID"/>
        <div class="container">

            <div class="arrow">↑</div>
            <button id="spin">SPIN</button>

            <div class="wrapper">
                <div class="one" id="${top1.discountID}">${top1.discountCode}</div>
                <div class="two">Chúc Bạn May Mắn</div>
                <div class="three" id="${top2.discountID}">${top2.discountCode}</div>
                <div class="four">Chúc Bạn May Mắn</div>
                <div class="five" id="${top3.discountID}">${top3.discountCode}</div>
                <div class="six">Chúc Bạn May Mắn</div>
                <div class="seven" id="${top4.discountID}">${top4.discountCode}</div>
                <div class="eight">Chúc Bạn May Mắn</div>
            </div>

        </div>
        <button hidden id="saveDiscountButton">Save Discount</button>
        <script>
            function sendRequest(userID, discountID) {
                let currentTime = new Date();
                let formattedTime = currentTime.toLocaleString();
                console.log("Current Date and Time:", formattedTime); 
                console.log("Discount ID:", discountID); 
                window.location.href = "spintimeprocess?id=" + userID + "&time=" + encodeURIComponent(formattedTime) + "&discountid=" + discountID;
            }
        </script>
        <script>
            let wrapper = document.querySelector(".wrapper");
            let btn = document.getElementById("spin");
            let number = Math.ceil(Math.random() * 5000);
            let isSpinning = false;
            let userID = "${userID}"; 

            btn.onclick = function () {
                if (isSpinning) {
                    return;
                }

                isSpinning = true;

                let newNumber = Math.ceil(Math.random() * 5000);
                let totalRotation = number + newNumber;
                wrapper.style.transform = "rotate(" + totalRotation + "deg)";

                setTimeout(() => {
                    let actualDeg = totalRotation % 360;
                    let segmentIndex = Math.floor((360 - actualDeg + 22.5) % 360 / 45);
                    let segmentNames = ["one", "two", "three", "four", "five", "six", "seven", "eight"];
                    let segment = document.querySelector("." + segmentNames[segmentIndex]);

                   
                    let allDivs = document.querySelectorAll(".wrapper > div");
                    allDivs.forEach(div => {
                        div.style.backgroundColor = "";
                    });

                  
                    segment.style.backgroundColor = "yellow";

                    
                    let discountID = segment.getAttribute("id");
                    console.log("Selected Discount ID:", discountID); 

                    alert("Quay vào ô: " + segment.innerText);

                    document.getElementById('saveDiscountButton').disabled = false;
                    document.getElementById('saveDiscountButton').onclick = function () {
                        sendRequest(userID, discountID); // Truyền userID và discountID
                    };
                    document.getElementById('saveDiscountButton').click();
                    isSpinning = false;
                }, 5000);

                number = totalRotation;
            };
        </script>

        <script>
         
            let dateSpin = new Date('<c:out value="${t}" />');

           
            let currentTime = new Date();

            
            let timeDiff = currentTime - dateSpin;

          
            let hoursDiff = timeDiff / (1000 * 60 * 60);

          
            if (hoursDiff < 24) {
                
                document.getElementById('spin').disabled = true;
                alert('Bạn chỉ được phép quay lại sau khi hết 24 giờ.');
            }
        </script>
    </body>
</html>
