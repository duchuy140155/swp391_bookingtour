<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Danh sách các tour đã đặt</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f9;
                margin: 0;
                padding: 0;
            }
            .container {
                width: 80%;
                margin: 20px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            .header {
                font-size: 28px;
                font-weight: bold;
                margin-bottom: 20px;
                text-align: center;
                color: #333;
            }
            .tour-card {
                border: 1px solid #e0e0e0;
                border-radius: 8px;
                padding: 20px;
                margin-bottom: 15px;
                transition: box-shadow 0.3s;
                background-color: #fff;
                position: relative;
            }
            .tour-card:hover {
                box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
            }
            .tour-title {
                font-size: 22px;
                font-weight: bold;
                color: #333;
                margin-bottom: 10px;
            }
            .tour-details {
                display: flex;
                flex-wrap: wrap;
                margin-bottom: 10px;
            }
            .tour-details div {
                flex: 1 1 30%;
                margin-bottom: 10px;
            }
            .tour-details div span {
                font-weight: bold;
                color: #333;
            }
            .tour-description {
                font-size: 14px;
                color: #888;
                margin-bottom: 10px;
            }
            .btn-container {
                position: absolute;
                right: 20px;
                bottom: 20px;
                margin-bottom: 35px;
            }
            .btn {
                display: inline-block;
                padding: 10px 20px;
                font-size: 16px;
                color: #fff;
                background-color: #007bff;
                border: none;
                border-radius: 4px;
                text-decoration: none;
                transition: background-color 0.3s;
            }
            .btn:hover {
                background-color: #0056b3;
            }
            .pagination {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }
            .pagination a {
                color: #007bff;
                padding: 8px 16px;
                text-decoration: none;
                transition: background-color 0.3s;
            }
            .pagination a.active {
                background-color: #007bff;
                color: white;
                border-radius: 5px;
            }
            .pagination a:hover {
                background-color: #0056b3;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <button type="button" name="back" onclick="history.back()" class="btn btn-secondary">Trở về</button>
            <div class="header">Danh sách các tour đã đặt</div>

            <c:choose>
                <c:when test="${not empty bookingDetails}">
                    <c:forEach items="${bookingDetails}" var="bookingDetail">
                        <div class="tour-card">
                            <div class="tour-title">${bookingDetail.tour.tourName}</div>
                            <div class="tour-description">
                                ${bookingDetail.tour.tourDescription}
                            </div>
                            <div class="tour-details">
                                <div><span>Ngày đặt:</span> <fmt:formatDate value="${bookingDetail.bookingDate}" pattern="dd/MM/yyyy" /></div>
                                <div><span>Ngày bắt đầu:</span> <fmt:formatDate value="${bookingDetail.tour.startDate}" pattern="dd/MM/yyyy" /></div>
                                <div><span>Ngày kết thúc:</span> <fmt:formatDate value="${bookingDetail.tour.endDate}" pattern="dd/MM/yyyy" /></div>
                                <div><span>Số lượng vé:</span> ${bookingDetail.totalPeople}</div>
                                <div><span>Giá:</span> ${bookingDetail.finalPrice}</div>
                                <div><span>Trạng thái:</span> 
                                    <c:choose>
                                        <c:when test="${bookingDetail.tour.endDate < currentDate && bookingDetail.status == 1}">
                                            Chuyến đi đã kết thúc
                                        </c:when>
                                        <c:when test="${bookingDetail.tour.startDate < currentDate && currentDate < bookingDetail.tour.endDate && bookingDetail.status == 1}">
                                            Chuyến đi đang diễn ra
                                        </c:when>
                                        <c:when test="${bookingDetail.tour.startDate > currentDate && bookingDetail.status == 1}">
                                            Chuyến đi chuẩn bị bắt đầu
                                        </c:when>
                                        <c:when test="${bookingDetail.tour.startDate > currentDate && bookingDetail.status == 0}">
                                            Đang chờ phê duyệt
                                        </c:when>
                                        <c:when test="${bookingDetail.tour.endDate < currentDate && bookingDetail.status == 0}">
                                            Đã bị huỷ
                                        </c:when>
                                        <c:when test="${bookingDetail.tour.startDate < currentDate && currentDate < bookingDetail.tour.endDate && bookingDetail.status == 0}">
                                            Đã bị huỷ
                                        </c:when>
                                        <c:otherwise>
                                            Đang cập nhật
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <div class="btn-container">
                                <a href="javascript:void(0);" class="btn" onclick="submitForm(${bookingDetail.bookingID})">Xem chi tiết</a>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <p>Không có tour nào đã đặt.</p>
                </c:otherwise>
            </c:choose>

            <div class="pagination">
                <c:if test="${currentPage > 1}">
                    <a href="?page=${currentPage - 1}">&laquo; Trước</a>
                </c:if>
                <c:forEach begin="1" end="${noOfPages}" var="i">
                    <a href="?page=${i}" class="${i == currentPage ? 'active' : ''}">${i}</a>
                </c:forEach>
                <c:if test="${currentPage < noOfPages}">
                    <a href="?page=${currentPage + 1}">Tiếp &raquo;</a>
                </c:if>
            </div>
        </div>
        <script>
            function submitForm(bookingID) {
                var form = document.createElement('form');
                form.method = 'POST';
                form.action = '<c:out value="${pageContext.request.contextPath}"/>/bookerTour';
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = 'bookingID';
                input.value = bookingID;
                form.appendChild(input);
                document.body.appendChild(form);
                form.submit();
            }
        </script>
    </body>
</html>
