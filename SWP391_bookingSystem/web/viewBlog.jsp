<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <title>View Blog</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image: url('img/another.jpg');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            margin: 0;
            padding: 0;
        }

        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .blog-post {
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .blog-header {
            display: flex;
            flex-direction: column;
            margin-bottom: 10px;
        }

        .blog-header h2 {
            margin: 0;
            font-size: 1.5em;
        }

        .blog-header p {
            margin: 5px 0;
            color: #555;
        }

        .blog-content {
            margin-bottom: 10px;
        }

        .blog-content img {
            max-width: 100%;
            height: auto;
            border-radius: 10px;
            margin-top: 10px;
        }

        .comments-section {
            margin-top: 20px;
        }

        .comment, .reply {
            background-color: #f0f0f0;
            padding: 10px;
            border-radius: 5px;
            margin-bottom: 10px;
            margin-left: 20px;
            position: relative;
        }

        .comment p, .reply p {
            margin: 5px 0;
        }

        .edit-textarea {
            width: 100%;
            padding: 10px;
            margin-top: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
            display: none; /* Initially hide the textarea */
            background-color: white; /* Ensure background is white */
            color: black; /* Ensure text color is black */
        }

        .edit-btn, .save-btn {
            background-color: #1877f2;
            color: black;
            padding: 5px 10px;
            border: none;
            border-radius: 5px;
            margin-top: 10px;
            cursor: pointer;
        }

        .edit-btn:hover, .save-btn:hover {
            background-color: #165db0;
        }

        .save-btn {
            display: none; /* Initially hide the save button */
        }

        .back-button {
            display: inline-block;
            margin-bottom: 20px;
        }

        .back-button img {
            width: 50px;
            height: auto;
            cursor: pointer;
        }

        .menu-icon {
            position: absolute;
            top: 10px;
            right: 10px;
            cursor: pointer;
            font-size: 20px;
            font-weight: bold;
            color: #888;
        }

        .menu {
            display: none;
            position: absolute;
            top: 25px;
            right: 10px;
            background-color: white;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            z-index: 1;
        }

        .menu button {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: white;
            border: none;
            cursor: pointer;
            text-align: left;
        }

        .menu button:hover {
            background-color: #f0f0f0;
        }

        /* Modal styles */
        .modal {
            display: none;
            position: fixed;
            z-index: 2;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
            padding-top: 60px;
        }

        .modal-content {
            background-color: #fefefe;
            margin: 5% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            max-width: 500px;
            border-radius: 10px;
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover, .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .report-form select, .report-form textarea {
            display: block;
            width: 100%;
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        .report-form button {
            background-color: #1877f2;
            color: white;
            padding: 10px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .report-form button:hover {
            background-color: #165db0;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        function toggleEdit(commentID) {
            var textarea = document.getElementById('edit-textarea-' + commentID);
            var saveButton = document.getElementById('save-btn-' + commentID);
            var editButton = document.getElementById('edit-btn-' + commentID);
            var menu = document.getElementById('menu-' + commentID);

            if (textarea.style.display === 'none' || textarea.style.display === '') {
                textarea.style.display = 'block';
                saveButton.style.display = 'block';
                editButton.style.display = 'none';
                menu.style.display = 'none'; // Hide the menu
            } else {
                textarea.style.display = 'none';
                saveButton.style.display = 'none';
                editButton.style.display = 'block';
            }
        }

        function confirmDelete() {
            return confirm("Bạn có muốn xóa bình luận này?");
        }

        function toggleMenu(commentID) {
            var menu = document.getElementById('menu-' + commentID);
            if (menu.style.display === 'none' || menu.style.display === '') {
                menu.style.display = 'block';
            } else {
                menu.style.display = 'none';
            }
        }

        function openModal() {
            document.getElementById('reportModal').style.display = "block";
        }

        function closeModal() {
            document.getElementById('reportModal').style.display = "none";
        }

        // Hide menu when clicking outside
        document.addEventListener('click', function(event) {
            var menus = document.querySelectorAll('.menu');
            menus.forEach(function(menu) {
                if (!menu.contains(event.target) && !event.target.classList.contains('menu-icon')) {
                    menu.style.display = 'none';
                }
            });

            var modal = document.getElementById('reportModal');
            if (event.target == modal) {
                modal.style.display = "none";
            }
        });

        $(document).ready(function() {
            // Disable submit button after form submission to prevent double submissions
            $('.report-form').submit(function(event) {
                event.preventDefault();
                var form = $(this);
                var submitButton = form.find('button[type="submit"]');
                submitButton.prop('disabled', true);
                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function(response) {
                        alert('Report submitted successfully.');
                        closeModal();
                    },
                    error: function(xhr, status, error) {
                        alert('An error occurred: ' + error);
                    },
                    complete: function() {
                        submitButton.prop('disabled', false);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <div class="container">
        <a href="listBlog" class="back-button"><img src="${pageContext.request.contextPath}/img/images (1).png" alt="Back to List"></a>
        <div class="blog-post">
            <div class="blog-header">
                <h2>${blog.title}</h2>
                <p>By: ${blog.user.userName}</p>
                <p>${blog.createdAt}</p>
            </div>
            <div class="blog-content">
                <p>${blog.content}</p>
                <c:forEach var="picture" items="${blog.pictures}">
                    <img src="${picture.pictureURL}" alt="Blog Image">
                </c:forEach>
            </div>
            <div class="comments-section">
                <h3>Comments</h3>
                <c:forEach var="comment" items="${blog.comments}">
                    <div class="comment">
                        <p><strong>${comment.user.userName}</strong>: ${comment.comment}</p>
                        <p>${comment.createdAt}</p>
                        <c:if test="${comment.user.userID == sessionScope.account.userID}">
                            <div class="menu-icon" onclick="toggleMenu('${comment.commentID}')">...</div>
                            <div class="menu" id="menu-${comment.commentID}">
                                <button type="button" id="edit-btn-${comment.commentID}" class="edit-btn" onclick="toggleEdit('${comment.commentID}')">Edit</button>
                                <form action="deleteComment" method="post" style="display:inline;" onsubmit="return confirmDelete();">
                                    <input type="hidden" name="commentID" value="${comment.commentID}">
                                    <button type="submit">Delete</button>
                                </form>
                            </div>
                            <form action="editComment" method="post" style="display:inline;">
                                <input type="hidden" name="commentID" value="${comment.commentID}">
                                <textarea id="edit-textarea-${comment.commentID}" class="edit-textarea" name="comment">${comment.comment}</textarea>
                                <button type="submit" id="save-btn-${comment.commentID}" class="save-btn">Save</button>
                            </form>
                        </c:if>
                        <form action="createComment" method="post">
                            <input type="hidden" name="blogID" value="${blog.blogID}">
                            <input type="hidden" name="parentCommentID" value="${comment.commentID}">
                            <textarea name="comment" placeholder="Reply to this comment"></textarea>
                            <button type="submit">Reply</button>
                        </form>
                        <c:forEach var="reply" items="${comment.replies}">
                            <div class="reply">
                                <p><strong>${reply.user.userName}</strong>: ${reply.comment}</p>
                                <p>${reply.createdAt}</p>
                                <c:if test="${reply.user.userID == sessionScope.account.userID}">
                                    <div class="menu-icon" onclick="toggleMenu('${reply.commentID}')">...</div>
                                    <div class="menu" id="menu-${reply.commentID}">
                                        <button type="button" id="edit-btn-${reply.commentID}" class="edit-btn" onclick="toggleEdit('${reply.commentID}')">Edit</button>
                                        <form action="deleteComment" method="post" style="display:inline;" onsubmit="return confirmDelete();">
                                            <input type="hidden" name="commentID" value="${reply.commentID}">
                                            <button type="submit">Delete</button>
                                        </form>
                                    </div>
                                    <form action="editComment" method="post" style="display:inline;">
                                        <input type="hidden" name="commentID" value="${reply.commentID}">
                                        <textarea id="edit-textarea-${reply.commentID}" class="edit-textarea" name="comment">${reply.comment}</textarea>
                                        <button type="submit" id="save-btn-${reply.commentID}" class="save-btn">Save</button>
                                    </form>
                                </c:if>
                            </div>
                        </c:forEach>
                    </div>
                </c:forEach>
                <form action="createComment" method="post">
                    <input type="hidden" name="blogID" value="${blog.blogID}">
                    <textarea name="comment" placeholder="Add a comment"></textarea>
                    <button type="submit">Comment</button>
                </form>
                <button onclick="openModal()">Report Blog</button>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div id="reportModal" class="modal">
        <div class="modal-content">
            <span class="close" onclick="closeModal()">&times;</span>
            <h2>Report Blog</h2>
            <form class="report-form" action="reportBlog" method="post">
                <input type="hidden" name="blogID" value="${blog.blogID}">
                <input type="hidden" name="userID" value="${sessionScope.account.userID}">
                <label for="typeID">Report Type:</label>
                <select name="typeID" id="typeID" required>
                    <c:forEach var="type" items="${reportTypes}">
                        <option value="${type.typeID}">${type.typeName}</option>
                    </c:forEach>
                </select>
                <label for="reason">Reason:</label>
                <textarea name="reason" id="reason" required></textarea>
                <button type="submit">Submit Report</button>
            </form>
        </div>
    </div>
</body>
</html>
