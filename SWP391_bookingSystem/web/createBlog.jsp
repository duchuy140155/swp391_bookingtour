<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Create Blog</title>
    <!-- Include CKEditor CSS and JS -->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
</head>
<body>
    <div class="create-blog-form">
        <h2>Chia sẻ trải nghiệm của bạn ở đây</h2>
        <form action="createBlog" method="post" enctype="multipart/form-data">
            <input type="text" name="title" placeholder="Blog Title" required>
            <textarea name="content" id="editor" placeholder="Blog Content" rows="5" required></textarea>
            <button type="submit">Đăng</button>
        </form>
    </div>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                ckfinder: {
                    uploadUrl: 'uploadImage'
                }
            })
            .catch(error => {
                console.error(error);
            });
    </script>
</body>
</html>
