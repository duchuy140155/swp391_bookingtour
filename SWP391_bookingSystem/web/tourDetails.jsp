<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết Tour</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f8f9fa;
            }
            .info-box, .support-box, .details-box {
                background-color: #fff;
                border: 1px solid #dee2e6;
                border-radius: 5px;
            }
            .info-box p, .details-box p {
                margin: 0;
                padding: 5px 0;
                font-size: 16px;
            }
            .support-box p {
                margin: 0 0 10px;
                font-size: 16px;
            }
            .btn-primary {
                background-color: #4c6ef5;
                border-color: #4c6ef5;
            }
            .btn-outline-primary {
                border-color: #4c6ef5;
                color: #4c6ef5;
            }
            .btn-outline-primary:hover {
                background-color: #4c6ef5;
                color: #fff;
            }
            .mt-5 {
                margin-top: 3rem;
            }
            .mt-3 {
                margin-top: 1rem;
            }
            .btn i {
                margin-right: 5px;
            }
            .info-box strong, .details-box strong {
                font-size: 16px;
            }
            p {
                color: #333;
            }
            .details-box .row {
                display: flex;
                align-items: center;
                flex-wrap: wrap;
            }
            .details-box .row div {
                flex: 1 0 50%;
                max-width: 50%;
            }
            .details-box .row:nth-child(2) {
                margin-top: 1.5rem;
            }
            .header-container {
                background: #50b3a2;
                color: #fff;
                padding: 10px 0;
                text-align: center;
            }
            .header-container a {
                color: #fff;
                text-decoration: none;
                margin-right: 10px;
            }
            .header-container .btn {
                background-color: #50b3a2;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 12px;
            }
            .card-header {
                cursor: pointer;
            }
            .container {
                width: 100%;
                max-width: 1400px;
                margin: auto;
                overflow: hidden;
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0,0,0,0.1);
            }
            .tour-header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                margin-bottom: 20px;
                flex-wrap: nowrap;
                width: 100%;
            }
            .tour-title {
                font-size: 1em;
                font-weight: bold;
                flex: 3;
            }
            .tour-rating, .tour-likes {
                display: flex;
                align-items: center;
                margin-right: 20px;
            }
            .tour-rating {
                background-color: #ffc107;
                border-radius: 5px;
                padding: 5px 10px;
                color: #fff;
            }
            .tour-rating .rating-score {
                font-size: 1.2em;
                font-weight: bold;
                margin-right: 5px;
            }
            .tour-likes i {
                color: red;
                margin-right: 5px;
            }
            .tour-price-actions {
                display: flex;
                align-items: center;
                justify-content: flex-end;
                flex: 2;
            }
            .tour-price {
                display: flex;
                align-items: center;
                margin-right: 20px;
            }
            .tour-price .original-price {
                text-decoration: line-through;
                margin-right: 10px;
                color: #999;
            }
            .tour-price .discounted-price {
                font-size: 1em;
                font-weight: bold;
                color: red;
                margin-right: 10px;
            }
            .discount-tag {
                background-color: red;
                color: white;
                padding: 2px 5px;
                border-radius: 3px;
                font-size: 0.9em;
                margin-right: 10px;
            }
            .tour-actions .btn {
                margin-left: 10px;
            }
            .container1 {
                display: flex;
                flex-wrap: nowrap;
            }
            .left {
                flex: 2;
                margin-right: 10px;
            }
            .right {
                flex: 1;
                display: flex;
                flex-direction: column;
            }
            .right img {
                margin-bottom: 10px;
            }
            .right img:last-child {
                margin-bottom: -4px;
            }
            img {
                width: 100%;
                height: auto;
                border-radius: 8px;
            }
            .tour-info, .images, .schedules-section, .guide-info {
                background: #fff;
                padding: 20px;
                margin: 20px 0;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .tour-info h2, .images h2, .schedules h2, .guide-info h2 {
                border-bottom: 2px solid #50b3a2;
                padding-bottom: 10px;
                margin-bottom: 10px;
            }
            .guide-info {
                margin-top: 20px;
            }
            .tour-header-left {
                display: flex;
                align-items: center;
            }
            .tour-title {
                font-size: 1em;
                font-weight: bold;
                margin-right: 20px;
            }
            .tour-price .original-price {
                text-decoration: line-through;
                margin-right: 10px;
                color: #999;
            }
            .tour-price .discounted-price {
                font-size: 1.3em;
                font-weight: bold;
                color: red;
            }
            .discount-tag {
                background-color: red;
                color: white;
                padding: 2px 5px;
                border-radius: 3px;
                font-size: 0.9em;
                margin-right: 10px;
            }
            .tour-actions {
                display: flex;
                align-items: center;
            }
            .tour-actions .btn {
                margin-left: 10px;
            }
            .image-gallery {
                display: flex;
                justify-content: space-between;
                margin-top: 20px;
            }
            .image-gallery img {
                border-radius: 8px;
                width: 100%;
            }
            .main-image {
                width: 65%;
                margin-right: 2%;
            }
            .side-images {
                width: 33%;
                display: flex;
                flex-direction: column;
                justify-content: space-between;
            }
            .side-images img {
                margin-bottom: 10px;
            }
            .guide-card {
                margin-top: 20px;
                padding: 15px;
                border: 1px solid #ddd;
                border-radius: 8px;
                background-color: #fafafa;
            }
            #review-sidebar {
                max-height: 70vh; /* Chiều cao tối đa của vùng đánh giá */
                overflow-y: auto; /* Tạo thanh cuộn dọc */
            }
            .guide-role {
                font-weight: bold;
                color: #0056b3;
            }
            .guide-name {
                font-weight: bold;
                color: #333;
            }
            .guide-address {
                color: #555;
            }
            .action-buttons {
                text-align: right;
            }
            .action-buttons .btn {
                background-color: #4CAF50;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 12px;
            }
            .action-buttons .btn.contact-consultant {
                background-color: #008CBA;
            }
            .image-grid {
                display: flex;
                flex-wrap: wrap;
            }
            .image-item {
                flex: 1 1 calc(33.333% - 10px);
                margin: 5px;
            }
            .image-item img {
                width: 100%;
                height: auto;
            }
            .calendar-container {
                display: flex;
                flex-wrap: wrap;
                padding: 20px;
                background-color: #ffffff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .calendar-left, .calendar-right {
                flex: 1;
                padding: 20px;
            }
            .calendar-header {
                text-align: center;
                margin-bottom: 20px;
                width: 100%;
            }
            .calendar-header h1 {
                font-size: 2.2em;
                color: #333;
                margin-bottom: 10px;
            }
            .calendar-body {
                display: flex;
                flex-direction: column;
                gap: 20px;
            }
            .calendar-day {
                display: flex;
                align-items: flex-start;
                margin-bottom: 20px;
            }
            .tour-info-box {
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
            }
            .tour-info-box p {
                margin-bottom: 10px;
                font-size: 1.1em;
            }
            .tour-info-box p strong {
                font-weight: bold;
            }
            .tour-info-box .btn-other-date {
                background: #fff;
                border: 1px solid #007bff;
                color: #007bff;
                border-radius: 8px;
                padding: 10px 20px;
                text-align: center;
            }
            .tour-info-box .btn-other-date:hover {
                background: #007bff;
                color: #fff;
            }
            .support-box {
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                text-align: center;
            }
            .support-box h5 {
                margin-bottom: 20px;
            }
            .support-box .btn {
                border-radius: 8px;
                padding: 10px 20px;
                font-size: 1.1em;
                width: 100%;
                margin-bottom: 10px;
            }
            .support-box .btn-support-call {
                background: #007bff;
                color: #fff;
                border: none;
            }
            .support-box .btn-support-call:hover {
                background: #0056b3;
            }
            .support-box .btn-support-request {
                background: #fff;
                color: #007bff;
                border: 1px solid #007bff;
            }
            .support-box .btn-support-request:hover {
                background: #007bff;
                color: #fff;
            }
            .calendar-day-number {
                flex-shrink: 0;
                width: 35px;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background-color: #c0392b;
                color: #fff;
                border-radius: 50%;
                font-size: 1.0em;
                margin-right: 20px;
            }
            .calendar-day-content {
                flex-grow: 1;
                padding-left: 15px;
                border-left: 2px dashed #c0392b;
            }
            .calendar-day-title {
                font-size: 1em;
                font-weight: bold;
                color: #333;
                margin-bottom: 10px;
            }
            .calendar-day-date {
                font-size: 1em;
                color: #000;
                margin-bottom: 10px;
                font-weight: bold;
            }
            .calendar-day-note {
                font-size: 0.9em;
                color: #333;
                margin-bottom: 10px;
            }
            .calendar-footer {
                text-align: center;
                margin-top: 30px;
                width: 100%;
            }
            .calendar-footer .btn {
                background-color: #2ecc71;
                color: #fff;
                border-radius: 20px;
                padding: 15px 30px;
                font-size: 0.9em;
                transition: background-color 0.3s, transform 0.3s, box-shadow 0.3s;
                text-decoration: none;
            }
            .calendar-footer .btn:hover {
                background-color: #27ae60;
                transform: scale(1.1);
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            }
            .end-message {
                text-align: center;
                font-size: 0.9em;
                color: #e74c3c;
                font-weight: bold;
                margin-top: 20px;
                width: 100%;
            }
            .end-note {
                font-size: 0.9em;
                color: #34495e;
                margin-top: 10px;
                width: 100%;
            }
            .fa-calendar-alt {
                margin-right: 10px;
                color: #c0392b;
            }
            .btn-review:hover {
                background-color: #3b5db3;
                cursor: pointer;
            }
            .guide-address {
                font-size: 16px;
                color: #777;
            }
            .guide-role {
                font-size: 18px;
                font-weight: bold;
                color: #4c6ef5;
            }
            .guide-name {
                font-size: 22px;
                color: #333;
            }
            .btn-review {
                display: inline-block;
                padding: 10px 20px;
                margin-top: 10px;
                font-size: 16px;
                color: #fff;
                background-color: #4c6ef5;
                border: none;
                border-radius: 5px;
                text-decoration: none;
                transition: background-color 0.3s ease;
            }
            .review-card {
                border: 1px solid #e6e6e6;
                border-radius: 8px;
                padding: 16px;
                margin: 16px auto;
                max-width: 600px;
                position: relative; /* Add this for positioning the options icon */
            }
            .review-header {
                display: flex;
                align-items: center;
                margin-bottom: 16px;
            }
            .review-header .user-icon {
                font-size: 40px;
                color: #6c757d;
                margin-right: 12px;
            }
            .review-header .name {
                font-weight: bold;
                font-size: 16px;
            }
            .review-header .country {
                font-size: 14px;
                color: gray;
            }
            .review-details {
                display: flex;
                align-items: center;
                margin-bottom: 8px;
            }
            .review-details .icon {
                font-size: 18px;
                color: #0071c2;
                margin-right: 8px;
            }
            .review-details .text {
                font-size: 14px;
            }
            .review-body {
                margin-bottom: 16px;
            }
            .review-body .title {
                font-size: 16px;
                color: #d79a00;
                font-weight: bold;
            }
            .review-body .description {
                font-size: 14px;
                color: #333;
            }
            .review-footer {
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
            .review-footer .rating {
                font-size: 36px;
                font-weight: bold;
                color: #0071c2;
            }
            .review-footer .actions {
                display: flex;
                align-items: center;
            }
            .star-rating {
                display: flex;
                flex-direction: row-reverse;
                justify-content: center;
            }
            .star-rating input[type="radio"] {
                display: none;
            }
            .star-rating label {
                font-size: 2rem;
                color: #ddd;
                cursor: pointer;
            }
            .message {
                display: none;
                font-size: 20px;
                color: green;
                text-align: center;
                margin-top: 20px;
            }
            .star-rating input[type="radio"]:checked ~ label {
                color: #f2b600;
            }
            .star-rating input[type="radio"]:not(:checked) ~ label:hover,
            .star-rating input[type="radio"]:not(:checked) ~ label:hover ~ label {
                color: #f2b600;
            }
            .review-footer .actions button {
                background: none;
                border: none;
                color: #0071c2;
                margin-left: 8px;
                cursor: pointer;
                font-size: 14px;
            }
            .options-icon {
                position: absolute;
                top: 10px;
                right: 10px;
                cursor: pointer;
            }
            .options-menu {
                display: none;
                position: absolute;
                top: 30px;
                right: 10px;
                background: #fff;
                border: 1px solid #e6e6e6;
                border-radius: 8px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
                z-index: 10;
            }
            .options-menu a {
                display: block;
                padding: 10px 20px;
                color: #333;
                text-decoration: none;
            }
            .options-menu a:hover {
                background: #f1f1f1;
            }
            .sidebar {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                right: -700px;
                background-color: #fff;
                box-shadow: -2px 0 5px rgba(0, 0, 0, 0.5);
                overflow-y: hidden;
                transition: 0.5s;
                padding-top: 60px;
                padding-left: 20px;
                padding-right: 20px;
            }
            .sidebar.open {
                width: 700px;
                right: 0;
            }
            .sidebar a {
                padding: 10px 15px;
                text-decoration: none;
                font-size: 15px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }
            .sidebar a:hover {
                color: #f1f1f1;
            }
            .sidebar .closebtn {
                position: absolute;
                top: 10px;
                right: 63px;
                font-size: 36px;
                margin-left: 50px;
                cursor: pointer;
            }
            .openbtn {
                font-size: 20px;
                cursor: pointer;
                background-color: #0071c2;
                color: white;
                padding: 10px 15px;
                border: none;
            }
            .openbtn:hover {
                background-color: #444;
            }
            .btn-custom {
                font-size: 18px; 
                padding: 10px 20px; 
                background-color: #bcd0c7;
                border: 2px solid #444;
                color: #000; 
                border-radius: 5px; 
                cursor: pointer; 
                transition: background-color 0.3s ease, color 0.3s ease; /* Hiệu ứng chuyển màu */
            }
            .btn-custom:hover {
                background-color: #0dcaf0; 
                color: whitesmoke; 
            }
            .reply-form {
    display: none;
    margin-top: 10px;
}

.reply-card {
    border-left: 2px solid #007bff;
    margin-left: 20px;
    padding-left: 10px;
    margin-top: 10px;
}

.reply-header {
    display: flex;
    align-items: center;
    margin-bottom: 8px;
}

.reply-header .user-icon {
    font-size: 20px;
    color: #6c757d;
    margin-right: 8px;
}

.reply-header .name {
    font-weight: bold;
    font-size: 14px;
}

.reply-body {
    font-size: 14px;
    color: #333;
    margin-bottom: 8px;
}

        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="tour-header">
                    <div class="tour-title">${tourDetails.tour.tourName}</div>
                    <div class="tour-price">
                        <div id="originalPrice" class="discounted-price">${tourDetails.tour.price}/Khách</div>
                    </div>
                    <div class="tour-actions">
                        <button class="btn btn-danger" onclick="bookNow(${tourDetails.tour.tourID})"><i class="fas fa-shopping-cart"></i> Đặt ngay</button>
                        <button class="btn btn-outline-primary" onclick="contactConsultant(${tourDetails.tour.tourID})">Liên hệ tư vấn</button>
                    </div>
                </div>
                <section class="section-02">
                    <div class="container1">
                        <div class="row animate__fadeIn animate__animated flex">
                            <div class="col-lg-7 col-md-12 col-sm-12 left">
                                <div class="image">
                                    <img src="${pageContext.request.contextPath}/${tourDetails.images[0].imageURL}" class="img-fluid" alt="Hình ảnh lớn">
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 right">
                                <div class="row gy-4 flex">
                                    <div class="col-md-12 col-12 small">
                                        <div class="row mb-2 flex">
                                            <div class="col-6">
                                                <div class="image">
                                                    <img src="${pageContext.request.contextPath}/${tourDetails.images[1].imageURL}" class="img-fluid" alt="Hình nhỏ 1">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="image">
                                                    <img src="${pageContext.request.contextPath}/${tourDetails.images[2].imageURL}" class="img-fluid" alt="Hình nhỏ 2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12 big">
                                        <div class="image">
                                            <img src="${pageContext.request.contextPath}/${tourDetails.images[3].imageURL}" class="img-fluid" alt="Hình nhỏ 3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="info-box p-4">
                                <p>Khởi hành <strong>28/06/2024 - Giờ đi: 13:06</strong></p>
                                <p>Tập trung <strong>10:06 ngày 28/06/2024</strong></p>
                                <p>Thời gian <strong>5 ngày</strong></p>
                                <p>TourID <strong>${tourDetails.tour.tourID}</strong></p>
                                <p>Nơi khởi hành <strong>${tourDetails.tour.startLocation}</strong></p>
                                <p>Số chỗ còn nhận <strong>${tourDetails.tour.numberOfPeople}</strong></p>
                                <button class="btn btn-outline-primary"><i class="far fa-calendar-alt"></i> Ngày khác</button>
                            </div>
                            <div class="support-box mt-3 p-4">
                                <p>Quý khách cần hỗ trợ?</p>
                                <button class="btn btn-primary mr-2"><i class="fas fa-phone-alt"></i> Gọi miễn phí qua internet</button>
                                <button class="btn btn-outline-primary"><i class="fas fa-envelope"></i> Gửi yêu cầu hỗ trợ ngay</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="details-box p-4">
                                <div class="row">
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="far fa-clock"></i> <strong>Thời gian</strong></p>
                                        <p>5 ngày 4 đêm</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-bus"></i> <strong>Phương tiện di chuyển</strong></p>
                                        <p>Máy bay, Xe du lịch</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-map-marker-alt"></i> <strong>Điểm tham quan</strong></p>
                                        <p>Thái Lan, Bangkok, Pattaya, Wat Benchamabophit</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-utensils"></i> <strong>Ẩm thực</strong></p>
                                        <p>Buffet sáng, Theo thực đơn, Đặc sản địa phương</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-hotel"></i> <strong>Khách sạn</strong></p>
                                        <p>Khách sạn 4 sao</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="far fa-calendar-check"></i> <strong>Thời gian lý tưởng</strong></p>
                                        <p>Quanh năm</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-users"></i> <strong>Đối tượng thích hợp</strong></p>
                                        <p>Cặp đôi, Gia đình nhiều thế hệ, Thanh niên</p>
                                    </div>
                                    <div class="col-lg-6 mb-3">
                                        <p><i class="fas fa-tags"></i> <strong>Ưu đãi</strong></p>
                                        <p>Đã bao gồm ưu đãi trong giá tour</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="schedules-section">
                    <div class="schedules">
                        <h2 style="margin-left:20px;">Lịch trình</h2>
                        <c:if test="${not empty schedules}">
                            <div class="calendar-container">
                                <div class="calendar-left">
                                    <div class="calendar-body">
                                        <c:forEach var="o" items="${schedules}" varStatus="status">
                                            <div class="calendar-day">
                                                <div class="calendar-day-number">${status.index + 1}</div>
                                                <div class="calendar-day-content">
                                                    <div class="calendar-day-date">
                                                        <i class="fas fa-calendar-alt"></i> <fmt:formatDate value="${o.scheduleDate}" pattern="dd/MM/yyyy" />
                                                    </div>
                                                    <div class="calendar-day-title">
                                                        ${o.scheduleTitle}
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                                <div class="calendar-right">
                                    <div class="calendar-body">
                                        <c:forEach var="o" items="${schedules}" varStatus="status">
                                            <div class="calendar-day">
                                                <div class="calendar-day-content">
                                                    <div class="calendar-day-title" style="color: #333;">
                                                        ${o.scheduleTitle}
                                                    </div>
                                                    <div class="calendar-day-note" style="color: #333;">
                                                        <c:forEach var="d" items="${o.details}">
                                                            <p><strong style="color: #333;">${d.detailTitle}</strong><br>
                                                                <span style="white-space: pre-line;">${d.detailContent}</span></p>
                                                            <p class="note" style="color: #333;">${d.note}</p>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                            <c:if test="${status.last}">
                                                <div class="end-message">
                                                    KẾT THÚC CHƯƠNG TRÌNH, TẠM BIỆT QUÝ KHÁCH!
                                                </div>
                                                <div class="end-note">
                                                    <strong>*Lưu ý :</strong>
                                                    <ul>
                                                        <li>Hành trình có thể thay đổi thứ tự điểm đến tùy vào điều kiện thực tế.</li>
                                                        <li>Lịch trình tham quan (tắm biển, ngắm hoa, trải nghiệm,...) rất dễ bị ảnh hưởng bởi thời tiết. Đây là trường hợp bất khả kháng mong Quý khách hiểu và thông cảm.</li>
                                                        <li>Khách Sạn có thể ở xa trung tâm thành phố vào các mùa Cao Điểm.</li>
                                                        <li>Vì những yếu tố khách quan trong giai đoạn này, điểm tham quan có thể đóng cửa và được thay bằng điểm khác phù hợp với chương trình.</li>
                                                    </ul>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </div>
                    <h2 style="margin-left: 20px;">Những thông tin cần lưu ý</h2>
                    <div class="container mt-5">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Giá tour bao gồm
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                Nội dung giá tour bao gồm.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Giá tour không bao gồm
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                            <div class="card-body">
                                                Nội dung giá tour không bao gồm.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Giá vé trẻ em
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <p>- Trẻ em dưới 2 tuổi: 30% giá tour người lớn</p>
                                                <p>- Trẻ em từ 2 tuổi dưới 12 tuổi:</p>
                                                <ul>
                                                    <li>Hàng không VU, VJ, DD, WE, QH: 85% giá tour người lớn (không có chế độ giường riêng, 100% giá tour người lớn sẽ có giường phụ)</li>
                                                    <li>Hàng không VN: 75% giá tour người lớn (không có chế độ giường riêng, 90% giá tour người lớn sẽ có giường phụ)</li>
                                                </ul>
                                                <p>- Trẻ em từ 12 tuổi trở lên: 100% giá tour người lớn</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Điều kiện thanh toán
                                        </div>
                                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                            <div class="card-body">
                                                Nội dung điều kiện thanh toán.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            Các điều kiện khi đăng ký tour
                                        </div>
                                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                            <div class="card-body">
                                                Nội dung điều kiện khi đăng ký tour.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            Liên hệ
                                        </div>
                                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                                            <div class="card-body">
                                                Nội dung liên hệ.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="accordion" id="accordionExample2">
                                    <div class="card">
                                        <div class="card-header" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                            Lưu ý khi chuyển/hủy tour
                                        </div>
                                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                Nội dung lưu ý khi chuyển/hủy tour.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingEight" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                            Các điều kiện hủy tour đối với ngày thường
                                        </div>
                                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                Nội dung điều kiện hủy tour đối với ngày thường.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingNine" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                            Các điều kiện hủy tour đối với ngày lễ, Tết
                                        </div>
                                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                Nội dung điều kiện hủy tour đối với ngày lễ, Tết.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTen" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                            Trường hợp bất khả kháng
                                        </div>
                                        <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                Nội dung trường hợp bất khả kháng.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingEleven" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                            Thông tin visa
                                        </div>
                                        <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample2">
                                            <div class="card-body">
                                                Nội dung thông tin visa.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="guide-info">
                    <h2>Thông tin hướng dẫn viên</h2>
                    <form id="guideForm" method="get" action="tourDetails">
                        <input type="hidden" id="guideIdInput" name="guideId" value="">
                        <input type="hidden" id="tourIdInput" name="tourID" value="${tourID}">
                        <c:choose>
                            <c:when test="${empty listGuide}">
                                <div class="guide-card">
                                    <p>Đang sắp xếp hướng dẫn viên, vui lòng chờ...</p>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="guide" items="${listGuide}">
                                    <div class="guide-card">
                                        <p class="guide-role">${guide.tourType}</p>
                                        <p class="guide-name">
                                            <a href="javascript:void(0)" onclick="openNav(${guide.guideID})">
                                                ${guide.name}
                                            </a>
                                        </p>
                                        <p class="guide-address">${guide.address}</p>
                                    </div>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </form>
                </div>
                <div class="guide-info">
                    <h2>Phản hồi của khách hàng</h2>
                    <button class="btn btn-custom" type="button" onclick="window.location.href = '/SWP391_bookingSystem/feedback?tourID=' + ${tourDetails.tour.tourID}">
                        Xem Các phản hồi về Tour
                    </button>
                </div>        
                <div id="mySidebar" class="sidebar">
                    <h2 style="margin-left:200px;">Đánh giá của khách</h2>
                    <button style="border:none;" onclick="openBookingModal()">Viết đánh giá</button>
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                    <div id="review-sidebar">
                        <c:forEach var="review" items="${reviews}">
                            <div class="review-card">
                                <div class="review-header">
                                    <i class="fas fa-user-circle user-icon"></i>
                                    <div>
                                        <div class="name">${review.username}</div>
                                    </div>
                                    <c:if test="${review.userID == sessionScope.userID}">
                                        <div class="options-icon">
                                            <i class="fas fa-bars" onclick="toggleOptionsMenu(this)"></i>
                                            <div class="options-menu">
                                                <a href="#" onclick="editReview(${review.reviewID})">Sửa</a>
                                                <a href="#" onclick="deleteReview(${review.reviewID})">Xóa</a>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                                <div class="review-details">
                                    <i class="fas fa-map-marker-alt icon"></i>
                                    <div class="text">Tên tour: ${review.tourName}</div>
                                </div>
                                <div class="review-details">
                                    <i class="fas fa-calendar-alt icon"></i>
                                    <div class="text">Ngày đi: ${review.startDate}</div>
                                </div>
                                <div class="review-details">
                                    <i class="fas fa-calendar-alt icon"></i>
                                    <div class="text">Ngày về: ${review.endDate}</div>
                                </div>
                                <div class="review-body">
                                    <div class="description">${review.comment}</div>
                                    <div class="text"><strong>Ngày đánh giá:</strong> ${review.reviewDate}</div>
                                    <div class="rating">
                                        <c:forEach var="star" begin="1" end="${review.rating}">
                                            <i class="fas fa-star" style="color: gold; font-size: 24px;"></i>
                                        </c:forEach>
                                        <c:forEach var="star" begin="${review.rating + 1}" end="5">
                                            <i class="far fa-star" style="color: gold; font-size: 24px;"></i>
                                        </c:forEach>
                                    </div>
                                </div>
                                   
                            </div>
                            <hr/>
                        </c:forEach>
                    </div>

                </div>

                <div class="modal fade" id="bookingModal" tabindex="-1" aria-labelledby="bookingModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="bookingModalLabel">Nhập mã pin chuyến đi của bạn</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="checkPinForm" action="CheckPinServlet" method="post">
                                    <div class="form-group">
                                        <label for="pinCode">Mã PIN chuyến đi của bạn</label>
                                        <input type="hidden" id="tourID" name="tourID" value="${tourDetails.tour.tourID}">
                                        <input type="text" id="pinCode" name="pin" class="form-control" placeholder="Nhập mã PIN" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Đánh giá hướng dẫn viên</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="reviewModal" tabindex="-1" aria-labelledby="reviewModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="reviewModalLabel">Viết đánh giá</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="reviewForm">
                                    <div class="form-group">
                                        <label for="userName">Tên</label>
                                        <input type="text" id="userName" name="userName" class="form-control" placeholder="Nhập tên của bạn">
                                    </div>
                                    <div class="form-group">
                                        <label for="reviewTitle">Tiêu đề đánh giá</label>
                                        <input type="text" id="reviewTitle" name="reviewTitle" class="form-control" placeholder="Nhập tiêu đề đánh giá">
                                    </div>
                                    <div class="form-group">
                                        <label for="reviewBody">Nội dung đánh giá</label>
                                        <textarea id="reviewBody" name="reviewBody" class="form-control" rows="4" placeholder="Nhập nội dung đánh giá"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="reviewRating">Xếp hạng</label>
                                        <div class="star-rating">
                                            <input type="radio" id="5-stars" name="rating" value="5" />
                                            <label for="5-stars" class="star">&#9733;</label>
                                            <input type="radio" id="4-stars" name="rating" value="4" />
                                            <label for="4-stars" class="star">&#9733;</label>
                                            <input type="radio" id="3-stars" name="rating" value="3" />
                                            <label for="3-stars" class="star">&#9733;</label>
                                            <input type="radio" id="2-stars" name="rating" value="2" />
                                            <label for="2-stars" class="star">&#9733;</label>
                                            <input type="radio" id="1-star" name="rating" value="1" />
                                            <label for="1-star" class="star">&#9733;</label>
                                        </div>
                                    </div>
                                    <input type="hidden" id="tourID" name="tourID" value="${tourDetails.tour.tourID}">
                                    <input type="hidden" id="guideID" name="guideID" value="${listGuide[0].guideID}">
                                    <input type="hidden" id="userID" name="userID" value="${sessionScope.userID}">
                                    <button type="submit" class="btn btn-primary">Gửi đánh giá</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    function openReviewModal(tourID, guideID, userID) {
                        document.getElementById("tourID").value = tourID;
                        document.getElementById("guideID").value = guideID;
                        document.getElementById("userID").value = userID;
                        $('#reviewModal').modal('show');
                    }

                    function bookNow(tourID) {
                        window.location.href = "booking?tourID=" + tourID;
                    }

                    function contactConsultant(tourID) {
                        window.location.href = 'contactUs.jsp?tourID=' + tourID;
                    }

                    function confirmDeleteSchedule(scheduleID) {
                        var result = confirm("Bạn có chắc chắn muốn xóa lịch trình này không?");
                        if (result) {
                            window.location.href = 'deleteSchedule?scheduleID=' + scheduleID;
                        }
                    }

                    function formatCurrency(value) {
                        return value.toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
                    }

                    var priceElement = document.getElementById('originalPrice');
                    if (priceElement) {
                        var priceValue = parseFloat(priceElement.textContent.replace(/[^0-9.-]+/g, ""));
                        priceElement.textContent = formatCurrency(priceValue) + ' / khách';
                    }

                    function openNav(guideId) {
                        document.getElementById("guideIdInput").value = guideId;
                        document.getElementById("guideForm").submit();
                        document.getElementById("mySidebar").classList.add("open");
                    }

                    function closeNav() {
                        document.getElementById("mySidebar").classList.remove("open");
                    }

                    window.onload = function () {
                        const urlParams = new URLSearchParams(window.location.search);
                        if (urlParams.has('guideId')) {
                            document.getElementById("mySidebar").classList.add("open");
                        }
                    }

                    function openBookingModal() {
                        var currentURL = window.location.href;
                        sessionStorage.setItem('previousURL', currentURL);
                        var isLoggedIn = <%= session.getAttribute("account") != null %>;

                        if (isLoggedIn) {
                            $('#bookingModal').modal('show');
                        } else {
                            window.location.href = "login.jsp";
                        }
                    }

                    document.getElementById("checkPinForm").addEventListener("submit", function (event) {
                        event.preventDefault(); 

                        var form = event.target;
                        var formData = $(form).serialize(); 

                        $.ajax({
                            url: form.action,
                            method: form.method,
                            data: formData,
                            success: function (response) {
                        
                                if (response.pinValid) {
                                    $('#reviewModal').modal('show');
                                } else {
                                    alert(response.errorMessage);
                                }
                            },
                            error: function () {
                                alert('Có lỗi xảy ra. Vui lòng thử lại.');
                            }
                        });
                    });

                    document.addEventListener("DOMContentLoaded", function () {
                        const urlParams = new URLSearchParams(window.location.search);
                        const pin = urlParams.get('pin');
                        if (pin) {
                            $.ajax({
                                url: 'CheckPinServlet',
                                method: 'POST',
                                data: {pin: pin},
                                success: function (response) {
                                    if (response.pinValid) {
                                        $('#reviewModal').modal('show');
                                    } else {
                                        alert(response.errorMessage);
                                    }
                                },
                                error: function () {
                                    alert('Có lỗi xảy ra. Vui lòng thử lại.');
                                }
                            });
                        }
                    });

                    $(document).ready(function () {
                        $('#reviewForm').submit(function (event) {
                            event.preventDefault(); 

                            
                            var formData = {
                                tourID: $('#tourID').val(),
                                guideID: $('#guideID').val(),
                                userID: $('#userID').val(),
                                rating: $('input[name="rating"]:checked').val(), // Sửa để lấy giá trị đánh giá sao
                                reviewBody: $('#reviewBody').val(),
                                reviewTitle: $('#reviewTitle').val()
                            };

                            $.ajax({
                                type: 'POST',
                                url: 'SubmitReviewServlet',
                                data: formData,
                                success: function (response) {
                                    $('#thankYouMessage').show();
                                    $('#reviewModal').modal('hide');
                                    $('#reviewForm')[0].reset();
                                    alert('Cảm ơn bạn đã góp ý về hướng dẫn viên của chúng tôi!');
                                },
                                error: function () {
                                    alert('Có lỗi xảy ra. Vui lòng thử lại.');
                                }
                            });
                        });
                    });

                    function toggleOptionsMenu(element) {
                        var menu = element.querySelector('.options-menu');
                        if (menu.style.display === 'block') {
                            menu.style.display = 'none';
                        } else {
                            menu.style.display = 'block';
                        }
                    }

                    function editReview(reviewID) {
                        var title = prompt("Nhập tiêu đề mới:");
                        var comment = prompt("Nhập nội dung mới:");
                        var rating = prompt("Nhập xếp hạng mới (1-5):");

                        if (title && comment && rating) {
                            $.post("tourDetails", {
                                action: "edit",
                                reviewID: reviewID,
                                title: title,
                                comment: comment,
                                rating: rating,
                                tourID: '<%= request.getParameter("tourID") %>',
                                guideID: '<%= request.getParameter("guideID") %>'
                            }, function () {
                                location.reload();
                            });
                        }
                    }

                    function deleteReview(reviewID) {
                        if (confirm("Bạn có chắc chắn muốn xóa đánh giá này không?")) {
                            $.post("tourDetails", {
                                action: "delete",
                                reviewID: reviewID,
                                tourID: '<%= request.getParameter("tourID") %>',
                                guideID: '<%= request.getParameter("guideID") %>'
                            }, function () {
                                location.reload();
                            });
                        }
                    }
                    function toggleOptionsMenu(element) {
                        var menu = element.nextElementSibling;
                        menu.style.display = menu.style.display === 'block' ? 'none' : 'block';
                        document.addEventListener('click', function (event) {
                            if (!element.contains(event.target)) {
                                menu.style.display = 'none';
                            }
                        }, {once: true});
                    }

                </script>
                <jsp:include page="footer.jsp" />
                </body>
                </html>
