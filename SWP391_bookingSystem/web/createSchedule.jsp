<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String tourID = request.getParameter("tourID");
    if (tourID == null || tourID.isEmpty()) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing tourID parameter");
        return;
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thêm lịch trình</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <style>
        body {
            background-color: #e0c8cc;
            font-family: 'Roboto', sans-serif;
            color: #343a40;
        }
        .navigation-buttons {
            display: flex;
            justify-content: flex-start;
            margin: 20px;
        }
        .navigation-buttons a, .navigation-buttons button {
            background-color: rgba(23, 162, 184, 0.9);
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 50px;
            cursor: pointer;
            transition: background-color 0.3s, box-shadow 0.3s;
            text-decoration: none;
            margin-right: 10px;
        }
        .navigation-buttons a:hover, .navigation-buttons button:hover {
            background-color: #0056b3;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
        .form-container {
            max-width: 700px;
            margin: 50px auto;
            padding: 30px;
            border-radius: 15px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
            background-color: #ffffff;
            background-image: url('your-image-url-here');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            color: white;
        }
        .modal-header {
            margin-bottom: 20px;
            background-color: rgba(23, 162, 184, 0.9);
            padding: 15px;
            border-radius: 15px 15px 0 0;
            text-align: center;
        }
        .modal-header h4 {
            color: white;
            margin: 0;
            font-weight: 700;
        }
        .form-group label {
            font-weight: 500;
            margin-top: 10px;
            color: white;
        }
        .form-group input, .form-group textarea {
            border-radius: 10px;
            border: 1px solid #ced4da;
            padding: 10px;
            transition: border-color 0.3s, box-shadow 0.3s;
            width: 100%;
            background-color: rgba(255, 255, 255, 0.8);
            color: #343a40;
        }
        .form-group input:focus, .form-group textarea:focus {
            border-color: #80bdff;
            box-shadow: 0 0 5px rgba(0, 123, 255, 0.25);
        }
        .modal-footer {
            display: flex;
            justify-content: flex-end;
            padding: 15px;
            border-top: 1px solid #e9ecef;
            background-color: rgba(248, 249, 250, 0.8);
            border-radius: 0 0 15px 15px;
        }
        .btn-success {
            background-color: #28a745;
            border-color: #28a745;
            transition: background-color 0.3s, border-color 0.3s, box-shadow 0.3s;
        }
        .btn-success:hover {
            background-color: #218838;
            border-color: #1e7e34;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
        .error {
            color: red;
            font-weight: bold;
            margin-bottom: 20px;
            text-align: center;
        }
        .success {
            color: green;
            font-weight: bold;
            margin-bottom: 20px;
            text-align: center;
        }
        .button {
            display: inline-block;
            padding: 10px 20px;
            font-size: 16px;
            font-weight: bold;
            color: white;
            background-color: #007bff;
            text-align: center;
            border-radius: 5px;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="navigation-buttons">
        <button type="button" class="back-button" onclick="history.back()">Quay lại</button>
        <a href="tourDetails?tourID=<%= tourID %>" class="detail-button">Xem lịch trình chi tiết</a>
    </div>
    <div class="container">
        <div class="form-container">
            <div id="message"></div>
            <% if (request.getAttribute("error") != null) { %>
            <div class="error"><%= request.getAttribute("error") %></div>
            <% } %>
            <form id="scheduleForm">
                <div class="modal-header">    
                    <h4 class="modal-title">Thêm Lịch trình</h4>
                </div>
                <div class="modal-body">                    
                    <div class="form-group">
                        <h1 style="font-size:15px;color: red;">(*)Các trường không được để trống</h1>
                        <label for="scheduleTitle">Tiêu đề lịch trình</label>
                        <input name="scheduleTitle" type="text" class="form-control" placeholder="Nhập tiêu đề lịch trình" required>
                    </div>
                    <div class="form-group">
                        <label for="scheduleDate">Ngày</label>
                        <input name="scheduleDate" type="date" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="detailTitle">Tiêu đề chi tiết</label>
                        <input name="detailTitle" type="text" class="form-control" placeholder="Nhập tiêu đề chi tiết" required>
                    </div>
                    <div class="form-group">
                        <label for="detailContent">Nội dung chi tiết</label>
                        <textarea name="detailContent" class="form-control" rows="4" placeholder="Nhập nội dung chi tiết" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="note">Ghi chú</label>
                        <textarea name="note" class="form-control" rows="2" placeholder="Nhập ghi chú"></textarea>
                    </div>
                    <input name="tourID" type="hidden" class="form-control" value="<%= tourID %>">
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Thêm" id="submitButton">
                </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        function disableSubmitButton() {
            document.getElementById('submitButton').disabled = true;
        }

        $(document).ready(function() {
            $('#scheduleForm').on('submit', function(event) {
                event.preventDefault();
                disableSubmitButton();
                $.ajax({
                    url: 'createSchedule',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function(response) {
                        $('#message').html('<div class="success">Thêm lịch trình thành công!</div>');
                        $('#scheduleForm')[0].reset();
                        document.getElementById('submitButton').disabled = false;
                    },
                    error: function() {
                        $('#message').html('<div class="error">Có lỗi xảy ra, vui lòng thử lại.</div>');
                        document.getElementById('submitButton').disabled = false;
                    }
                });
            });
        });
    </script>
</body>
</html>
