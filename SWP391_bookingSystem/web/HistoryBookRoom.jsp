<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bill Booking Room</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"></script>
    <style>
        body {
            font-family: 'Helvetica Neue', Arial, sans-serif;
            background: linear-gradient(to right, #ffecd2, #fcb69f);
            color: #343a40;
            margin: 0;
            padding: 0;
        }
        .container {
            margin-top: 50px;
        }
        .header, .footer {
            background-color: #343a40;
            color: white;
            padding: 10px 0;
            text-align: center;
        }
        .footer {
            position: fixed;
            width: 100%;
            bottom: 0;
        }
        .booking-item {
            padding: 20px;
            background-color: #f0f8ff;
            border: 1px solid #b0c4de;
            border-radius: 10px;
            margin-bottom: 20px;
            transition: transform 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
        }
        .booking-item:hover {
            transform: scale(1.02);
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        }
        .booking-item h5 {
            margin-bottom: 10px;
            color: #007bff;
        }
        .booking-item p {
            margin: 0;
            font-size: 1.1em;
            color: #495057;
        }
        .back-button {
            position: absolute;
            top: 10px;
            right: 100px;
            background-color: #007bff;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1em;
            transition: background-color 0.3s ease;
        }
        .back-button:hover {
            background-color: #0056b3;
        }
        .save-button {
            background-color: #4caf50;
            color: white;
            border: none;
            padding: 8px 16px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 0.9em;
            transition: background-color 0.3s ease;
            margin-top: 10px;
        }
        .save-button:hover {
            background-color: #45a049;
        }
        .total-container {
            margin-top: 20px;
            padding: 20px;
            background-color: #fff3e0;
            border: 2px solid #ffab91;
            border-radius: 15px;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.1);
            text-align: center;
        }
        .total-container h2 {
            color: #e64a19;
            margin-bottom: 15px;
        }
        .total-container .total-amount {
            font-size: 2em;
            color: #d84315;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <c:if test="${empty sessionScope.account || sessionScope.account.roleID != 2}">
        <script type="text/javascript">
            window.location.href = 'login.jsp';
        </script>
    </c:if>
      
    <div class="header">
        <h1>Đơn Đặt Phòng</h1>
        <button class="back-button" onclick="goBack()">Back</button>
    </div>
    
    <div class="container" id="bookingContainer">
        <c:if test="${not empty sessionScope.cartItems}">
            <c:forEach items="${sessionScope.cartItems}" var="item">
                <div class="booking-item">
                    <p><strong>Room Name:</strong> <span>${item.room.name}</span></p>
                    <p><strong>Price:</strong> <span>${item.room.price} đ</span></p>
                    <p><strong>Number of Beds:</strong> <span>${item.room.number_beds}</span></p>
                    <p><strong>Max Guests:</strong> <span>${item.room.max_guests}</span></p>
                    <p><strong>Description:</strong> <span>${item.room.description}</span></p>
                </div>
            </c:forEach>
        </c:if>

        <div class="total-container">
            <h2>Tổng tiền</h2>
            <p class="total-amount" id="totalAmount">${sessionScope.total} đ</p>
            <button class="save-button" onclick="saveBookings()">Save Bookings</button>
        </div>
    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script>
        function goBack() {
            window.history.back();
        }

        function saveBookings() {
            const saveButton = document.querySelector('.save-button');
            saveButton.style.visibility = 'hidden'; // Ẩn nút Save Bookings

            html2canvas(document.querySelector("#bookingContainer")).then(canvas => {
                canvas.toBlob(function(blob) {
                    saveAs(blob, 'bookings.png');
                }).then(() => {
                    saveButton.style.visibility = 'visible'; // Hiện lại nút Save Bookings sau khi chụp ảnh xong
                });
            });
        }
    </script>
</body>
</html>
