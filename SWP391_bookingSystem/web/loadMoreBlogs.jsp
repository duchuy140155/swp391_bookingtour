<%-- 
    Document   : loadMoreBlogs
    Created on : Jul 9, 2024, 2:47:15 PM
    Author     : MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:forEach var="blog" items="${blogs}">
    <div class="blog-post">
        <div class="blog-header">
            <div class="title-icons-container">
                <h2 class="blog-title">${blog.title}</h2>
                <div>
                    <c:if test="${blog.user.userID == sessionScope.account.userID}">
                        <span class="edit-delete-icons edit-blog" data-blogid="${blog.blogID}" data-blogtitle="${blog.title}" data-blogcontent="${blog.content}">
                            <i class="bi bi-pencil-square"></i>
                        </span>
                        <span class="edit-delete-icons delete-blog" data-blogid="${blog.blogID}">
                            <i class="bi bi-trash"></i>
                        </span>
                    </c:if>
                    <c:if test="${blog.favorite}">
                        <span class="edit-delete-icons favorite-blog" data-blogid="${blog.blogID}" data-action="unfavorite">
                            <i class="bi bi-flag-fill favorite-icon" style="color: red;"></i>
                        </span>
                    </c:if>
                    <c:if test="${!blog.favorite}">
                        <span class="edit-delete-icons favorite-blog" data-blogid="${blog.blogID}" data-action="favorite">
                            <i class="bi bi-flag favorite-icon"></i>
                        </span>
                    </c:if>
                </div>
            </div>
            <div class="details-container">
                <p class="blog-author">Đăng bởi: ${blog.user.userName}</p>
                <p class="blog-date">Ngày tạo: ${blog.createdAt}</p>
            </div>
        </div>

        <div class="blog-content">
            <p>${blog.content}</p>
            <c:forEach var="picture" items="${blog.pictures}">
                <img src="${picture.pictureURL}" alt="Blog Image">
            </c:forEach>
        </div>
        <div class="blog-footer">
            <button class="like-button ${blog.liked ? 'liked' : ''}" data-blogid="${blog.blogID}">
                Likes: <span class="like-count">${blog.likes.size()}</span>
            </button>
            <c:set var="totalComments" value="${blog.comments.size()}"/>
            <c:forEach var="comment" items="${blog.comments}">
                <c:set var="totalComments" value="${totalComments + comment.replies.size()}"/>
            </c:forEach>
            <p>Total Comments: ${totalComments}</p>
            <a href="viewBlog?blogID=${blog.blogID}" class="view-blog-button">Xem chi tiết</a>
        </div>
    </div>
</c:forEach>

