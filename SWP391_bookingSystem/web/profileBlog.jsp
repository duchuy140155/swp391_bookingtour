<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
    <title>User Profile - Blogs</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <style>
        .container {
            width: 90%;
            margin: 50px auto;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .create-blog-form {
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
        }

        .create-blog-form input,
        .create-blog-form textarea {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        .create-blog-form button {
            background-color: #1877f2;
            color: white;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        .create-blog-form button:hover {
            background-color: #165db0;
        }

        .blogs-container {
            display: flex;
            justify-content: space-between;
            gap: 20px;
        }

        .blogs-section {
            flex: 1;
            display: flex;
            flex-direction: column;
        }

        .blog-list {
            display: flex;
            flex-direction: column;
            gap: 20px;
        }

        .blog-post {
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .blog-header {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
        }

        .title-icons-container {
            display: flex;
            justify-content: space-between;
            align-items: center;
            width: 100%;
            position: relative;
        }

        .blog-title {
            margin: 0;
            flex-grow: 1;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-toggle {
            cursor: pointer;
        }

        .dropdown-menu {
            display: none;
            position: absolute;
            background-color: white;
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
            z-index: 1;
            right: 0;
            min-width: 150px;
        }

        .dropdown-menu .dropdown-item {
            padding: 12px 16px;
            display: block;
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .dropdown-menu .dropdown-item:hover {
            background-color: #ddd;
        }

        .details-container {
            display: flex;
            justify-content: space-between;
            width: 100%;
        }

        .blog-author, .blog-date {
            margin: 0;
        }

        .blog-header h2 {
            margin: 0;
            font-size: 1.5em;
        }

        .blog-header p {
            margin: 5px 0;
            color: #555;
        }

        .blog-content {
            margin-bottom: 10px;
        }

        .blog-content img {
            max-width: 100%;
            height: auto;
            border-radius: 10px;
            margin-top: 10px;
        }

        .blog-footer {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .blog-footer p {
            margin: 0;
            color: #555;
        }

        .like-button {
            background-color: grey;
            color: white;
            padding: 5px 10px;
            text-decoration: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .like-button.liked {
            background-color: green;
        }

        .favorite-icon {
            font-size: 24px;
            cursor: pointer;
            margin-left: 10px;
        }

        .back-button-container {
            margin: 20px;
            text-align: left;
            position: absolute;
            top: 10px;
            left: 10px;
        }

        .back-button-container .back-btn {
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .back-button-container .back-btn:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="back-button-container">
            <button onclick="window.location.href = 'homepage'" class="back-btn">Back to Homepage</button>
        </div>
        <div class="blogs-container">
            <div class="blogs-section">
                <div class="header">
                    <h2>Your Blogs</h2>
                </div>
                <div id="userBlogsContainer" class="blog-list">
                    <c:forEach var="blog" items="${userBlogs}">
                        <div class="blog-post">
                            <div class="blog-header">
                                <div class="title-icons-container">
                                    <h2 class="blog-title">${blog.title}</h2>
                                    <div class="dropdown">
                                        <span class="dropdown-toggle">
                                            <i class="bi bi-three-dots"></i>
                                        </span>
                                        <div class="dropdown-menu">
                                            <c:if test="${blog.user.userID == sessionScope.account.userID}">
                                                <span class="dropdown-item edit-blog" data-blogid="${blog.blogID}" data-blogtitle="${blog.title}" data-blogcontent="${fn:escapeXml(blog.content)}" data-blogpictures="${blog.pictures}">
                                                    <i class="bi bi-pencil-square"></i> Edit
                                                </span>
                                                <span class="dropdown-item delete-blog" data-blogid="${blog.blogID}">
                                                    <i class="bi bi-trash"></i> Delete
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="details-container">
                                    <p class="blog-author">Posted by: ${blog.user.userName}</p>
                                    <p class="blog-date">Created at: ${blog.createdAt}</p>
                                </div>
                            </div>

                            <div class="blog-content">
                                <c:choose>
                                    <c:when test="${fn:length(blog.content) > 200}">
                                        <p>${fn:substring(blog.content, 0, 200)}...</p>
                                    </c:when>
                                    <c:otherwise>
                                        <p>${blog.content}</p>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${!empty blog.pictures}">
                                    <img src="${blog.pictures[0].pictureURL}" alt="Blog Image">
                                </c:if>
                            </div>
                            <div class="blog-footer">
                                <button class="like-button ${blog.liked ? 'liked' : ''}" data-blogid="${blog.blogID}">
                                    Likes: <span class="like-count">${blog.likes.size()}</span>
                                </button>
                                <c:set var="totalComments" value="${blog.comments.size()}"/>
                                <c:forEach var="comment" items="${blog.comments}">
                                    <c:set var="totalComments" value="${totalComments + comment.replies.size()}"/>
                                </c:forEach>
                                <p>Total Comments: ${totalComments}</p>
                                <a href="viewBlog?blogID=${blog.blogID}" class="view-blog-button">View Details</a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <button id="loadMoreUserBlogs" class="btn btn-primary">Load More</button>
            </div>
            <div class="blogs-section">
                <div class="header">
                    <h2>Your Favorite Blogs</h2>
                </div>
                <div id="favoriteBlogsContainer" class="blog-list">
                    <c:forEach var="blog" items="${favoriteBlogs}">
                        <div class="blog-post">
                            <div class="blog-header">
                                <div class="title-icons-container">
                                    <h2 class="blog-title">${blog.title}</h2>
                                    <div class="dropdown">
                                        <span class="dropdown-toggle">
                                            <i class="bi bi-three-dots"></i>
                                        </span>
                                        <div class="dropdown-menu">
                                            <c:if test="${blog.user.userID == sessionScope.account.userID}">
                                                <span class="dropdown-item edit-blog" data-blogid="${blog.blogID}" data-blogtitle="${blog.title}" data-blogcontent="${fn:escapeXml(blog.content)}" data-blogpictures="${blog.pictures}">
                                                    <i class="bi bi-pencil-square"></i> Edit
                                                </span>
                                                <span class="dropdown-item delete-blog" data-blogid="${blog.blogID}">
                                                    <i class="bi bi-trash"></i> Delete
                                                </span>
                                            </c:if>
                                            <c:if test="${blog.user.userID != sessionScope.account.userID}">
                                                <span class="dropdown-item favorite-blog" data-blogid="${blog.blogID}" data-action="unfavorite">
                                                    <i class="bi bi-flag-fill" style="color: red;"></i> Unfavorite
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="details-container">
                                    <p class="blog-author">Posted by: ${blog.user.userName}</p>
                                    <p class="blog-date">Created at: ${blog.createdAt}</p>
                                </div>
                            </div>

                            <div class="blog-content">
                                <c:choose>
                                    <c:when test="${fn:length(blog.content) > 200}">
                                        <p>${fn:substring(blog.content, 0, 200)}...</p>
                                    </c:when>
                                    <c:otherwise>
                                        <p>${blog.content}</p>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${!empty blog.pictures}">
                                    <img src="${blog.pictures[0].pictureURL}" alt="Blog Image">
                                </c:if>
                            </div>
                            <div class="blog-footer">
                                <button class="like-button ${blog.liked ? 'liked' : ''}" data-blogid="${blog.blogID}">
                                    Likes: <span class="like-count">${blog.likes.size()}</span>
                                </button>
                                <c:set var="totalComments" value="${blog.comments.size()}"/>
                                <c:forEach var="comment" items="${blog.comments}">
                                    <c:set var="totalComments" value="${totalComments + comment.replies.size()}"/>
                                </c:forEach>
                                <p>Total Comments: ${totalComments}</p>
                                <a href="viewBlog?blogID=${blog.blogID}" class="view-blog-button">View Details</a>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <button id="loadMoreFavoriteBlogs" class="btn btn-primary">Load More</button>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            let userBlogsOffset = ${blogsPerPage};
            let favoriteBlogsOffset = ${blogsPerPage};

            $('#loadMoreUserBlogs').click(function () {
                $.ajax({
                    url: 'profileBlog',
                    type: 'POST',
                    data: {
                        offset: userBlogsOffset,
                        isFavorite: false
                    },
                    success: function (data) {
                        $('#userBlogsContainer').append(data);
                        userBlogsOffset += ${blogsPerPage};
                    },
                    error: function () {
                        alert('An error occurred.');
                    }
                });
            });

            $('#loadMoreFavoriteBlogs').click(function () {
                $.ajax({
                    url: 'profileBlog',
                    type: 'POST',
                    data: {
                        offset: favoriteBlogsOffset,
                        isFavorite: true
                    },
                    success: function (data) {
                        $('#favoriteBlogsContainer').append(data);
                        favoriteBlogsOffset += ${blogsPerPage};
                    },
                    error: function () {
                        alert('An error occurred.');
                    }
                });
            });

            $(document).on('click', '.favorite-blog', function () {
                var button = $(this);
                var blogID = button.data('blogid');
                var action = button.data('action');

                $.ajax({
                    url: 'favoriteBlog',
                    type: 'POST',
                    data: {blogID: blogID, action: action},
                    success: function (response) {
                        if (response.success) {
                            button.find('i').toggleClass('bi-flag bi-flag-fill').css('color', action === 'favorite' ? 'red' : '');
                            button.data('action', action === 'favorite' ? 'unfavorite' : 'favorite');
                        } else {
                            alert('Error: ' + response.message);
                        }
                    },
                    error: function () {
                        alert('An error occurred.');
                    }
                });
            });

            // Dropdown toggle
            $(document).on('click', '.dropdown-toggle', function (event) {
                event.stopPropagation();
                var dropdownMenu = $(this).next('.dropdown-menu');
                $('.dropdown-menu').not(dropdownMenu).hide();
                dropdownMenu.toggle();
            });

            // Close dropdown if clicked outside
            $(document).click(function () {
                $('.dropdown-menu').hide();
            });
        });
    </script>
</body>
</html>
