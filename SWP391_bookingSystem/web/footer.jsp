<%-- 
    Document   : footer
    Created on : Jun 27, 2024, 1:22:12 PM
    Author     : TranT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <title>Footer Example</title>
</head>
<body>
    <footer>
        <div class="footer-container">
            <div class="footer-section contact">
                <h4>Liên hệ</h4>
                <p><i class="fas fa-map-marker-alt"></i> 123 Đường, New York, USA</p>
                <p><i class="fas fa-envelope"></i> info@example.com</p>
                <p><i class="fas fa-phone"></i> +012 345 67890</p>
                <p><i class="fas fa-fax"></i> +012 345 67890</p>
                <div class="social-icons">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
            <div class="footer-section company">
                <h4>Công ty</h4>
                <ul>
                    <li><a href="#">Về chúng tôi</a></li>
                    <li><a href="#">Tuyển dụng</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Báo chí</a></li>
                    <li><a href="#">Thẻ quà tặng</a></li>
                    <li><a href="#">Tạp chí</a></li>
                </ul>
            </div>
            <div class="footer-section support">
                <h4>Hỗ trợ</h4>
                <ul>
                    <li><a href="#">Liên hệ</a></li>
                    <li><a href="#">Thông báo pháp lý</a></li>
                    <li><a href="#">Chính sách bảo mật</a></li>
                    <li><a href="#">Điều khoản và điều kiện</a></li>
                    <li><a href="#">Sơ đồ trang web</a></li>
                    <li><a href="#">Chính sách cookie</a></li>
                </ul>
            </div>
            <div class="footer-section payment">
               
                <div class="payment-icons">
                    <i class="fab fa-cc-amex"></i>
                    <i class="fab fa-cc-visa"></i>
                    <i class="fab fa-cc-mastercard"></i>
                    <i class="fab fa-cc-paypal"></i>
                </div>
            </div>
        </div>
    </footer>
</body>
<style>
    body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
}

footer {
    background-color: #0f2a62;
    color: #fff;
    padding: 40px 0;
}

.footer-container {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    max-width: 1200px;
    margin: 0 auto;
    padding: 0 20px;
}

.footer-section {
    flex: 1;
    margin: 10px;
    min-width: 200px;
}

.footer-section h4 {
    margin-bottom: 20px;
}

.footer-section p,
.footer-section a {
    color: #ccc;
    text-decoration: none;
}

.footer-section a:hover {
    color: #fff;
}

.footer-section ul {
    list-style: none;
    padding: 0;
}

.footer-section ul li {
    margin-bottom: 10px;
}

.footer-section .social-icons a {
    color: #fff;
    margin-right: 10px;
    text-decoration: none;
}

.footer-section .social-icons a:hover {
    color: #ccc;
}

.language-currency button {
    background-color: #333;
    border: none;
    color: #fff;
    padding: 10px 20px;
    margin-right: 10px;
    cursor: pointer;
}

.language-currency button:hover {
    background-color: #555;
}

.payment-icons i {
    font-size: 24px;
    margin-right: 10px;
}

</style>
</html>
