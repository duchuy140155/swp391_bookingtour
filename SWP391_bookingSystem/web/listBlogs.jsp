<%-- 
    Document   : listBlogs
    Created on : Jul 8, 2024, 3:59:03 PM
    Author     : MSI
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Blog" %>
<%@ page import="model.BlogComment" %>
<%@ page import="model.BlogLike" %>
<%@ page import="model.BlogPicture" %>
<%@ page import="dal.BlogDAO" %>

<%
    BlogDAO blogDAO = new BlogDAO();
    List<Blog> blogs = blogDAO.getAllBlogs();
    request.setAttribute("blogs", blogs);
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blogs</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            width: 50%;
            margin: 0 auto;
        }
        .form-container, .blog-container {
            border: 1px solid #ccc;
            padding: 15px;
            margin-bottom: 15px;
            border-radius: 5px;
        }
        .form-container textarea, .form-container input[type="text"] {
            width: 100%;
            padding: 10px;
            margin: 5px 0;
        }
        .form-container input[type="submit"] {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        .form-container input[type="submit"]:hover {
            background-color: #45a049;
        }
        .blog-container img {
            max-width: 100%;
            height: auto;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="form-container">
            <h2>Create a New Blog</h2>
            <form action="CreateBlogController" method="post" enctype="multipart/form-data">
                <label for="title">Title:</label><br>
                <input type="text" id="title" name="title"><br>
                
                <label for="content">Content:</label><br>
                <textarea id="content" name="content" rows="5"></textarea><br>
                
                <label for="picture">Upload Picture:</label><br>
                <input type="file" id="picture" name="picture"><br><br>
                
                <input type="submit" value="Create Blog">
            </form>
        </div>

        <c:forEach var="blog" items="${blogs}">
            <div class="blog-container">
                <h2>${blog.title}</h2>
                <p>${blog.content}</p>
                <p>Created at: ${blog.createdAt}</p>
                <p>Likes: ${blog.likes.size()} <form action="LikeBlogController" method="post" style="display:inline;"><input type="hidden" name="blogID" value="${blog.blogID}"><input type="submit" value="Like"></form></p>
                <p>Comments: ${blog.comments.size()}</p>

                <h3>Pictures</h3>
                <c:forEach var="picture" items="${blog.pictures}">
                    <img src="${picture.pictureURL}" alt="Blog Picture" style="width:300px;height:auto;">
                </c:forEach>

                <h3>Add a Comment</h3>
                <form action="AddCommentController" method="post">
                    <textarea name="comment"></textarea><br>
                    <input type="hidden" name="blogID" value="${blog.blogID}">
                    <input type="hidden" name="userID" value="${sessionScope.currentUserID}">
                    <input type="submit" value="Submit">
                </form>

                <h3>Comments</h3>
                <ul>
                    <c:forEach var="comment" items="${blog.comments}">
                        <li>${comment.comment} (Posted on: ${comment.createdAt}) 
                            <form action="ReplyCommentController" method="post" style="display:inline;">
                                <textarea name="reply" rows="2" cols="20"></textarea>
                                <input type="hidden" name="blogID" value="${blog.blogID}">
                                <input type="hidden" name="parentCommentID" value="${comment.commentID}">
                                <input type="submit" value="Reply">
                            </form>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </c:forEach>
    </div>
</body>
</html>
