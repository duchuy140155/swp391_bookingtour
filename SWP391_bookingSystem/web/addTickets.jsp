<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Tạo Vé Mới</title>
        <link href="css/addLocation.css" rel="stylesheet" type="text/css"/>
        <style>
            .age-inputs {
                display: flex;
                align-items: center;
            }

            .age-inputs p {
                margin: 0 10px;
            }

            .age-inputs input {
                margin: 0 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
                <h1>Tạo vé mới</h1>
            </div>
            <form action="addTicket" method="post" onsubmit="return validateForm()">
                <input type="hidden" name="tourID" value="${tourID}">
                <div class="form-group">
                    <label for="typeName">Loại vé:</label>
                    <input type="text" id="typeName" name="typeName" pattern="^[\p{L}0-9 ]+$" title="Tên loại vé không được chứa ký tự đặc biệt" placeholder="Nhập tên loại vé ở đây" required>
                </div>
                <div class="form-group">
                    <label for="typeCode">Mã vé:</label>
                    <input type="text" id="typeCode" name="typeCode" pattern="^[A-Za-z0-9]+$" title="Mã vé chỉ chứa chữ cái và số" placeholder="Nhập mã vé tại đây" required>
                </div>
                <div class="form-group">
                    <label for="age">Độ tuổi:</label>
                    <div class="age-inputs">
                        Từ <input type="number" id="ageMin" name="ageMin" min="0" max="99" step="1" required>
                        đến <input type="number" id="ageMax" name="ageMax" min="1" max="100" step="1" required>
                    </div>
                </div>
                <button type="submit" class="btn submit-btn">Tạo vé</button>
            </form>

            <c:if test="${not empty success}">
                <p class="success-message">${success}</p>
            </c:if>
            <c:if test="${not empty error}">
                <p class="error-message">${error}</p>
            </c:if>
        </div>

        <script>
            function validateForm() {
                var ageMin = document.getElementById("ageMin").value ? parseInt(document.getElementById("ageMin").value) : null;
                var ageMax = document.getElementById("ageMax").value ? parseInt(document.getElementById("ageMax").value) : null;

                if ((ageMin !== null && (ageMin < 0 || ageMin > 99)) || (ageMax !== null && (ageMax < 1 || ageMax > 100))) {
                    alert("Độ tuổi phải nằm trong khoảng từ 0 đến 99 cho độ tuổi tối thiểu và từ 1 đến 100 cho độ tuổi tối đa.");
                    return false;
                }

                if (ageMin !== null && ageMax !== null && ageMin >= ageMax) {
                    alert("Độ tuổi tối thiểu phải nhỏ hơn độ tuổi tối đa.");
                    return false;
                }

                if (document.getElementById("typeName").value.trim() === "") {
                    alert("Tên loại vé không được để trống.");
                    return false;
                }

                if (document.getElementById("typeCode").value.trim() === "") {
                    alert("Mã vé không được để trống.");
                    return false;
                }

                if (document.getElementById("priceTicket").value.trim() === "") {
                    alert("Giá vé không được để trống.");
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>
