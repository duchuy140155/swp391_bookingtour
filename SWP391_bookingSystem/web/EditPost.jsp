<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Post</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container {
            margin-top: 50px;
        }
        .card {
            border-radius: 10px;
        }
        .btn-custom {
            background-color: #007bff;
            color: #fff;
        }
    </style>
</head>
<body>
    <c:set var="p"  value="${sessionScope.post}" />
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                <h3>Edit Post</h3>
            </div>
            <div class="card-body">
                <form action="updatepost" method="post">
                     <div class="form-group">
                        <label for="postTitle">Post ID</label>
                        <input type="text" value="${p.postId}" class="form-control" id="postTitle" name="id" readonly="true">
                    </div>
                    <div class="form-group">
                        <label for="postTitle">Post Title</label>
                        <input type="text" value="${p.postTitle}" class="form-control" id="postTitle" name="postTitle" required>
                    </div>
                    <div class="form-group">
                        <label for="postContent">Post Content</label>
                        <textarea class="form-control" id="postContent" name="postContent" rows="5" required>${p.postContent}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Image URL</label>
                        <input type="text" value="${p.image}" class="form-control" id="image" name="image">
                    </div>
                    <button type="submit" class="btn btn-custom">Update Post</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
