<%-- 
    Document   : adminLocation
    Created on : May 21, 2024, 7:03:46 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Locations</title>
        <link href="css/adminLocation.css" rel="stylesheet" type="text/css"/>
        <script>
            function confirmDelete(locationName, deleteUrl) {
                if (confirm('Bạn có chắc chắn muốn xóa địa điểm ' + locationName + ' không?')) {
                    window.location.href = deleteUrl;
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <h1>Admin Locations</h1>
                <form method="get" action="locations" class="search-form">
                    <input type="text" name="search" placeholder="Tìm kiếm..." value="${search}">
                    <button type="submit">Search</button>
                </form>
            </div>
            <a href="admin.jsp" class="btn back-btn">Back to Admin</a>
            <a href="locations?action=add" class="btn add-btn">Thêm Địa điểm</a>
            <a href="locations?sortField=locationName&sortOrder=${sortOrder == 'ASC' ? 'DESC' : 'ASC'}" class="btn sort-btn">
                Sắp xếp theo tên ${sortOrder == 'ASC' ? '▲' : '▼'}
            </a>
            <table>
                <thead>
                    <tr>
                        <th>Tên Địa điểm</th>
                        <th>Mô tả</th>
                        <th>Hình ảnh</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="location" items="${locations}">
                        <tr>
                            <td>${location.locationName}</td>
                            <td>${location.locationDescription}</td>
                            <td><img src="${pageContext.request.contextPath}/uploads/${location.locationImage}" alt="${location.locationName}" width="100" height="100"></td>
                            <td class="actions">
                                <a href="locations?action=edit&locationId=${location.locationId}" class="btn edit-btn">Sửa</a>
                                <a href="javascript:void(0);" onclick="confirmDelete('${location.locationName}', 'locations?action=delete&locationId=${location.locationId}');" class="btn delete-btn">Xóa</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <c:if test="${totalPages > 1}">
                <div class="pagination">
                    <a href="locations?page=1" class="btn page-btn">Trang đầu</a>
                    <c:forEach var="i" begin="1" end="${totalPages}">
                        <a href="locations?page=${i}" class="btn page-btn ${i == currentPage ? 'active' : ''}">${i}</a>
                    </c:forEach>
                    <a href="locations?page=${totalPages}" class="btn page-btn">Trang cuối</a>
                </div>
            </c:if>
            <c:if test="${not empty success}">
                <p class="success-message">${success}</p>
            </c:if>
            <c:if test="${not empty error}">
                <p class="error-message">${error}</p>
            </c:if>
        </div>
    </body>
</html>







