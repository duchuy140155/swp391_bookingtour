<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lý Bài Đăng</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            max-width: 1200px;
            margin: 50px auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        .table-container {
            overflow-x: auto;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0;
        }
        table th, table td {
            padding: 12px;
            border: 1px solid #ddd;
            text-align: left;
        }
        table th {
            background-color: #f2f2f2;
        }
        .btn {
            padding: 10px 20px;
            border: none;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            font-size: 14px;
            display: inline-block;
            margin: 2px;
            width: 80px; /* Đảm bảo chiều rộng cố định */
            text-align: center;
        }
        .btn:hover {
            opacity: 0.9;
        }
        .btn-danger {
            background-color: #dc3545;
        }
        .btn-danger:hover {
            background-color: #c82333;
        }
        .btn-success {
            background-color: #28a745;
        }
        .btn-success:hover {
            background-color: #218838;
        }
        .btn-back {
            background-color: #007bff;
            position: absolute;
            top: 20px;
            left: 20px;
        }
        .btn-back:hover {
            background-color: #0056b3;
        }
        .actions {
            display: flex;
            justify-content: space-around;
        }
    </style>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</head>
<body>
    <button class="btn btn-back" onclick="goBack()">Quay lại</button>
    <div class="container">
        <h1>Quản lý Bài Đăng</h1>
        <div class="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Post ID</th>
                        <th>Post Title</th>
                        <th>Post Content</th>
                        <th>Create At</th>
                        <th>Partner ID</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="post" items="${sessionScope.listpost}">
                        <tr>
                            <td>${post.postId}</td>
                            <td>${post.postTitle}</td>
                            <td>${post.postContent}</td>
                            <td><fmt:formatDate value="${post.createAt}" pattern="yyyy/MM/dd" /></td>
                            <td>${post.partnerID}</td>
                            <td><img src="${post.image}" alt="Image" style="width: 100px; height: auto;"></td>
                            <td class="actions">
                                <a href="editpost?postID=${post.postId}" class="btn btn-success">Edit</a>
                                <a href="deletepost?postID=${post.postId}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this post?');">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
