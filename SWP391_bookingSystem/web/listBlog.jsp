<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Include your CSS and other header elements here -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500;600&family=Roboto&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-image: url('img/another.jpg');
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                background-attachment: fixed;
            }
            .container {
                width: 80%;
                margin: 50px auto 0 auto;
                padding: 20px;
                background-color: rgba(255, 255, 255, 0.8);
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                margin-bottom: 20px;
                margin-top: 50px;
            }
            .create-blog-form {
                background-color: white;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
            }
            .create-blog-form input,
            .create-blog-form textarea {
                width: 100%;
                padding: 10px;
                margin: 10px 0;
                border-radius: 5px;
                border: 1px solid #ccc;
            }
            .create-blog-form button {
                background-color: #1877f2;
                color: white;
                padding: 10px 20px;
                border-radius: 5px;
                border: none;
                cursor: pointer;
            }
            .create-blog-form button:hover {
                background-color: #165db0;
            }
            .blog-list {
                display: flex;
                flex-direction: column;
                gap: 20px;
            }
            .blog-post {
                background-color: white;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .blog-header {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
            }
            .title-icons-container {
                display: flex;
                justify-content: space-between;
                align-items: center;
                width: 100%;
                position: relative;
            }
            .blog-title {
                margin: 0;
                flex-grow: 1;
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-toggle {
                cursor: pointer;
            }
            .dropdown-menu {
                display: none;
                position: absolute;
                background-color: white;
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
                z-index: 1;
                right: 0;
                min-width: 150px;
            }
            .dropdown-menu .dropdown-item {
                padding: 12px 16px;
                display: block;
                color: black;
                text-decoration: none;
                cursor: pointer;
            }
            .dropdown-menu .dropdown-item:hover {
                background-color: #ddd;
            }
            .details-container {
                display: flex;
                justify-content: space-between;
                width: 100%;
            }
            .blog-author, .blog-date {
                margin: 0;
            }
            .blog-header h2 {
                margin: 0;
                font-size: 1.5em;
            }
            .blog-header p {
                margin: 5px 0;
                color: #555;
            }
            .blog-content {
                margin-bottom: 10px;
            }
            .blog-content img {
                max-width: 100%;
                height: auto;
                border-radius: 10px;
                margin-top: 10px;
            }
            .blog-footer {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .blog-footer p {
                margin: 0;
                color: #555;
            }
            .like-button {
                background-color: grey;
                color: white;
                padding: 5px 10px;
                text-decoration: none;
                border-radius: 5px;
                cursor: pointer;
            }
            .like-button.liked {
                background-color: green;
            }
            .favorite-icon {
                font-size: 24px;
                cursor: pointer;
                margin-left: 10px;
            }
            .favorite-icon.favorited {
                color: red;
            }
            .back-button-container {
                margin: 20px;
                text-align: left;
                position: absolute;
                top: 10px;
                left: 10px;
            }
            .back-button-container .back-btn {
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
            }
            .back-button-container .back-btn:hover {
                background-color: #0056b3;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="back-button-container">
            <button onclick="window.location.href = 'homepage'" class="back-btn">Back to Homepage</button>
        </div>
        <div class="header">
            <h2>Chia sẻ trải nghiệm của bạn ở đây</h2>
        </div>
        <div class="create-blog-form">
            <form id="createBlogForm" action="createBlog" method="post" enctype="multipart/form-data">
                <input type="text" name="title" placeholder="Blog Title" required maxlength="300">
                <textarea name="content" id="editor" placeholder="Blog Content" rows="5"></textarea>
                <button type="submit">Đăng</button>
            </form>
        </div>
        <div class="header">
            <h2>Blogs</h2>
        </div>
        <div class="blog-list">
            <c:forEach var="blog" items="${blogs}">
                <div class="blog-post">
                    <div class="blog-header">
                        <div class="title-icons-container">
                            <h2 class="blog-title">${blog.title}</h2>
                            <div class="dropdown">
                                <span class="dropdown-toggle">
                                    <i class="bi bi-three-dots"></i>
                                </span>
                                <div class="dropdown-menu">
                                    <c:if test="${blog.user.userID == sessionScope.account.userID}">
                                        <a href="editBlog?blogID=${blog.blogID}" class="dropdown-item">
                                            <i class="bi bi-pencil-square"></i> Edit
                                        </a>
                                        <span class="dropdown-item delete-blog" data-blogid="${blog.blogID}">
                                            <i class="bi bi-trash"></i> Delete
                                        </span>
                                    </c:if>
                                    <c:if test="${blog.user.userID != sessionScope.account.userID}">
                                        <c:if test="${blog.favorite}">
                                            <span class="dropdown-item favorite-blog" data-blogid="${blog.blogID}" data-action="unfavorite">
                                                <i class="bi bi-flag-fill favorite-icon favorited"></i> Unfavorite
                                            </span>
                                        </c:if>
                                        <c:if test="${!blog.favorite}">
                                            <span class="dropdown-item favorite-blog" data-blogid="${blog.blogID}" data-action="favorite">
                                                <i class="bi bi-flag favorite-icon"></i> Favorite
                                            </span>
                                        </c:if>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="details-container">
                            <p class="blog-author">Đăng bởi: ${blog.user.userName}</p>
                            <p class="blog-date">Ngày tạo: ${blog.createdAt}</p>
                        </div>
                    </div>
                    <div class="blog-content">
                        <c:choose>
                            <c:when test="${fn:length(blog.content) > 200}">
                                <p>${fn:substring(blog.content, 0, 200)}...</p>
                            </c:when>
                            <c:otherwise>
                                <p>${blog.content}</p>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${!empty blog.pictures}">
                            <img src="${blog.pictures[0].pictureURL}" alt="Blog Image">
                        </c:if>
                    </div>
                    <div class="blog-footer">
                        <button class="like-button ${blog.liked ? 'liked' : ''}" data-blogid="${blog.blogID}">
                            Likes: <span class="like-count">${blog.likes.size()}</span>
                        </button>
                        <c:set var="totalComments" value="${blog.comments.size()}"/>
                        <c:forEach var="comment" items="${blog.comments}">
                            <c:set var="totalComments" value="${totalComments + comment.replies.size()}"/>
                        </c:forEach>
                        <p>Total Comments: ${totalComments}</p>
                        <a href="viewBlog?blogID=${blog.blogID}" class="view-blog-button">Xem chi tiết</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <button id="loadMoreButton" class="btn btn-primary" style="display: ${currentPage * blogsPerPage < totalBlogs ? 'block' : 'none'};">Load More</button>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>

    <script>
        $(document).ready(function () {
            let createEditor;

            ClassicEditor
                .create(document.querySelector('#editor'), {
                    ckfinder: {
                        uploadUrl: 'uploadImage' // This should point to your image upload handler
                    }
                })
                .then(editor => {
                    createEditor = editor;
                })
                .catch(error => {
                    console.error(error);
                });

            const createBlogForm = document.getElementById('createBlogForm');
            createBlogForm.addEventListener('submit', function (event) {
                event.preventDefault();
                const content = createEditor.getData();
                if (!content.trim()) {
                    alert('Content cannot be empty.');
                    return;
                }
                const textarea = document.createElement('textarea');
                textarea.name = 'content';
                textarea.value = content;
                textarea.style.display = 'none';
                createBlogForm.appendChild(textarea);
                createBlogForm.submit();
            });

            // Like Button Handler
            $(document).on('click', '.like-button', function () {
                var button = $(this);
                var blogID = button.data('blogid');
                var action = button.hasClass('liked') ? 'removeLike' : 'createLike';

                $.ajax({
                    url: action,
                    type: 'POST',
                    data: { blogID: blogID },
                    success: function (response) {
                        if (response.success) {
                            button.toggleClass('liked');
                            var likeCount = button.find('.like-count');
                            likeCount.text(response.totalLikes);
                        } else {
                            alert('Error: ' + response.message);
                        }
                    },
                    error: function () {
                        alert('An error occurred.');
                    }
                });
            });

            // Favorite Button Handler
            $(document).on('click', '.favorite-blog', function () {
                var button = $(this);
                var blogID = button.data('blogid');
                var action = button.data('action');

                $.ajax({
                    url: 'favoriteBlog',
                    type: 'POST',
                    data: { blogID: blogID, action: action },
                    success: function (response) {
                        if (response.success) {
                            button.find('i').toggleClass('bi-flag bi-flag-fill').css('color', response.favorited ? 'red' : '');
                            button.data('action', response.favorited ? 'unfavorite' : 'favorite');
                        } else {
                            alert('Error: ' + response.message);
                        }
                    },
                    error: function () {
                        alert('An error occurred.');
                    }
                });
            });

            // Delete Blog Handler
            $(document).on('click', '.delete-blog', function () {
                if (confirm('Are you sure you want to delete this blog?')) {
                    var blogID = $(this).data('blogid');
                    $.ajax({
                        url: 'deleteBlog',
                        type: 'POST',
                        data: { blogID: blogID },
                        success: function (response) {
                            if (response.success) {
                                location.reload();
                            } else {
                                alert('Error: ' + response.message);
                            }
                        },
                        error: function () {
                            alert('An error occurred.');
                        }
                    });
                }
            });

            // Load More Blogs
            let currentPage = parseInt("${currentPage}");
            const totalBlogs = parseInt("${totalBlogs}");
            const blogsPerPage = parseInt("${blogsPerPage}");

            function truncateContent() {
                $('.blog-content').each(function () {
                    const content = $(this).text();
                    if (content.length > 200) {
                        $(this).html(content.substring(0, 200) + '...');
                    }
                });
            }

            function loadMoreBlogs() {
                $.ajax({
                    url: 'listBlog',
                    type: 'GET',
                    data: { page: currentPage + 1, ajax: 'true' },
                    success: function (response) {
                        $('.blog-list').append(response);
                        currentPage++;
                        truncateContent();
                        if (currentPage * blogsPerPage >= totalBlogs) {
                            $('#loadMoreButton').hide();
                        }
                    },
                    error: function () {
                        alert('An error occurred while loading more blogs.');
                    }
                });
            }

            $('#loadMoreButton').click(function () {
                loadMoreBlogs();
            });

            if (currentPage * blogsPerPage >= totalBlogs) {
                $('#loadMoreButton').hide();
            }

            // Initial truncation
            truncateContent();

            // Dropdown toggle
            $(document).on('click', '.dropdown-toggle', function (event) {
                event.stopPropagation();
                var dropdownMenu = $(this).next('.dropdown-menu');
                $('.dropdown-menu').not(dropdownMenu).hide();
                dropdownMenu.toggle();
            });

            // Close dropdown if clicked outside
            $(document).click(function () {
                $('.dropdown-menu').hide();
            });
        });
    </script>

</body>
</html>
