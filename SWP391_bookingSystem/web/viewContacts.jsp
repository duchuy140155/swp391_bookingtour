<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách liên hệ mới</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #e0f7fa;
            font-family: 'Candara', sans-serif;
        }
        .side-nav {
            background: linear-gradient(to bottom, #0066cc, #0099ff);
            color: white;
            padding: 20px;
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            position: fixed;
            width: 270px;
            border-radius: 0 15px 15px 0;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .user-info {
            text-align: center;
            margin-bottom: 20px;
        }
        .user-image {
            border-radius: 50%;
            width: 60px;
            height: 60px;
            object-fit: cover;
            margin-bottom: 10px;
            border: 2px solid #ffffff;
        }
        .user-info p {
            font-size: 18px;
            font-weight: bold;
            margin: 0;
        }
        .side-nav a {
            color: white;
            text-decoration: none;
            padding: 10px 20px;
            transition: background-color 0.3s;
            position: relative;
            display: block;
            margin-bottom: 10px;
            border-radius: 25px;
            text-align: center;
        }
        .side-nav a:hover {
            background-color: #0059b3;
        }
        .side-nav .active {
            background-color: #004080;
            font-weight: bold;
        }
        .notification {
            background-color: red;
            color: white;
            border-radius: 50%;
            padding: 2px 8px;
            font-size: 12px;
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .main-content {
            margin-left: 290px;
            padding: 20px;
        }
        .container {
            margin-top: 30px;
        }
        h1 {
            text-align: center;
            margin-bottom: 30px;
            font-size: 28px;
            color: #004080;
            font-weight: 700;
        }
        .table-responsive {
            background-color: #ffffff;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            margin-top: 20px;
        }
        .table {
            border-radius: 15px;
            overflow: hidden;
        }
        .table thead {
            background-color: #004080;
            color: white;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
            border: none;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .btn {
            border: none;
            border-radius: 25px;
            font-size: 14px;
            padding: 10px 20px;
            transition: all 0.3s ease;
        }
        .btn-primary {
            background-color: #004080;
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-primary:hover {
            background-color: #003366;
            border-color: #003366;
        }
        .btn-info {
            background-color: #17a2b8;
            border-color: #17a2b8;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-info:hover {
            background-color: #138496;
            border-color: #138496;
        }
        .btn-danger {
            background-color: #dc3545;
            border-color: #dc3545;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-danger:hover {
            background-color: #bd2130;
            border-color: #bd2130;
        }
        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .pagination a {
            color: #004080;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #dee2e6;
            margin: 0 4px;
            border-radius: 25px;
            transition: background-color 0.3s;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .pagination a.active {
            background-color: #004080;
            color: white;
            border: 1px solid #004080;
        }
        .pagination a:hover:not(.active) {
            background-color: #dee2e6;
        }
    </style>
    <script>
         

        function updateStatus(contactId, checkbox) {
            fetch('viewContacts', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'action=updateStatus&contactId=' + contactId
            }).then(response => response.text()).then(data => {
                if (data === "success") {
                    checkbox.closest('tr').querySelector('.status').innerText = 'Đã xử lý';
                } else {
                    console.error('Error updating status');
                }
            }).catch(error => {
                console.error('Error:', error);
            });
        }

        function showDetails(name, email, phone, subject, message, status) {
            document.getElementById('modal-name').innerText = name;
            document.getElementById('modal-email').innerText = email;
            document.getElementById('modal-phone').innerText = phone;
            document.getElementById('modal-subject').innerText = subject;
            document.getElementById('modal-message').innerText = message;
            document.getElementById('modal-status').innerText = status ? 'Đã xử lý' : 'Chưa xử lý';
            $('#detailModal').modal('show');
        }
    </script>
</head>
<body>
    <div class="side-nav">
        <div>
            <div class="user-info">
                <img src="${sessionScope.partner.certificate}" alt="User Image" class="user-image">
                <p>Xin chào ${sessionScope.partner.partnerName}!</p>
            </div>
            <a href="${pageContext.request.contextPath}/viewContacts" class="active"><i class="fas fa-envelope"></i> Quản lí liên hệ
                <c:if test="${newContactCount > 0}">
                    <span class="notification">${newContactCount}</span>
                </c:if>
            </a>
            <a href="#"><i class="fas fa-comments"></i> Tin nhắn</a>
        </div>
        <div>
            <a href="logout" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
        </div>
    </div>
    <div class="main-content">
        <div class="container">
            <h1>Danh sách liên hệ</h1>
              <a href="PartnerInfo" class="btn btn-danger">
                <i class="fas fa-sign-out-alt"></i> Quay lại
            </a>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th style="background:#004080">Tên</th>
                            <th style="background:#004080">Email</th>
                            <th style="background:#004080">Số điện thoại</th>
                            <th style="background:#004080">Chủ đề</th>
                            <th style="background:#004080">Trạng thái</th>
                            <th style="background:#004080">Tình trạng</th>
                            <th style="background:#004080">Ngày gửi</th>
                            <th style="background:#004080">Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="contact" items="${contactRequests}">
                            <tr>
                                <td>${contact.name}</td>
                                <td>${contact.email}</td>
                                <td>${contact.phone}</td>
                                <td>${contact.subject}</td>
                                <td class="status">${contact.status ? 'Đã xử lý' : 'Chưa xử lý'}</td>
                                <td>
                                    <input type="checkbox" onclick="updateStatus(${contact.id}, this)" ${contact.status ? 'checked' : ''}>
                                </td>
                                <td>${contact.createdAt}</td>
                                <td>
                                    <button class="btn btn-info btn-sm" onclick="showDetails('${contact.name}', '${contact.email}', '${contact.phone}', '${contact.subject}', '${contact.message}', ${contact.status})">Xem chi tiết</button>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailModalLabel">Chi tiết liên hệ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><strong>Tên:</strong> <span id="modal-name"></span></p>
                    <p><strong>Email:</strong> <span id="modal-email"></span></p>
                    <p><strong>Số điện thoại:</strong> <span id="modal-phone"></span></p>
                    <p><strong>Chủ đề:</strong> <span id="modal-subject"></span></p>
                    <p><strong>Nội dung:</strong> <span id="modal-message"></span></p>
                    <p><strong>Trạng thái:</strong> <span id="modal-status"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
