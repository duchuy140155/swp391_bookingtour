<%-- 
    Document   : registerPartner
    Created on : May 21, 2024, 3:06:08 AM
    Author     : MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Register Partner</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
            }
            .container {
                width: 50%;
                margin: 50px auto;
                background: #fff;
                padding: 20px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h2 {
                text-align: center;
                margin-bottom: 20px;
                color: #333;
            }
            .form-group {
                margin-bottom: 15px;
            }
            .form-group label {
                display: block;
                margin-bottom: 5px;
                color: #555;
            }
            .form-group input[type="text"],
            .form-group input[type="email"],
            .form-group input[type="password"],
            .form-group input[type="file"] {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .form-group input[type="submit"] {
                width: 100%;
                padding: 10px;
                background-color: #007bff;
                border: none;
                border-radius: 4px;
                color: white;
                font-size: 16px;
            }
            .form-group input[type="submit"]:hover {
                background-color: #0056b3;
            }
            .message {
                text-align: center;
                margin-bottom: 15px;
                color: red;
            }
            .preview {
                margin-top: 10px;
                text-align: center;
            }
            .preview img {
                max-width: 100%;
                height: auto;
            }
        </style>
        <script>
            function previewImage() {
                var file = document.getElementById("certificate").files[0];
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.getElementById("preview").src = e.target.result;
                    document.getElementById("preview").style.display = "block";
                }

                reader.readAsDataURL(file);
            }

            window.onload = function () {
                var message = "<%= request.getAttribute("message") %>";
                var messageType = "<%= request.getAttribute("messageType") %>";
                if (message) {
                    alert(message);
                    if (messageType === "success") {
                        document.getElementById("registerForm").reset();
                        document.getElementById("preview").style.display = "none";
                    }
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <h2>Register Partner</h2>
            <form id="registerForm" action="registerPartner" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="partnerName">Partner Name:</label>
                    <input type="text" id="partnerName" name="partnerName" required minlength="2" maxlength="30" pattern="[a-zA-Z0-9\s]+" title="Partner name must be between 2 and 30 characters and cannot contain special characters.">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" required pattern="^[a-zA-Z0-9._%+-]+@gmail\.com$" title="Email must be in the form @gmail.com">
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" required pattern="(?=.*[!@#$%^&*(),.?\":{}|<>]).{8,}" title="Password must be at least 8 characters long and contain at least one special character.">
                </div>
                <div class="form-group">
                    <label for="phoneNumber">Phone Number:</label>
                    <input type="text" id="phoneNumber" name="phoneNumber" required pattern="\d{10}" title="Phone number must be exactly 10 digits long and contain only numbers.">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" id="address" name="address" required minlength="2" maxlength="30" pattern="[a-zA-Z0-9\s]+" title="Address must be between 2 and 30 characters and cannot contain special characters.">
                </div>
                <div class="form-group">
                    <label for="certificate">Certificate:</label>
                    <input type="file" id="certificate" name="certificate" accept="image/*" onchange="previewImage()" required>
                    <div class="preview">
                        <img id="preview" src="#" alt="Certificate Preview" style="display:none;">
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Register">
                </div>
            </form>
        </div>
    </body>
</html>
