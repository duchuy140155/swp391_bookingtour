<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>BookingTour - Tourism Website Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500;600&family=Roboto&display=swap" rel="stylesheet"> 

        <!-- Icon Font Stylesheet -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">


        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">

        <!-- tu thiet ke -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

    </head>

    <body>

        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->

        <!-- Topbar Start -->
        <div class="container-fluid bg-primary px-5 d-none d-lg-block">
            <div class="row gx-0">
                <div class="col-lg-8 text-center text-lg-start mb-2 mb-lg-0">
                    <div class="d-inline-flex align-items-center" style="height: 45px;">
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="Trangtrang.html"><i class="fab fa-twitter fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="Trangtrang.html"><i class="fab fa-facebook-f fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="Trangtrang.html"><i class="fab fa-linkedin-in fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="Trangtrang.html"><i class="fab fa-instagram fw-normal"></i></a>
                        <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle" href="Trangtrang.html"><i class="fab fa-youtube fw-normal"></i></a>
                    </div>
                </div>
               <div class="col-lg-4 text-center text-lg-end">
                        <div class="d-inline-flex align-items-center" style="height: 45px;">
                            <c:if test="${sessionScope.account==null && sessionScope.partner == null }" >
                                <a href="#"><small class="me-3 text-light"><i class="fa fa-user me-2"></i>Đăng Kí</small></a>
                                <a href="login"><small class="me-3 text-light"><i class="fa fa-sign-in-alt me-2"></i>Đăng Nhập</small></a>
                            </c:if>
                           <c:if test="${sessionScope.account != null || sessionScope.partner != null}">
                                <a href="logout"><small class="me-3 text-light"><i class="fa fa-sign-in-alt me-2"></i>Đăng Xuất</small></a>

                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle text-light" data-bs-toggle="dropdown"><small><i class="fa fa-home me-2"></i> Tài Khoản</small></a>
                                    <div class="dropdown-menu rounded">
                                        <a href="viewProfile" class="dropdown-item"><i class="fas fa-user-alt me-2"></i> Thông Tin Tài Khoản</a>
                                        <a href="bill" class="dropdown-item"><i class="fas fa-user-alt me-2"></i> Lịch sử thanh toán</a>
                                        <a href="profileBlog" class="dropdown-item"><i class="fas fa-comment-alt me-2"></i>Blog của tôi</a>
                                        <a href="bookerTour" class="dropdown-item"><i class="bi bi-eye-fill"></i> Xem các tour đã đặt</a>
                                        <a href="#" class="dropdown-item"><i class="fas fa-cog me-2"></i> Cài Đặt Tài Khoản</a>
                                        <a href="logout" class="dropdown-item"><i class="fas fa-power-off me-2"></i> Đăng Xuất</a>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <!-- Topbar End -->

        <!-- Navbar & Hero Start -->
        <div class="container-fluid position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0"><i class="fa fa-map-marker-alt me-3"></i>BookingTour</h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="homepage" class="nav-item nav-link active">Home</a>
                        <a href="newsList" class="nav-item nav-link active">Tin Tức</a>
                        <a href="listBlog" class="nav-item nav-link active">Blog</a>
                        <a href="hotel" class="nav-item nav-link active">Khách sạn</a>
                        <a href="customerpost" class="nav-item nav-link active">Bài Đăng</a>
                    </div>
                </div>
                <a href="displayTour" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4">Đặt Chỗ</a>
                <a href="checkPin.jsp" class="btn btn-primary rounded-pill py-2 px-4 ms-lg-4">Hủy Chỗ</a>
            </nav>

            <!-- Carousel Start -->
            <div class="carousel-header">
                <div id="carouselId" class="carousel slide" data-bs-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-bs-target="#carouselId" data-bs-slide-to="0" class="active"></li>
                        <li data-bs-target="#carouselId" data-bs-slide-to="1"></li>
                        <li data-bs-target="#carouselId" data-bs-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="img/carousel-2.jpg" class="img-fluid" alt="Image">
                            <div class="carousel-caption">
                                <div class="p-3" style="max-width: 900px;">
                                    <h4 class="text-white text-uppercase fw-bold mb-4" style="letter-spacing: 3px;">Khám phá thế giới</h4>
                                    <h1 class="display-2 text-capitalize text-white mb-4">Hãy cùng nhau thế giới!</h1>
                                    <p class="mb-5 fs-5">"Khám phá thế giới cùng chúng tôi, nơi mỗi hành trình là một câu chuyện độc đáo. Hãy để chúng tôi biến những giấc mơ du lịch của bạn thành hiện thực, tạo dựng kỷ niệm khó quên trên từng chặng đường. Trải nghiệm dịch vụ tuyệt vời và những điểm đến tuyệt hảo, chỉ có tại website bán tour của chúng tôi."
                                    </p>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <a class="btn-hover-bg btn btn-primary rounded-pill text-white py-3 px-5" href="displayTour">Khám phá ngay</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel End -->
        </div>

        <div class="container-fluid search-bar position-relative" style="top: -50%; transform: translateY(-50%);"></div>

        <!-- Search cua minh -->
        <style>
            .search-container {
                display: flex;
                justify-content: space-around;
                align-items: center;
                margin-top: 50px;
                padding: 20px;
                background-color: #fff;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .search-container .input-group {
                flex: 1;
                margin: 0 10px;
                display: flex;
                align-items: center;
                position: relative;
            }
            .search-container .input-group-prepend .input-group-text {
                background-color: #fff;
                border-right: none;
                display: flex;
                align-items: center;
                padding-right: 10px;
            }
            .search-container .input-group .form-control {
                border-left: none;
                height: 80px;
                font-size: 18px;
            }
            .search-container .input-group i {
                color: #ffbf00;
                margin-right: 10px;
            }
            .search-container .btn-search {
                background-color: #ffbf00;
                color: #fff;
                border: none;
                height: 80px;
                font-size: 18px;
                flex: 0 0 120px;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .search-container .btn-search:hover {
                background-color: #e0a800;
            }
            select.form-control {
                height: 80px;
                font-size: 18px;
                appearance: none;
            }
            .search-container .input-group input[type="date"] {
                height: 80px;
                font-size: 18px;
            }
        </style>

        <div class="container">
            <div class="search-container">
                <form action="displayTour" method="GET" style="display: flex; width: 100%;">
                    <!-- Điểm đi -->
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select id="start-location" name="startLocation" class="form-control">
                            <option value="" disabled>Nơi khởi hành</option>
                            <option value="" selected>Không rõ</option>
                            <!-- Các tỉnh/thành phố sẽ được thêm vào đây -->
                        </select>
                    </div>

                    <!-- Điểm đến -->
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select id="end-location" name="endLocation" class="form-control">
                            <option value="" disabled>Hãy chọn điểm đến</option>
                            <option value="" selected>Không rõ</option>
                            <!-- Các tỉnh/thành phố sẽ được thêm vào đây -->
                        </select>
                    </div>

                    <!-- Ngày đi -->
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <input type="date" name="startDate" class="form-control">
                    </div>

                    <!-- Nút tìm kiếm -->
                    <button type="submit" class="btn btn-search"><i class="fas fa-search"></i> Tìm kiếm</button>
                </form>
            </div>
        </div>
        <!-- Hàm khởi tạo các tỉnh/thành phố -->
        <script>
            function initLocations() {
                const locations = [
                    "Hà Nội", "TP Hồ Chí Minh", "An Giang", "Bà Rịa – Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh",
                    "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau",
                    "Cần Thơ", "Cao Bằng", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai",
                    "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương",
                    "Hải Phòng", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang",
                    "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định",
                    "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên", "Quảng Bình",
                    "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La",
                    "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang",
                    "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái"
                ];

                const startLocationSelect = document.getElementById('start-location');
                const endLocationSelect = document.getElementById('end-location');

                locations.forEach(location => {
                    const option1 = document.createElement('option');
                    option1.value = location;
                    option1.text = location;
                    startLocationSelect.add(option1);

                    const option2 = document.createElement('option');
                    option2.value = location;
                    option2.text = location;
                    endLocationSelect.add(option2);
                });
            }

            // Gọi hàm khởi tạo khi tài liệu đã sẵn sàng
            document.addEventListener('DOMContentLoaded', initLocations);
        </script>

        <!-- Thư viện JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <!-- Packages Start -->
        <div class="container-fluid packages py-5">
            <div class="container py-5">
                <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                    <h5 class="section-title px-3">Du Lịch</h5>
                    <h1 class="mb-0">Các Tour hấp dẫn</h1>
                </div>
                <div class="packages-carousel owl-carousel">
                    <c:forEach var="o" items="${listP}">
                        <div class="packages-item">
                            <div class="packages-img">
                                <img src="${o.thumbnails}" class="img-fluid w-100 rounded-top" alt="Image" width="200" height="130">
                                <div class="packages-info d-flex border border-start-0 border-end-0 position-absolute" style="width: 100%; bottom: 0; left: 0; z-index: 5;">
                                    <small class="flex-fill text-center border-end py-2"><i class="fa fa-map-marker-alt me-2"></i>${o.startLocation} - ${o.endLocation}</small>
                                    <small class="flex-fill text-center border-end py-2"><i class="fa fa-calendar-alt me-2"></i>${o.startDate} - ${o.endDate}</small>
                                    <small class="flex-fill text-center py-2"><i class="fa fa-user me-2"></i>${o.numberOfPeople} Person(s)</small>
                                </div>
                                <div class="packages-price py-2 px-4">${o.price}</div>
                            </div>
                            <div class="packages-content bg-light">
                                <div class="p-4 pb-0">
                                    <h5 class="mb-0">${o.tourName}</h5>
                                    <div class="mb-3">
                                        <small class="fa fa-star text-primary"></small>
                                        <small class="fa fa-star text-primary"></small>
                                        <small class="fa fa-star text-primary"></small>
                                        <small class="fa fa-star text-primary"></small>
                                        <small class="fa fa-star text-primary"></small>
                                    </div>
                                    <p class="mb-4">${o.tourDescription}</p>
                                </div>
                                <div class="row bg-primary rounded-bottom mx-0">
                                    <div class="col-6 text-start px-0">
                                        <a href="tourDetails?tourID=${o.tourID}" class="btn-hover btn text-white py-2 px-4">Đặt ngay</a>
                                    </div>
                                    <div class="col-6 text-end px-0">
                                        <a href="scheduleDetails?tourID=${o.tourID}" class="btn-hover btn text-white py-2 px-4">Xem Thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- Packages End -->

        <!-- Travel Guide Start -->
        <div class="container-fluid guide py-5">
            <div class="container py-5">
                <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                    <h5 class="section-title px-3">Hướng dẫn viên</h5>
                    <h1 class="mb-0">Các hướng dẫn viên nổi bật trong tháng.</h1>
                </div>
                <div class="row g-4">
                    <c:forEach var="guide" items="${topGuides}">
                        <div class="col-md-6 col-lg-3">
                            <div class="guide-item">
                                <div class="guide-img">
                                    <div class="guide-img-efects">
                                        <img src="${guide.imageURL}" class="img-fluid w-100 rounded-top" alt="Image">
                                    </div>
                                    <div class="guide-icon rounded-pill p-2">
                                        <a class="btn btn-square btn-primary rounded-circle mx-1" href=""><i class="fab fa-facebook-f"></i></a>
                                        <a class="btn btn-square btn-primary rounded-circle mx-1" href=""><i class="fab fa-twitter"></i></a>
                                        <a class="btn btn-square btn-primary rounded-circle mx-1" href=""><i class="fab fa-instagram"></i></a>
                                        <a class="btn btn-square btn-primary rounded-circle mx-1" href=""><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                                <div class="guide-title text-center rounded-bottom p-4">
                                    <div class="guide-title-inner">
                                        <h4 class="mt-3">${guide.comment}</h4>
                                        <div class="star-rating">
                                            <c:forEach var="i" begin="1" end="5">
                                                <c:choose>
                                                    <c:when test="${i <= guide.rating}">
                                                        <i class="fa fa-star"></i>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <i class="fa fa-star-o"></i>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- Travel Guide End -->

        <!-- Post Start -->
      
        <!-- Post End -->

        <!-- Blog Start -->
        <div class="container-fluid blog py-5">
            <div class="container py-5">
                <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                    <h5 class="section-title px-3">Our Blog</h5>
                    <h1 class="mb-4">Popular Travel Blogs</h1>
                    <p class="mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt tenetur sapiente atque. Magni non explicabo beatae sit, vel reiciendis consectetur numquam id similique sunt error obcaecati ducimus officia maiores.
                    </p>
                </div>
                <div class="row g-4 justify-content-center">
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-item">
                            <div class="blog-img">
                                <div class="blog-img-inner">
                                    <img class="img-fluid w-100 rounded-top" src="img/blog-1.jpg" alt="Image">
                                    <div class="blog-icon">
                                        <a href="#" class="my-auto"><i class="fas fa-link fa-2x text-white"></i></a>
                                    </div>
                                </div>
                                <div class="blog-info d-flex align-items-center border border-start-0 border-end-0">
                                    <small class="flex-fill text-center border-end py-2"><i class="fa fa-calendar-alt text-primary me-2"></i>28 Jan 2050</small>
                                    <a href="#" class="btn-hover flex-fill text-center text-white border-end py-2"><i class="fa fa-thumbs-up text-primary me-2"></i>1.7K</a>
                                    <a href="#" class="btn-hover flex-fill text-center text-white py-2"><i class="fa fa-comments text-primary me-2"></i>1K</a>
                                </div>
                            </div>
                            <div class="blog-content border border-top-0 rounded-bottom p-4">
                                <p class="mb-3">Posted By: Royal Hamblin </p>
                                <a href="#" class="h4">Adventures Trip</a>
                                <p class="my-3">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam eos</p>
                                <a href="#" class="btn btn-primary rounded-pill py-2 px-4">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-item">
                            <div class="blog-img">
                                <div class="blog-img-inner">
                                    <img class="img-fluid w-100 rounded-top" src="img/blog-2.jpg" alt="Image">
                                    <div class="blog-icon">
                                        <a href="#" class="my-auto"><i class="fas fa-link fa-2x text-white"></i></a>
                                    </div>
                                </div>
                                <div class="blog-info d-flex align-items-center border border-start-0 border-end-0">
                                    <small class="flex-fill text-center border-end py-2"><i class="fa fa-calendar-alt text-primary me-2"></i>28 Jan 2050</small>
                                    <a href="#" class="btn-hover flex-fill text-center text-white border-end py-2"><i class="fa fa-thumbs-up text-primary me-2"></i>1.7K</a>
                                    <a href="#" class="btn-hover flex-fill text-center text-white py-2"><i class="fa fa-comments text-primary me-2"></i>1K</a>
                                </div>
                            </div>
                            <div class="blog-content border border-top-0 rounded-bottom p-4">
                                <p class="mb-3">Posted By: Royal Hamblin </p>
                                <a href="#" class="h4">Adventures Trip</a>
                                <p class="my-3">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam eos</p>
                                <a href="#" class="btn btn-primary rounded-pill py-2 px-4">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-item">
                            <div class="blog-img">
                                <div class="blog-img-inner">
                                    <img class="img-fluid w-100 rounded-top" src="img/blog-3.jpg" alt="Image">
                                    <div class="blog-icon">
                                        <a href="#" class="my-auto"><i class="fas fa-link fa-2x text-white"></i></a>
                                    </div>
                                </div>
                                <div class="blog-info d-flex align-items-center border border-start-0 border-end-0">
                                    <small class="flex-fill text-center border-end py-2"><i class="fa fa-calendar-alt text-primary me-2"></i>28 Jan 2050</small>
                                    <a href="#" class="btn-hover flex-fill text-center text-white border-end py-2"><i class="fa fa-thumbs-up text-primary me-2"></i>1.7K</a>
                                    <a href="#" class="btn-hover flex-fill text-center text-white py-2"><i class="fa fa-comments text-primary me-2"></i>1K</a>
                                </div>
                            </div>
                            <div class="blog-content border border-top-0 rounded-bottom p-4">
                                <p class="mb-3">Posted By: Royal Hamblin </p>
                                <a href="#" class="h4">Adventures Trip</a>
                                <p class="my-3">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam eos</p>
                                <a href="#" class="btn btn-primary rounded-pill py-2 px-4">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->

        <!-- Testimonial Start -->
        <div class="container-fluid testimonial py-5">
            <div class="container py-5">
                <div class="mx-auto text-center mb-5" style="max-width: 900px;">
                    <h5 class="section-title px-3">Testimonial</h5>
                    <h1 class="mb-0">Our Clients Say!!!</h1>
                </div>
                <div class="testimonial-carousel owl-carousel">
                    <div class="testimonial-item text-center rounded pb-4">
                        <div class="testimonial-comment bg-light rounded p-4">
                            <p class="text-center mb-5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis nostrum cupiditate, eligendi repellendus saepe illum earum architecto dicta quisquam quasi porro officiis. Vero reiciendis,
                            </p>
                        </div>
                        <div class="testimonial-img p-1">
                            <img src="img/testimonial-1.jpg" class="img-fluid rounded-circle" alt="Image">
                        </div>
                        <div style="margin-top: -35px;">
                            <h5 class="mb-0">John Abraham</h5>
                            <p class="mb-0">New York, USA</p>
                            <div class="d-flex justify-content-center">
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-item text-center rounded pb-4">
                        <div class="testimonial-comment bg-light rounded p-4">
                            <p class="text-center mb-5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis nostrum cupiditate, eligendi repellendus saepe illum earum architecto dicta quisquam quasi porro officiis. Vero reiciendis,
                            </p>
                        </div>
                        <div class="testimonial-img p-1">
                            <img src="img/testimonial-2.jpg" class="img-fluid rounded-circle" alt="Image">
                        </div>
                        <div style="margin-top: -35px;">
                            <h5 class="mb-0">John Abraham</h5>
                            <p class="mb-0">New York, USA</p>
                            <div class="d-flex justify-content-center">
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-item text-center rounded pb-4">
                        <div class="testimonial-comment bg-light rounded p-4">
                            <p class="text-center mb-5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis nostrum cupiditate, eligendi repellendus saepe illum earum architecto dicta quisquam quasi porro officiis. Vero reiciendis,
                            </p>
                        </div>
                        <div class="testimonial-img p-1">
                            <img src="img/testimonial-3.jpg" class="img-fluid rounded-circle" alt="Image">
                        </div>
                        <div style="margin-top: -35px;">
                            <h5 class="mb-0">John Abraham</h5>
                            <p class="mb-0">New York, USA</p>
                            <div class="d-flex justify-content-center">
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-item text-center rounded pb-4">
                        <div class="testimonial-comment bg-light rounded p-4">
                            <p class="text-center mb-5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis nostrum cupiditate, eligendi repellendus saepe illum earum architecto dicta quisquam quasi porro officiis. Vero reiciendis,
                            </p>
                        </div>
                        <div class="testimonial-img p-1">
                            <img src="img/testimonial-4.jpg" class="img-fluid rounded-circle" alt="Image">
                        </div>
                        <div style="margin-top: -35px;">
                            <h5 class="mb-0">John Abraham</h5>
                            <p class="mb-0">New York, USA</p>
                            <div class="d-flex justify-content-center">
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                                <i class="fas fa-star text-primary"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->

        <!-- Footer Start -->
        <div class="container-fluid footer py-5">
            <div class="container py-5">
                <div class="row g-5">
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white">Thông tin website</h4>
                            <a href=""><i class="fas fa-home me-2"></i> Thạch Thất Hòa Lạc</a>
                            <a href=""><i class="fas fa-envelope me-2"></i> BookingTour@example.com</a>
                            <a href=""><i class="fas fa-phone me-2"></i> +012 345 67890</a>
                            <a href="" class="mb-3"><i class="fas fa-print me-2"></i> +012 345 67890</a>
                            <div class="d-flex align-items-center">
                                <i class="fas fa-share fa-2x text-white me-2"></i>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href="Trangtrang.html"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href="Trangtrang.html"><i class="fab fa-twitter"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href="Trangtrang.html"><i class="fab fa-instagram"></i></a>
                                <a class="btn-square btn btn-primary rounded-circle mx-1" href="Trangtrang.html"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white">Công ty</h4>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Nhân lực</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Khách sạn</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Đặt Phòng</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Trợ giúp</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Liên hệ</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Về chúng tôi</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white">Hỗ trợ</h4>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Chuyến/Huỷ tour</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Điều kiện thanh toán</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Các điều kiện huỷ tour </a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Trường hợp bất khả kháng</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Nội dung điều kiện thanh toán </a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Các điều kiện khi đăng ký tour</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="footer-item d-flex flex-column">
                            <h4 class="mb-4 text-white">Hỗ trợ</h4>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Liên hệ</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Thông báo pháp lý</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Chính sách bảo mật</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Điều khoản và điều kiệns</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Sơ đồ trang web</a>
                            <a href="Trangtrang.html"><i class="fas fa-angle-right me-2"></i> Chính sách cookie</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->

        <!-- Copyright Start -->
        <div class="container-fluid copyright text-body py-4">
            <div class="container">
                <div class="row g-4 align-items-center">
                    <div class="col-md-6 text-center text-md-end mb-md-0">
                        <i class="fas fa-copyright me-2"></i><a class="text-white" href="#">Booking Tour</a>, All right reserved.
                    </div>
                    <div class="col-md-6 text-center text-md-start">
                        Designed By <a class="text-white" href="https://htmlcodex.com">HTML Codex</a> Distributed By MinhPD</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->

        <!-- Back to Top -->
        <a href="#" class="btn btn-primary btn-primary-outline-0 btn-md-square back-to-top"><i class="fa fa-arrow-up"></i></a>   

        <!-- JavaScript Libraries -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/lightbox/js/lightbox.min.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <style>
            .guide-img-efects img, .post-img-efects img {
                width: 100%;
                height: 300px; /* Đặt chiều cao cố định cho hình ảnh */
                object-fit: cover; /* Đảm bảo rằng hình ảnh được bao phủ toàn bộ khung */
            }
            .star-rating {
                color: #FFD700; /* Màu vàng cho ngôi sao */
                font-size: 20px; /* Kích thước của ngôi sao */
                display: flex; /* Đặt ngôi sao theo hàng ngang */
                justify-content: center; /* Căn giữa các ngôi sao */
                margin-top: 10px; /* Khoảng cách phía trên ngôi sao */
            }
        </style>
    </body>
</html>
