<%-- 
    Document   : editBooking
    Created on : Jun 20, 2024, 12:42:11 AM
    Author     : Admin
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chỉnh sửa thông tin Đặt Tour</title>
        <link href="css/editBooking.css" rel="stylesheet" type="text/css"/>
        <script>
            function confirmUpdate() {
                return confirm("Bạn có chắc chắn muốn cập nhật không?");
            }
        </script>
    <h2 class="centered">Chỉnh sửa Thông tin đặt tour</h2>

</head>
<body>
    <div class="container">
        <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>
        <h2>Thông tin khách hàng</h2>
        <form action="updateBooking" method="post" onsubmit="return confirmUpdate();">
            <div class="form-group">
                <label for="customerName">Họ và Tên:</label>
                <input type="text" id="customerName" name="customerName" value="${bookingDetails.customerName}" />
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" value="${bookingDetails.email}" />
            </div>
            <div class="form-group">
                <label for="phoneNumber">Số điện thoại:</label>
                <input type="text" id="phoneNumber" name="phoneNumber" value="${bookingDetails.phoneNumber}" />
            </div>
            <div class="form-group">
                <label for="customerAddress">Địa chỉ:</label>
                <input type="text" id="customerAddress" name="customerAddress" value="${bookingDetails.customerAddress}" />
            </div>
            <div class="form-group">
                <label for="note">Ghi chú:</label>
                <input type="text" id="note" name="note" value="${bookingDetails.note}" />
            </div>
            <h2>Thông tin Tour</h2>
            <p><strong>Tên Tour:</strong> ${bookingDetails.tour.tourName}</p>
            <p><strong>Điểm đi:</strong> ${bookingDetails.tour.startLocation}</p>
            <p><strong>Điểm đến:</strong> ${bookingDetails.tour.endLocation}</p>
            <p><strong>Ngày đi:</strong> <fmt:formatDate value="${bookingDetails.tour.startDate}" pattern="dd-MM-yyyy" /></p>
            <p><strong>Ngày về:</strong> <fmt:formatDate value="${bookingDetails.tour.endDate}" pattern="dd-MM-yyyy" /></p>

            <h2>Thông tin hành khách</h2>
            <c:forEach var="ticket" items="${bookingDetails.tickets}" varStatus="status">
                               <div class="passenger-info">
                    <input type="hidden" name="ticketID_${status.index}" value="${ticket.ticketID}" />
                    <div class="form-group">
                        <label for="name_${status.index}">Họ và tên:</label>
                        <input type="text" id="name_${status.index}" name="name_${status.index}" value="${ticket.name}" />
                    </div>
                    <div class="form-group">
                        <label for="gender_${status.index}">Giới tính:</label>
                        <select id="gender_${status.index}" name="gender_${status.index}">
                            <option value="1" ${ticket.gender ? 'selected' : ''}>Nam</option>
                            <option value="0" ${!ticket.gender ? 'selected' : ''}>Nữ</option>
                        </select>
                    </div>
                    <div class="form-group1">
                        <label for="dob_${status.index}">Ngày sinh:</label>
                        <input type="date" id="dob_${status.index}" name="dob_${status.index}" value="${ticket.dob}" />
                    </div>
                    <div class="form-group">
                        <label>Loại vé:</label>
                        <p1>
                            ${ticket.tickettype.typeName}
                        </p>
                    </div>
                </div>
            </c:forEach>

            <h2>Thông tin thanh toán</h2>
            <p><strong>Phương thức thanh toán:</strong> ${bookingDetails.paymentMethod}</p>
            <p><strong>Giá đầu tiên (Chưa có):</strong> </p>
            <p><strong>Giá cuối cùng:</strong> <fmt:formatNumber value="${bookingDetails.finalPrice}" type="number" groupingUsed="true" /> vnđ</p>
            <p><strong>Mã discount:</strong> ${bookingDetails.discountID.discountName}</p>

            <h2>Trạng thái:</h2>
            <p><c:choose>
                    <c:when test="${bookingDetails.status == 0}">
                        Đang duyệt
                    </c:when>
                    <c:when test="${bookingDetails.status == 1}">
                        Đã được chấp nhận
                    </c:when>
                    <c:otherwise>
                        Trạng thái không xác định
                    </c:otherwise>
                </c:choose></p>
            <button type="submit" class="btn-update">Cập nhật</button>
        </form>
    </div>
</body>
</html>


