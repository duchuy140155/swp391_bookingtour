<%-- 
    Document   : handbookList
    Created on : May 25, 2024, 4:38:24 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Handbook List</title>
    </head>
    <body>
        <h2>Handbook List</h2>
        <a href="${pageContext.request.contextPath}/manageHandbook?action=create">Create New Handbook</a>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Partner ID</th>
                <th>Title</th>
                <th>Short Content</th>
                <th>Image Theme</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="handbook" items="${handbooks}">
                <tr>
                    <td>${handbook.handbookID}</td>
                    <td>${handbook.partnerID}</td>
                    <td>${handbook.title}</td>
                    <td>${handbook.shortContent}</td>
                    <td>${handbook.imageTheme}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/manageHandbook?action=edit&id=${handbook.handbookID}">Edit</a>
                        <a href="${pageContext.request.contextPath}/manageHandbook?action=delete&id=${handbook.handbookID}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
