<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
    <head>
        <meta charset="utf-8">

        <title>Coupon Alert Boxes - Bootdey.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                margin-top: 20px;
                padding: 50px;
            }

            .alert-coupon {
                border: 1px dashed;
                position: relative;
            }

            .alert-coupon h1,
            .alert-coupon h2,
            .alert-coupon h3,
            .alert-coupon h4,
            .alert-coupon h5,
            .alert-coupon h6 {
                font-size: 20px;
                line-height: 1.5;
                text-align: left;
            }

            .alert-coupon p:last-child {
                margin-bottom: 0;
                text-align: left;
            }

            .btn-coupon {
                position: absolute;
                top: 50%;
                right: 10px;
                transform: translateY(-50%);
            }
        </style>
    </head>
    <body>
        <div class="container bootstrap snippets bootdey">
            <c:if test="${sessionScope.account == null}">
                <h4 style="color: #6a7dfe;text-align: center">Bạn nên đăng nhập để có thể sử dụng các khuyến mãi của app</h4>
            </c:if>
            <div class="row">
                <div class="col-lg-12">
                    <c:forEach items="${sessionScope.listDiscount}" var="p">
                        <div class="alert alert-warning alert-coupon">
                            <c:if test="${sessionScope.account != null}">
                                <c:if test="${p.discountType != 1}">
                                    <button class="btn btn-primary btn-coupon" onclick="redirectToServlet('${pageContext.request.contextPath}/discountcontroller?type=1&id=${p.discountID}')" >Sử Dụng</button>
                                </c:if>
                            </c:if>

                            <h4><b>${p.discountCode}</b></h4>
                            <p>${p.discountName}</p>
                            <p>HSD: ${p.discountTo}</p>
                           <c:forEach items="${sessionScope.listTour}" var="tour">
                        <c:if test="${tour.tourID == p.tourID}">
                            <p>Áp Dụng Cho Tour: ${tour.tourName}</p>
                        </c:if>
                    </c:forEach>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function redirectToServlet(url) {
                window.location.href = url;
            }
        </script>
    </body>
</html>
