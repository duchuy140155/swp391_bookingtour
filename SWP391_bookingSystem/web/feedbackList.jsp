<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Feedback List</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f5f5f5;
                margin: 0;
                padding: 0;
            }
            .container {
                max-width: 800px;
                margin: 20px auto;
                padding: 20px;
                background-color: #fff;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .feedback-container {
                margin: 10px 0;
                padding: 15px;
                border-radius: 8px;
                background-color: #ffffff;
                display: flex;
                align-items: center;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                position: relative;
            }
            .feedback-container + .feedback-container {
                margin-top: 10px;
            }
            .profile-img {
                width: 50px;
                height: 50px;
                border-radius: 50%;
                margin-right: 15px;
                flex-shrink: 0;
            }
            .feedback-content {
                display: flex;
                flex-direction: column;
                width: 100%;
            }
            .feedback-author {
                font-weight: bold;
                color: #333;
                margin-bottom: 5px;
            }
            .feedback-text {
                color: #555;
            }
            h2 {
                text-align: center;
                color: #333;
            }
            .feedback-options {
                position: absolute;
                top: 10px;
                right: 15px;
                cursor: pointer;
                z-index: 1000;
            }
            .feedback-options span {
                display: inline-block;
                width: 20px;
                text-align: center;
            }
            .feedback-options ul {
                list-style: none;
                padding: 5px 0;
                margin: 0;
                display: none;
                position: absolute;
                background: white;
                border: 1px solid #ddd;
                right: 0;
                border-radius: 5px;
                box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
                min-width: 150px;
                z-index: 1000;
            }
            .feedback-options:hover ul {
                display: block;
            }
            .feedback-options li {
                padding: 10px 20px;
                color: #333;
                cursor: pointer;
                text-align: left;
            }
            .feedback-options li:hover {
                background-color: #f0f0f0;
            }
            .feedback-options a {
                text-decoration: none;
                color: #333;
            }
            .feedback-options a:hover {
                color: #000;
            }
            .add-feedback-form,
            .edit-feedback-form {
                display: flex;
                align-items: center;
                padding: 15px;
                border: 1px solid #ddd;
                border-radius: 8px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
                margin-top: 20px;
                background-color: #fff;
            }
            .add-feedback-form img,
            .edit-feedback-form img {
                width: 40px;
                height: 40px;
                border-radius: 50%;
                margin-right: 10px;
            }
            .add-feedback-form textarea,
            .edit-feedback-form textarea {
                flex: 1;
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 20px;
                margin-right: 10px;
                resize: none;
                height: 40px;
                box-sizing: border-box;
            }
            .add-feedback-form button,
            .edit-feedback-form button {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 20px;
                cursor: pointer;
                height: 40px;
                flex-shrink: 0;
            }
            .add-feedback-form button:hover,
            .edit-feedback-form button:hover {
                background-color: #45a049;
            }
            .center-text {
                display: flex;
                justify-content: center;
                align-items: center;
                text-align: center;
                margin-top: 20px;
            }
            .feedback-date {
                color: green;
                font-size: 0.7em;
                margin-top: 5px;
            }
            .search-filter-container {
                display: flex;
                justify-content: center;
                align-items: center;
                margin-bottom: 20px;
            }
            .search-input {
                width: 60%;
                padding: 10px;
                border-radius: 20px 0 0 20px;
                border: 1px solid #ddd;
                border-right: none;
                font-size: 1em;
                box-sizing: border-box;
            }
            .search-button {
                padding: 8px 15px;
                border-radius: 0 20px 20px 0;
                border: 1px solid #ddd;
                border-left: none;
                background-color: #4CAF50;
                color: white;
                cursor: pointer;
                font-size: 0.9em;
                height: 38px;
                box-sizing: border-box;
            }
            .filter-button {
                padding: 8px 15px;
                border-radius: 20px;
                border: 1px solid #ddd;
                background-color: #2196F3;
                color: white;
                cursor: pointer;
                font-size: 0.9em;
                margin-left: 10px;
                height: 38px;
                box-sizing: border-box;
            }
            .filter-button:hover,
            .search-button:hover {
                background-color: #45a049;
            }
            .bottom-section {
                text-align: center;
                margin-top: 20px;
            }
            .btn-login {
                font-size: 20px;
                text-decoration: underline;
                color: red;
                cursor: pointer;
            }
            .btn-login:hover {
                color: #0099ff;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <button type="button" name="back" onclick="history.back()" class="btn btn-secondary">Trở về</button>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <div class="search-filter-container">
                <input type="text" id="searchInput" class="form-control search-input" placeholder="Tìm kiếm theo tên người dùng...">
                <button onclick="searchFeedback()" id="searchButton" class="btn btn-success search-button">Tìm kiếm</button>
            </div>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <div id="feedbackContainer">
                <c:choose>
                    <c:when test="${not empty feedbackList}">
                        <c:forEach var="feedback" items="${feedbackList}" varStatus="status">
                            <div class="feedback-container" data-index="${status.index}" style="display: ${status.index < 5 ? 'flex' : 'none'};">
                                <c:if test="${not empty feedback.user.userPicture}">
                                    <img src="${feedback.user.userPicture}" alt="Profile" class="profile-img">
                                </c:if>
                                <div class="feedback-content">
                                    <span class="feedback-author">${feedback.user.userName}</span>
                                    <c:choose>
                                        <c:when test="${editFeedback != null && editFeedback.feedbackID == feedback.feedbackID}">
                                            <form action="feedback" method="post" class="edit-feedback-form">
                                                <input type="hidden" name="action" value="updateFeedback">
                                                <input type="hidden" name="tourID" value="${tourID}">
                                                <input type="hidden" name="feedbackID" value="${feedback.feedbackID}">
                                                <textarea name="feedbackContent" rows="1" required>${editFeedback.feedbackContent}</textarea>
                                                <button type="submit">Cập nhật</button>
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="feedback-text">${feedback.feedbackContent}</span>
                                            <fmt:formatDate value="${feedback.createDate}" pattern="dd/MM/yyyy" var="formattedDate"/>
                                            <span class="feedback-date">${formattedDate}</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <c:if test="${user.userID == feedback.user.userID}">
                                    <div class="feedback-options">
                                        <span>...</span>
                                        <ul>
                                            <li><a href="feedback?action=updateFeedback&tourID=${tourID}&feedbackID=${feedback.feedbackID}">Chỉnh sửa</a></li>
                                            <li><a href="#" onclick="confirmDelete('${feedback.feedbackID}', '${tourID}')">Xóa</a></li>
                                        </ul>
                                    </div>
                                </c:if>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <p>Không có feedback nào để hiển thị.</p>
                    </c:otherwise>
                </c:choose>
                <div class="bottom-section">
                    <div id="loadMoreContainer">
                        <button onclick="loadMore()" class="btn btn-warning">Xem thêm</button>
                    </div>
                    <c:if test="${empty user && empty partner}">
                        <div class="center-text">
                            <em style="color: red;">Quý khách vui lòng <span><a href="/SWP391_bookingSystem/login" class="btn-login"> Đăng Nhập</a></span> để thêm phản hồi về tour hoặc chỉnh sửa các phản hồi của mình</em>
                        </div>
                    </c:if>
                </div>
                <div class="add-feedback-form">
                    <c:if test="${not empty bookedtour}">
                        <c:if test="${isCheck}">
                            <img src="${user.userPicture}" alt="Profile">
                            <form action="feedback" method="post" style="display: flex; align-items: center; width: 100%;" onsubmit="return validateForm()">
                                <input type="hidden" name="action" value="insertFeedback">
                                <input type="hidden" name="tourID" value="${tourID}">
                                <input type="hidden" name="userID" value="${user.userID}">
                                <textarea id="feedbackContent" name="feedbackContent" placeholder="Viết bình luận..." rows="1" required minlength="1" maxlength="500"></textarea>
                                <button type="submit">Gửi</button>
                            </form>
                        </c:if>
                        <c:if test="${!isCheck}">
                            <p>Hãy để lại những phản hồi cho chúng tôi sau khi trải nghiệm chuyến đi nhé</p>
                        </c:if>
                    </c:if>
                </div>
            </div>
        </div>
        <script>
            function confirmDelete(feedbackID, tourID) {
                if (confirm('Bạn có chắc chắn muốn xóa phản hồi này?')) {
                    window.location.href = 'feedback?action=deleteFeedback&tourID=' + tourID + '&feedbackID=' + feedbackID;
                }
            }

            $(document).ready(function () {
            <c:if test="${not empty sessionScope.successMessage}">
                toastr.success('${sessionScope.successMessage}', {
                    timeOut: 5000,
                    closeButton: true,
                    progressBar: true,
                    positionClass: 'toast-top-right',
                    showMethod: 'slideDown',
                    hideMethod: 'slideUp',
                    preventDuplicates: true,
                    backgroundColor: '#4CAF50',
                    textColor: '#45a049'
                }).css("color", "#45a049");
            </c:if>
            <c:if test="${not empty sessionScope.errorMessage}">
                toastr.error('${sessionScope.errorMessage}', {
                    timeOut: 5000,
                    closeButton: true,
                    progressBar: true,
                    positionClass: 'toast-top-right',
                    showMethod: 'slideDown',
                    hideMethod: 'slideUp',
                    preventDuplicates: true,
                    backgroundColor: '#FF0000',
                    textColor: '#e53935'
                }).css("color", "#e53935");
            </c:if>
            });


            function loadMore() {
                var feedbackContainers = document.getElementsByClassName('feedback-container');
                var index = 0;
                for (var i = 0; i < feedbackContainers.length; i++) {
                    if (feedbackContainers[i].style.display === 'none') {
                        index = i;
                        break;
                    }
                }
                for (var j = index; j < index + 5 && j < feedbackContainers.length; j++) {
                    feedbackContainers[j].style.display = 'flex';
                }
                if (index + 5 >= feedbackContainers.length) {
                    document.getElementById('loadMoreContainer').style.display = 'none';
                }
            }

            function validateForm() {
                const feedbackContent = document.getElementById('feedbackContent').value.trim();
                if (feedbackContent === "") {
                    alert('Nội dung không được để trống hoặc chỉ chứa ký tự khoảng trắng.');
                    return false;
                }
                return true;
            }

            function searchFeedback() {
                var searchValue = $('#searchInput').val().toLowerCase();
                $('.feedback-container').each(function () {
                    var userName = $(this).find('.feedback-author').text().toLowerCase();
                    if (userName.includes(searchValue)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }

        </script>
    </body>
</html>
