<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="model.BookingDetails" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Kiểm tra PIN</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <style>
        body {
            background-color: #e0f7fa;
            color: #004d40;
        }
        .container {
            margin-top: 50px;
        }
        h2 {
            color: #00796b;
        }
        .btn-primary {
            background-color: #00796b;
            border-color: #00796b;
        }
        .btn-primary:hover {
            background-color: #004d40;
            border-color: #004d40;
        }
        .btn-secondary {
            background-color: #00796b;
            border-color: #00796b;
        }
        .btn-secondary:hover {
            background-color: #004d40;
            border-color: #004d40;
        }
        .modal-content {
            border-radius: 15px;
        }
        .form-control {
            border-radius: 10px;
        }
        .alert {
            border-radius: 10px;
        }
        .modal-header {
            background-color: #00796b;
            color: white;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
        }
        .close {
            color: white;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2><i class="fas fa-key"></i> Kiểm tra PIN</h2>
        <form action="CheckPin" method="POST">
            <div class="form-group">
                <label for="pin"><i class="fas fa-lock"></i> PIN:</label>
                <h1 style="font-size:15px;">(*) Mã Pin đã được gửi đến email của bạn khi đặt tour.</h1>
                <input type="text" id="pin" name="pin" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-check-circle"></i> Kiểm tra PIN</button>
        </form>

        <a href="homepage" class="btn btn-secondary mt-3"><i class="fas fa-home"></i> Trở về trang chủ</a>

        <% 
            String pinValid = (String) request.getAttribute("pinValid");
            String errorMessage = (String) request.getAttribute("errorMessage");
            BookingDetails bookingDetails = (BookingDetails) request.getAttribute("bookingDetails");
            Boolean emailSent = (Boolean) request.getAttribute("emailSent");
            if (pinValid != null) {
                if (pinValid.equals("true")) {
        %>
                    <div class="modal fade show" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:block;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fas fa-info-circle"></i> Thông tin đặt tour</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="ProcessCancelBooking" method="POST">
                                        <input type="hidden" name="bookingID" value="<%= bookingDetails.getBookingID() %>">
                                        <p><i class="fas fa-tag"></i> Mã Tour: <%= bookingDetails.getTourCode() %></p>
                                        <p><i class="fas fa-user"></i> Tên Khách Hàng: <%= bookingDetails.getCustomerName() %></p>
                                        <p><i class="fas fa-calendar-alt"></i> Ngày Đặt: <%= bookingDetails.getBookingDate() %></p>
                                        <p><i class="fas fa-clock"></i> Trạng Thái: Đang Hoạt động</p>
                                        <div class="form-group">
                                            <label for="cancelReason"><i class="fas fa-exclamation-circle"></i> Lý do hủy:</label>
                                            <select id="cancelReason" name="cancelReason" class="form-control" required>
                                                <option value="Lý do cá nhân">Lý do cá nhân</option>
                                                <option value="Vấn đề sức khỏe">Vấn đề sức khỏe</option>
                                                <option value="Xung đột lịch trình">Xung đột lịch trình</option>
                                                <option value="Khác">Khác</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-danger"><i class="fas fa-times-circle"></i> Hủy đặt tour</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        <% 
                } else {
        %>
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle"></i> <%= errorMessage %>
                    </div>
        <%
                }
            }

            if (emailSent != null && emailSent) {
        %>
            <script>
                $(document).ready(function() {
                    alert("Yêu cầu hủy tour đã được gửi. Chúng tôi sẽ phản hồi bạn trong thời gian sớm nhất.");
                });
            </script>
        <%
            }
        %>
    </div>
    <script>
        $(document).ready(function() {
            $('.modal').modal('show');
        });
    </script>
</body>
</html>
