<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="model.TourTicket" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tour Tickets</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
        <link rel="stylesheet" type="text/css" href="css/tourTicket.css">
    </head>
    <body>
        <a href="/SWP391_bookingSystem/listTour" class="back-link">Trở về</a>
        <h1 class="header" style="color: red;">Các loại vé</h1>
     

        <div class="messages" id="messages">
            <c:if test="${not empty errors}">
                <div class="error-messages">
                    <c:forEach var="error" items="${errors}">
                        <p>${error}</p>
                    </c:forEach>
                    <c:remove var="errors" scope="session" />
                </div>
            </c:if>
            <c:if test="${not empty message}">
                <div class="success-message">${message}</div>
                <c:remove var="message" scope="session" />
            </c:if>
        </div>

        <form action="ticketController" method="post" id="createtour" onsubmit="return validatePrices();">
            <div class="ticket-container">
                <input type="hidden" name="tourID" value="${tourID}">
                <c:forEach var="ticket" items="${listTickets}">
                    <div class="ticket-card">
                        <div class="ticket-row">
                            <label class="ticket-label">Loại vé: ${ticket.tickettype.typeName}</label><br>
                            <label class="ticket-code">Mã vé: ${ticket.tickettype.typeCode}</label><br>
                            <c:choose>
                                <c:when test="${ticket.priceTicket < 0}">
                                    <label class="ticket-label">Giá vé:</label>
                                    <input type="hidden" name="typeIDs" value="${ticket.typeID}">
                                    <input type="text" id="price${ticket.typeID}" name="price[${ticket.typeID}]" class="ticket-input double" placeholder="Nhập giá vé vào đây" value="">
                                </c:when>
                                <c:otherwise>
                                    <label class="ticket-label">Giá vé: ${ticket.priceTicket}</label>
                                    <a href="javascript:void(0);" onclick="showModal(${ticket.typeID}, '${ticket.tickettype.typeName}', ${ticket.priceTicket});">Chỉnh sửa</a>
                                    <c:if test="${!ticket.tickettype.isDefault}">
                                        <li><a href="#" onclick="confirmDelete('${ticket.typeID}', '${tourID}')">Xóa</a></li>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <c:set var="hasInvalidPrice" value="false" />
            <c:forEach var="ticket" items="${listTickets}">
                <c:if test="${ticket.priceTicket < 0}">
                    <c:set var="hasInvalidPrice" value="true" />
                </c:if>
            </c:forEach>
            <c:if test="${hasInvalidPrice}">
                <div class="submit-container">
                    <button type="submit" class="btn btn-create">Cập nhật</button>
                </div>
            </c:if>
        </form>

        <div id="editModal" style="display:none;">
            <div class="modal-content">
                <span onclick="closeModal();" class="close">&times;</span>
                <h2>Chỉnh sửa giá vé</h2>
                <form action="editPrice" method="post">
                    <input type="hidden" name="tourID" value="${tourID}">
                    <input type="hidden" id="editTypeID" name="typeIDs" value="">
                    <label>Loại vé: <span id="editTypeName"></span></label><br>
                    <label>Giá vé:</label>
                    <input type="text" id="editPrice" name="price" class="ticket-input double" value=""><br>
                    <button type="submit" class="btn btn-create">Lưu</button>
                </form>
            </div>
        </div>
<!--        <div class="ticket-container">
            <strong>Bạn có thể thêm những loại vé riêng cho tour tại đây!</strong>
            <span><a href="addTicket?tourID=${tourID}" class="text-danger">Thêm Vé Mới</a></span>
        </div>-->
        <script>
//            function confirmDelete(typeID, tourID) {
//                if (confirm('Bạn có chắc chắn muốn xóa phản hồi này?')) {
//                    window.location.href = 'editPrice?typeID=' + typeID + '&tourID=' + tourID;
//                }
//            }

            function showModal(typeID, typeName, priceTicket) {
                document.getElementById('editTypeID').value = typeID;
                document.getElementById('editTypeName').innerText = typeName;
                document.getElementById('editPrice').value = priceTicket;
                document.getElementById('editModal').style.display = 'block';
            }

            function closeModal() {
                document.getElementById('editModal').style.display = 'none';
            }

            setTimeout(function () {
                var messages = document.getElementById('messages');
                if (messages) {
                    messages.style.display = 'none';
                }
            }, 5000);

            function validatePrices() {
                var price1 = document.getElementById('price1').value;
                var price2 = document.getElementById('price2').value;
                var price3 = document.getElementById('price3').value;
                var price4 = document.getElementById('price4').value;

                if (price1 !== "" && price2 !== "" && parseFloat(price1) <= parseFloat(price2)) {
                    alert('Giá vé loại Trẻ em phải nhỏ hơn giá vé loại Người lớn.');
                    return false;
                }
                if (price2 !== "" && price3 !== "" && parseFloat(price2) <= parseFloat(price3)) {
                    alert('Giá vé loại Trẻ nhỏ phải nhỏ hơn giá vé loại Trẻ em.');
                    return false;
                }
                if (price3 !== "" && price4 !== "" && parseFloat(price3) <= parseFloat(price4)) {
                    alert('Giá vé loại Em bé phải lớn hơn giá vé loại Trẻ nhỏ.');
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>
