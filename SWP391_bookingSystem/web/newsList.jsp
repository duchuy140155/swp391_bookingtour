<%-- 
    Document   : newList
    Created on : May 21, 2024, 11:54:03 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>News List</title>
        <link rel="stylesheet" type="text/css" href="css/newList.css">
        <script>
            function debounce(func, delay) {
                let debounceTimer;
                return function () {
                    const context = this;
                    const args = arguments;
                    clearTimeout(debounceTimer);
                    debounceTimer = setTimeout(() => func.apply(context, args), delay);
                }
            }

            function searchNews() {
                var query = document.getElementById("searchQuery").value;
                window.location.href = 'newsList?query=' + encodeURIComponent(query);
            }

            const debouncedSearch = debounce(searchNews, 300);

            function highlightSearchTerm(term) {
                if (term) {
                    var titles = document.querySelectorAll("h2");
                    titles.forEach(function (title) {
                        var regex = new RegExp(term, "gi");
                        var newText = title.textContent.replace(regex, function (match) {
                            return '<span class="found">' + match + '</span>';
                        });
                        title.innerHTML = newText;
                    });
                }
            }

            window.onload = function () {
                var query = '<%= request.getParameter("query") %>';
                if (query) {
                    highlightSearchTerm(query);
                }
            }
        </script>
    </head>
    <body>
        <div class="wrapper">
            <div class="back-button">
                <button onclick="window.location.href = 'homepage.jsp'">Trở về trang chủ</button>
                <input type="text" id="searchQuery" class="search-box" placeholder="Search by title" oninput="debouncedSearch()" value="<%= request.getParameter("query") != null ? request.getParameter("query") : "" %>">
            </div>
            <h1>Thông tin du lịch</h1>
            <ul>
                <c:forEach var="news" items="${newsList}">
                    <li>
                        <img src="uploads/${news.imageTheme}" alt="${news.title}">
                        <div class="text-container">
                            <div class="title-date-container">
                                <a href="newsDetail?idNew=${news.idNew}">
                                    <h2>${news.title}</h2>
                                </a>
                                <p>${news.shortDescription}</p>
                            </div>
                            <p class="publish-date">Ngày đăng: ${news.publishDate}</p>
                        </div>
                    </li>
                </c:forEach>
            </ul>
            <div class="pagination">
                <c:if test="${currentPage > 1}">
                    <a href="newsList?page=${currentPage - 1}&query=${query}" class="btn">Previous</a>
                </c:if>
                <c:forEach begin="1" end="${totalPages}" var="i">
                    <a href="newsList?page=${i}&query=${query}" class="btn <c:if test='${i == currentPage}'>active</c:if>">${i}</a>
                </c:forEach>
                <c:if test="${currentPage < totalPages}">
                    <a href="newsList?page=${currentPage + 1}&query=${query}" class="btn">Next</a>
                </c:if>
            </div>
        </div>
    </body>
</html>