<%-- 
    Document   : editNews
    Created on : May 22, 2024, 8:44:03 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit News</title>
        <link rel="stylesheet" type="text/css" href="css/createNews.css">
    </head>
    <body>
        <h1>Sửa tin tức</h1>
        <div class="wrapper">
            <div class="back-button">
                <button onclick="window.location.href = 'manageNews?action=list'" class="btn">Trở về quản lý tin</button>
            </div>
            <form action="manageNews" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="update">
                <input type="hidden" name="idNew" value="${news.idNew}">
                <label for="title">Title:</label>
                <input type="text" id="title" name="title" value="${news.title}" required><br><br>
                <label for="shortDescription">Short Description:</label>
                <textarea id="shortDescription" name="shortDescription" required>${news.shortDescription}</textarea><br><br>
                <label for="longDescription">Long Description:</label>
                <textarea id="longDescription" name="longDescription" required>${news.longDescription}</textarea><br><br>
                <label for="imageTheme">Image Theme:</label>
                <input type="file" id="imageTheme" name="imageTheme"><br><br>
                <label for="newsImages">Additional Images:</label>
                <input type="file" id="newsImages" name="newsImages" multiple><br><br>
                <button type="submit">Sửa thông tin</button>
            </form>
        </div>
    </body>
</html>