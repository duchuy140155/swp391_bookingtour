<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Results</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.2/font/bootstrap-icons.min.css" rel="stylesheet">
    <style>
        body { background-color: #f8f9fa; }
        .filter-container, .search-results-container, .order-summary-container {
            background-color: white;
            border-radius: 10px;
            padding: 20px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .hotel-card {
            border: 1px solid #e9ecef;
            border-radius: 10px;
            margin-bottom: 20px;
            overflow: hidden;
            transition: transform 0.3s ease;
        }
        .hotel-card:hover {
            transform: scale(1.02);
        }
        .hotel-card img { width: 100%; border-bottom: 1px solid #e9ecef; }
        .hotel-card-body { padding: 20px; }
        .hotel-card-body h5 { margin-bottom: 10px; font-size: 1.25rem; font-weight: 600; }
        .hotel-card-body .price { color: red; font-weight: bold; font-size: 1.2rem; }
        .hotel-card-body .btn-book {
            background-color: #ff9800;
            color: white;
            font-weight: 600;
            padding: 10px 20px;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }
        .hotel-card-body .btn-book:hover {
            background-color: #e68900;
        }
        .blinking {
            animation: blinkingText 1.5s infinite;
        }
        @keyframes blinkingText {
            0% { opacity: 1; }
            50% { opacity: 0; }
            100% { opacity: 1; }
        }
        .special-offer {
            color: red;
        }
        .hidden-form {
            display: none;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="#"><i class="bi bi-geo-alt-fill"></i> Travela</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link active" aria-current="page" href="#">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Destinations</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Tours</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 d-flex justify-content-between align-items-center mb-3">
                <button class="btn btn-secondary" onclick="window.history.back()">Quay lại</button>
            </div>
            <div class="col-md-8">
                <div class="search-results-container">
                    <h5>Chỗ ở</h5>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <div>
                            <span>${destination} |</span>
                            <span>${checkin} - ${checkout} |</span>
                            <span>${guestsString} |</span>
                            <span>${roomsString}</span>
                        </div>
                        <button class="btn btn-danger" id="searchButton">Đổi nội dung tìm kiếm</button>
                    </div>
                    <div class="hotel-grid">
                        <c:if test="${empty sessionScope.hotelList}">
                            <p class="text-center text-danger">Không tìm thấy khách sạn nào phù hợp.</p>
                        </c:if>
                        <c:forEach items="${sessionScope.hotelList}" var="p">
                            <div class="hotel-card">
                                <img src="${p.image}" class="img-fluid" alt="Hotel Image" width="100px" height="100px">
                                <div class="hotel-card-body">
                                    <h5>${p.name}</h5>
                                    <p class="mb-1">
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                        <i class="bi bi-star-fill text-warning"></i>
                                    </p>
                                    <p>${p.address}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        
                                        <a href="detailroom?id=${p.hotel_id}" class="btn btn-book">Đặt Phòng</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h5>Contact Us</h5>
                    <p class="contact-item"><i class="bi bi-geo-alt-fill"></i> 123 Street, New York, USA</p>
                    <p class="contact-item"><i class="bi bi-envelope-fill"></i> info@example.com</p>
                    <p class="contact-item"><i class="bi bi-phone-fill"></i> +012 345 67890</p>
                </div>
                <div class="col-md-4 quick-links">
                    <h5>Quick Links</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Destinations</a></li>
                        <li><a href="#">Tours</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>Follow Us</h5>
                    <div class="social-icons">
                        <a href="#"><i class="bi bi-facebook"></i></a>
                        <a href="#"><i class="bi bi-twitter"></i></a>
                        <a href="#"><i class="bi bi-instagram"></i></a>
                        <a href="#"><i class="bi bi-linkedin"></i></a>
                    </div>
                    <h5 class="mt-4">Newsletter</h5>
                    <div class="newsletter">
                        <input type="email" placeholder="Your Email">
                        <button type="submit">Subscribe</button>
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <p>&copy; 2024 Travela. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script>
        document.getElementById('searchButton').addEventListener('click', function() {
            window.location.href = 'SearchHotel.jsp';
        });

        document.querySelectorAll('.bookingLink').forEach(link => {
            link.addEventListener('click', function(event) {
                event.preventDefault();
                const hotelId = this.dataset.hotelId;
                window.location.href = `http://localhost:9999/SWP391_bookingSystem/detailroom?hotel_id=${hotelId}`;
            });
        });

        document.querySelectorAll('input[type="checkbox"]').forEach(checkbox => {
            checkbox.addEventListener('change', function() {
                const formId = this.id + 'Form';
                const form = document.getElementById(formId);
                if (this.checked) {
                    form.style.display = 'block';
                } else {
                    form.style.display = 'none';
                }
            });
        });
    </script>
</body>
</html>
