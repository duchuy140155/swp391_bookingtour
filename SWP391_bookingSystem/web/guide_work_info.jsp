<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin làm việc của hướng dẫn viên</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <style>
        body {
            background-color: #e0f7fa;
            font-family: 'Candara', sans-serif;
        }
        .container {
            margin-top: 50px;
            padding: 30px;
            max-width: 90%;
        }
        .notebook {
            background: #ffffff;
            padding: 30px;
            border-radius: 20px;
            box-shadow: 0 4px 20px rgba(0, 0, 0, 0.1);
            position: relative;
            overflow: hidden;
        }
        .notebook:before, .notebook:after {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            border: 1px solid #dcdcdc;
            border-radius: 20px;
            z-index: -1;
        }
        .notebook:before {
            transform: rotate(2deg);
        }
        .notebook:after {
            transform: rotate(-2deg);
        }
        h1 {
            text-align: center;
            margin-bottom: 30px;
            font-size: 28px;
            color: #007bff;
            font-weight: 700;
        }
        .table-responsive {
            margin-top: 20px;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
        }
        .table th {
            background: #007bff;
            color: #ffffff;
            border: none;
        }
        .table tr {
            transition: background-color 0.3s, transform 0.3s;
        }
        .table tr:hover {
            background-color: #b3e5fc;
            transform: scale(1.01);
        }
        .table th i, .table td i {
            margin-right: 8px;
            color: #ffffff;
        }
        .icon-tour, .icon-start, .icon-end, .icon-role {
            color: #ffffff;
        }
        .table td {
            border: none;
        }
        .table {
            border-radius: 20px;
            overflow: hidden;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="notebook">
            <h1>Thông tin làm việc của hướng dẫn viên</h1>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><i class="fas fa-map-marker-alt icon-tour"></i> Tên tour</th>
                            <th><i class="fas fa-calendar-alt icon-start"></i> Ngày bắt đầu</th>
                            <th><i class="fas fa-calendar-check icon-end"></i> Ngày kết thúc</th>
                            <th><i class="fas fa-user-tag icon-role"></i> Chức vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="tour" items="${tours}">
                            <tr>
                                <td>${tour.tourName}</td>
                                <td><fmt:formatDate value="${tour.startDate}" pattern="dd-MM-yyyy" /></td>
                                <td><fmt:formatDate value="${tour.endDate}" pattern="dd-MM-yyyy" /></td>
                                <td>${tour.tourType}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
