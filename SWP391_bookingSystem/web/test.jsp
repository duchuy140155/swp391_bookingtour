<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String website = (String) session.getAttribute("website");
        %>
        
        <button onclick="goBack()">Quay lại trang</button>
        
        <script>
            var website = '<%= website %>';
            if (website) {
                window.onload = function() {
                    window.open(website, '_blank');
                };
            }

            function goBack() {
                window.history.back();
            }
        </script>
    </body>
</html>
