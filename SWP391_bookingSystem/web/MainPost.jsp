<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Posts Page</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .posts-container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
        }
        .post {
            display: inline-block;
            width: 30%;
            margin: 1%;
            vertical-align: top;
        }
        .post img {
            width: 100%;
            height: auto;
        }
        .post-title {
            font-weight: bold;
            font-size: 16px;
            margin: 10px 0;
        }
        .post-date {
            color: #888;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <div class="posts-container">
        <c:forEach var="post" items="${posts}">
            <div class="post">
                <img src="${post.imageUrl}" alt="${post.title}">
                <div class="post-title">${post.title}</div>
                <div class="post-date">${post.date}</div>
            </div>
        </c:forEach>
    </div>
</body>
</html>
