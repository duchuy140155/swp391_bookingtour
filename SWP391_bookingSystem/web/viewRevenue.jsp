<%-- 
    Document   : viewRevenue
    Created on : Jul 9, 2024, 11:03:33 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Revenue By Partners</title>
        <link href="css/viewRevenue.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <button type="button" name="back" onclick="history.back()" class="btn-back">Trở về</button>

        <div class="tabs">
            <div class="tab">
                <h3>Tổng số tour đã bán trong tháng</h3>
                <c:forEach var="tour" items="${toursSoldByMonth}">
                    <p>Tháng ${tour.month}: ${tour.tourCount} tours</p>
                </c:forEach>
            </div>
            <div class="tab">
                <h3>Tổng số khách hàng trong tháng</h3>
                <c:forEach var="customer" items="${totalCustomersByMonth}">
                    <p>Tháng ${customer.month}: ${customer.totalCustomers} customers</p>
                </c:forEach>
            </div>
        </div>

        <div class="charts">
            <div class="chart">
                <h3>Revenue by Partner</h3>
                <img src="${pageContext.request.contextPath}/viewRevenue?chartType=revenueByPartner" alt="Revenue Chart"/>
            </div>
            <div class="chart">
                <h3>Số tour đã bán trong năm</h3>
                <img src="${pageContext.request.contextPath}/viewRevenue?chartType=chartA" alt="Chart A"/>
            </div>
            <div class="chart">
                <h3>Tổng số khách hàng trong năm</h3>
                <img src="${pageContext.request.contextPath}/viewRevenue?chartType=chartB" alt="Chart B"/>
            </div>
            <div class="chart">
                <h3>Doanh thu của từng partner theo tháng</h3>
                <img src="${pageContext.request.contextPath}/viewRevenue?chartType=chartC" alt="Chart C"/>
            </div>
        </div>
    </body>
</html>