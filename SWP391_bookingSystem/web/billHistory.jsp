<%-- 
    Document   : billHistory
    Created on : Jun 25, 2024, 9:58:55 AM
    Author     : MSI
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Bill History</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f9f9f9;
            color: #333;
        }
        h1 {
            color: #444;
            text-align: center;
        }
        table {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
            box-shadow: 0 2px 15px rgba(0,0,0,0.1);
            background-color: white;
        }
        th, td {
            padding: 12px 15px;
            text-align: left;
            border-bottom: 1px solid #dddddd;
        }
        th {
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        tr:hover {
            background-color: #ddd;
        }
        a, span {
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            border: 1px solid #ddd;
            margin-right: 5px;
        }
        a:hover {
            background-color: #f4f4f4;
        }
        span {
            background-color: #eee;
            color: black;
            border-color: #ccc;
        }
        .homepage-button {
            display: inline-block;
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            text-decoration: none;
            font-size: 16px;
            border-radius: 5px;
            transition: background-color 0.3s;
        }
        .homepage-button:hover {
            background-color: #367c39;
        }
    </style>
</head>
<body>
    <h1>Bill History</h1>
    <table id="myTable">
        <thead>
            <tr>
                <th>Final Price</th>
                <th>Booking Date</th>
                <th>Tour Name</th>
                <th>Total People</th>
                <th>Payment Method</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="bill" items="${billHistories}">
                <tr>
                    <td>${bill.finalPrice}</td>
                    <td>${bill.bookingDate}</td>
                    <td>${bill.tour.tourName}</td> <!-- Accessing tourName through the tour object -->
                    <td>${bill.totalPeople}</td>
                    <td>${bill.paymentMethod}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div style="text-align:center; margin-top: 20px;">
        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${i == currentPage}">
                    <span>${i}</span>
                </c:when>
                <c:otherwise>
                    <a href="bill?page=${i}">${i}</a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
    <div style="text-align: center; margin-top: 20px;">
        <button onclick="window.location.href = 'homepage?action=list'" class="btn">Trở về trang chủ</button>
    </div>
</body>
</html>
