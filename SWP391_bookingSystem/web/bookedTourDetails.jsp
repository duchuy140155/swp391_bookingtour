<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết Tour</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha384-jLKHWM+eNxG3Vjt4t3ALw8+2L/Jcoaa8hHdQVw6k8PUv20nG5YxXGwe9e0ojxO2P" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha384-jLKHWM+eNxG3Vjt4t3ALw8+2L/Jcoaa8hHdQVw6k8PUv20nG5YxXGwe9e0ojxO2P" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f8f9fa;
            }

            .info-box, .support-box, .details-box {
                background-color: #fff;
                border: 1px solid #dee2e6;
                border-radius: 5px;
            }

            .info-box p, .details-box p {
                margin: 0;
                padding: 5px 0;
                font-size: 16px;
            }

            .support-box p {
                margin: 0 0 10px;
                font-size: 16px;
            }

            .btn-primary {
                background-color: #4c6ef5;
                border-color: #4c6ef5;
            }

            .btn-outline-primary {
                border-color: #4c6ef5;
                color: #4c6ef5;
            }

            .btn-outline-primary:hover {
                background-color: #4c6ef5;
                color: #fff;
            }

            .mt-5 {
                margin-top: 3rem;
            }

            .mt-3 {
                margin-top: 1rem;
            }

            .btn i {
                margin-right: 5px;
            }

            .info-box strong, .details-box strong {
                font-size: 16px;
            }

            p {
                color: #333;
            }

            .details-box .row {
                display: flex;
                align-items: center;
                flex-wrap: wrap;
            }

            .details-box .row div {
                flex: 1 0 50%;
                max-width: 50%;
            }

            .details-box .row:nth-child(2) {
                margin-top: 1.5rem;
            }

            .header-container {
                background: #50b3a2;
                color: #fff;
                padding: 10px 0;
                text-align: center;
            }

            .header-container a {
                color: #fff;
                text-decoration: none;
                margin-right: 10px;
            }

            .header-container .btn {
                background-color: #50b3a2;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 12px;
            }

            .card-header {
                cursor: pointer;
            }

            .container {
                width: 100%;
                max-width: 1400px;
                margin: auto;
                overflow: hidden;
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0,0,0,0.1);
            }

            .tour-header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                margin-bottom: 20px;
                flex-wrap: nowrap;
                width: 100%;
            }

            .tour-title {
                font-size: 1em;
                font-weight: bold;
                flex: 3;
            }

            .tour-rating, .tour-likes {
                display: flex;
                align-items: center;
                margin-right: 20px;
            }

            .tour-rating {
                background-color: #ffc107;
                border-radius: 5px;
                padding: 5px 10px;
                color: #fff;
            }

            .tour-rating .rating-score {
                font-size: 1.2em;
                font-weight: bold;
                margin-right: 5px;
            }

            .tour-likes i {
                color: red;
                margin-right: 5px;
            }

            .tour-price-actions {
                display: flex;
                align-items: center;
                justify-content: flex-end;
                flex: 2;
            }

            .tour-price {
                display: flex;
                align-items: center;
                margin-right: 20px;
            }

            .tour-price .original-price {
                text-decoration: line-through;
                margin-right: 10px;
                color: #999;
            }

            .tour-price .discounted-price {
                font-size: 1em;
                font-weight: bold;
                color: red;
                margin-right: 10px;
            }

            .discount-tag {
                background-color: red;
                color: white;
                padding: 2px 5px;
                border-radius: 3px;
                font-size: 0.9em;
                margin-right: 10px;
            }

            .tour-actions .btn {
                margin-left: 10px;
            }

            .container1 {
                display: flex;
                flex-wrap: nowrap;
            }

            .left {
                flex: 2;
                margin-right: 10px;
            }

            .right {
                flex: 1;
                display: flex;
                flex-direction: column;
            }

            .right img {
                margin-bottom: 10px;
            }

            .right img:last-child {
                margin-bottom: -4px;
            }

            img {
                width: 100%;
                height: auto;
                border-radius: 8px;
            }

            .tour-info, .images, .schedules-section, .guide-info {
                background: #fff;
                padding: 20px;
                margin: 20px 0;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }

            .tour-info h2, .images h2, .schedules h2, .guide-info h2 {
                border-bottom: 2px solid #50b3a2;
                padding-bottom: 10px;
                margin-bottom: 10px;
            }

            .guide-info {
                margin-top: 20px;
            }

            .tour-header-left {
                display: flex;
                align-items: center;
            }

            .tour-title {
                font-size: 1em;
                font-weight: bold;
                margin-right: 20px;
            }

            .tour-price .original-price {
                text-decoration: line-through;
                margin-right: 10px;
                color: #999;
            }

            .tour-price .discounted-price {
                font-size: 1.3em;
                font-weight: bold;
                color: red;
            }

            .discount-tag {
                background-color: red;
                color: white;
                padding: 2px 5px;
                border-radius: 3px;
                font-size: 0.9em;
                margin-right: 10px;
            }

            .tour-actions {
                display: flex;
                align-items: center;
            }

            .tour-actions .btn {
                margin-left: 10px;
            }

            .image-gallery {
                display: flex;
                justify-content: space-between;
                margin-top: 20px;
            }

            .image-gallery img {
                border-radius: 8px;
                width: 100%;
            }

            .main-image {
                width: 65%;
                margin-right: 2%;
            }

            .side-images {
                width: 33%;
                display: flex;
                flex-direction: column;
                justify-content: space-between;
            }

            .side-images img {
                margin-bottom: 10px;
            }

            .guide-card {
                margin-top: 20px;
                padding: 15px;
                border: 1px solid #ddd;
                border-radius: 8px;
                background-color: #fafafa;
            }
            #review-sidebar {
                max-height: 70vh; /* Chiều cao tối đa của vùng đánh giá */
                overflow-y: auto; /* Tạo thanh cuộn dọc */
            }

            .guide-role {
                font-weight: bold;
                color: #0056b3;
            }

            .guide-name {
                font-weight: bold;
                color: #333;
            }

            .guide-address {
                color: #555;
            }

            .action-buttons {
                text-align: right;
            }

            .action-buttons .btn {
                background-color: #4CAF50;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 12px;
            }

            .action-buttons .btn.contact-consultant {
                background-color: #008CBA;
            }

            .image-grid {
                display: flex;
                flex-wrap: wrap;
            }

            .image-item {
                flex: 1 1 calc(33.333% - 10px);
                margin: 5px;
            }

            .image-item img {
                width: 100%;
                height: auto;
            }

            .calendar-container {
                display: flex;
                flex-wrap: wrap;
                padding: 20px;
                background-color: #ffffff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }

            .calendar-left, .calendar-right {
                flex: 1;
                padding: 20px;
            }

            .calendar-header {
                text-align: center;
                margin-bottom: 20px;
                width: 100%;
            }

            .calendar-header h1 {
                font-size: 2.2em;
                color: #333;
                margin-bottom: 10px;
            }

            .calendar-body {
                display: flex;
                flex-direction: column;
                gap: 20px;
            }

            .calendar-day {
                display: flex;
                align-items: flex-start;
                margin-bottom: 20px;
            }

            .tour-info-box {
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
            }

            .tour-info-box p {
                margin-bottom: 10px;
                font-size: 1.1em;
            }

            .tour-info-box p strong {
                font-weight: bold;
            }

            .tour-info-box .btn-other-date {
                background: #fff;
                border: 1px solid #007bff;
                color: #007bff;
                border-radius: 8px;
                padding: 10px 20px;
                text-align: center;
            }

            .tour-info-box .btn-other-date:hover {
                background: #007bff;
                color: #fff;
            }

            .support-box {
                background: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                text-align: center;
            }

            .support-box h5 {
                margin-bottom: 20px;
            }

            .support-box .btn {
                border-radius: 8px;
                padding: 10px 20px;
                font-size: 1.1em;
                width: 100%;
                margin-bottom: 10px;
            }

            .support-box .btn-support-call {
                background: #007bff;
                color: #fff;
                border: none;
            }

            .support-box .btn-support-call:hover {
                background: #0056b3;
            }

            .support-box .btn-support-request {
                background: #fff;
                color: #007bff;
                border: 1px solid #007bff;
            }

            .support-box .btn-support-request:hover {
                background: #007bff;
                color: #fff;
            }

            .calendar-day-number {
                flex-shrink: 0;
                width: 35px;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background-color: #c0392b;
                color: #fff;
                border-radius: 50%;
                font-size: 1.0em;
                margin-right: 20px;
            }

            .calendar-day-content {
                flex-grow: 1;
                padding-left: 15px;
                border-left: 2px dashed #c0392b;
            }

            .calendar-day-title {
                font-size: 1em;
                font-weight: bold;
                color: #333;
                margin-bottom: 10px;
            }

            .calendar-day-date {
                font-size: 1em;
                color: #000;
                margin-bottom: 10px;
                font-weight: bold;
            }

            .calendar-day-note {
                font-size: 0.9em;
                color: #333;
                margin-bottom: 10px;
            }

            .calendar-footer {
                text-align: center;
                margin-top: 30px;
                width: 100%;
            }

            .calendar-footer .btn {
                background-color: #2ecc71;
                color: #fff;
                border-radius: 20px;
                padding: 15px 30px;
                font-size: 0.9em;
                transition: background-color 0.3s, transform 0.3s, box-shadow 0.3s;
                text-decoration: none;
            }

            .calendar-footer .btn:hover {
                background-color: #27ae60;
                transform: scale(1.1);
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            }

            .end-message {
                text-align: center;
                font-size: 0.9em;
                color: #e74c3c;
                font-weight: bold;
                margin-top: 20px;
                width: 100%;
            }

            .end-note {
                font-size: 0.9em;
                color: #34495e;
                margin-top: 10px;
                width: 100%;
            }

            .fa-calendar-alt {
                margin-right: 10px;
                color: #c0392b;
            }

            .btn-review:hover {
                background-color: #3b5db3;
                cursor: pointer;
            }

            .guide-address {
                font-size: 16px;
                color: #777;
            }

            .guide-role {
                font-size: 18px;
                font-weight: bold;
                color: #4c6ef5;
            }

            .guide-name {
                font-size: 22px;
                color: #333;
            }

            .btn-review {
                display: inline-block;
                padding: 10px 20px;
                margin-top: 10px;
                font-size: 16px;
                color: #fff;
                background-color: #4c6ef5;
                border: none;
                border-radius: 5px;
                text-decoration: none;
                transition: background-color 0.3s ease;
            }

            .review-card {
                border: 1px solid #e6e6e6;
                border-radius: 8px;
                padding: 16px;
                margin: 16px auto;
                max-width: 600px;
            }

            .review-header {
                display: flex;
                align-items: center;
                margin-bottom: 16px;
            }

            .review-header .user-icon {
                font-size: 40px;
                color: #6c757d;
                margin-right: 12px;
            }

            .review-header .name {
                font-weight: bold;
                font-size: 16px;
            }

            .review-header .country {
                font-size: 14px;
                color: gray;
            }

            .review-details {
                display: flex;
                align-items: center;
                margin-bottom: 8px;
            }

            .review-details .icon {
                font-size: 18px;
                color: #0071c2;
                margin-right: 8px;
            }

            .review-details .text {
                font-size: 14px;
            }

            .review-body {
                margin-bottom: 16px;
            }

            .review-body .title {
                font-size: 16px;
                color: #d79a00;
                font-weight: bold;
            }

            .review-body .description {
                font-size: 14px;
                color: #333;
            }

            .review-footer {
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            .review-footer .rating {
                font-size: 36px;
                font-weight: bold;
                color: #0071c2;
            }

            .review-footer .actions {
                display: flex;
                align-items: center;
            }

            .star-rating {
                display: flex;
                flex-direction: row-reverse;
                justify-content: center;
            }

            .star-rating input[type="radio"] {
                display: none;
            }

            .star-rating label {
                font-size: 2rem;
                color: #ddd;
                cursor: pointer;
            }
            .message {
                display: none;
                font-size: 20px;
                color: green;
                text-align: center;
                margin-top: 20px;
            }

            .star-rating input[type="radio"]:checked ~ label {
                color: #f2b600;
            }

            .star-rating input[type="radio"]:not(:checked) ~ label:hover,
            .star-rating input[type="radio"]:not(:checked) ~ label:hover ~ label {
                color: #f2b600;
            }

            .review-footer .actions button {
                background: none;
                border: none;
                color: #0071c2;
                margin-left: 8px;
                cursor: pointer;
                font-size: 14px;
            }

            .sidebar {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                right: -700px;
                background-color: #fff;
                box-shadow: -2px 0 5px rgba(0, 0, 0, 0.5);
                overflow-y: hidden;
                transition: 0.5s;
                padding-top: 60px;
                padding-left: 20px;
                padding-right: 20px;
            }

            .sidebar.open {
                width: 700px;
                right: 0;
            }

            .sidebar a {
                padding: 10px 15px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

            .sidebar a:hover {
                color: #f1f1f1;
            }

            .sidebar .closebtn {
                position: absolute;
                top: 10px;
                right: 63px;
                font-size: 36px;
                margin-left: 50px;
                cursor: pointer;
            }

            .openbtn {
                font-size: 20px;
                cursor: pointer;
                background-color: #0071c2;
                color: white;
                padding: 10px 15px;
                border: none;
            }

            .openbtn:hover {
                background-color: #444;
            }

            .btn-custom {
                font-size: 18px; /* Tăng kích thước chữ */
                padding: 10px 20px; /* Tăng kích thước nút */
                background-color: #bcd0c7; /* Màu nền tùy chỉnh */
                border: 2px solid #444; /* Thêm viền */
                color: #000; /* Màu chữ */
                border-radius: 5px; /* Bo góc */
                cursor: pointer; /* Con trỏ chuột */
                transition: background-color 0.3s ease, color 0.3s ease; /* Hiệu ứng chuyển màu */
            }

            .btn-custom:hover {
                background-color: #0dcaf0; /* Màu nền khi hover */
                color: whitesmoke; /* Màu chữ khi hover */
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="tour-header">
                <div class="tour-title"><h2 style="color: red">${tourDetails.tour.tourName}</h2></div>
            </div>

            <div class="container mt-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="info-box p-4">
                            <p>Số Booking: <strong style="color: red">${tourCode.tourCode}</strong></p>
                            <p>Khởi hành <strong>28/06/2024 - Giờ đi: 13:06</strong></p>
                            <p>Tập trung <strong>10:06 ngày 28/06/2024</strong></p>
                            <p>Thời gian <strong>5 ngày</strong></p>
                            <p>Nơi khởi hành <strong>${tourDetails.tour.startLocation}</strong></p>
                        </div>
                        <div class="support-box mt-3 p-4">
                            <p style="color: red">DANH SÁCH VÉ ĐÃ ĐĂNG KÍ</p>
                            <c:if test="${not empty tickets}">
                                <table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
                                    <thead>
                                        <tr>
                                            <th scope="col">Họ và tên</th>
                                            <th scope="col">Giới tính</th>
                                            <th scope="col">Ngày sinh</th>
                                            <th scope="col">Loại vé</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="ticket" items="${tickets}">
                                            <tr>
                                                <td style="text-align: center;">${ticket.name}</td>
                                                <td style="text-align: center;">
                                                    <c:choose>
                                                        <c:when test="${ticket.gender == true}">Nam</c:when>
                                                        <c:otherwise>Nữ</c:otherwise>
                                                    </c:choose>
                                                </td>
                                                <td style="text-align: center;"><fmt:formatDate value="${ticket.dob}" pattern="dd/MM/yyyy"/></td>
                                                <td style="text-align: center;">${ticket.tickettype.typeName}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="details-box p-4">
                            <div class="row">
                                <div class="col-lg-6 mb-3">
                                    <p><i class="far fa-clock"></i> <strong>Thời gian</strong></p>
                                    <p>5 ngày 4 đêm</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-bus"></i> <strong>Phương tiện di chuyển</strong></p>
                                    <p>Máy bay, Xe du lịch</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-map-marker-alt"></i> <strong>Điểm tham quan</strong></p>
                                    <p>Thái Lan, Bangkok, Pattaya, Wat Benchamabophit</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-utensils"></i> <strong>Ẩm thực</strong></p>
                                    <p>Buffet sáng, Theo thực đơn, Đặc sản địa phương</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-hotel"></i> <strong>Khách sạn</strong></p>
                                    <p>Khách sạn 4 sao</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="far fa-calendar-check"></i> <strong>Thời gian lý tưởng</strong></p>
                                    <p>Quanh năm</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-users"></i> <strong>Đối tượng thích hợp</strong></p>
                                    <p>Cặp đôi, Gia đình nhiều thế hệ, Thanh niên</p>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <p><i class="fas fa-tags"></i> <strong>Ưu đãi</strong></p>
                                    <p>Đã bao gồm ưu đãi trong giá tour</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="schedules-section">
                <div class="schedules">
                    <h2 style="margin-left:20px;">Lịch trình</h2>
                    <c:if test="${not empty schedules}">
                        <div class="calendar-container">
                            <div class="calendar-left">
                                <div class="calendar-body">
                                    <c:forEach var="o" items="${schedules}" varStatus="status">
                                        <div class="calendar-day">
                                            <div class="calendar-day-number">${status.index + 1}</div>
                                            <div class="calendar-day-content">
                                                <div class="calendar-day-date">
                                                    <i class="fas fa-calendar-alt"></i> <fmt:formatDate value="${o.scheduleDate}" pattern="dd/MM/yyyy" />
                                                </div>
                                                <div class="calendar-day-title">
                                                    ${o.scheduleTitle}
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="calendar-right">
                                <div class="calendar-body">
                                    <c:forEach var="o" items="${schedules}" varStatus="status">
                                        <div class="calendar-day">
                                            <div class="calendar-day-content">
                                                <div class="calendar-day-title" style="color: #333;">
                                                    ${o.scheduleTitle}
                                                </div>
                                                <div class="calendar-day-note" style="color: #333;">
                                                    <c:forEach var="d" items="${o.details}">
                                                        <p><strong style="color: #333;">${d.detailTitle}</strong><br>
                                                            <span style="white-space: pre-line;">${d.detailContent}</span></p>
                                                        <p class="note" style="color: #333;">${d.note}</p>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                        <c:if test="${status.last}">
                                            <div class="end-message">
                                                KẾT THÚC CHƯƠNG TRÌNH, TẠM BIỆT QUÝ KHÁCH!
                                            </div>
                                            <div class="end-note">
                                                <strong>*Lưu ý :</strong>
                                                <ul>
                                                    <li>Hành trình có thể thay đổi thứ tự điểm đến tùy vào điều kiện thực tế.</li>
                                                    <li>Lịch trình tham quan (tắm biển, ngắm hoa, trải nghiệm,...) rất dễ bị ảnh hưởng bởi thời tiết. Đây là trường hợp bất khả kháng mong Quý khách hiểu và thông cảm.</li>
                                                    <li>Khách Sạn có thể ở xa trung tâm thành phố vào các mùa Cao Điểm.</li>
                                                    <li>Vì những yếu tố khách quan trong giai đoạn này, điểm tham quan có thể đóng cửa và được thay bằng điểm khác phù hợp với chương trình.</li>
                                                </ul>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>

                <div class="guide-info">
                    <h2>Thông tin hướng dẫn viên</h2>
                    <form id="guideForm" method="get" action="tourDetails">
                        <input type="hidden" id="guideIdInput" name="guideId" value="">
                        <input type="hidden" id="tourIdInput" name="tourID" value="${tourID}">
                        <c:choose>
                            <c:when test="${empty listGuide}">
                                <div class="guide-card">
                                    <p>Đang sắp xếp hướng dẫn viên, vui lòng chờ...</p>
                                </div>
                            </c:when>
                            <c:otherwise>

                                <c:forEach var="guide" items="${listGuide}">
                                    <div class="guide-card">
                                        <p class="guide-role">${guide.tourType}</p>
                                        <p class="guide-name">
                                            <a href="javascript:void(0)" onclick="openNav(${guide.guideID})">
                                                ${guide.name}
                                            </a>
                                        </p>
                                        <p class="guide-address">${guide.address}</p>
                                    </div>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </form>
                </div>
            </div>
    </body>
</html>
