<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <meta charset="UTF-8">
        <title>Cập nhật lịch trình</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                background-color: #f0f2f5;
                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
                color: #333;
            }
            .card {
                margin: 50px auto;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
                max-width: 800px;
            }
            .card-header {
                background-color: #3498db;
                color: #fff;
                font-weight: bold;
                padding: 15px;
                border-radius: 10px 10px 0 0;
                text-align: center;
            }
            .form-group label {
                font-weight: bold;
                color: #555;
            }
            .collapsible {
                cursor: pointer;
                padding: 10px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 18px;
                background-color: #ecf0f1;
                margin-bottom: 10px;
                border-radius: 5px;
                transition: background-color 0.3s, color 0.3s;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .collapsible i {
                transition: transform 0.3s;
            }
            .active, .collapsible:hover {
                background-color: #dfe6e9;
            }
            .active i {
                transform: rotate(180deg);
            }
            .content {
                padding: 15px;
                display: none;
                overflow: hidden;
                background-color: #f9f9f9;
                border-radius: 5px;
                margin-bottom: 10px;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
            }
            .btn-success {
                background-color: #28a745;
                border-color: #28a745;
                transition: background-color 0.3s, border-color 0.3s;
            }
            .btn-success:hover {
                background-color: #218838;
                border-color: #1e7e34;
            }
            .alert {
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h4>Cập nhật Lịch trình</h4>
                </div>
                <h1 style="font-size:15px">
                    (*) Các trường không được bỏ trống.
                </h1>
                <div class="card-body">
                    <% if (request.getAttribute("error") != null) { %>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Lỗi!</strong> <%= request.getAttribute("error") %>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <% } %>
                    <% if (request.getAttribute("success") != null) { %>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Thành công!</strong> <%= request.getAttribute("success") %>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <% } %>
                    <form action="updateSchedule" method="post">
                        <div class="form-group">
                            <label for="scheduleTitle">Tiêu đề lịch trình</label>
                            <input name="scheduleTitle" type="text" class="form-control" placeholder="Nhập tiêu đề lịch trình" value="${schedule.scheduleTitle}" required>
                        </div>
                        <div class="form-group">
                            <label for="scheduleDate">Ngày</label>
                            <input name="scheduleDate" type="date" class="form-control" value="<fmt:formatDate value='${schedule.scheduleDate}' pattern='yyyy-MM-dd' />" required>
                        </div>
                        <input name="scheduleID" type="hidden" value="${schedule.scheduleID}">
                        <input name="tourID" type="hidden" value="${schedule.tourID}">

                        <div class="card-header">
                            <h4>Chi tiết lịch trình</h4>
                        </div>
                        <c:forEach var="detail" items="${schedule.details}">
                            <button type="button" class="collapsible"><span>${detail.detailTitle}</span><i class="fas fa-angle-down"></i></button>
                            <div class="content">
                                <div class="form-group">
                                    <label for="detailTitle-${detail.detailID}">Tiêu đề chi tiết</label>
                                    <input name="detailTitle-${detail.detailID}" type="text" class="form-control" placeholder="Nhập tiêu đề chi tiết" value="${detail.detailTitle}" >
                                </div>
                                <div class="form-group">
                                    <label for="detailContent-${detail.detailID}">Nội dung chi tiết</label>
                                    <textarea name="detailContent-${detail.detailID}" class="form-control" rows="4" placeholder="Nhập nội dung chi tiết">${detail.detailContent}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="note-${detail.detailID}">Ghi chú</label>
                                    <textarea name="note-${detail.detailID}" class="form-control" rows="2" placeholder="Nhập ghi chú">${detail.note}</textarea>
                                </div>
                                <input name="detailID" type="hidden" value="${detail.detailID}">
                            </div>
                        </c:forEach>

                        <div class="text-center">
                            <input type="submit" class="btn btn-success btn-block" value="Cập nhật">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var coll = document.getElementsByClassName("collapsible");
                for (var i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        this.classList.toggle("active");
                        var content = this.nextElementSibling;
                        if (content.style.display === "block") {
                            content.style.display = "none";
                        } else {
                            content.style.display = "block";
                        }
                    });
                }
            });
        </script>
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
    </body>
</html>
