<%-- 
    Document   : profile
    Created on : May 18, 2024, 12:50:45 AM
    Author     : Admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Profile</title>
        <link href="css/profile.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="sidebar">
                <nav>
                    <ul>
                        <li><a href="viewProfile">Thông tin cá nhân</a></li>
                        <li><a href="#">Đổi mật khẩu</a></li>
                        <li><a href="#">Thông tin thanh toán</a></li>
                        <li><a href="#">Yêu cầu xóa tài khoản</a></li>
                    </ul>
                </nav>
            </div>
            <div class="main-content">
                <h2>Thông tin cá nhân</h2>
                <div class="profile-header">
                    <div class="profile-picture">
                        <img src="${user.userPicture}" alt="User Picture">
                    </div>
                    <h1>${user.userName}</h1>
                    <p>${user.email}</p>
                </div>
                <div class="profile-content">
                    <table>
                        <tr>
                            <th>Họ và Tên</th>
                            <td>${user.userName}</td>
                            <td><a href="editProfile?field=userName">Edit</a></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>${user.email}</td>
                        </tr>
                        <tr>
                            <th>DOB</th>
                            <td>${user.userDOB}</td>
                            <td><a href="editProfile?field=userDOB">Edit</a></td>
                        </tr>
                        <tr>
                            <th>Số điện thoại</th>
                            <td>${user.phoneNumber}</td>
                            <td><a href="editProfile?field=phoneNumber">Edit</a></td>
                        </tr>
                        <tr>
                            <th>Địa chỉ</th>
                            <td>${user.address}</td>
                            <td><a href="editProfile?field=address">Edit</a></td>
                        </tr>
                        <tr>
                            <th>Ảnh</th>
                            <td><img src="${user.userPicture}" alt="User Picture" class="profile-pic"></td>
                            <td><a href="editProfile?field=userPicture">Edit</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="back-button-container">
            <button onclick="window.location.href = 'homepage'" class="back-btn">Back to Homepage</button>
        </div>
    </body>
</html>

