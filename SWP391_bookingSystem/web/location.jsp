<%-- 
    Document   : location
    Created on : May 19, 2024, 11:45:21 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Chi tiết Địa điểm</title>
    </head>
    <body>
        <h1>Chi tiết Địa điểm</h1>
        <c:forEach var="location" items="${locations}">
            <h2>${location.locationName}</h2>
            <p>${location.locationDescription}</p>
            <img src="uploads/${location.locationImage}" width="200"><br><br>
        </c:forEach>
    </body>
</html>
