<!doctype html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Snippet - BBBootstrap</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <style>
            .placeicon {
                font-family: fontawesome
            }

            .custom-control-label::before {
                background-color: #dee2e6;
                border: #dee2e6
            }
            .alert {
                margin-top: 20px;
                font-size: 18px;
            }
            .btn-center {
                display: flex;
                align-items: center;
                justify-content: center;
                text-align: center;
            }

        </style>
    </head>
    <body oncontextmenu='return false' class='snippet-body bg-info'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css">
        <div>
            <!-- Container containing all contents -->
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-9 col-lg-7 col-xl-6 mt-5">
                        <!-- White Container -->
                        <div class="container bg-white rounded mt-2 mb-2 px-0">
                            <!-- Main Heading -->
                            <div class="row justify-content-center align-items-center pt-3">
                                <h1>
                                    <strong>Đổi mật khẩu</strong>
                                </h1>
                            </div>
                            <div class="pt-3 pb-3">
                                <form class="form-horizontal" action="newPassword" method="POST" onsubmit="return validatePasswords()">
                                    <!-- Password Input -->
                                    <div class="form-group row justify-content-center px-3">
                                        <div class="col-9 px-0">
                                            <input type="password" name="password" id="password" placeholder="&#xf084; &nbsp; Mật khẩu mới"
                                                   class="form-control border-info placeicon" required pattern="^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$" 
                                                   title="Mật khẩu phải có ít nhất 8 ký tự, bao gồm chữ cái, số và ký tự đặc biệt, và không chứa khoảng trắng.">
                                        </div>
                                    </div>
                                    <!-- Confirm Password Input -->
                                    <div class="form-group row justify-content-center px-3">
                                        <div class="col-9 px-0">
                                            <input type="password" name="confPassword" id="confPassword"
                                                   placeholder="&#xf084; &nbsp; Xác nhận mật khẩu mới"
                                                   class="form-control border-info placeicon" required>
                                        </div>
                                    </div>

                                    <!-- Submit Button -->
                                    <div class="form-group row justify-content-center">
                                        <div class="col-3 px-3 mt-3">
                                            <input type="submit" value="Đổi mật khẩu" class="btn btn-info btn-block btn-center">
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <div class="col-9 px-0">
                                            <div id="error-message" class="text-danger"></div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- Alternative Login -->
                            <div class="mx-0 px-0 bg-light">
                                <!-- Horizontal Line -->
                                <div class="px-4 pt-5">
                                    <hr>
                                </div>
                                <!-- Register Now -->
                                <div class="pt-2">
                                    <div class="row justify-content-center">
                                        <h5>
                                            Nếu bạn chưa có tài khoản?<span><a href="/SWP391_bookingSystem/login"
                                                                               class="text-danger"> Đăng kí tại đây!</a></span>
                                        </h5>
                                    </div>
                                    <div class="row justify-content-center align-items-center pt-4 pb-5">
                                        <div class="col-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js'></script>
        <script>
            function validatePasswords() {
                var password = document.getElementById("password").value;
                var confPassword = document.getElementById("confPassword").value;
                var errorMessage = document.getElementById("error-message");

                // Clear previous error message
                errorMessage.textContent = "";

                // Check if passwords match
                if (password !== confPassword) {
                    errorMessage.textContent = "Mật khẩu xác nhận không khớp.";
                    return false;
                }

                // Check if password meets the criteria
                var passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
                if (!passwordPattern.test(password)) {
                    errorMessage.textContent = "Mật khẩu phải có ít nhất 8 ký tự, bao gồm chữ cái, số và ký tự đặc biệt, và không chứa khoảng trắng.";
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>
