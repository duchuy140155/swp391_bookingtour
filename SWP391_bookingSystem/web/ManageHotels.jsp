<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Quản Lý Khách Sạn</title>
        <script type="text/javascript">
            function showAlert(message) {
                alert(message);
            }

            function confirmDelete(hotelId) {
                if (confirm('Bạn có chắc chắn muốn xóa khách sạn này không?')) {
                    window.location.href = 'deletehotel?id=' + hotelId;
                }
            }

            window.onload = function() {
                var urlParams = new URLSearchParams(window.location.search);
                if (urlParams.has('deleteStatus')) {
                    var deleteStatus = urlParams.get('deleteStatus');
                    if (deleteStatus === 'success') {
                        showAlert('Xóa thành công!');
                    } else if (deleteStatus === 'fail') {
                        showAlert('Xóa không thành công. Vui lòng thử lại.');
                    }
                }
            };
        </script>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <style>
            ::-webkit-scrollbar {
                width: 8px;
            }
            ::-webkit-scrollbar-track {
                background: #f1f1f1; 
            }
            ::-webkit-scrollbar-thumb {
                background: #888; 
            }
            ::-webkit-scrollbar-thumb:hover {
                background: #555; 
            } 
            body {
                background: linear-gradient(to right, #c04848, #480048);
                min-height: 100vh
            }
            .text-gray {
                color: #aaa
            }
            img {
                height: 170px;
                width: 140px
            }
            .back-button {
                position: absolute;
                top: 20px;
                right: 20px;
            }
            .btn-group {
                display: flex;
                gap: 5px;
            }
        </style>
    </head>
    <body className='snippet-body'>
        <div class="container py-5">
            <div class="back-button">
                <button onclick="history.back()" class="btn btn-secondary">Quay Lại</button>
                <a href="partner.jsp" class="btn btn-primary">Trang Chủ</a>
            </div>
            <div class="row text-center text-white mb-5">
                <div class="col-lg-7 mx-auto">
                    <h1 class="display-4">Quản Lý Khách Sạn</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <!-- List group-->
                    <ul class="list-group shadow">
                        <!-- list group item-->
                        <c:forEach items="${sessionScope.listhotel}" var="p">
                        <li class="list-group-item">
                            <!-- Custom content-->
                            <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                                <div class="media-body order-2 order-lg-1">
                                    <a href="detailhotel?id=${p.hotel_id}">
                                        <h5 class="mt-0 font-weight-bold mb-2">${p.name}</h5>
                                    </a>
                                    <p class="font-italic text-muted mb-0 small"><b>Address : </b>${p.address}</p>
                                    <p class="font-italic text-muted mb-0 small"><b>Email : </b>${p.email}</p>
                                    <p class="font-italic text-muted mb-0 small"><b>City : </b>${p.city}</p>
                                    <p class="font-italic text-muted mb-0 small"><b>Hotline : </b>${p.phone}</p>
                                    <p class="font-italic text-muted mb-0 small"><b>Website : </b>${p.website}</p>
                                    <div class="d-flex align-items-center justify-content-between mt-1">
                                        <div class="btn-group">
                                            <a href="edithotel?id=${p.hotel_id}" class="btn btn-primary btn-sm">Edit</a>
                                            <button onclick="confirmDelete(${p.hotel_id})" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </div>
                                <img src="${p.image}" alt="Generic placeholder image" width="200" class="ml-lg-5 order-1 order-lg-2">
                            </div>
                        </li>
                        </c:forEach>
                    </ul> 
                </div>
            </div>
        </div>
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript'>#</script>
        <script type='text/javascript'>
            var myLink = document.querySelector('a[href="#"]');
            myLink.addEventListener('click', function(e) {
                e.preventDefault();
            });
        </script>
    </body>
</html>
