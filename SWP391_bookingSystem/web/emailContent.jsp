<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thông tin Đặt Tour</title>
        <link rel="stylesheet" href="css/emailContent.css" type="text/css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap">
    </head>
    <body>
        <div class="container">

            <p>Kính gửi Anh/Chị <strong>${bookingDetails.customerName}</strong>,</p>
            <p>Trước tiên <strong>Booking Tour</strong> xin chân thành cám ơn Anh/Chị đã quan tâm, tin tưởng sử dụng dịch vụ của <strong>Booking Tour</strong>.</p>
            <p><strong>Booking Tour</strong> xin xác nhận Anh/Chị đã đăng ký giữ chỗ thành công cho hành trình <strong>${bookingDetails.tour.tourName}</strong></p>


            <!--//-->
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal"><b><span style="color:#c50000;text-transform:uppercase">CHI TIẾT BOOKING: <u></u><u></u></span></b></p>
            <table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
                <tbody>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:solid #f4f5f6 1.0pt;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Số booking:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border:solid #f4f5f6 1.0pt;border-left:none;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <b><span style="font-size:10.5pt;font-family:Roboto">${tourCode}</span></b>
                                <span style="font-size:10.5pt;font-family:Roboto">&nbsp;&nbsp;</span>
                                <span style="font-size:10.5pt;font-family:Roboto">
                                    <br><i>Quý khách vui lòng nhớ số booking (Booking No) để thuận tiện cho các giao dịch sau này.</i>
                                    <u></u><u></u>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Giá cuối cùng:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <b><span style="font-size:10.5pt;font-family:Roboto;color:#c50000"><fmt:formatNumber value="${bookingDetails.finalPrice}" type="number" groupingUsed="true" /> vnđ</span></b>
                                <span style="font-size:10.5pt;font-family:Roboto"><u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Ngày đăng ký:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto"><fmt:formatDate value="${bookingDetails.bookingDate}" pattern="dd/MM/yyyy"/><u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Hình thức thanh toán:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            ${bookingDetails.paymentMethod}
                            <br>
                            <!-- Adding QR Code for payment -->
<!--                            <img src="${pageContext.request.contextPath}/img/qrCode.png" alt="QR Code for Payment" style="width:150px;height:150px;">
                            <p>Scan to Pay</p>-->
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Tình trạng:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <b><span style="font-size:10.5pt;font-family:Roboto;color:#c50000">Đã được duyệt!<u></u><u></u></span></b>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal"><b><span style="color:red">Thông tin chi tiết tour<u></u><u></u></span></b></p>
            <table border="0" cellspacing="0" cellpadding="0" width="680" style="width:510.0pt;background:white">
                <tbody>
                    <tr>
                        <td colspan="6" style="padding:0in 0in 0in 0in">
                            <table border="0" cellspacing="0" cellpadding="0" width="678" style="width:508.15pt;border-collapse:collapse">
                                <tbody>
                                    <tr>
                                        <td style="border:solid #dddddd 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">Tên tour:</span></p>
                                        </td>
                                        <td style="border:solid #dddddd 1.0pt;border-left:none;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">${bookingDetails.tour.tourName}</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border:solid #dddddd 1.0pt;border-top:none;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">Điểm xuất phát:</span></p>
                                        </td>
                                        <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">${bookingDetails.tour.startLocation}</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border:solid #dddddd 1.0pt;border-top:none;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">Điểm đến:</span></p>
                                        </td>
                                        <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">${bookingDetails.tour.endLocation}</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border:solid #dddddd 1.0pt;border-top:none;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">Ngày xuất phát:</span></p>
                                        </td>
                                        <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt"><fmt:formatDate value="${bookingDetails.tour.startDate}" pattern="dd/MM/yyyy"/></span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border:solid #dddddd 1.0pt;border-top:none;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt">Ngày về:</span></p>
                                        </td>
                                        <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                                            <p class="MsoNormal"><span style="font-size:13.5pt"><fmt:formatDate value="${bookingDetails.tour.endDate}" pattern="dd/MM/yyyy"/></span></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="background:#e0dfdf;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                            <p class="MsoNormal"><b><span style="font-size:13.5pt;font-family:Roboto;color:#2d4271">Thông tin tập trung</span></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:3.75pt 3.75pt 3.75pt 3.75pt">
                            <p class="MsoNormal"><span style="font-size:13.5pt;font-family:Roboto;color:#2d4271">Thời gian tập trung: </span></p>
                        </td>
                        <td colspan="4" style="padding:3.75pt 3.75pt 3.75pt 3.75pt">
                            <p class="MsoNormal"><span style="font-size:13.5pt;font-family:Roboto;color:#2d4271">6:00am Ngày <fmt:formatDate value="${bookingDetails.tour.startDate}" pattern="dd/MM/yyyy"/></span></p>
                        </td>
                    </tr> 
                </tbody>
            </table>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <div style="margin-bottom:7.5pt"><p class="MsoNormal"><b><span style="font-family:Roboto;color:#c50000;text-transform:uppercase">Thông tin khách hàng:<u></u><u></u></span></b></p></div> 
            <table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
                <tbody>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:solid #f4f5f6 1.0pt;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Họ và tên:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border:solid #f4f5f6 1.0pt;border-left:none;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto">${bookingDetails.customerName}<u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Địa chỉ:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto">${bookingDetails.customerAddress}<u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Điện thoại:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto">0${bookingDetails.phoneNumber}<u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Email:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto"><a href="mailto:nguyenhai2k1@gmail.com" target="_blank"><i><span style="color:#306eb7">${bookingDetails.email}</span></i></a><u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Ghi chú:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <span style="font-size:10.5pt;font-family:Roboto">${bookingDetails.note}<u></u><u></u></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Lưu ý:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <b><span style="font-size:10.5pt;font-family:Roboto">Số ghế trên xe được hệ thống tự động sắp xếp dựa trên thứ tự Quý khách đăng ký và thanh toán tour.<u></u><u></u></span></b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="width:112.5pt;border-top:none;border-left:solid #f4f5f6 1.0pt;border-bottom:solid white 1.0pt;border-right:none;background:#f4f5f6;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" align="right" style="text-align:right">
                                <span style="font-size:10.5pt;font-family:Roboto">Tổng số khách:<u></u><u></u></span>
                            </p>
                        </td>
                        <td style="border-top:none;border-left:none;border-bottom:solid #f4f5f6 1.0pt;border-right:solid #f4f5f6 1.0pt;padding:3.75pt 7.5pt 3.75pt 7.5pt">
                            <p class="MsoNormal" style="line-height:15.0pt">
                                <b><span style="font-size:10.5pt;font-family:Roboto;color:#c50000">${bookingDetails.totalPeople} </span></b>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <div style="margin-bottom:7.5pt"><p class="MsoNormal"><b><span style="font-family:Roboto;color:#c50000;text-transform:uppercase">Danh sách vé đã đăng kí:<u></u><u></u></span></b></p></div> 

            <c:if test="${not empty tickets}">
                <table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
                    <thead>
                        <tr>
                            <th scope="col">Họ và tên</th>
                            <th scope="col">Giới tính</th>
                            <th scope="col">Ngày sinh</th>
                            <th scope="col">Loại vé</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="ticket" items="${tickets}">
                            <tr>
                                <td style="text-align: center;">${ticket.name}</td>
                                <td style="text-align: center;">
                                    <c:choose>
                                        <c:when test="${ticket.gender == true}">Nam</c:when>
                                        <c:otherwise>Nữ</c:otherwise>
                                    </c:choose>
                                </td>
                                <td style="text-align: center;"><fmt:formatDate value="${ticket.dob}" pattern="dd/MM/yyyy"/></td>
                                <td style="text-align: center;">${ticket.tickettype.typeName}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal" style="margin-bottom:6.0pt"><span style="color:black"><u></u>&nbsp;<u></u></span></p>
            <p class="MsoNormal">
                <b><span style="color:#212121;border:none windowtext 1.0pt;padding:0in;background:yellow">***Lưu ý :</span></b>
                <span style="color:#212121;background:yellow"><br></span>
                <span style="color:#212121">
                    - &nbsp; VÉ KHÔNG HOÀN, KHÔNG ĐỔI. SAU KHI XÁC NHẬN THANH TOÁN HỦY MẤT 100%.
                    - &nbsp; Hành trình có thể thay đổi thứ tự điểm đến tùy vào điều kiện thực tế.&nbsp;<br>
                    - &nbsp; Lịch trình tham quan (tắm biển, ngắm hoa, trải nghiệm,...) rất dễ bị ảnh hưởng bởi thời tiết. Đây là trường hợp bất khả kháng mong Quý khách hiểu và thông cảm.<br>
                    - &nbsp; Khách Sạn có thể ở xa trung tâm thành phố vào các mùa Cao Điểm.
                </span>
                <span style="color:black"><u></u><u></u></span>
            </p>

            <div class="footer">
                <p>Nếu cần thêm thông tin, Anh/chị vui lòng liên hệ lại với chúng tôi để được hỗ trợ.</p>
                <p>Hoặc nhắn tin vào Fanpage Travela: <a href="#">Travela</a></p>
                <p>Trân trọng cảm ơn!</p>
                <p>Đội ngũ hỗ trợ của Booking Tour</p>
                <hr style="border: none; border-top: 1px solid #ddd;" />
                <p style="font-size: 12px; color: #999;">
                    Booking Tour - xxxxxx<br />
                    Email: support@bookingtour.com<br />
                    Hotline: 1900 123 456
                </p>
            </div>
        </div>
    </body>
</html>
