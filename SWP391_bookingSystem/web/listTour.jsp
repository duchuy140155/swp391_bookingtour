<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List of Tours</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                max-width: 90%;
                margin: 50px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h1 {
                text-align: center;
                color: #333;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            table, th, td {
                border: 1px solid #ddd;
            }
            th, td {
                padding: 8px;
                text-align: left;
            }
            th {
                background-color: #f2f2f2;
            }
            img.thumbnail {
                display: block;
                margin: 0 auto;
                max-width: 100px;
                max-height: 100px;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin: 2px;
                font-size: 14px;
                font-weight: normal;
                line-height: 1.428571429;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                border: 1px solid transparent;
                border-radius: 4px;
                text-decoration: none;
            }
            .btn-edit {
                color: #fff;
                background-color: #5bc0de;
                border-color: #46b8da;
            }
            .btn-delete {
                color: #fff;
                background-color: #d9534f;
                border-color: #d43f3a;
            }
            .btn-edit:hover {
                background-color: #31b0d5;
                border-color: #269abc;
            }
            .btn-delete:hover {
                background-color: #c9302c;
                border-color: #ac2925;
            }
            .pagination {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }
            .pagination a {
                color: #333;
                padding: 8px 16px;
                text-decoration: none;
                border: 1px solid #ddd;
                margin: 0 4px;
                border-radius: 4px;
            }
            .pagination a.active {
                background-color: #007bff;
                color: white;
                border: 1px solid #007bff;
            }
            .pagination a:hover:not(.active) {
                background-color: #ddd;
            }
            .search-form {
                text-align: center;
                margin-bottom: 20px;
            }
            .search-input {
                padding: 8px;
                font-size: 16px;
                width: 300px;
            }
            .search-button {
                padding: 8px 16px;
                font-size: 16px;
                cursor: pointer;
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }
            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }
            .dropdown-content a:hover {
                background-color: #f1f1f1;
            }
            .dropdown:hover .dropdown-content {
                display: block;
            }
            .nested-dropdown {
                position: relative;
            }
            .nested-dropdown .nested-dropdown-content {
                display: none;
                position: absolute;
                left: 100%;
                top: 0;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }
            .nested-dropdown:hover .nested-dropdown-content {
                display: block;
            }
            .btn-ticket {
                background-color: #008CBA; /* Blue */
                color: yellow;
            }
            .btn-view {
                background-color: #31b0d5; /* Blue */
                color: yellow;
            }

        </style>
        <script>
            function confirmDelete(scheduleID, scheduleDate, tourID) {
                if (confirm('Bạn có chắc chắn muốn xóa lịch trình vào ngày ' + scheduleDate + ' không?')) {
                    window.location.href = 'deleteSchedule?scheduleID=' + scheduleID + '&tourID=' + tourID;
                }
            }

            function confirmDeleteTour(tourID) {
                if (confirm('Bạn có chắc chắn muốn xóa tour này không?')) {
                    window.location.href = 'deleteTour?tourID=' + tourID;
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="back-button">
                <button onclick="window.location.href = 'partner.jsp'" class="btn">Trở về trang quản lý</button>
            </div>
            <h1>Danh sách Tours</h1>
            <div class="search-form">
                <form action="${pageContext.request.contextPath}/listTour" method="get">
                    <input type="text" name="search" class="search-input" placeholder="Tìm kiếm tours..." value="${fn:escapeXml(search)}">
                    <button type="submit" class="search-button">Tìm kiếm</button>
                    <button onclick="window.location.href = '/unresolvedBookings'">Xem Đơn Hàng</button>

                </form>
            </div>
            <table>
                <tr>
                    <th>Tour ID</th>
                    <th>Tên tour</th>
                    <th>Mô tả</th>
                    <th>Điểm đi</th>
                    <th>Điểm đến</th>
                    <th>Ngày đi</th>
                    <th>Ngày về</th>
                    <th>Giá</th>
                    <th>Số người</th>
                    <th>Ảnh nền</th>
                    <th>Hành động</th>
                </tr>
                <c:forEach var="tour" items="${tours}">
                    <tr>
                        <td>${tour.tourID}</td>
                        <td><a href="${pageContext.request.contextPath}/tourDetails?tourID=${tour.tourID}">${tour.tourName}</a></td>
                        <td>
                            <c:choose>
                                <c:when test="${fn:length(tour.tourDescription) > 50}">
                                    ${fn:substring(tour.tourDescription, 0, 50)}...
                                </c:when>
                                <c:otherwise>
                                    ${tour.tourDescription}
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>${tour.startLocation}</td>
                        <td>${tour.endLocation}</td>
                        <td><fmt:formatDate value="${tour.startDate}" pattern="yyyy-MM-dd" /></td>
                        <td><fmt:formatDate value="${tour.endDate}" pattern="yyyy-MM-dd" /></td>
                        <td><fmt:formatNumber value="${tour.price}" type="number" /></td>
                        <td>${tour.numberOfPeople}</td>
                        <td><img src="${pageContext.request.contextPath}/${tour.thumbnails}" alt="Thumbnail" class="thumbnail"></td>
                        <td>
                            <a href="${pageContext.request.contextPath}/editTour?tourID=${tour.tourID}" class="btn btn-edit">Chỉnh sửa</a>

                            <div class="dropdown">
                                <button class="btn btn-edit dropbtn">Lịch trình</button>
                                <div class="dropdown-content">
                                    <a href="${pageContext.request.contextPath}/createSchedule.jsp?tourID=${tour.tourID}">Thêm lịch trình</a>
                                    <c:forEach var="schedule" items="${tourSchedulesMap[tour.tourID]}">
                                        <div class="nested-dropdown">
                                            <a href="javascript:void(0);">Lịch trình (${schedule.scheduleDate})</a>
                                            <div class="nested-dropdown-content">
                                                <a href="updateSchedule?scheduleID=${schedule.scheduleID}">Cập nhật</a>
                                                <a href="javascript:void(0);" onclick="confirmDelete(${schedule.scheduleID}, '${schedule.scheduleDate}', ${tour.tourID})">Xóa</a>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <a href="${pageContext.request.contextPath}/viewBooking?tourID=${tour.tourID}" class="btn btn-view">Xem đặt tour</a>
                            <a href="${pageContext.request.contextPath}/ticketController?tourID=${tour.tourID}" class="btn btn-ticket">Tạo vé</a>
                            <a href="javascript:void(0);" onclick="confirmDeleteTour(${tour.tourID})" class="btn btn-delete">Xóa</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <div class="pagination">
                <c:forEach var="i" begin="1" end="${totalPages}">
                    <a href="${pageContext.request.contextPath}/listTour?page=${i}&search=${fn:escapeXml(search)}" class="${i == currentPage ? 'active' : ''}">${i}</a>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
