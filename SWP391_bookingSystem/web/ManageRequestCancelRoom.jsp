<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cancel Room Requests</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <!-- Custom CSS -->
    <style>
        body {
            background-color: #f7f9fc;
            color: #333;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }

        .card {
            border-radius: 10px;
        }

        .table-hover tbody tr:hover {
            background-color: #e0f7fa;
            cursor: pointer;
            transform: scale(1.01);
            transition: all 0.2s ease-in-out;
        }

        .btn {
            transition: background-color 0.3s, transform 0.3s;
        }

        .btn:hover {
            background-color: #dc3545;
            transform: scale(1.05);
        }

        .modal-content {
            transition: transform 0.3s ease-out;
        }

        .modal-content.show {
            transform: scale(1.1);
        }

        h1, h2 {
            color: #007bff;
        }

        .card-title {
            color: #6c757d;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ff0000;
            transition: .4s;
            border-radius: 34px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked + .slider {
            background-color: #4caf50;
        }

        input:checked + .slider:before {
            transform: translateX(26px);
        }
    </style>
    <!-- jQuery and Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script>
        $(document).ready(function() {
            // Handle cancel button click
            $(document).on('click', '.cancelBtn', function() {
                var requestId = $(this).data('id');
                $('#cancelModal').data('id', requestId).modal('show');
            });

            // Handle confirm cancellation
            $('#confirmCancel').click(function() {
                var requestId = $('#cancelModal').data('id');
                // Perform AJAX request to cancel the request
                // Example:
                // $.post('/cancelRequest', { id: requestId }, function(response) {
                //     // Handle response
                // });

                $('#cancelModal').modal('hide');
                // Remove the row from the table
                $('button[data-id="' + requestId + '"]').closest('tr').remove();
            });

            // Add animation to the modal
            $('#cancelModal').on('show.bs.modal', function() {
                $(this).find('.modal-content').addClass('show');
            }).on('hidden.bs.modal', function() {
                $(this).find('.modal-content').removeClass('show');
            });

            // Handle switch toggle
            $(document).on('change', '.status-switch', function() {
                var isChecked = $(this).is(':checked');
                if (isChecked) {
                    // Handle acceptance logic
                    // Example: $.post('/acceptRequest', { id: requestId }, function(response) { ... });
                    alert('Request accepted');
                } else {
                    // Handle rejection logic
                    // Example: $.post('/rejectRequest', { id: requestId }, function(response) { ... });
                    alert('Request rejected');
                }
            });
        });
    </script>
</head>
<body>
<div class="container mt-5">
    <h1 class="text-center">Cancel Room Requests</h1>
    <h2 class="text-center">Partner Management</h2>
    <p class="text-center">This page allows partners to manage their room cancellation requests.</p>
    
    <div class="card mt-4 shadow-lg">
        <div class="card-body">
            <h3 class="card-title">Current Requests</h3>
            <p class="card-text">Below is a list of all the current room cancellation requests submitted by partners.</p>
            <table class="table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Request ID</th>
                    <th>Partner Name</th>
                    <th>Room</th>
                    <th>Reason</th>
                    <th>Date Requested</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="requestTable">
                <!-- Hardcoded rows for testing -->
                <tr>
                    <td>1</td>
                    <td>Partner A</td>
                    <td>Room 101</td>
                    <td>Personal</td>
                    <td>2024-07-01</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" class="status-switch">
                            <span class="slider"></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Partner B</td>
                    <td>Room 102</td>
                    <td>Maintenance</td>
                    <td>2024-07-02</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" class="status-switch">
                            <span class="slider"></span>
                        </label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal for Cancel Confirmation -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cancelModalLabel">Confirm Cancellation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to cancel this request?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="confirmCancel">Confirm</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
