<%-- 
    Document   : customerLocation
    Created on : May 21, 2024, 7:09:04 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Danh sách Địa điểm</title>
        <link href="css/viewLocation.css" rel="stylesheet" type="text/css"/>
        <script>
            function searchTour(locationName) {
                window.location.href = "listTour?startLocation=" + locationName;
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <a href="homepage" class="btn back-btn">Back to Homepage</a>


                <h1>Danh sách Địa điểm</h1>
                <form method="get" action="viewLocation" class="search-form">
                    <input type="text" name="search" placeholder="Tìm kiếm..." value="${search}">
                    <button type="submit">Search</button>
                </form>
                <a href="viewLocation?sortField=locationName&sortOrder=${sortOrder == 'ASC' ? 'DESC' : 'ASC'}" class="btn sort-btn">
                    Sắp xếp theo tên ${sortOrder == 'ASC' ? '▲' : '▼'}
                </a>
            </div>
            <div class="location-container">
                <c:forEach var="location" items="${locations}">
                    <div class="location-item" onclick="searchTour('${location.locationName}')">
                        <img src="${pageContext.request.contextPath}/uploads/${location.locationImage}" alt="Location Image" class="location-image">
                        <div class="location-info">
                            <h2 class="location-name">${location.locationName}</h2>
                            <p class="location-description">${location.locationDescription}</p>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <c:if test="${totalPages > 1}">
                <div class="pagination">
                    <a href="viewLocation?page=1" class="btn page-btn">Trang đầu</a>
                    <c:forEach var="i" begin="1" end="${totalPages}">
                        <a href="viewLocation?page=${i}" class="btn page-btn ${i == currentPage ? 'active' : ''}">${i}</a>
                    </c:forEach>
                    <a href="viewLocation?page=${totalPages}" class="btn page-btn">Trang cuối</a>
                </div>
            </c:if>
        </div>
    </body>
</html>






