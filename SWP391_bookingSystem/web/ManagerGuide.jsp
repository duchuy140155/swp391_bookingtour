<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quản lý hướng dẫn viên</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #e0f7fa;
            font-family: 'Candara', sans-serif;
        }
        .side-nav {
            background: linear-gradient(to bottom, #0066cc, #0099ff);
            color: white;
            padding: 20px;
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            position: fixed;
            width: 270px;
            border-radius: 0 15px 15px 0;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .user-info {
            text-align: center;
            margin-bottom: 20px;
        }
        .user-image {
            border-radius: 50%;
            width: 60px;
            height: 60px;
            object-fit: cover;
            margin-bottom: 10px;
            border: 2px solid #ffffff;
        }
        .user-info p {
            font-size: 18px;
            font-weight: bold;
            margin: 0;
        }
        .side-nav a {
            color: white;
            text-decoration: none;
            padding: 10px 20px;
            transition: background-color 0.3s;
            position: relative;
            display: block;
            margin-bottom: 10px;
            border-radius: 25px;
            text-align: center;
        }
        .side-nav a:hover {
            background-color: #0059b3;
        }
        .side-nav .active {
            background-color: #004080;
            font-weight: bold;
        }
        .notification {
            background-color: red;
            color: white;
            border-radius: 50%;
            padding: 2px 8px;
            font-size: 12px;
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .main-content {
            margin-left: 290px;
            padding: 20px;
        }
        .container {
            margin-top: 30px;
        }
        h1 {
            text-align: center;
            margin-bottom: 30px;
            font-size: 28px;
            color: #004080;
            font-weight: 700;
        }
        .table-responsive {
            background-color: #ffffff;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            margin-top: 20px;
        }
        .table {
            border-radius: 15px;
            overflow: hidden;
        }
        .table thead {
            background-color: #004080;
            color: white;
        }
        .table th, .table td {
            vertical-align: middle;
            text-align: center;
            border: none;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 150px;
        }
        .table tbody tr.no-guide {
            background-color: #f0f0f0; /* Màu xám nhẹ */
        }
        .btn {
            border: none;
            border-radius: 25px;
            font-size: 14px;
            padding: 10px 20px;
            transition: all 0.3s ease;
        }
        .btn-primary {
            background-color: #004080;
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-primary:hover {
            background-color: #003366;
            border-color: #003366;
        }
        .btn-danger {
            background-color:#004080;
            border-color: #004080;
            color: #ffffff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-danger:hover {
            background-color: #004080;
            border-color: #004080;
        }
        .btn-warning {
            background-color: #004080;
            border-color: #004080;
            color: white;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .btn-warning:hover {
            background-color: #e0a800;
            border-color: #e0a800;
        }
        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .pagination a {
            color: #004080;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #dee2e6;
            margin: 0 4px;
            border-radius: 25px;
            transition: background-color 0.3s;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .pagination a.active {
            background-color: #004080;
            color: white;
            border: 1px solid #004080;
        }
        .pagination a:hover:not(.active) {
            background-color: #dee2e6;
        }
    </style>
</head>
<body>
    <div class="side-nav">
        <div>
            <div class="user-info">
                <img src="${sessionScope.partner.certificate}" alt="User Image" class="user-image">
                <p>Xin chào ${sessionScope.partner.partnerName}!</p>
            </div>
            <c:if test="${newContactCount > 0}">
                <span class="notification">${newContactCount}</span>
            </c:if>
            <a href="#"><i class="fas fa-envelope"></i> Tin nhắn</a>
        </div>
        <div>
            <a href="logout" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
        </div>
    </div>
    <div class="main-content">
        <div class="container">
            <h1>Quản lý hướng dẫn viên</h1>
            
            <a href="PartnerInfo" class="btn btn-danger">
                <i class="fas fa-sign-out-alt"></i> Quay lại
            </a>
            <h1 style="font-size:15px;">(*) Các tour có màu xám là những tour chưa được sắp xếp hướng dẫn viên.</h1>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th style="background:#004080;">Tên tour</th>
                            <th style="background:#004080;">Ngày đi</th>
                            <th style="background:#004080;">Ngày về</th>
                            <th style="background:#004080;">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="tour" items="${tours}">
                            <tr class="${not tourGuideStatus[tour.tourID] ? 'no-guide' : ''}">
                                <td>${tour.tourName}</td>
                                <td><fmt:formatDate value="${tour.startDate}" pattern="dd/MM/yyyy" /></td>
                                <td><fmt:formatDate value="${tour.endDate}" pattern="dd/MM/yyyy" /></td>
                                <td>
                                    <button class="btn btn-warning btn-sm" onclick="openAddGuideModal(${tour.tourID})">Thêm</button>
                                    <button class="btn btn-danger btn-sm" onclick="openRemoveGuideModal(${tour.tourID})">Xóa</button>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal for add guide to tour -->
    <div class="modal fade" id="addGuideModal" tabindex="-1" role="dialog" aria-labelledby="addGuideModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addGuideModalLabel">Thêm hướng dẫn viên vào tour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addGuideForm" action="addGuideToTour" method="post">
                        <input type="hidden" name="tourID" id="tourIDForAdd">
                        <div class="form-group">
                            <label for="guideID">Hướng dẫn viên</label>
                            <select class="form-control" id="guideID" name="guideID">
                                <c:forEach var="guide" items="${inactiveGuides}">
                                    <option value="${guide.guideID}">${guide.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tourType">Loại hướng dẫn viên</label>
                            <select class="form-control" id="tourType" name="tourType">
                                <option value="HDV dẫn đoàn">HDV dẫn đoàn</option>
                                <option value="Hướng dẫn viên tiễn">Hướng dẫn viên tiễn</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Thêm</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for remove guide from tour -->
    <div class="modal fade" id="removeGuideModal" tabindex="-1" role="dialog" aria-labelledby="removeGuideModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="removeGuideModalLabel">Xóa hướng dẫn viên khỏi tour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="removeGuideForm" action="removeGuideFromTourServlet" method="post">
                        <input type="hidden" name="tourID" id="tourIDForRemove">
                        <input type="hidden" name="action" value="removeGuideFromTour">
                        <div class="form-group">
                            <label for="guideIDForRemove">Hướng dẫn viên</label>
                            <select class="form-control" id="guideIDForRemove" name="guideID">
                                <!-- Options will be populated dynamically by JavaScript -->
                            </select>
                        </div>
                        <button type="submit" class="btn btn-danger">Xóa</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        function goBack() {
            window.history.back();
        }

        function openAddGuideModal(tourID) {
            $('#tourIDForAdd').val(tourID);
            $('#addGuideModal').modal('show');
        }

        function openRemoveGuideModal(tourID) {
            $('#tourIDForRemove').val(tourID);

            $.ajax({
                url: 'tourList',
                method: 'GET',
                data: { tourID: tourID },
                success: function(response) {
                    $('#guideIDForRemove').empty();
                    response.forEach(function(guide) {
                        $('#guideIDForRemove').append(new Option(guide.name, guide.guideID));
                    });
                    $('#removeGuideModal').modal('show');
                },
                error: function(xhr, status, error) {
                    console.error("AJAX error: ", status, error);
                    alert('Không thể lấy danh sách hướng dẫn viên.');
                }
            });
        }
    </script>
</body>
</html>
