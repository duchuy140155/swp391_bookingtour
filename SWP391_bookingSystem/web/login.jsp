<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <style>
        /* Existing CSS */
        #toast-container > .toast {
            opacity: 1 !important;
        }
        #toast-container > .toast-success {
            background-color: #28a745 !important;
            color: #fff !important;
        }
        #toast-container > .toast-error {
            background-color: #dc3545 !important;
            color: #fff !important;
        }
        #toast-container > .toast-warning {
            background-color: #ffc107 !important;
            color: #212529 !important;
        }
        #toast-container > .toast-info {
            background-color: #17a2b8 !important;
            color: #fff !important;
        }

        /* Loader CSS */
        
        /* Improved Layout and Transition Effects */
        html, body * {
            box-sizing: border-box;
            font-family: 'Times', sans-serif;
        }

        body {
            background: linear-gradient(rgba(246,247,249,0.8), rgba(246,247,249,0.8)), url(https://dl.dropboxusercontent.com/u/22006283/preview/codepen/sky-clouds-cloudy-mountain.jpg) no-repeat center center fixed;
            background-size: cover;
        }

        .container {
            width: 100%;
            padding-top: 60px;
            padding-bottom: 100px;
        }

        .frame {
            height: 575px;
            width: 430px;
            background: linear-gradient(rgba(35,43,85,0.75), rgba(35,43,85,0.95)), url(https://dl.dropboxusercontent.com/u/22006283/preview/codepen/clouds-cloudy-forest-mountain.jpg) no-repeat center center;
            background-size: cover;
            margin-left: auto;
            margin-right: auto;
            border-top: solid 1px rgba(255,255,255,.5);
            border-radius: 5px;
            box-shadow: 0px 2px 7px rgba(0,0,0,0.2);
            overflow: hidden;
            transition: all .5s ease;
        }

        .frame-long {
            height: 800px;
        }

        .frame-short {
            height: 400px;
            margin-top: 50px;
            box-shadow: 0px 2px 7px rgba(0,0,0,0.1);
        }

        .nav {
            width: 100%;
            height: 100px;
            padding-top: 40px;
            opacity: 1;
            transition: all .5s ease;
        }

        .nav-up {
            transform: translateY(-100px);
            opacity: 0;
        }

        li {
            padding-left: 10px;
            font-size: 18px;
            display: inline;
            text-align: left;
            text-transform: uppercase;
            padding-right: 10px;
            color: #ffffff;
        }

        .signin-active a {
            padding-bottom: 10px;
            color: #ffffff;
            text-decoration: none;
            border-bottom: solid 2px #1059FF;
            transition: all .25s ease;
            cursor: pointer;
        }

        .signin-inactive a {
            padding-bottom: 0;
            color: rgba(255,255,255,.3);
            text-decoration: none;
            border-bottom: none;
            cursor: pointer;
        }

        .signup-active a {
            cursor: pointer;
            color: #ffffff;
            text-decoration: none;
            border-bottom: solid 2px #1059FF;
            padding-bottom: 10px;
        }

        .signup-inactive a {
            cursor: pointer;
            color: rgba(255,255,255,.3);
            text-decoration: none;
            transition: all .25s ease;
        }

        .form-signin {
            width: 430px;
            height: 375px;
            font-size: 16px;
            font-weight: 300;
            padding-left: 37px;
            padding-right: 37px;
            padding-top: 55px;
            transition: opacity .5s ease, transform .5s ease;
        }

        .form-signin-left {
            transform: translateX(-400px);
            opacity: .0;
        }

        .form-signup {
            width: 430px;
            height: 375px;
            font-size: 16px;
            font-weight: 300;
            padding-left: 37px;
            padding-right: 37px;
            padding-top: 55px;
            position: relative;
            top: -375px;
            left: 400px;
            opacity: 0;
            transition: all .5s ease;
        }

        .form-signup-left {
            transform: translateX(-399px);
            opacity: 1;
        }

        .form-signup-down {
            top: 0px;
            opacity: 0;
        }

        .success {
            width: 80%;
            height: 150px;
            text-align: center;
            position: relative;
            top: -890px;
            left: 450px;
            opacity: .0;
            transition: all .8s .4s ease;
        }

        .success-left {
            transform: translateX(-406px);
            opacity: 1;
        }

        .successtext {
            color: #ffffff;
            font-size: 16px;
            font-weight: 300;
            margin-top: -35px;
            padding-left: 37px;
            padding-right: 37px;
        }

        #check path {
            stroke: #ffffff;
            stroke-linecap:round;
            stroke-linejoin:round;
            stroke-width: .85px;
            stroke-dasharray: 60px 300px;
            stroke-dashoffset: -166px;
            fill: rgba(255,255,255,.0);
            transition: stroke-dashoffset 2s ease .5s, fill 1.5s ease 1.0s;
        }

        #check.checked path {
            stroke-dashoffset: 33px;
            fill: rgba(255,255,255,.03);
        }

        .form-signin input, .form-signup input {
            color: #ffffff;
            font-size: 13px;
        }

        .form-styling {
            width: 100%;
            height: 35px;
            padding-left: 15px;
            border: none;
            border-radius: 20px;
            margin-bottom: 20px;
            background: rgba(255,255,255,.2);
        }

        label {
            font-weight: 400;
            text-transform: uppercase;
            font-size: 13px;
            padding-left: 15px;
            padding-bottom: 10px;
            color: rgba(255,255,255,.7);
            display: block;
        }

        :focus {
            outline: none;
        }

        .form-signin input:focus, textarea:focus, .form-signup input:focus, textarea:focus {
            background: rgba(255,255,255,.3);
            border: none;
            padding-right: 40px;
            transition: background .5s ease;
        }

        [type="checkbox"]:not(:checked),
        [type="checkbox"]:checked {
            position: absolute;
            display: none;
        }

        [type="checkbox"]:not(:checked) + label,
        [type="checkbox"]:checked + label {
            position: relative;
            padding-left: 85px;
            padding-top: 2px;
            cursor: pointer;
            margin-top: 8px;
        }

        [type="checkbox"]:not(:checked) + label:before,
        [type="checkbox"]:checked + label:before,
        [type="checkbox"]:not(:checked) + label:after,
        [type="checkbox"]:checked + label:after {
            content: '';
            position: absolute;
        }

        [type="checkbox"]:not(:checked) + label:before,
        [type="checkbox"]:checked + label:before {
            width: 65px;
            height: 30px;
            background: rgba(255,255,255,.2);
            border-radius: 15px;
            left: 0;
            top: -3px;
            transition: all .2s ease;
        }

        [type="checkbox"]:not(:checked) + label:after,
        [type="checkbox"]:checked + label:after {
            width: 10px;
            height: 10px;
            background: rgba(255,255,255,.7);
            border-radius: 50%;
            top: 7px;
            left: 10px;
            transition: all .2s ease;
        }

        /* on checked */
        [type="checkbox"]:checked + label:before {
            background: #0F4FE6;
        }

        [type="checkbox"]:checked + label:after {
            background: #ffffff;
            top: 7px;
            left: 45px;
        }

        [type="checkbox"]:checked + label .ui,
        [type="checkbox"]:not(:checked) + label .ui:before,
        [type="checkbox"]:checked + label .ui:after {
            position: absolute;
            left: 6px;
            width: 65px;
            border-radius: 15px;
            font-size: 14px;
            font-weight: bold;
            line-height: 22px;
            transition: all .2s ease;
        }

        [type="checkbox"]:not(:checked) + label .ui:before {
            content: "no";
            left: 32px;
            color: rgba(255,255,255,.7);
        }

        [type="checkbox"]:checked + label .ui:after {
            content: "yes";
            color: #ffffff;
        }

        [type="checkbox"]:focus + label:before {
            box-sizing: border-box;
            margin-top: -1px;
        }

        .btn-signup {
            float: left;
            font-weight: 700;
            text-transform: uppercase;
            font-size: 13px;
            text-align: center;
            color: #ffffff;
            padding-top: 8px;
            width: 100%;
            height: 35px;
            border: none;
            border-radius: 20px;
            margin-top: 23px;
            background-color: #1059FF;
        }

        .btn-signin {
            background-color: red;
            float: left;
            padding-top: 3px;
            width: 100%;
            height: 35px;
            border: none;
            border-radius: 20px;
            margin-top: -8px;
        }

        .btn-animate {
            float: left;
            font-weight: 700;
            text-transform: uppercase;
            font-size: 13px;
            text-align: center;
            color: rgba(255,255,255, 1);
            padding-top: 8px;
            width: 100%;
            height: 35px;
            border: none;
            border-radius: 20px;
            margin-top: 23px;
            background-color: rgba(16,89,255, 1);
            left: 0px;
            top: 0px;
            transition: all .5s ease, top .5s ease .5s, height .5s ease .5s, background-color .5s ease .75s;
        }

        .btn-animate-grow {
            width: 130%;
            height: 625px;
            position: relative;
            left: -55px;
            top: -420px;
            color: rgba(255,255,255,0);
            background-color: rgba(255,255,255,1);
        }

        a.btn-signup:hover, a.btn-signin:hover {
            cursor: pointer;
            background-color: #0F4FE6;
            transition: background-color .5s;
        }

        .forgot {
            height: 100px;
            width: 80%;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            padding-top: 24px;
            margin-top: -535px;
            border-top: solid 1px rgba(255,255,255,.3);
            transition: all 0.5s ease;
        }

        .forgot-left {
            transform: translateX(-400px);
            opacity: 0;
        }

        .forgot-fade {
            opacity: 0;
        }

        .forgot a {
            color: rgba(255,255,255,.3);
            font-weight: 400;
            font-size: 13px;
            text-decoration: none;
        }

        .welcome {
            width: 100%;
            height: 50px;
            position: relative;
            color: rgba(35,43,85,0.75);
            opacity: 0;
            transition: transform 1.5s ease .25s, opacity .1s ease 1s;
        }

        .welcome-left {
            transform: translateY(-780px);
            opacity: 1;
        }

        .cover-photo {
            height: 150px;
            position: relative;
            left: 0px;
            top: -900px;
            background: linear-gradient(rgba(35,43,85,0.75), rgba(35,43,85,0.95)), url(https://dl.dropboxusercontent.com/u/22006283/preview/codepen/landscape-nature-man-person.jpeg);
            background-size: cover;
            opacity: 0;
            transition: all 1.5s ease 0.55s;
        }

        .cover-photo-down {
            top: -575px;
            opacity: 1;
        }

        .profile-photo {
            height: 125px;
            width: 125px;
            position: relative;
            border-radius: 70px;
            left: 155px;
            top: -1000px;
            background: url(https://dl.dropboxusercontent.com/u/22006283/preview/codepen/nature-water-rocks-hiking.jpg);
            background-size: 100% 135%;
            background-position: 100% 100%;
            opacity: 0;
            transition: top 1.5s ease 0.35s, opacity .75s ease .5s;
            border: solid 3px #ffffff;
        }

        .profile-photo-down {
            top: -636px;
            opacity: 1;
        }

        h1 {
            color: #ffffff;
            font-size: 35px;
            font-weight: 300;
            text-align: center;
        }

        .btn-goback {
            position: relative;
            margin-right: auto;
            top: -400px;
            float: left;
            padding: 8px;
            width: 83%;
            margin-left: 37px;
            margin-right: 37px;
            height: 35px;
            border-radius: 20px;
            font-weight: 700;
            text-transform: uppercase;
            font-size: 13px;
            text-align: center;
            color: #1059FF;
            margin-top: -8px;
            border: solid 1px #1059FF;
            opacity: 0;
            transition: top 1.5s ease 0.35s, opacity .75s ease .5s;
        }

        .btn-goback-up {
            top: -1080px;
            opacity: 1;
        }

        a.btn-goback:hover {
            cursor: pointer;
            background-color: #0F4FE6;
            transition: all .5s;
            color: #ffffff;
        }

        /* refresh button styling */

        #refresh {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background-color: #ffffff;
            width: 50px;
            height: 50px;
            border-radius: 25px;
            box-shadow: 0px 2px 7px rgba(0,0,0,0.1);
            padding: 13px 0 0 13px;
        }

        .refreshicon {
            fill: #d3d3d3;
            transform: rotate(0deg);
            transition: fill .25s ease, transform .25s ease;
        }

        .refreshicon:hover {
            cursor: pointer;
            fill: #1059FF;
            transform: rotate(180deg);
        }
    </style>
</head>
<body>

    <!-- Display status if use forgotpass-->
    <c:choose>
        <c:when test="${not empty sessionScope.status}">
            <script type="text/javascript">
                $(document).ready(function () {
                    var status = '${sessionScope.status}';
                    console.log("Status: ", status); // Log status to console
                    if (status === 'resetSuccess') {
                        toastr.success('Đổi mật khẩu thành công!');
                    } else if (status === 'resetFailed') {
                        toastr.error('Đổi mật khẩu thất bại!');
                    } else if (status === 'error') {
                        toastr.error('Có lỗi xảy ra!');
                    } else if (status === 'mismatch') {
                        toastr.warning('Mật khẩu không khớp!');
                    } else {
                        toastr.info('Test thông báo mặc định!'); // Default test message
                    }
                });
            </script>
            <c:remove var="status" scope="session"/>
        </c:when>
    </c:choose>

    <c:set var="cookie" value="${pageContext.request.cookies}"/>
    
    <div class="container">
        <div class="frame">
            <div class="nav">
                <ul class="links">
                    <li class="signin-active"><a class="btn">Đăng Nhập</a></li>
                    <li class="signup-inactive"><a class="btn">Đăng Kí</a></li>
                </ul>
            </div>

            <div ng-app ng-init="checked = false">
                 
                <form class="form-signin" action="login" method="post" name="loginForm" onsubmit="return validateLoginForm()">
                    <p class="text-danger" style="color: red">${error}</p>
                    <label for="username">Email</label>
                    <input class="form-styling" type="text" name="email" value="${email != null ? email : ''}" placeholder=""/>
                    <label for="password">Mật khẩu</label>
                    <input class="form-styling" type="password" name="password" value="${password != null ? password : ''}" placeholder=""/>
                    <div class="form-group">
                        <input type="checkbox" ${cookie.customer_cuser != null ? 'checked' : ''} name="remember" id="remember" class="agree-term" />
                        <label for="remember" class="label-agree-term"><span><span></span></span> <a class="term-service">Nhớ mật khẩu</a></label>
                    </div>
                    <div class="btn-animate">
                        <input type="submit" name="signin" class="btn-signin" id="signin" class="form-submit" value="Đăng nhập"/>
                        <a style="margin-top:30px; background-color: transparent; border: none;" href="https://accounts.google.com/o/oauth2/auth?scope=email profile openid&redirect_uri=http://localhost:9999/SWP391_bookingSystem/login1&response_type=code&client_id=863872992776-c7k5ascb0q28lfmnm97eaq95kduetn59.apps.googleusercontent.com&approval_prompt=force" class="btn btn-lg btn-danger">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-google" viewBox="0 0 16 16">
                                <path d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z" />
                            </svg>
                        </a>
                    </div>
                </form>

                <form class="form-signup" action="register" method="post" name="registerForm" onsubmit="return validateRegisterForm()">
                    <p class="text-danger" style="color: red">${abc}</p>
                    <label for="userName">Họ và tên</label>
                    <input class="form-styling" type="text" name="userName" placeholder="" required minlength="10" maxlength="30" />

                    <label for="email">Email</label>
                    <input class="form-styling" type="email" name="email" placeholder="" required minlength="10" maxlength="30" />

                    <label for="password">Mật khẩu</label>
                    <input class="form-styling" type="password" name="password" placeholder="" required />

                    <label for="userDOB">Ngày sinh</label>
                    <input class="form-styling" type="date" name="userDOB" placeholder="" required />

                    <label for="phoneNumber">Số điện thoại</label>
                    <input class="form-styling" type="text" name="phoneNumber" placeholder="" required />

                    <label for="address">Địa chỉ</label>
                    <input class="form-styling" type="text" name="address" placeholder="" required minlength="10" maxlength="30" />

                    <button type="submit" class="btn-signup">Đăng kí</button>
                </form>

                <div class="success">
                    <svg width="270" height="270" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 60 60" id="check" ng-class="checked ? 'checked' : ''">
                    <path fill="#ffffff" d="M40.61,23.03L26.67,36.97L13.495,23.788c-1.146-1.147-1.359-2.936-0.504-4.314
                          c3.894-6.28,11.169-10.243,19.283-9.348c9.258,1.021,16.694,8.542,17.622,17.81c1.232,12.295-8.683,22.607-20.849,22.042
                          c-9.9-0.46-18.128-8.344-18.972-18.218c-0.292-3.416,0.276-6.673,1.51-9.578" />
                   
                </div>
            </div>

            <div class="forgot">
                 <a href="forgotPassword.jsp">Quên mật khẩu</a>
                 <a href="loginPartner" style="margin-left:120px">Đăng nhập đối tác</a>
            </div>
                    
        </div>

        <a id="refresh" value="Refresh" onClick="history.go()">
            <svg class="refreshicon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="25px" height="25px" viewBox="0 0 322.447 322.447" style="enable-background:new 0 0 322.447 322.447;"
                 xml:space="preserve">
            <path d="M321.832,230.327c-2.133-6.565-9.184-10.154-15.75-8.025l-16.254,5.281C299.785,206.991,305,184.347,305,161.224
                   c0-84.089-68.41-152.5-152.5-152.5C68.411,8.724,0,77.135,0,161.224s68.411,152.5,152.5,152.5c6.903,0,12.5-5.597,12.5-12.5
                   c0-6.902-5.597-12.5-12.5-12.5c-70.304,0-127.5-57.195-127.5-127.5c0-70.304,57.196-127.5,127.5-127.5
                   c70.305,0,127.5,57.196,127.5,127.5c0,19.372-4.371,38.337-12.723,55.568l-5.553-17.096c-2.133-6.564-9.186-10.156-15.75-8.025
                   c-6.566,2.134-10.16,9.186-8.027,15.751l14.74,45.368c1.715,5.283,6.615,8.642,11.885,8.642c1.279,0,2.582-0.198,3.865-0.614
                   l45.369-14.738C320.371,243.946,323.965,236.895,321.832,230.327z"/>
            </svg>
        </a>
    </div>

    <script>
        function showLoader() {
            document.getElementById('loader').style.display = 'block';
        }

        $(function () {
            $(".btn").click(function () {
                $(".form-signin").toggleClass("form-signin-left");
                $(".form-signup").toggleClass("form-signup-left");
                $(".frame").toggleClass("frame-long");
                $(".signup-inactive").toggleClass("signup-active");
                $(".signin-active").toggleClass("signin-inactive");
                $(".forgot").toggleClass("forgot-left");
                $(this).removeClass("idle").addClass("active");
            });
        });

        $(function () {
            $(".btn-signup").click(function () {
                $(".nav").toggleClass("nav-up");
                $(".form-signup-left").toggleClass("form-signup-down");
                $(".success").toggleClass("success-left");
                $(".frame").toggleClass("frame-short");
            });
        });

        function validateLoginForm() {
            const email = document.forms["loginForm"]["email"].value.trim();
            const password = document.forms["loginForm"]["password"].value.trim();

            if (email === "" || password === "") {
                toastr.error("Email và Mật khẩu không được để trống.");
                return false;
            }
            return true;
        }

        function validateRegisterForm() {
            const email = document.forms["registerForm"]["email"].value.trim();
            const userName = document.forms["registerForm"]["userName"].value.trim();
            const password = document.forms["registerForm"]["password"].value.trim();
            const phoneNumber = document.forms["registerForm"]["phoneNumber"].value.trim();
            const address = document.forms["registerForm"]["address"].value.trim();
            const dobString = document.forms["registerForm"]["userDOB"].value;

            if (email === "" || userName === "" || password === "" || phoneNumber === "" || address === "" || dobString === "") {
                toastr.error("Vui lòng điền đầy đủ thông tin.");
                return false;
            }

            if (!email.endsWith("@gmail.com")) {
                toastr.error("Email phải có dạng @gmail.com");
                return false;
            }

            if (userName.length < 10 || userName.length > 30) {
                toastr.error("Họ và tên phải từ 10 đến 30 ký tự.");
                return false;
            }

            if (address.length < 10 || address.length > 30) {
                toastr.error("Địa chỉ phải từ 10 đến 30 ký tự.");
                return false;
            }

            const phonePattern = /^\d{10}$/;
            if (!phonePattern.test(phoneNumber)) {
                toastr.error("Số điện thoại phải là 10 chữ số.");
                return false;
            }

            const passwordPattern = /^(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
            if (!passwordPattern.test(password)) {
                toastr.error("Mật khẩu phải chứa ít nhất 8 ký tự, bao gồm ít nhất một chữ cái thường, một chữ số và một ký tự đặc biệt.");
                return false;
            }

            const userDOB = new Date(dobString);
            const today = new Date();
            let age = today.getFullYear() - userDOB.getFullYear();
            const monthDiff = today.getMonth() - userDOB.getMonth();
            if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < userDOB.getDate())) {
                age--;
            }

            if (age < 18) {
                toastr.error("Bạn phải từ 18 tuổi trở lên.");
                return false;
            }

            if (age > 100) {
                toastr.error("Tuổi không được quá 100.");
                return false;
            }

            return true;
        }
    </script>
  
</body>
</html>
