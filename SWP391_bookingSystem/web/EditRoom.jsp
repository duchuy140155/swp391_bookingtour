<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Hotel Room</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f8f9fa;
        }
        .top-bar {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 10px 30px;
            background-color: #007bff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .top-bar button {
            padding: 10px 20px;
            background-color: #ffffff;
            border: none;
            color: #007bff;
            cursor: pointer;
            margin-left: 10px;
            border-radius: 4px;
            transition: background-color 0.3s ease;
        }
        .top-bar button:hover {
            background-color: #e0e0e0;
        }
        .container {
            max-width: 800px;
            margin: 80px auto;
            padding: 40px;
            background-color: #ffffff;
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 10px;
            border: 1px solid #ddd;
        }
        h1 {
            text-align: center;
            color: #333;
            font-size: 28px;
            margin-bottom: 30px;
            text-transform: uppercase;
            letter-spacing: 1.5px;
            border-bottom: 2px solid #ddd;
            padding-bottom: 10px;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin: 10px 0 5px;
            color: #555;
            font-weight: bold;
        }
        .form-group {
            margin-bottom: 20px;
        }
        textarea {
            resize: vertical;
            height: 120px;
        }
        .btn-primary {
            background-color: #28a745;
            border-color: #28a745;
        }
        .btn-primary:hover {
            background-color: #218838;
            border-color: #1e7e34;
        }
        .modal-content .btn {
            margin: 0 10px;
        }
        .modal-content .btn-group {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .toast {
            position: fixed;
            top: 20px;
            right: 20px;
            z-index: 1050;
        }
    </style>
</head>
<body>
    <div class="top-bar">
        <button onclick="history.back()">Back</button>
        <button onclick="location.href='EditHotel.jsp'">Back to Edit Hotel</button>
    </div>
    <div class="container">
        <c:set var="roomid" value="${sessionScope.roomid}"/>
        <c:set var="price" value="${sessionScope.price}"/>
        <c:set var="description" value="${sessionScope.description}"/>
        <c:set var="guest" value="${sessionScope.guest}"/>
        <c:set var="bed" value="${sessionScope.bed}"/>
        <c:set var="image" value="${sessionScope.image}"/>
        <c:set var="hotel" value="${sessionScope.hotel}"/>
        <c:set var="name" value="${sessionScope.name}"/>
        <h1>Edit Hotel Room</h1>
        <form action="editroom" method="post" onsubmit="return validateForm()">
            <div class="form-group">
                <label for="hotelId">Room Id</label>
                <input class="form-control" type="number" name="roomid" readonly value="${roomid}">
            </div>
            <div class="form-group" hidden="true">
                <label for="hotelId">Hotel ID</label>
                <input class="form-control" type="number" name="hotelid" readonly value="${hotel}">
            </div>
            <div class="form-group">
                <label for="hotelId">Image Id</label>
                <input class="form-control" type="number" name="imageid" readonly value="${image}">
            </div>
            <div class="form-group">
                <label for="roomType">Room Type:</label>
                <select class="form-control" id="roomType" name="roomType" required>
                    <c:forEach items="${sessionScope.listroomtype}" var="p">
                        <option value="${p.room_type}">${p.roomTypeName}</option>
                    </c:forEach>
                </select>
            </div>
                        <div class="form-group">
                <label for="hotelId">Room Name</label>
                <input class="form-control" type="text" name="roomnae" readonly value="${name}">
            </div>
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" class="form-control" id="price" name="price" required value="${price}">
            </div>
            <div class="form-group">
                <label for="numberBeds">Number of Beds:</label>
                <input type="text" class="form-control" id="numberBeds" name="numberBeds" required readonly value="${bed}" data-toggle="modal" data-target="#numberBedsModal">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" id="description" name="description" required>${description}</textarea>
            </div>
            <div class="form-group">
                <label for="maxGuests">Max Guests:</label>
                <input type="text" class="form-control" id="maxGuests" name="maxGuests" required readonly value="${guest}" data-toggle="modal" data-target="#maxGuestsModal">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Edit Room</button>
        </form>
    </div>

    <!-- Max Guests Modal -->
    <div class="modal fade" id="maxGuestsModal" tabindex="-1" role="dialog" aria-labelledby="maxGuestsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="maxGuestsModalLabel">Set Max Guests</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>People</label>
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline-secondary" onclick="decrement('peopleCount')">-</button>
                            <input type="text" id="peopleCount" class="form-control text-center" value="${guest}" readonly>
                            <button type="button" class="btn btn-outline-secondary" onclick="increment('peopleCount', 10)">+</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateMaxGuests()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Number of Beds Modal -->
    <div class="modal fade" id="numberBedsModal" tabindex="-1" role="dialog" aria-labelledby="numberBedsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="numberBedsModalLabel">Set Number of Beds</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Beds</label>
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline-secondary" onclick="decrement('bedsCount')">-</button>
                            <input type="text" id="bedsCount" class="form-control text-center" value="${bed}" readonly>
                            <button type="button" class="btn btn-outline-secondary" onclick="increment('bedsCount', 5)">+</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateNumberBeds()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        function increment(id, max) {
            var count = document.getElementById(id);
            if (parseInt(count.value) < max) {
                count.value = parseInt(count.value) + 1;
            }
        }

        function decrement(id) {
            var count = document.getElementById(id);
            if (parseInt(count.value) > 1) {
                count.value = parseInt(count.value) - 1;
            }
        }

        function updateMaxGuests() {
            var people = document.getElementById('peopleCount').value;
            var maxBeds = parseInt(document.getElementById('bedsCount').value) * 2;
            if (parseInt(people) > maxBeds) {
                alert('Max Guests không được vượt quá ' + maxBeds);
                people = maxBeds;
                document.getElementById('peopleCount').value = maxBeds;
            }
            document.getElementById('maxGuests').value = people + ' People';
        }

        function updateNumberBeds() {
            var beds = document.getElementById('bedsCount').value;
            document.getElementById('numberBeds').value = beds + ' Beds';
        }

        function showToast() {
            $('#hotelToast').toast('show');
        }

        function validateForm() {
            var maxGuests = document.getElementById('maxGuests').value;
            var numberBeds = document.getElementById('numberBeds').value;

            if (maxGuests.trim() === '') {
                alert('Max Guests không được bỏ trống.');
                return false;
            }

            if (numberBeds.trim() === '') {
                alert('Number of Beds không được bỏ trống.');
                return false;
            }

            var numberBedsValue = parseInt(numberBeds);
            if (numberBedsValue > 5) {
                alert('Number of Beds không được quá 5.');
                return false;
            }

            var maxGuestsValue = parseInt(maxGuests);
            if (maxGuestsValue > (numberBedsValue * 2)) {
                alert('Max Guests không được vượt quá ' + (numberBedsValue * 2));
                return false;
            }

            return true;
        }

        $(document).ready(function() {
            $('.toast').toast('show');
        });
    </script>
</body>
</html>
