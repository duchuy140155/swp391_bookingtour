<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Danh sách các bài đăng</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 20px;
        }
        .container {
            max-width: 1200px;
            margin: 0 auto;
        }
        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 30px;
        }
        .header h1 {
            text-align: center;
            color: #333;
            margin: 0;
        }
        .back-button {
            padding: 10px 15px;
            background-color: #007BFF;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }
        .back-button:hover {
            background-color: #0056b3;
        }
        .post {
            background-color: #fff;
            border: 1px solid #ddd;
            margin: 10px;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transition: transform 0.3s ease, box-shadow 0.3s ease;
        }
        .post:hover {
            transform: translateY(-5px);
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
        }
        .post img {
            max-width: 100%;
            height: auto;
            display: block;
            margin-bottom: 15px;
            border-radius: 10px;
        }
        .post h2 {
            margin: 0;
            font-size: 1.8em;
            color: #333;
        }
        .post p {
            color: #666;
            font-size: 1em;
            line-height: 1.5em;
        }
        .post .read-more {
            display: inline-block;
            margin-top: 10px;
            padding: 10px 15px;
            background-color: #007BFF;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }
        .post .read-more:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Các bài đăng nổi bật</h1>
            <a href="javascript:history.back()" class="back-button">Quay lại trang trước</a>
        </div>
        <c:forEach var="post" items="${sessionScope.allpost}">
            <div class="post">
                <img src="${post.image}" alt="${post.postTitle}">
                <h2>${post.postTitle}</h2>
                <p>${post.createAt}</p>
                <a href="postcus?id=${post.postId}" class="read-more">Read More</a>
            </div>
        </c:forEach>
    </div>
</body>
</html>
