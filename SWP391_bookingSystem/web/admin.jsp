<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Dashboard</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/admin.css">
        <style>
            body {
                background-color: #f8f9fa;
            }
            .topbar {
                background-color: #343a40;
                padding: 10px 20px;
                color: white;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .topbar .search {
                width: 200px;
                padding: 5px;
                border-radius: 5px;
                border: none;
            }
            .topbar .icons a {
                margin-left: 15px;
                color: white;
            }
            .topbar .profile span {
                margin-right: 10px;
            }
            .main-container {
                display: flex;
                height: calc(100vh - 50px);
            }
            .sidebar {
                background-color: #343a40;
                width: 250px;
                padding: 20px;
                color: white;
            }
            .sidebar h2 {
                margin-bottom: 30px;
            }
            .sidebar ul {
                list-style: none;
                padding: 0;
            }
            .sidebar ul li {
                margin-bottom: 10px;
            }
            .sidebar ul li a {
                color: white;
                text-decoration: none;
            }
            .content {
                flex-grow: 1;
                padding: 20px;
                background-color: #fff;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .dropdown-content {
                display: none;
                padding-left: 20px;
            }
        </style>
    </head>
    <body>
        <div class="topbar">
            <input type="text" placeholder="Search..." class="search">
            <div class="icons">
                <a href="#"><img src="mail-icon.png" alt="Mail"></a>
                <a href="#"><img src="notification-icon.png" alt="Notification"></a>
                <div class="profile">
                    <span>User Name</span>
                    <a href="logout.jsp" class="btn btn-danger btn-sm">Đăng xuất</a>
                </div>
            </div>
        </div>
        <div class="main-container">
            <div class="sidebar">
                <h2>Quản lý</h2>
                <ul>
                    <!-- Dashboard -->
                    <li class="dropdown">
                        <span onclick="toggleDropdown('manageTour')">Dashboard</span>
                        <div class="dropdown-content" id="manageTour">
                            <ul>
                                <li><a href="viewRevenue">Doanh thu theo partner</a></li>
                            </ul>
                        </div>
                    </li>
                    <!-- Dashboard -->
                    <li class="dropdown">
                        <span onclick="toggleDropdown('manageTour')">Quản lý tour</span>
                        <div class="dropdown-content" id="manageTour">
                            <ul>
                                <li><a href="locations">Địa điểm</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="manageRoles">Quản Lí Tài Khoản</a></li>
                    <li><a href="listTicket">Quản lý các loại Vé Tour</a></li>
                </ul>
            </div>
            <div class="content" id="content">
                <h1>Welcome to Admin Dashboard</h1>
                <p>Select an option from the sidebar to get started.</p>
            </div>
        </div>
        <script>
            function toggleDropdown(id) {
                var element = document.getElementById(id);
                if (element.style.display === "none" || element.style.display === "") {
                    element.style.display = "block";
                } else {
                    element.style.display = "none";
                }
            }

            function showContent(contentId) {
                var contentDiv = document.getElementById('content');
                contentDiv.innerHTML = '';

                if (contentId === 'revenueTour') {
                    contentDiv.innerHTML = '<h1>Doanh thu theo Tour</h1><p>Content for Doanh thu theo Tour</p>';
                } else if (contentId === 'revenuePartner') {
                    contentDiv.innerHTML = '<h1>Doanh thu theo Partner</h1><p>Content for Doanh thu theo Partner</p>';
                } else if (contentId === 'viewUsers') {
                    contentDiv.innerHTML = '<h1>Xem danh sách người dùng</h1><p>Content for Xem danh sách người dùng</p>';
                } else if (contentId === 'assignRoles') {
                    contentDiv.innerHTML = '<h1>Phân quyền</h1><p>Content for Phân quyền</p>';
                }
            }
        </script>

        <!-- Scripts Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
