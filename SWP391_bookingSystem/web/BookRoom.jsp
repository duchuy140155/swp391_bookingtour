<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hotel Rooms</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .intro {
            max-width: 800px;
            margin: 20px auto;
            padding: 10px;
            background-color: #f7e7a9;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .intro h1 {
            font-size: 1.2em;
            color: #0033cc;
            margin-bottom: 10px;
        }
        .intro p {
            font-size: 0.75em;
            color: #333;
            line-height: 1.6;
            text-align: left;
        }
        .header, .footer {
            width: 100%;
            padding: 10px 20px;
            background-color: #0033cc;
            color: white;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            text-align: center;
            position: relative;
        }
        .header h1, .footer h2 {
            font-size: 1.2em;
            margin-bottom: 5px;
        }
        .header .address, .footer p {
            font-size: 0.8em;
            margin-bottom: 5px;
        }
        .header .stars {
            color: gold;
            margin-bottom: 5px;
        }
        .carousel {
            width: 100%;
            max-width: 400px;
            margin: 10px auto;
        }
        .carousel-item img {
            width: 100%;
            height: 200px;
            object-fit: cover;
        }
        .room-container {
            max-width: 1000px;
            margin: 20px auto;
            padding: 20px;
            display: flex;
            flex-direction: column;
            gap: 20px;
        }
        .room {
            background-color: white;
            border: 1px solid #ccc;
            border-radius: 5px;
            display: flex;
            overflow: hidden;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .room img {
            width: 150px;
            object-fit: cover;
        }
        .room-info {
            flex-grow: 1;
            padding: 20px;
        }
        .room-info h2 {
            margin: 0 0 10px 0;
            font-size: 1.3em;
            color: #0033cc;
        }
        .room-info .service-list {
            list-style: none;
            padding: 0;
            margin: 0 0 10px 0;
            max-height: 100px;
            overflow-y: auto;
        }
        .room-info .service-list li {
            font-size: 0.8em;
            margin-bottom: 5px;
        }
        .room-info a {
            color: #0033cc;
            text-decoration: none;
        }
        .room-info a:hover {
            text-decoration: underline;
        }
        .room-price {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: flex-end;
            padding: 20px;
            background-color: #f8f8f8;
            border-left: 1px solid #ccc;
        }
        .room-price h3 {
            margin: 0 0 10px 0;
            color: red;
            font-size: 1.3em;
        }
        .room-price button {
            background-color: #ff3333;
            color: white;
            border: none;
            padding: 4px 8px;
            border-radius: 3px;
            cursor: pointer;
            font-size: 0.8em;
            transition: background-color 0.3s ease, transform 0.3s ease;
        }
        .room-price button:hover {
            background-color: #cc0000;
            transform: scale(1.1);
        }
        .back-button {
            position: fixed;
            top: 10px;
            left: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1em;
            transition: background-color 0.3s ease;
            z-index: 1000;
        }
        .back-button:hover {
            background-color: #0056b3;
        }
        .cart-icon {
            position: fixed;
            top: 10px;
            right: 10px;
            background-color: transparent;
            border: none;
            cursor: pointer;
            z-index: 1000;
            display: flex;
            align-items: center;
        }
        .cart-icon i {
            font-size: 24px;
            color: white;
        }
        .cart-count {
            margin-left: 5px;
            background-color: red;
            color: white;
            border-radius: 50%;
            padding: 0 7px;
            font-size: 14px;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <c:if test="${empty sessionScope.account || (not empty sessionScope.account && sessionScope.account.roleID != 2)}">
    <script type="text/javascript">
        window.location.href = 'login.jsp';
    </script>
</c:if>

    <button class="back-button" onclick="history.back()">Quay lại</button>
    <button class="cart-icon" onclick="submitCart()">
        <i class="fa-solid fa-cart-shopping"></i><span class="cart-count" id="cart-count">0</span>
    </button>
    <div class="header">
        <h1>Hotel Name</h1>
        <p>123 Street, City, Country</p>
        <div class="stars">★★★★★</div>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="1500">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li> <!-- New Indicator -->
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/about-img-1.png" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/blog-1.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/blog-3.jpg" alt="Third slide">
                </div>
                <div class="carousel-item"> <!-- New Item -->
                    <img class="d-block w-100" src="your_uploaded_image_path_here" alt="Fourth slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="intro">
                    <h1>Giới thiệu</h1>
                    <p>
                        Được xây dựng tại vị trí hạng nhất của thành phố Hà Nội, chỉ cần bước ra bên ngoài Lotte Hotel Hanoi bạn có thể dễ dàng tiếp cận các điểm đến hấp dẫn ở thành phố này. Khách sạn đem đến hàng loạt các dịch vụ và tiện nghi để đem đến kì nghỉ thoải mái và dễ chịu. Dịch vụ phòng 24 giờ, miễn phí wifi tất cả các phòng, quầy lễ tân 24 giờ, tiện nghi cho người khuyết tật, nhận/trả phòng nhanh chỉ là một trong số những tiện nghi cung cấp tại đây. Phòng nghỉ được thiết kế để tạo một không gian ấm cúng và một số phòng còn cung cấp các trang thiết bị tiện nghi như tivi màn hình phẳng, giá treo quần áo, cafe hòa tan miễn phí, trà miễn phí, gương để đem đến sự thoải mái tuyệt vời. Hãy tự thưởng kì nghỉ sau những ngày dài căng thẳng và tận hưởng phòng thể dục, phòng sauna, hồ bơi ngoài trời, hồ bơi trong nhà, massage. Cho dù bạn đến Hà Nội vì lý do gì, Lotte Hotel Hanoi sẽ khiến bạn có cảm giác thân quen như đang ở nhà.
                    </p>
                </div>
                <div class="room-container">
                    <c:forEach items="${sessionScope.loadroom}" var="p">
                        <div class="room">
                            <img src="img/carousel-1.jpg" alt="Phòng Deluxe 2 Giường đơn">
                            <div class="room-info">
                                <h2>${p.name}</h2>
                                <ul class="service-list">
                                    <li>Dịch vụ:</li>
                                    <li>Bãi đậu xe</li>
                                    <li>Nhận phòng nhanh</li>
                                    <li>Wifi cao cấp miễn phí</li>
                                    <li>WiFi miễn phí</li>
                                    <li>Nước uống</li>
                                    <li>Phòng tập</li>
                                </ul>
                                <a href="#">Quy định hủy phòng</a>
                            </div>
                            <div class="room-price">
                                <h3>Giá :${p.price} đ</h3>
                                <button onclick="addToCart(${p.room_id})">Thêm vào đơn hàng</button>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>&copy; 2024 Hotel Name. All rights reserved.</p>
        <p>123 Street, City, Country | Phone: (123) 456-7890 | Email: info@hotelname.com</p>
    </div>

    <!-- Hidden form to submit cart items -->
    <form id="cartForm" action="cart" method="POST" style="display:none;">
        <input type="hidden" name="cartItems" id="cartItemsInput">
    </form>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Font Awesome JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/js/all.min.js"></script>
    <script>
        let cartCount = 0;
        let cartItems = [];

        function addToCart(roomId) {
            cartCount++;
            cartItems.push(roomId);
            document.getElementById('cart-count').innerText = cartCount;
            console.log(cartItems); // To verify the IDs are being added
        }

        function submitCart() {
            document.getElementById('cartItemsInput').value = JSON.stringify(cartItems);
            document.getElementById('cartForm').submit();
        }
    </script>
</body>
</html>
