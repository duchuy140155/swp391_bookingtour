<%-- 
    Document   : addLocation
    Created on : May 21, 2024, 12:02:18 AM
    Author     : Admin
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Tạo Địa điểm</title>
        <link href="css/addLocation.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <a href="locations" class="btn back-btn">Back to Locations</a>
                <h1>Tạo địa điểm</h1>
            </div>
            <form action="locations" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="locationName">Tên địa điểm:</label>
                    <input type="text" id="locationName" name="locationName" required>
                </div>
                <div class="form-group">
                    <label for="locationDescription">Mô tả:</label>
                    <textarea id="locationDescription" name="locationDescription" required></textarea>
                </div>
                <div class="form-group">
                    <label for="locationImage">Hình ảnh:</label>
                    <input type="file" id="locationImage" name="locationImage" required>
                </div>
                <button type="submit" class="btn submit-btn">Tạo địa điểm</button>
            </form>
            <c:if test="${not empty success}">
                <p class="success-message">${success}</p>
            </c:if>
            <c:if test="${not empty error}">
                <p class="error-message">${error}</p>
            </c:if>
        </div>
    </body>
</html>
