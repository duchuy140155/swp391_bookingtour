<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html><%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                min-width: 1000px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #435d7d;
                color: #fff;
                padding: 16px 30px;
                min-width: 100%;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn-group {
                float: right;
            }
            .table-title .btn {
                color: #fff;
                float: right;
                font-size: 13px;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
                outline: none !important;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.edit {
                color: #FFC107;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
            /* Custom checkbox */
            .custom-checkbox {
                position: relative;
            }
            .custom-checkbox input[type="checkbox"] {
                opacity: 0;
                position: absolute;
                margin: 5px 0 0 3px;
                z-index: 9;
            }
            .custom-checkbox label:before{
                width: 18px;
                height: 18px;
            }
            .custom-checkbox label:before {
                content: '';
                margin-right: 10px;
                display: inline-block;
                vertical-align: text-top;
                background: white;
                border: 1px solid #bbb;
                border-radius: 2px;
                box-sizing: border-box;
                z-index: 2;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                content: '';
                position: absolute;
                left: 6px;
                top: 3px;
                width: 6px;
                height: 11px;
                border: solid #000;
                border-width: 0 3px 3px 0;
                transform: inherit;
                z-index: 3;
                transform: rotateZ(45deg);
            }
            .custom-checkbox input[type="checkbox"]:checked + label:before {
                border-color: #03A9F4;
                background: #03A9F4;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                border-color: #fff;
            }
            .custom-checkbox input[type="checkbox"]:disabled + label:before {
                color: #b8b8b8;
                cursor: auto;
                box-shadow: none;
                background: #ddd;
            }
            /* Modal styles */
            .modal .modal-dialog {
                max-width: 400px;
            }
            .modal .modal-header, .modal .modal-body, .modal .modal-footer {
                padding: 20px 30px;
            }
            .modal .modal-content {
                border-radius: 3px;
                font-size: 14px;
            }
            .modal .modal-footer {
                background: #ecf0f1;
                border-radius: 0 0 3px 3px;
            }
            .modal .modal-title {
                display: inline-block;
            }
            .modal .form-control {
                border-radius: 2px;
                box-shadow: none;
                border-color: #dddddd;
            }
            .modal textarea.form-control {
                resize: vertical;
            }
            .modal .btn {
                border-radius: 2px;
                min-width: 100px;
            }
            .modal form label {
                font-weight: normal;
            }
        </style>

    </head>
    <body>
       
        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Quản Lý <b>Mã Giảm Giá</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <a href="homepage.jsp" class="btn btn-success" > <span>Trở lại trang trước</span></a>

                                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#user-form-modal">Tạo mã giảm giá</button>
                               
                            </div>




                        </div>
                    </div>



                    

                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>

                                    <th>Discount ID</th>
                                    <th>Discount Code</th>
                                    <th>Discount Name</th>
                                    <th>Discount From</th>
                                    <th>Discount To</th>
                                    <th>Status</th>
                                    <th>Discount Type</th>
                                    <th>Discount Percentage</th>
                                    <th>Discount Reduce</th>
                                    <th>Tour Id</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${sessionScope.loadDiscount}" var="p">




                                    <tr>

                                        <td>${p.discountID}</td>
                                        <td>${p.discountCode}</td>
                                        <td>${p.discountName}</td>
                                        <td>${p.discountFrom}</td>
                                        <td>${p.discountTo}</td>
                                        <td>${p.status}</td>
                                        <td>${p.discountType}</td>
                                        <td>${p.discountPercentage}</td>
                                        <td>${p.discountReduce}</td>
                                        <td>${p.tourID}</td>


                                        <td>
                                            <a href="actiondiscount?type=2&id=${p.discountID}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a href="#" onclick="doDelete('${p.discountID}')"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        </td>

                                    </tr>
                                </c:forEach>



                            </tbody>
                         

                    </table>
                    <div class="clearfix">


                        <ul class="pagination">
                            <li class="page-item disabled"><a href="#">Previous</a></li>
                                <c:forEach  begin="1" end="${sessionScope.endP}" var="p">
                                <li class="${sessionScope.tag == p?"active":""}  page-item "><a href="listpostmanage?id=${p}" class="page-link">${p}</a></li>
                                </c:forEach>
                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                        </ul>

                    </div>
                </div>
            </div>        
        </div>
        <div class="modal fade" role="dialog" tabindex="-1" id="user-form-modal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create New Discount</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="py-1">
                            <form class="form" novalidate action="insertdiscount" method="post" onsubmit="return validateInput()">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label><b>Discount Code</b></label>
                                                    <input class="form-control" type="text" name="code" placeholder="Discount Code" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label><b>Discount Name</b></label>
                                                    <input class="form-control" type="text" placeholder="Discount Name" name="name" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Discount From</b></label>
                                                    <input type="date" class="form-control" placeholder="Discount From" name="from" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Discount To</b></label><br>
                                                    <input type="date" class="form-control" placeholder="Discount To" name="to" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Status</b></label><br>
                                                    <select style="height: 30px" name="status">
                                                        <option value="1">True</option>
                                                        <option value="0">False</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Discount Type</b></label><br>
                                                    <select style="height: 30px" name="type">
                                                        <option value="0">Discount không điều kiện</option>
                                                        <option value="1">Discount có điều kiện</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Discount Percentage</b></label>
                                                    <input type="number" class="form-control" placeholder="Discount Percentage" name="percent" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Discount Reduce</b></label>
                                                    <input type="number" class="form-control" placeholder="Discount Reduce" name="reduce" required>
                                                </div>
                                            </div>
                                            <div class="col mb-3">
                                                <div class="form-group">
                                                    <label><b>Tour Id</b></label>
                                                    <select name="tourid" class="form-control">
                                                        <option value="0">Không có Tour Nào</option>
                                                        <c:forEach items="${sessionScope.loadTourId}" var="tourId">
                                                            <option value="${tourId}" <c:if test="${p.tourID == tourId}">selected</c:if>>Tour Số :${tourId}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col d-flex justify-content-end">
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function redirectToOtherPage() {
                window.location.href = 'loadluckydiscount';
            }
        </script>



        <script type="text/javascript">
            function doDelete(id) {
                var option = confirm("Are you sure to delete Discount with id = " + id);
                if (option == true) {
                    window.location.href = "actiondiscount?type=1&id=" + id;
                }
            }
        </script>
        <script>
            function validateInput() {
                var percentage = document.getElementsByName("percent")[0].value;
                var reduce = document.getElementsByName("reduce")[0].value;
                var discountFrom = document.getElementsByName("from")[0].value;
                var discountTo = document.getElementsByName("to")[0].value;
                var discountCode = document.getElementsByName("code")[0].value.trim();
                var discountName = document.getElementsByName("name")[0].value.trim();
                var status = document.getElementsByName("status")[0].value;
                var discountType = document.getElementsByName("type")[0].value;
                var tourId = document.getElementsByName("tourid")[0].value;


                percentage = parseFloat(percentage);
                reduce = parseFloat(reduce);


                if (discountCode === "") {
                    alert("Không được bỏ trống discountCode");
                    return false;
                }
                if (discountName === "") {
                    alert("Không được bỏ trống discountName");
                    return false;
                }
                if (discountFrom === "") {
                    alert("Không được bỏ trống Discount From");
                    return false;
                }
                if (discountTo === "") {
                    alert("Không được bỏ trống Discount To");
                    return false;
                }
                if (status === "") {
                    alert("Không được bỏ trống Status");
                    return false;
                }
                if (discountType === "") {
                    alert("Không được bỏ trống Discount Type");
                    return false;
                }
                if (isNaN(percentage) || isNaN(reduce)) {
                    alert("Không được bỏ trống Discount Percentage và Discount Reduce");
                    return false;
                }
                if (tourId === "") {
                    alert("Không được bỏ trống Tour Id");
                    return false;
                }


                if (discountName.length < 8) {
                    alert("DiscountName phải lớn hơn 8 kí tự!");
                    return false;
                }
                if ((percentage !== 0 && reduce !== 0) || (percentage === 0 && reduce === 0)) {
                    alert("Chỉ được nhập 1 trong 2 ô Discount Percentage và Discount Reduce. Ô còn lại phải bằng 0!!");
                    return false;
                }
                if (new Date(discountTo) < new Date(discountFrom)) {
                    alert("Discount To phải lớn hơn hoặc bằng Discount From!");
                    return false;
                }


                if (discountType === "0" && tourId !== "0") {
                    alert("Nếu chọn 'Không điều kiện', bạn phải chọn 'Không có Tour Nào'.");
                    return false;
                }


                if (discountType === "1" && tourId === "0") {
                    alert("Nếu chọn 'Có điều kiện', bạn không được chọn 'Không có Tour Nào'.");
                    return false;
                }

                return true;
            }
        </script>

        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script></body>
</html>